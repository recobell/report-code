--@drop-tb_product_view_stats
drop table if exists tb_product_view_stats;
--@end

--@create-tb_product_view_stats:export
create table tb_product_view_stats as 
select
	a.item_id, 
	sum(recency_score) sum_recency_score,
	max(recency_score) max_recency_score,
	avg(recency_score) avg_recency_score,
	count(*) view_count,
	count(distinct login_id) login_id_count
from
	tb_log_view a
group by a.item_id;
--@end

--@drop-tb_product_stats
drop table if exists tb_product_stats;
--@end

--@create-tb_product_stats
create table tb_product_stats as
select
	item_id, '' category, score, rank
from
	(select 
		*
		,row_number() over (order by score desc) as rank 
	from
		(
		select 
			item_id
			,sum(score) score
		from
			(select
				item_id,
				sum_recency_score * login_id_count score
			from tb_product_view_stats
			union all
			select
				a.item_id, a.score * (0.9 ^ (3/24)) score
			from tb_product_view_best_his a)
		group by item_id
		)

	)
where rank <= 40;
--@end

--@drop-tb_product_view_best
drop table if exists tb_product_view_best;
--@end

--@create-tb_product_view_best:export
create table tb_product_view_best as
select
	'm002' target_item_id, item_id, '' category, score, rank
from tb_product_stats
--@end
