
--@drop-tmp_log_view_unrefine
drop table if exists tmp_log_view_unrefine;
--@end

--@create-tmp_log_view_unrefine
CREATE TABLE tmp_log_view_unrefine (
	cuid varchar(200),
	type varchar(200),
	server_time timestamp NULL,
	ip_address varchar(200),
	user_agent varchar(2000),
	api_version varchar(200),
	domain varchar(200),
	device varchar(200),
	pc_id varchar(200),
	session_id varchar(200),
	ad_id varchar(200),
	push_id varchar(200),
	gc_id varchar(200),
	guid varchar(200),
	user_id varchar(1000),
	ab_group varchar(200),
	rc_code varchar(200),
	ad_code varchar(200),
	locale varchar(200),
	item_id varchar(200),
	search_term varchar(200)
);
--@end

--@drop-tmp_product
drop table if exists tmp_product;
--@end

--@create-tmp_product
create table tmp_product (
	cuid varchar(200),
	type varchar(200),
	item_id varchar(200),
	item_name varchar(2000),
	item_image varchar(200),
	item_url varchar(2000),
	original_price real,
	sale_price real,
	category1 varchar(200),
	category2 varchar(200),
	category3 varchar(200),
	category4 varchar(200),
	category5 varchar(200),
	reg_date varchar(200),
	update_date timestamp,
	expire_date timestamp,
	stock bigint,
	state varchar(200),
	description varchar(200),
	extra_image varchar(200),
	locale varchar(200),
	reply_count bigint,
	server_time timestamp
);
--@end

--@drop-tb_asc_recmd_view_result_his
drop table if exists tb_asc_recmd_view_result_his;
--@end

--@create-tb_asc_recmd_view_result_his
create table tb_asc_recmd_view_result_his
(
	target_item_id 		varchar(100) NOT NULL,
	cross_sell_item_id 	varchar(100) NOT NULL,
	category			varchar(50) NOT NULL,
	score				decimal(16,6) NOT NULL,
	rank				int	NOT NULL
);
--@end

--@drop-tb_product_view_best_his
drop table if exists tb_product_view_best_his;
--@end

--@create-tb_product_view_best_his
create table tb_product_view_best_his
(
	target_item_id 		varchar(100) NOT NULL,
	item_id 			varchar(100) NOT NULL,
	category			varchar(50) NOT NULL,
	score				decimal(16,6) NOT NULL,
	rank				int	NOT NULL
);
--@end

--@drop-tb_product_his
drop table if exists tb_product_his;
--@end

--@create-tb_product_his
create table tb_product_his
(
	item_id varchar(200),
	item_name varchar(2000),
	item_image varchar(2000),
	item_url varchar(2000),
	reply_count int,
	count int
);
--@end