--@drop-tb_asc_recmd_view_result_cur
drop table if exists tb_asc_recmd_view_result_cur;
--@end

--@create-tb_asc_recmd_view_result_cur
create table tb_asc_recmd_view_result_cur as
select a.target_item_id, a.cross_sell_item_id, a.score from (
	select
		a.target_item_id, a.cross_sell_item_id, a.score, row_number() over (partition by target_item_id order by score desc) as rank
	from (
		select
			a.item_id target_item_id, b.item_id cross_sell_item_id, sum(a.recency_score * b.recency_score) score
		from
			tb_log_view a
			inner join tb_log_view b on b.login_id = a.login_id and b.session_id = a.session_id and b.item_id <> a.item_id
		group by a.item_id, b.item_id
	) a
) a
where a.rank <= 40;
--@end