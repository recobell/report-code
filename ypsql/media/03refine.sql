--@drop-tmp_log_view
drop table if exists tmp_log_view;
--@end

--@create-tmp_log_view
create table tmp_log_view as
select
	item_id,
	pc_id,
	user_id login_id,
	server_time view_timestamp,
	session_id
from tmp_log_view_unrefine;
--@end

--@drop-tb_med_product
drop table if exists tb_med_product;
--@end

--@create-tb_med_product
create table tb_med_product as
select
	a.item_id,
	a.item_name,
	a.item_image,
	a.item_url,
	count
from (
	select
		a.item_id,
		a.item_name,
		a.item_image,
		a.item_url,
		count,
		row_number() over (partition by a.item_id order by a.count desc) rank
	from (
		select item_id, item_name, item_image, item_url, count(*) as count from tmp_product
		where item_id is not null and item_name is not null
		group by item_id, item_name, item_image, item_url
	) a
) a
where a.rank = 1;
--@end

--@drop-tb_recent_product
drop table if exists tb_recent_product;
--@end

--@create-tb_recent_product
create table tb_recent_product as
select
	item_id, reply_count
from (
	select
		item_id,
		reply_count,
		row_number() over (partition by item_id order by server_time desc) as rank
	from tmp_product
) a
where a.rank = 1;
--@end

--@drop-tb_product
drop table if exists tb_product;
--@end

--@create-tb_product
create table tb_product as
select 
	a.item_id,
	a.item_name,
	a.item_image,
	a.item_url,
	a.count,
	b.reply_count
from
	tb_med_product a, tb_recent_product b
where
	a.item_id = b.item_id
--@end
	
--@drop-tmp_tb_log_view;
drop table if exists tmp_tb_log_view;
--@end

--@create-tmp_tb_log_view
create table tmp_tb_log_view as
select
	a.item_id, a.pc_id, 
	a.pc_id login_id,
	a.view_timestamp view_timestamp,
	a.session_id,
	extract(hour from sysdate - a.view_timestamp) hours
from
	tmp_log_view a
	inner join tb_product c on c.item_id = a.item_id;

--@drop-tb_log_view
drop table if exists tb_log_view;
--@end
	
--@create-tb_log_view
create table tb_log_view as 
select
	item_id, pc_id, login_id, view_timestamp, hours, session_id,
	row_number() over (partition by login_id order by view_timestamp asc) seq,
	0.9 ^ (hours/24) recency_score
from tmp_tb_log_view;
--@end