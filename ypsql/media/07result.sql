--#var $AWS_ACCESS_KEY_ID = springproperty(aws.access-key-id)
--#var $AWS_SECRET_ACCESS_KEY = springproperty(aws.secret-access-key)
--#var $CUID = springproperty(recobell.site.cuid)
--#var $NOW = datenow(UTC)
--#var $TIME_STR = dateformat($NOW, yyyy/MM/dd/HH)

--@unload-a001
unload ('select target_item_id, cross_sell_item_id, \'\', score, rank from tb_asc_recmd_view_result')
to 's3://rb-rec-his-use1/$CUID/dmp/$TIME_STR/a001.csv'
credentials 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
DELIMITER ','
ADDQUOTES
ESCAPE
PARALLEL OFF
ALLOWOVERWRITE;
--@end

--@unload-m002
unload ('select * from tb_product_view_best')
to 's3://rb-rec-his-use1/$CUID/dmp/$TIME_STR/m002.csv'
credentials 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
DELIMITER ','
ADDQUOTES
ESCAPE
PARALLEL OFF
ALLOWOVERWRITE;
--@end

--@unload-tb_asc_recmd_result_all
unload ('select target_item_id, cross_sell_item_id, \'\', score, rank from tb_asc_recmd_view_result')
to 's3://rb-rec-his-use1/$CUID/dmp/$TIME_STR/tb_asc_recmd_result_all.csv'
credentials 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
DELIMITER ','
ADDQUOTES
ESCAPE
PARALLEL OFF
ALLOWOVERWRITE;
--@end

--@unload-tb_product
--#withschema
unload ('select * from tb_product_all')
to 's3://rb-rec-his-use1/$CUID/dmp/$TIME_STR/tb_product.csv'
credentials 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
DELIMITER ','
ADDQUOTES
ESCAPE
PARALLEL OFF
ALLOWOVERWRITE;
--@end