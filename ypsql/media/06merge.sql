--@drop-tb_product_all
drop table if exists tb_product_all;
--@end

--@create-tb_product_all
create table tb_product_all as
select
	item_id,
	item_name,
	max(item_image) as item_image,
	max(item_url) as item_url,
	max(reply_count) reply_count,
	sum(count) as count
from (
	select	
		item_id, item_name, item_image, item_url, reply_count, count
	from
		tb_product
	union all
	select
		item_id, item_name, item_image, item_url, 0, count
	from
		tb_product_his
	) a
group by a.item_id, item_name;

--@drop-tb_asc_recmd_view_result
drop table if exists tb_asc_recmd_view_result;
--@end

--@create-tb_asc_recmd_view_result:export
create table tb_asc_recmd_view_result as
select
	target_item_id, cross_sell_item_id, score, rank
from (
	select
		a.target_item_id, a.cross_sell_item_id, a.score,
		row_number() over (partition by target_item_id order by score desc) as rank
	from (
		select
			target_item_id, cross_sell_item_id, sum(score) score
		from (
			select
				a.target_item_id, a.cross_sell_item_id, a.score score
			from
				tb_asc_recmd_view_result_cur a
			union all
			select
				a.target_item_id, a.cross_sell_item_id, a.score * (0.9 ^ (3/24)) score
			from
				tb_asc_recmd_view_result_his a
			) a
		group by target_item_id, cross_sell_item_id
		) a
	) a
where rank <= 40;
--@end