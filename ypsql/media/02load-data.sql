--#var $AWS_ACCESS_KEY_ID = springproperty(aws.access-key-id)
--#var $AWS_SECRET_ACCESS_KEY = springproperty(aws.secret-access-key)
--#var $CUID = springproperty(recobell.site.cuid)
--#var $NOW = datenow(UTC)

--@copy-tmp_log_view_unrefine
--#foreach $i in (1...3)
--#var $date = datesub($NOW, $i, hours)
--#var $DATE_STR = dateformat($date, yyyy/MM/dd)
--#var $TIME_STR = dateformat($date, yyyyMMddHH)
copy tmp_log_view_unrefine
from 's3://rb-logs-apne1-rep-use1/$CUID/view/$DATE_STR/view_$TIME_STR.json.gz'
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
FORMAT AS JSON 's3://rb-logic-rec-conf-apne1-rep-use1/media/view.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto';
--@end

--@copy-tmp_product
--#foreach $i in (1...3)
--#var $date = datesub($NOW, $i, hours)
--#var $DATE_STR = dateformat($date, yyyy/MM/dd)
--#var $TIME_STR = dateformat($date, yyyyMMddHH)
copy tmp_product
from 's3://rb-logs-apne1-rep-use1/$CUID/product/$DATE_STR/product_$TIME_STR.json.gz'
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
FORMAT AS JSON 's3://rb-logic-rec-conf-apne1-rep-use1/media/product.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto';
--@end

--@copy-tb_asc_recmd_view_result_his
--#tryeach $i in (0...108) until succeed
--#var $date = datesub($NOW, $i, hours)
--#var $TIME_STR = dateformat($date, yyyy/MM/dd/hh)
copy tb_asc_recmd_view_result_his
from 's3://rb-rec-his-use1/$CUID/dmp/$TIME_STR/tb_asc_recmd_result_all.csv0'
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
DELIMITER ','
REMOVEQUOTES
ESCAPE
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto';
--@end


--@copy-tb_product_view_best_his
--#tryeach $i in (0...108) until succeed
--#var $date = datesub($NOW, $i, hours)
--#var $TIME_STR = dateformat($date, yyyy/MM/dd/hh)
copy tb_product_view_best_his
from 's3://rb-rec-his-use1/$CUID/dmp/$TIME_STR/m002.csv0'
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
DELIMITER ','
REMOVEQUOTES
ESCAPE
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto';
--@end


--@copy-tb_product_his
--#tryeach $i in (0...108) until succeed
--#var $date = datesub($NOW, $i, hours)
--#var $TIME_STR = dateformat($date, yyyy/MM/dd/hh)
copy tb_product_his
from 's3://rb-rec-his-use1/$CUID/dmp/$TIME_STR/tb_product.csv0'
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
DELIMITER ','
REMOVEQUOTES
ESCAPE
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto';
--@end