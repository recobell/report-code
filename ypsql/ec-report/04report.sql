--@drop-tb_pv_count
drop table if exists tb_pv_count;
--@end

--@create-tb_pv_count
CREATE TABLE tb_pv_count AS
select
	device
	,case when rc_code is not null then 2 else 1 end as is_rec
	,case when rc_code is null then 'na' else rc_code end as rc_code
	,count(*) pv_count
from tb_log_view_report
group by 1,2,3;
--@end

--@drop-tb_session_count
drop table if exists tb_session_count;
--@end

--@create-tb_session_count
CREATE TABLE tb_session_count AS
select
	device
	,case when reco_view>0 then 2 else 1 end as is_rec
	,case when rc_code_num = 0 then 'na' when rc_code_num=1 then first_rc_code else 'multiple_rc' end as rc_code
	,count(*) session_count
from tmp_session_id_view
group by 1,2,3;
--@end



--@drop-tb_order_item_within_24h
drop table if exists tb_order_item_within_24h;
--@end
--@create-tb_order_item_within_24h
create table tb_order_item_within_24h as
select 
	a.pc_id
	,a.session_id
	,a.rc_code
	,a.item_id
	,a.server_time view_time
	,b.device
	,b.server_time order_time
	,b.order_id
	,b.price
	,b.quantity
from tb_log_view_report a
inner join tb_log_order_report b on a.pc_id = b.pc_id and a.item_id = b.item_id
where datediff (minute, a.server_time, b.server_time) between 0 and 1440;
--@end


--@drop-tb_product_rc_rank
drop table if exists tb_product_rc_rank;
--@end

--@create-tb_product_rc_rank
CREATE TABLE tb_product_rc_rank AS
select
		*, row_number() over (partition by session_id,item_id order by is_rec desc, view_time) as rank
	from
	(select
		device 
		,case when rc_code is not null then 2 else 1 end as is_rec 
		,case when rc_code is null then 'na' else rc_code end as rc_code 
		,item_id 
		,session_id 
		,min(view_time) view_time
		,max(price) price
		,max(quantity) quantity
	 from tb_order_item_within_24h
	group by 1,2,3,4,5);
--@end



--@drop-tb_product_order_count
drop table if exists tb_product_order_count;
--@end

--@create-tb_product_order_count
CREATE TABLE tb_product_order_count AS
select 
	device, is_rec, rc_code
	,count(*) pv_conversion_count
	,sum(price * quantity) purchase_item_revenue
	,sum(quantity) purchase_item_quantity
from
	tb_product_rc_rank
where rank = 1
group by 1,2,3
order by 1,2,3;
--@end


--@drop-tb_order_rc_item_rank
drop table if exists tb_order_rc_item_rank;
--@end

--@create-tb_order_rc_item_rank
CREATE TABLE tb_order_rc_item_rank AS
select
		*, row_number() over (partition by order_id order by is_rec desc, view_time) as rank
	from
	(select
		device 
		,case when rc_code is not null then 1 else 0 end as is_rec 
		,case when rc_code is null then 'na' else rc_code end as rc_code 
		,item_id 	
		,order_id
		,session_id 
		,min(view_time) view_time
		,max(price) price
		,max(quantity) quantity
	 from tb_order_item_within_24h
	group by 1,2,3,4,5,6);
--@end


--@drop-tb_order_multi_rc
drop table if exists tb_order_multi_rc;
--@end

--@create-tb_order_multi_rc
CREATE TABLE tb_order_multi_rc AS
select
	order_id
	,'multiple_rc' rc_code
from
(select 
	order_id
	,sum(case when rc_code <> 'na' then 1 else 0 end) rc_count
from tb_order_rc_item_rank
group by 1)
where rc_count>1;
--@end



--@drop-tb_order_rc_code_map
drop table if exists tb_order_rc_code_map;
--@end

--@create-tb_order_rc_code_map
CREATE TABLE tb_order_rc_code_map AS
select
	a.order_id
	,case when b.order_id is not null then b.rc_code else a.rc_code end rc_code
from 
	(select 
		order_id
		,rc_code
	from
		tb_order_rc_item_rank
	where rank = 1
	group by 1,2) a
left join tb_order_multi_rc b on a.order_id = b.order_id;
--@end

--@drop-tb_order_stat
drop table if exists tb_order_stat;
--@end

--@create-tb_order_stat
CREATE TABLE tb_order_stat AS
select
	order_id
	,max(device) device
	,count(distinct item_id) order_item_count
	,sum(price * quantity) order_revenue
from tb_log_order_report
group by 1;
--@end

--@drop-tb_order_count
drop table if exists tb_order_count;
--@end

--@create-tb_order_count
CREATE TABLE tb_order_count AS
select
	device
	,is_rec
	,rc_code
	,count(*) order_count
	,sum(order_item_count) order_item_count
	,sum(order_revenue) order_revenue
from
(select 
	a.order_id
	,a.device
	,case when b.rc_code <> 'na' then 2 when b.rc_code = 'na' then 1 else 0 end as is_rec
	,case when b.rc_code is not null then b.rc_code else 'no_click' end as rc_code 
	,a.order_item_count
	,a.order_revenue
from tb_order_stat a 
left join tb_order_rc_code_map b on a.order_id = b.order_id)
group by 1,2,3;
--@end







--@drop-tb_session_rc_item_rank
drop table if exists tb_session_rc_item_rank;
--@end

--@create-tb_session_rc_item_rank
CREATE TABLE tb_session_rc_item_rank AS
select
		*, row_number() over (partition by session_id order by is_rec desc, view_time) as rank
	from
	(select
		device 
		,case when rc_code is not null then 1 else 0 end as is_rec 
		,case when rc_code is null then 'na' else rc_code end as rc_code 
		,item_id 	
		,session_id 
		,min(view_time) view_time
		,max(price) price
		,max(quantity) quantity
	 from tb_order_item_within_24h
	group by 1,2,3,4,5);
--@end


--@drop-tb_session_multi_rc
drop table if exists tb_session_multi_rc;
--@end

--@create-tb_session_multi_rc
CREATE TABLE tb_session_multi_rc AS
select
	session_id
	,'multiple_rc' rc_code
from
(select 
	session_id
	,sum(case when rc_code <> 'na' then 1 else 0 end) rc_count
from tb_session_rc_item_rank
group by 1)
where rc_count>1;
--@end


--@drop-tb_session_rc_code_map
drop table if exists tb_session_rc_code_map;
--@end

--@create-tb_session_rc_code_map
CREATE TABLE tb_session_rc_code_map AS
select
	a.session_id
	,case when b.session_id is not null then b.rc_code else a.rc_code end rc_code
from 
	(select 
		session_id
		,rc_code
	from
		tb_session_rc_item_rank
	where rank = 1
	group by 1,2) a
left join tb_session_multi_rc b on a.session_id = b.session_id;
--@end





--@drop-tb_session_stat
drop table if exists tb_session_stat;
--@end

--@create-tb_session_stat
CREATE TABLE tb_session_stat AS
select
	session_id
	,max(device) device
from tb_log_order_report
group by 1;
--@end

--@drop-tb_session_conversion_count
drop table if exists tb_session_conversion_count;
--@end

--@create-tb_session_conversion_count
CREATE TABLE tb_session_conversion_count AS
select
	device
	,is_rec
	,rc_code
	,count(*) session_conversion_count
from
(select 
	a.session_id
	,a.device
	,case when b.rc_code <> 'na' then 2 when b.rc_code = 'na' then 1 else 0 end as is_rec
	,case when b.rc_code is not null then b.rc_code else 'no_click' end as rc_code 
from tb_session_stat a 
left join tb_session_rc_code_map b on a.session_id = b.session_id)
group by 1,2,3;
--@end

--@drop-tb_ec_index
drop table if exists tb_ec_index;
--@end

--@create-tb_ec_index
CREATE TABLE tb_ec_index AS
select 
	a.device	
	,a.is_rec
	,a.rc_code	
	,d.pv_count
	,e.session_count
	,c.pv_conversion_count
	,b.session_conversion_count
	,c.purchase_item_quantity	
	,c.purchase_item_revenue
	,a.order_count
	,a.order_item_count
	,a.order_revenue
from tb_order_count a
left join tb_session_conversion_count b on a.device = b.device and a.is_rec = b.is_rec and a.rc_code = b.rc_code
left join tb_product_order_count c on a.device = c.device and a.is_rec = c.is_rec  and a.rc_code = c.rc_code
left join tb_pv_count d on a.device = d.device and a.is_rec = d.is_rec and a.rc_code = d.rc_code
left join tb_session_count e on a.device = e.device and a.is_rec = e.is_rec and a.rc_code = e.rc_code;
--@end



select * from tb_ec_index;

select
	device
	,is_rec
	,rc_code
	, sum(pv_conversion_count::float)/sum(pv_count::float) page_cr
	, sum(session_conversion_count::float)/sum(session_count::float) session_cr
	, sum(purchase_item_revenue::float)/sum(purchase_item_quantity::float) avg_item_price
	, sum(order_item_count::float)/sum(order_count::float) avg_item_count
	, sum(order_revenue::float)/ sum(order_count::float) avg_order_revenue
from tb_ec_index 
--group by 1,2
order by 1,2;



select
	device
	,is_rec
	,rc_code
	,pv_count
	,session_count
	,pv_conversion_count
	,session_conversion_count
	,purchase_item_quantity	
	,purchase_item_revenue
	,order_count
	,order_item_count
	,order_revenue
	,pv_conversion_count::float/pv_count::float page_cr
	,session_conversion_count::float/session_count::float session_cr
	,purchase_item_revenue::float/purchase_item_quantity::float avg_item_price
	,order_item_count::float/order_count::float avg_item_count
	,order_revenue::float/order_count::float avg_order_revenue
from tb_ec_index 
--group by 1,2
order by 1,2,3;

