--@drop-tb_recommend_result_prevenue_daily_s1
drop table if exists tb_recommend_result_prevenue_daily_s1;
--@end

--@create-tb_recommend_result_prevenue_daily_s1
CREATE TABLE tb_recommend_result_prevenue_daily_s1 AS
select
	A.date
	,sum(A.revenue)/sum(A.frequency) prevenue
    ,sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end) reco_prevenue
    ,sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end) no_reco_prevenue	
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_prevenue_daily
drop table if exists tb_recommend_result_prevenue_daily;
--@end

--@create-tb_recommend_result_prevenue_daily
CREATE TABLE tb_recommend_result_prevenue_daily AS 
SELECT *,
    round(reco_prevenue :: real / no_reco_prevenue :: real, 4) as reco_up_ratio
FROM tb_recommend_result_prevenue_daily_s1;
--@end

--quantity (구매당 아이템)

--@drop-tb_recommend_result_quantity_daily_s1
drop table if exists tb_recommend_result_quantity_daily_s1;
--@end

--@create-tb_recommend_result_quantity_daily_s1
CREATE TABLE tb_recommend_result_quantity_daily_s1 AS
select
	A.date
	,round(avg(A.quantity :: real),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: real end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: real end),3) no_reco_quantity
    --,round(avg(case when B.session_id is not null then A.quantity :: real end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id group by 1
order by 1;
--@end

--@drop-tb_recommend_result_quantity_daily
drop table if exists tb_recommend_result_quantity_daily;
--@end

--@create-tb_recommend_result_quantity_daily
CREATE TABLE tb_recommend_result_quantity_daily AS SELECT *,
    round(reco_quantity :: real / no_reco_quantity :: real, 4) as reco_up_ratio
FROM tb_recommend_result_quantity_daily_s1;
--@end


--pclick data 

--@drop-tb_recommend_result_click_daily_s1
drop table if exists tb_recommend_result_click_daily_s1;
--@end

--@create-tb_recommend_result_click_daily_s1
CREATE TABLE tb_recommend_result_click_daily_s1 AS 
SELECT 
	date
	,round(AVG(view :: real),3) click
    ,round(AVG(case when reco_view>0 then view :: real end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: real end),3) no_reco_click
    --,round(AVG(case when view > 1 then view :: real  end),3) loyal_click
    
FROM
    tmp_recommend_result_view_session
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_click_daily
drop table if exists tb_recommend_result_click_daily;
--@end

--@create-tb_recommend_result_click_daily
CREATE TABLE tb_recommend_result_click_daily AS SELECT *,
    round(reco_click / no_reco_click, 4) as reco_up_ratio
FROM tb_recommend_result_click_daily_s1;   
--@end

-- CVR data

--@drop-tb_recommend_result_cvr_daily_s1
drop table if exists tb_recommend_result_cvr_daily_s1;
--@end

--@create-tb_recommend_result_cvr_daily_s1
CREATE TABLE tb_recommend_result_cvr_daily_s1 AS 
select 
	A.date
	,round(sum(B.frequency :: real)/count(distinct A.session_id),3) cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_cr
	,round(sum(case when A.reco_view =0 then B.frequency :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_cr
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.session_id = B.session_id
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_cvr_daily
drop table if exists tb_recommend_result_cvr_daily;
--@end

--@create-tb_recommend_result_cvr_daily
CREATE TABLE tb_recommend_result_cvr_daily AS SELECT *,
    round(reco_cr / no_reco_cr, 4) as reco_up_ratio
FROM tb_recommend_result_cvr_daily_s1;   
--@end

-- sa data

--@drop-tb_recommend_result_sa_daily_s1
drop table if exists tb_recommend_result_sa_daily_s1;
--@end

--@create-tb_recommend_result_sa_daily_s1
CREATE TABLE tb_recommend_result_sa_daily_s1 AS 
select 
	A.date
	,round(sum(B.revenue :: real)/count(distinct A.session_id),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
	,round(sum(case when A.reco_view =0 then B.revenue :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date  and A.session_id = B.session_id
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_sa_daily
drop table if exists tb_recommend_result_sa_daily;
--@end

--@create-tb_recommend_result_sa_daily
CREATE TABLE tb_recommend_result_sa_daily AS SELECT *,
    round(reco_sa / no_reco_sa, 4) as reco_up_ratio
FROM tb_recommend_result_sa_daily_s1; 
--@end
