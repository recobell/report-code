-- YPSQL BASED
--#var $AWS_ACCESS_KEY_ID = springproperty(aws.access-key)
--#var $AWS_SECRET_ACCESS_KEY = springproperty(aws.secret-access-key)
--#var $CUID = springproperty(recobell.rec.cuid)
--#var $NOW = datenow(UTC)
--#var $TIME_STR = dateformat($NOW, yyyy/MM/dd/HH)
--#var $DATE_STR = dateformat($NOW, yyyy/MM)

--@unload-total_lotte_report
unload ('select * from total_lotte_report')
to 's3://rb-ds/$CUID/dmp/$DATE_STR/total_lotte_report.csv'
credentials 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
DELIMITER ','
PARALLEL OFF
ALLOWOVERWRITE;
--@end


--@unload-pc_lotte_report
unload ('select * from pc_lotte_report')
to 's3://rb-ds/$CUID/dmp/$DATE_STR/pc_lotte_report.csv'
credentials 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
DELIMITER ','
PARALLEL OFF
ALLOWOVERWRITE;
--@end


--@unload-mo_lotte_report
unload ('select * from mo_lotte_report')
to 's3://rb-ds/$CUID/dmp/$DATE_STR/mo_lotte_report.csv'
credentials 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
DELIMITER ','
PARALLEL OFF
ALLOWOVERWRITE;
--@end


--@unload-push_lotte_report
unload ('select * from push_lotte_report')
to 's3://rb-ds/$CUID/dmp/$DATE_STR/push_lotte_report.csv'
credentials 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
DELIMITER ','
PARALLEL OFF
ALLOWOVERWRITE;
--@end

