--@drop-tb_log_view_report
drop table if exists tb_log_view_report;
--@end

--@create-tb_log_view_report
CREATE TABLE tb_log_view_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date,
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term,
	plan_id
from 
tb_log_view;
--@end

--@drop-tb_log_order_report
drop table if exists tb_log_order_report;
--@end

--@create-tb_log_order_report
CREATE TABLE tb_log_order_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term, 
	order_id, 
	order_price,
	price, 
	quantity
from 
tb_log_order;
--@end

--@drop-tb_log_view_report_pc
drop table if exists tb_log_view_report_pc;
--@end

--@create-tb_log_view_report_pc
CREATE TABLE tb_log_view_report_pc as
select
	server_time,
	convert_timezone('KST', server_time)::date as date,
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term,
	plan_id
from 
tb_log_view
where device = 'PW';
--@end

--@drop-tb_log_order_report_pc
drop table if exists tb_log_order_report_pc;
--@end

--@create-tb_log_order_report_pc
CREATE TABLE tb_log_order_report_pc as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term, 
	order_id, 
	order_price,
	price, 
	quantity
from 
tb_log_order
where device = 'PW';
--@end


--@drop-tb_log_view_report_mo
drop table if exists tb_log_view_report_mo;
--@end

--@create-tb_log_view_report_mo
CREATE TABLE tb_log_view_report_mo as
select
	server_time,
	convert_timezone('KST', server_time)::date as date,
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term,
	plan_id
from 
tb_log_view
where device <> 'PW';
--@end

--@drop-tb_log_order_report_mo
drop table if exists tb_log_order_report_mo;
--@end

--@create-tb_log_order_report_mo
CREATE TABLE tb_log_order_report_mo as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term, 
	order_id, 
	order_price,
	price, 
	quantity
from 
tb_log_order
where device <> 'PW';
--@end


-- --@drop-tb_product_info
-- drop table if exists tb_product_info;
-- --@end

-- --@create-tb_product_info
-- create table tb_product_info as
-- select
--   item_id,
--   max(item_name) item_name,
--   max(item_image) item_image,
--   max(item_url) item_url,
--   max(original_price) original_price,
--   max(sale_price) sale_price,
--   max(COALESCE(category3, category2, category1, '')) category,
--   coalesce(max(category1), '') category1,
--   coalesce(max(category2), '') category2,
--   coalesce(max(category3), '') category3,
--   coalesce(max(category4), '') category4,
--   coalesce(max(category5), '') category5,
--   min(reg_date) reg_date,
--   min(update_date) update_date,
--   min(expire_date) expire_date,
--   min(stock) stock,
--   min(state) state,
--   max(description) description, 
--   max(extra_image) extra_image,
--   max(locale) locale
--  from
--   tmp_product_info
--  where
--   original_price is not null and original_price > 0 and sale_price > 0
--  group by item_id;
--  --@end

-- --@drop-tb_product_info_report 
-- drop table if exists tb_product_info_report;
-- --@end

-- --@create-tb_product_info_report
-- CREATE TABLE tb_product_info_report as
-- select
-- 	item_id, 
-- 	item_name, 
-- 	original_price, 
-- 	sale_price, 
-- 	category1, 
-- 	category2, 
-- 	category3, 
-- 	category4, 
-- 	category5, 
-- 	reg_date
-- 	stock
-- from 
-- tb_product_info;
-- --@end

--recommendation resource table

--@drop-tmp_recommend_result_view
drop table if exists tmp_recommend_result_view;
--@end

--@create-tmp_recommend_result_view
CREATE TABLE tmp_recommend_result_view as
select 
	date
	,pc_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by date , pc_id, device;
--@end

--@drop-tmp_recommend_result_order
drop table if exists tmp_recommend_result_order;
--@end

--@create-tmp_recommend_result_order
CREATE TABLE tmp_recommend_result_order as
select
	date
	,pc_id 
	,device
	
	,sum(price*quantity) revenue
	,sum(case when rc_code is not null then price*quantity else 0 end) reco_revenue
	,sum(case when rc_code is null then price*quantity else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(distinct case when rc_code is not null then order_id else null end) reco_frequency
	,count(distinct case when rc_code is null then order_id else null end) no_reco_frequency
	
from tb_log_order_report
group by date, pc_id, device;
--@end

--@drop-tmp_recommend_result_view_session
drop table if exists tmp_recommend_result_view_session;
--@end

--@create-tmp_recommend_result_view_session
CREATE TABLE tmp_recommend_result_view_session as
select
	date
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by date,session_id, device;
--@end

--@drop-tmp_recommend_result_order_session
drop table if exists tmp_recommend_result_order_session;
--@end

--@create-tmp_recommend_result_order_session
CREATE TABLE tmp_recommend_result_order_session as
select
	A.date
	,A.session_id 
	,A.order_id
	,A.device
	
	,sum(A.price*A.quantity) revenue
	,sum(case when B.reco_view > 0 then A.price*A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.price*A.quantity else 0 end) no_reco_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_price else 0 end) no_reco_actual_payment
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_id end) no_reco_frequency
	
from tb_log_order_report A
left join tmp_recommend_result_view_session B on A.session_id = B.session_id and A.date = B.date
group by A.date,A.session_id,A.order_id,A.device;
--@end

--loyal group : comparison

--@drop-tb_recommend_loyal_group
drop table if exists tb_recommend_loyal_group;
--@end

--@create-tb_recommend_loyal_group
CREATE TABLE tb_recommend_loyal_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
WHERE
	view > 1;
--@end	

--@drop-tb_recommend_reco_group
drop table if exists tb_recommend_reco_group;
--@end

--@create-tb_recommend_reco_group
CREATE TABLE tb_recommend_reco_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
where reco_view > 0;
--@end
	
###############################################################################################################################


--@drop-tmp_recommend_result_view_pc
drop table if exists tmp_recommend_result_view_pc;
--@end

--@create-tmp_recommend_result_view_pc
CREATE TABLE tmp_recommend_result_view_pc as
select 
	date
	,pc_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_report_pc
group by date , pc_id, device;
--@end

--@drop-tmp_recommend_result_order_pc
drop table if exists tmp_recommend_result_order_pc;
--@end

--@create-tmp_recommend_result_order_pc
CREATE TABLE tmp_recommend_result_order_pc as
select
	date
	,pc_id 
	,device
	
	,sum(price*quantity) revenue
	,sum(case when rc_code is not null then price*quantity else 0 end) reco_revenue
	,sum(case when rc_code is null then price*quantity else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(distinct case when rc_code is not null then order_id else null end) reco_frequency
	,count(distinct case when rc_code is null then order_id else null end) no_reco_frequency
	
from tb_log_order_report_pc
group by date, pc_id, device;
--@end

--@drop-tmp_recommend_result_view_session_pc
drop table if exists tmp_recommend_result_view_session_pc;
--@end

--@create-tmp_recommend_result_view_session_pc
CREATE TABLE tmp_recommend_result_view_session_pc as
select
	date
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_report_pc
group by date,session_id, device;
--@end

--@drop-tmp_recommend_result_order_session_pc
drop table if exists tmp_recommend_result_order_session_pc;
--@end

--@create-tmp_recommend_result_order_session_pc
CREATE TABLE tmp_recommend_result_order_session_pc as
select
	A.date
	,A.session_id 
	,A.order_id
	,A.device
	
	,sum(A.price*A.quantity) revenue
	,sum(case when B.reco_view > 0 then A.price*A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.price*A.quantity else 0 end) no_reco_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_price else 0 end) no_reco_actual_payment
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_id end) no_reco_frequency
	
from tb_log_order_report_pc A
left join tmp_recommend_result_view_session_pc B on A.session_id = B.session_id and A.date = B.date
group by A.date,A.session_id,A.order_id,A.device;
--@end

--loyal group : comparison

--@drop-tb_recommend_loyal_group_pc
drop table if exists tb_recommend_loyal_group_pc;
--@end

--@create-tb_recommend_loyal_group_pc
CREATE TABLE tb_recommend_loyal_group_pc AS 
SELECT * FROM
    tmp_recommend_result_view_session_pc
WHERE
	view > 1;
--@end	

--@drop-tb_recommend_reco_group_pc
drop table if exists tb_recommend_reco_group_pc;
--@end

--@create-tb_recommend_reco_group_pc
CREATE TABLE tb_recommend_reco_group_pc AS 
SELECT * FROM
    tmp_recommend_result_view_session_pc
where reco_view > 0;
--@end
	
###############################################################################################################################


--@drop-tmp_recommend_result_view_mo
drop table if exists tmp_recommend_result_view_mo;
--@end

--@create-tmp_recommend_result_view_mo
CREATE TABLE tmp_recommend_result_view_mo as
select 
	date
	,pc_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_report_mo
group by date , pc_id, device;
--@end

--@drop-tmp_recommend_result_order_mo
drop table if exists tmp_recommend_result_order_mo;
--@end

--@create-tmp_recommend_result_order_mo
CREATE TABLE tmp_recommend_result_order_mo as
select
	date
	,pc_id 
	,device
	
	,sum(price*quantity) revenue
	,sum(case when rc_code is not null then price*quantity else 0 end) reco_revenue
	,sum(case when rc_code is null then price*quantity else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(distinct case when rc_code is not null then order_id else null end) reco_frequency
	,count(distinct case when rc_code is null then order_id else null end) no_reco_frequency
	
from tb_log_order_report_mo
group by date, pc_id, device;
--@end

--@drop-tmp_recommend_result_view_session_mo
drop table if exists tmp_recommend_result_view_session_mo;
--@end

--@create-tmp_recommend_result_view_session_mo
CREATE TABLE tmp_recommend_result_view_session_mo as
select
	date
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_report_mo
group by date,session_id, device;
--@end

--@drop-tmp_recommend_result_order_session_mo
drop table if exists tmp_recommend_result_order_session_mo;
--@end

--@create-tmp_recommend_result_order_session_mo
CREATE TABLE tmp_recommend_result_order_session_mo as
select
	A.date
	,A.session_id 
	,A.order_id
	,A.device
	
	,sum(A.price*A.quantity) revenue
	,sum(case when B.reco_view > 0 then A.price*A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.price*A.quantity else 0 end) no_reco_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_price else 0 end) no_reco_actual_payment
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_id end) no_reco_frequency
	
from tb_log_order_report_mo A
left join tmp_recommend_result_view_session_mo B on A.session_id = B.session_id and A.date = B.date
group by A.date,A.session_id,A.order_id,A.device;
--@end

--loyal group : comparison

--@drop-tb_recommend_loyal_group_mo
drop table if exists tb_recommend_loyal_group_mo;
--@end

--@create-tb_recommend_loyal_group_mo
CREATE TABLE tb_recommend_loyal_group_mo AS 
SELECT * FROM
    tmp_recommend_result_view_session_mo
WHERE
	view > 1;
--@end	

--@drop-tb_recommend_reco_group_mo
drop table if exists tb_recommend_reco_group_mo;
--@end

--@create-tb_recommend_reco_group_mo
CREATE TABLE tb_recommend_reco_group_mo AS 
SELECT * FROM
    tmp_recommend_result_view_session_mo
where reco_view > 0;
--@end

###############################################################################################################################

--@drop-tmp_recommend_result_view_push
drop table if exists tmp_recommend_result_view_push;
--@end

--@create-tmp_recommend_result_view_push
CREATE TABLE tmp_recommend_result_view_push as
select 
	date
	,pc_id 
	,device
	,count(*) as view
	,sum(case when rc_code like '%push%' then 1 else 0 end) as reco_view
	,sum(case when rc_code not like '%push%' then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by date , pc_id, device;
--@end

--@drop-tmp_recommend_result_order_push
drop table if exists tmp_recommend_result_order_push;
--@end

--@create-tmp_recommend_result_order_push
CREATE TABLE tmp_recommend_result_order_push as
select
	date
	,pc_id 
	,device
	
	,sum(price*quantity) revenue
	,sum(case when rc_code like '%push%' then price*quantity else 0 end) reco_revenue
	,sum(case when rc_code not like '%push%' then price*quantity else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(distinct case when rc_code like '%push%' then order_id else null end) reco_frequency
	,count(distinct case when rc_code not like '%push%' then order_id else null end) no_reco_frequency
	
from tb_log_order_report
group by date, pc_id, device;
--@end

--@drop-tmp_recommend_result_view_session_push
drop table if exists tmp_recommend_result_view_session_push;
--@end

--@create-tmp_recommend_result_view_session_push
CREATE TABLE tmp_recommend_result_view_session_push as
select
	date
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code like '%push%' then 1 else 0 end) as reco_view
	,sum(case when rc_code not like '%push%' then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by date,session_id, device;
--@end

--@drop-tmp_recommend_result_order_session_push
drop table if exists tmp_recommend_result_order_session_push;
--@end

--@create-tmp_recommend_result_order_session_push
CREATE TABLE tmp_recommend_result_order_session_push as
select
	A.date
	,A.session_id 
	,A.order_id
	,A.device
	
	,sum(A.price*A.quantity) revenue
	,sum(case when B.reco_view > 0 then A.price*A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.price*A.quantity else 0 end) no_reco_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_price else 0 end) no_reco_actual_payment
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_id end) no_reco_frequency
	
from tb_log_order_report A
left join tmp_recommend_result_view_session_push B on A.session_id = B.session_id and A.date = B.date
group by A.date,A.session_id,A.order_id,A.device;
--@end

--loyal group : comparison

--@drop-tb_recommend_loyal_group_push
drop table if exists tb_recommend_loyal_group_push;
--@end

--@create-tb_recommend_loyal_group_push
CREATE TABLE tb_recommend_loyal_group_push AS 
SELECT * FROM
    tmp_recommend_result_view_session_push
WHERE
	view > 1;
--@end	

--@drop-tb_recommend_reco_group_push
drop table if exists tb_recommend_reco_group_push;
--@end

--@create-tb_recommend_reco_group_push
CREATE TABLE tb_recommend_reco_group_push AS 
SELECT * FROM
    tmp_recommend_result_view_session_push
where reco_view > 0;
--@end


