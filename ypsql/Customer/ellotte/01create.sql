-- Be careful editing comments that begin with '--@', which will be interpreted and processed by SqlSet.

--@drop-tmp_product_info
drop table if exists tmp_product_info;
--@end

--@create-tmp_product_info
create table tmp_product_info
(
	cuid varchar(200),
	type varchar(200),
	item_id varchar(200),
	item_name varchar(2000),
	item_image varchar(200),
	item_url varchar(2000),
	original_price float,
	sale_price float,
	category1 varchar(200),
	category2 varchar(200),
	category3 varchar(200),
	category4 varchar(200),
	category5 varchar(200),
	reg_date varchar(200),
	update_date timestamp,
	expire_date timestamp,
	stock int,
	state varchar(200),
	description varchar(200),
	extra_image varchar(200),
	locale varchar(200)
);
--@end

--@drop-tb_log_view
drop table if exists tb_log_view;
--@end

--@tb_log_view
create table tb_log_view
(
	cuid varchar(200),
	type varchar(200),
	server_time timestamp NOT NULL,
	ip_address varchar(200),
	user_agent varchar(2000),
	api_version varchar(200),
	domain varchar(200),
	device varchar(200),
	pc_id varchar(200) NOT NULL,
	session_id varchar(200) NOT NULL,
	ad_id varchar(200),
	push_id varchar(200),
	gc_id varchar(200),
	user_id varchar(1000),
	ab_group varchar(200),
	rc_code varchar(200),
	ad_code varchar(200),
	locale varchar(200),
	item_id varchar(200),
	search_term varchar(200),
	plan_id varchar(200)
);
--@end


--@drop-tb_log_order
drop table if exists tb_log_order;
--@end

--@create-tb_log_order
create table tb_log_order
(
	cuid varchar(200),
	type varchar(200),
	server_time timestamp NOT NULL,
	ip_address varchar(200),
	user_agent varchar(2000),
	api_version varchar(200),
	domain varchar(200),
	device varchar(200),
	pc_id varchar(200) NOT NULL,
	session_id varchar(200) NOT NULL,
	ad_id varchar(200),
	push_id varchar(200),
	gc_id varchar(200),
	user_id varchar(200),
	ab_group varchar(200),
	rc_code varchar(200),
	ad_code varchar(200),
	locale varchar(200),
	item_id varchar(200),
	search_term varchar(200),
	order_id varchar(200),
	order_price float,
	price float,
	quantity bigint
);
--@end

--@drop-tb_log_search
drop table if exists tb_log_search;
--@end

--@create-tb_log_search
create table tb_log_search
(
	cuid varchar(200),
	type varchar(200),
	server_time timestamp NOT NULL,
	ip_address varchar(200),
	user_agent varchar(2000),
	api_version varchar(200),
	domain varchar(200),
	device varchar(200),
	pc_id varchar(200) NOT NULL,
	session_id varchar(200) NOT NULL,
	ad_id varchar(200),
	push_id varchar(200),
	gc_id varchar(200),
	user_id varchar(200),
	ab_group varchar(200),
	rc_code varchar(200),
	ad_code varchar(200),
	locale varchar(200),
	search_term varchar(200),
	search_result varchar(200)
);
--@end
