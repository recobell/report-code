
--UV check

--@drop-tb_recommend_result_uv_daily_s1
drop table if exists tb_recommend_result_uv_daily_s1;
--@end

--@create-tb_recommend_result_uv_daily_s1
CREATE TABLE tb_recommend_result_uv_daily_s1 AS
select
	date
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
from tmp_recommend_result_view
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_uv_daily
drop table if exists tb_recommend_result_uv_daily;
--@end

--@create-tb_recommend_result_uv_daily
CREATE TABLE tb_recommend_result_uv_daily AS
select
	*
	,round( reco_uv :: real / uv :: real , 2) ratio
from tb_recommend_result_uv_daily_s1;
--@end

--visit

--@drop-tb_recommend_result_visit_daily_s1
drop table if exists tb_recommend_result_visit_daily_s1;
--@end

--@create-tb_recommend_result_visit_daily_s1
CREATE TABLE tb_recommend_result_visit_daily_s1 AS
select
	date
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit    
    
from tmp_recommend_result_view_session
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_visit_daily
drop table if exists tb_recommend_result_visit_daily;
--@end

--@create-tb_recommend_result_visit_daily
CREATE TABLE tb_recommend_result_visit_daily AS
select
	*
	,round( reco_visit :: real / visit :: real , 2) ratio
from tb_recommend_result_visit_daily_s1;
--@end

--detail view click

--@drop-tb_recommend_result_pv_daily_s1
drop table if exists tb_recommend_result_pv_daily_s1;
--@end

--@create-tb_recommend_result_pv_daily_s1
CREATE TABLE tb_recommend_result_pv_daily_s1 AS
select
	date
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv   
from tmp_recommend_result_view_session
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_pv_daily
drop table if exists tb_recommend_result_pv_daily;
--@end

--@create-tb_recommend_result_pv_daily
CREATE TABLE tb_recommend_result_pv_daily AS
select
	*
	,round( reco_pv :: real / pv :: real , 2) ratio
from tb_recommend_result_pv_daily_s1;
--@end

-- frequency

--@drop-tb_recommend_result_frequency_daily_s1
drop table if exists tb_recommend_result_frequency_daily_s1;
--@end

--@create-tb_recommend_result_frequency_daily_s1
CREATE TABLE tb_recommend_result_frequency_daily_s1 AS
select
	date
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency    
    
from tmp_recommend_result_order_session
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_frequency_daily
drop table if exists tb_recommend_result_frequency_daily;
--@end

--@create-tb_recommend_result_frequency_daily
CREATE TABLE tb_recommend_result_frequency_daily AS
select
	*
	,round( reco_frequency :: real / frequency :: real , 2) ratio
from tb_recommend_result_frequency_daily_s1;
--@end

-- revenue

--@drop-tb_recommend_result_revenue_daily_s1
drop table if exists tb_recommend_result_revenue_daily_s1;
--@end

--@create-tb_recommend_result_revenue_daily_s1
CREATE TABLE tb_recommend_result_revenue_daily_s1 AS
select
	date
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
    
from tmp_recommend_result_order_session
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_revenue_daily
drop table if exists tb_recommend_result_revenue_daily;
--@end

--@create-tb_recommend_result_revenue_daily
CREATE TABLE tb_recommend_result_revenue_daily AS
select
	*
	,round( reco_revenue :: real / revenue :: real , 2) ratio
from tb_recommend_result_revenue_daily_s1;
--@end



####################################################################################################################################################################################
####################################################################################################################################################################################


--visit

--@drop-tb_recommend_result_visit_daily_s1_pc
drop table if exists tb_recommend_result_visit_daily_s1_pc;
--@end

--@create-tb_recommend_result_visit_daily_s1_pc
CREATE TABLE tb_recommend_result_visit_daily_s1_pc AS
select
	date
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit    
    
from tmp_recommend_result_view_session_pc
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_visit_daily_pc
drop table if exists tb_recommend_result_visit_daily_pc;
--@end

--@create-tb_recommend_result_visit_daily_pc
CREATE TABLE tb_recommend_result_visit_daily_pc AS
select
	*
	,round( reco_visit :: real / visit :: real , 2) ratio
from tb_recommend_result_visit_daily_s1_pc;
--@end



--detail view click

--@drop-tb_recommend_result_pv_daily_s1_pc
drop table if exists tb_recommend_result_pv_daily_s1_pc;
--@end

--@create-tb_recommend_result_pv_daily_s1_pc
CREATE TABLE tb_recommend_result_pv_daily_s1_pc AS
select
	date
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv   
from tmp_recommend_result_view_session_pc
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_pv_daily_pc
drop table if exists tb_recommend_result_pv_daily_pc;
--@end

--@create-tb_recommend_result_pv_daily_pc
CREATE TABLE tb_recommend_result_pv_daily_pc AS
select
	*
	,round( reco_pv :: real / pv :: real , 2) ratio
from tb_recommend_result_pv_daily_s1_pc;
--@end


-- revenue

--@drop-tb_recommend_result_revenue_daily_s1_pc
drop table if exists tb_recommend_result_revenue_daily_s1_pc;
--@end

--@create-tb_recommend_result_revenue_daily_s1_pc
CREATE TABLE tb_recommend_result_revenue_daily_s1_pc AS
select
	date
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
    
from tmp_recommend_result_order_session_pc
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_revenue_daily_pc
drop table if exists tb_recommend_result_revenue_daily_pc;
--@end

--@create-tb_recommend_result_revenue_daily_pc
CREATE TABLE tb_recommend_result_revenue_daily_pc AS
select
	*
	,round( reco_revenue :: real / revenue :: real , 2) ratio
from tb_recommend_result_revenue_daily_s1_pc;
--@end



####################################################################################################################################################################################
####################################################################################################################################################################################




--visit

--@drop-tb_recommend_result_visit_daily_s1_mo
drop table if exists tb_recommend_result_visit_daily_s1_mo;
--@end

--@create-tb_recommend_result_visit_daily_s1_mo
CREATE TABLE tb_recommend_result_visit_daily_s1_mo AS
select
	date
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit    
    
from tmp_recommend_result_view_session_mo
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_visit_daily_mo
drop table if exists tb_recommend_result_visit_daily_mo;
--@end

--@create-tb_recommend_result_visit_daily_mo
CREATE TABLE tb_recommend_result_visit_daily_mo AS
select
	*
	,round( reco_visit :: real / visit :: real , 2) ratio
from tb_recommend_result_visit_daily_s1_mo;
--@end



--detail view click

--@drop-tb_recommend_result_pv_daily_s1_mo
drop table if exists tb_recommend_result_pv_daily_s1_mo;
--@end

--@create-tb_recommend_result_pv_daily_s1_mo
CREATE TABLE tb_recommend_result_pv_daily_s1_mo AS
select
	date
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv   
from tmp_recommend_result_view_session_mo
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_pv_daily_mo
drop table if exists tb_recommend_result_pv_daily_mo;
--@end

--@create-tb_recommend_result_pv_daily_mo
CREATE TABLE tb_recommend_result_pv_daily_mo AS
select
	*
	,round( reco_pv :: real / pv :: real , 2) ratio
from tb_recommend_result_pv_daily_s1_mo;
--@end


-- revenue

--@drop-tb_recommend_result_revenue_daily_s1_mo
drop table if exists tb_recommend_result_revenue_daily_s1_mo;
--@end

--@create-tb_recommend_result_revenue_daily_s1_mo
CREATE TABLE tb_recommend_result_revenue_daily_s1_mo AS
select
	date
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
    
from tmp_recommend_result_order_session_mo
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_revenue_daily_mo
drop table if exists tb_recommend_result_revenue_daily_mo;
--@end

--@create-tb_recommend_result_revenue_daily_mo
CREATE TABLE tb_recommend_result_revenue_daily_mo AS
select
	*
	,round( reco_revenue :: real / revenue :: real , 2) ratio
from tb_recommend_result_revenue_daily_s1_mo;
--@end



####################################################################################################################################################################################
####################################################################################################################################################################################



--visit

--@drop-tb_recommend_result_visit_daily_s1_email
drop table if exists tb_recommend_result_visit_daily_s1_email;
--@end

--@create-tb_recommend_result_visit_daily_s1_email
CREATE TABLE tb_recommend_result_visit_daily_s1_email AS
select
	date
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit    
    
from tmp_recommend_result_view_session_email
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_visit_daily_email
drop table if exists tb_recommend_result_visit_daily_email;
--@end

--@create-tb_recommend_result_visit_daily_email
CREATE TABLE tb_recommend_result_visit_daily_email AS
select
	*
	,round( reco_visit :: real / visit :: real , 2) ratio
from tb_recommend_result_visit_daily_s1_email;
--@end



--detail view click

--@drop-tb_recommend_result_pv_daily_s1_email
drop table if exists tb_recommend_result_pv_daily_s1_email;
--@end

--@create-tb_recommend_result_pv_daily_s1_email
CREATE TABLE tb_recommend_result_pv_daily_s1_email AS
select
	date
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv   
from tmp_recommend_result_view_session_email
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_pv_daily_email
drop table if exists tb_recommend_result_pv_daily_email;
--@end

--@create-tb_recommend_result_pv_daily_email
CREATE TABLE tb_recommend_result_pv_daily_email AS
select
	*
	,round( reco_pv :: real / pv :: real , 2) ratio
from tb_recommend_result_pv_daily_s1_email;
--@end


-- revenue

--@drop-tb_recommend_result_revenue_daily_s1_email
drop table if exists tb_recommend_result_revenue_daily_s1_email;
--@end

--@create-tb_recommend_result_revenue_daily_s1_email
CREATE TABLE tb_recommend_result_revenue_daily_s1_email AS
select
	date
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
    
from tmp_recommend_result_order_session_email
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_revenue_daily_email
drop table if exists tb_recommend_result_revenue_daily_email;
--@end

--@create-tb_recommend_result_revenue_daily_email
CREATE TABLE tb_recommend_result_revenue_daily_email AS
select
	*
	,round( reco_revenue :: real / revenue :: real , 2) ratio
from tb_recommend_result_revenue_daily_s1_email;
--@end




####################################################################################################################################################################################
####################################################################################################################################################################################



--visit

--@drop-tb_recommend_result_visit_daily_s1_push
drop table if exists tb_recommend_result_visit_daily_s1_push;
--@end

--@create-tb_recommend_result_visit_daily_s1_push
CREATE TABLE tb_recommend_result_visit_daily_s1_push AS
select
	date
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit    
    
from tmp_recommend_result_view_session_push
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_visit_daily_push
drop table if exists tb_recommend_result_visit_daily_push;
--@end

--@create-tb_recommend_result_visit_daily_push
CREATE TABLE tb_recommend_result_visit_daily_push AS
select
	*
	,round( reco_visit :: real / visit :: real , 2) ratio
from tb_recommend_result_visit_daily_s1_push;
--@end



--detail view click

--@drop-tb_recommend_result_pv_daily_s1_push
drop table if exists tb_recommend_result_pv_daily_s1_push;
--@end

--@create-tb_recommend_result_pv_daily_s1_push
CREATE TABLE tb_recommend_result_pv_daily_s1_push AS
select
	date
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv   
from tmp_recommend_result_view_session_push
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_pv_daily_push
drop table if exists tb_recommend_result_pv_daily_push;
--@end

--@create-tb_recommend_result_pv_daily_push
CREATE TABLE tb_recommend_result_pv_daily_push AS
select
	*
	,round( reco_pv :: real / pv :: real , 2) ratio
from tb_recommend_result_pv_daily_s1_push;
--@end


-- revenue

--@drop-tb_recommend_result_revenue_daily_s1_push
drop table if exists tb_recommend_result_revenue_daily_s1_push;
--@end

--@create-tb_recommend_result_revenue_daily_s1_push
CREATE TABLE tb_recommend_result_revenue_daily_s1_push AS
select
	date
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
    
from tmp_recommend_result_order_session_push
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_revenue_daily_push
drop table if exists tb_recommend_result_revenue_daily_push;
--@end

--@create-tb_recommend_result_revenue_daily_push
CREATE TABLE tb_recommend_result_revenue_daily_push AS
select
	*
	,round( reco_revenue :: real / revenue :: real , 2) ratio
from tb_recommend_result_revenue_daily_s1_push;
--@end












