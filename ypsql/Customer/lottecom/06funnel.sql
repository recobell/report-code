
###############################################
--Total Funnel
###############################################

--@drop-tb_log_session_pc_id
drop table if exists tb_log_session_pc_id;
--@end

--@create-tb_log_session_pc_id
CREATE TABLE tb_log_session_pc_id as
select
	pc_id
	,session_id
from tb_log_view_report
group by pc_id, session_id;
--@end

--@drop-tb_log_view_report_rank_s1
drop table if exists tb_log_view_report_rank_s1;
--@end

--@create-tb_log_view_report_rank_s1
CREATE TABLE tb_log_view_report_rank_s1 as
select
	* 
	,rank() over (partition by session_id order by server_time)
from tb_log_view_report;
--@end

--@drop-tb_log_view_report_rank_s2
drop table if exists tb_log_view_report_rank_s2;
--@end

--@create-tb_log_view_report_rank_s2
CREATE TABLE tb_log_view_report_rank_s2 as
select
	session_id
	,min(rank) first_reco_view
	,max(rank) last_reco_view
from tb_log_view_report_rank_s1
where rc_code is not null
group by session_id;
--@end

--@drop-tb_log_view_report_rank_s3
drop table if exists tb_log_view_report_rank_s3;
--@end

--@create-tb_log_view_report_rank_s3
CREATE TABLE tb_log_view_report_rank_s3 as
select * from tb_log_view_report_rank_s1 where rc_code is not null;
--@end

--@drop-tb_log_view_report_rank_s4
drop table if exists tb_log_view_report_rank_s4;
--@end

--@create-tb_log_view_report_rank_s4
CREATE TABLE tb_log_view_report_rank_s4 as
select 
A.session_id
,A.first_reco_view
,B.rc_code first_rc_code
,A.last_reco_view
from
tb_log_view_report_rank_s2 A
inner join tb_log_view_report_rank_s3 B 
on A.session_id = B.session_id and A.first_reco_view = B.rank;
--@end

--@drop-tb_log_view_report_rank
drop table if exists tb_log_view_report_rank;
--@end

--@create-tb_log_view_report_rank
CREATE TABLE tb_log_view_report_rank as
select 
A.session_id
,A.first_reco_view
,A.first_rc_code
,A.last_reco_view
,B.rc_code last_rc_code

from tb_log_view_report_rank_s4 A
inner join tb_log_view_report_rank_s3 B
on A.session_id = B.session_id and A.last_reco_view = B.rank;
--@end

--@drop-tb_crm_kpi_view_s1
drop table if exists tb_crm_kpi_view_s1;
--@end

--@create-tb_crm_kpi_view_s1
create table tb_crm_kpi_view_s1 as
select
	A.pc_id
	,A.device
	,A.session_id
	,count(*) as view
	,sum(case when A.rc_code is not null then 1 else 0 end) as reco_view
	,count(distinct A.item_id) as itemvar
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) first_server_time

from tb_log_view_report A
group by A.pc_id, A.device, A.session_id;
--@end

--@drop-tb_crm_kpi_view_s2
drop table if exists tb_crm_kpi_view_s2;
--@end

--@create-tb_crm_kpi_view_s2
create table tb_crm_kpi_view_s2 as
select
	A.*
	,B.first_reco_view
	,B.first_rc_code
	,B.last_reco_view
	,B.last_rc_code
from tb_crm_kpi_view_s1 A 
left join tb_log_view_report_rank B on A.session_id = B.session_id;
--@end

--@drop-tb_reco_revenue_s1
drop table if exists tb_reco_revenue_s1;
--@end

--@create-tb_reco_revenue_s1
create table tb_reco_revenue_s1 as
select 
	A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code is not null then server_time end) server_time, session_id, item_id, max(case when rc_code is not null then 1 else 0 end) rc_num from tb_log_view_report group by session_id, item_id) A
inner join tb_log_order_report B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time;
--@end

--@drop-tb_reco_revenue
drop table if exists tb_reco_revenue;
--@end

--@create-tb_reco_revenue
create table tb_reco_revenue as
select 
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s1
group by session_id;
--@end

--@drop-tb_crm_kpi_order_s1
drop table if exists tb_crm_kpi_order_s1;
--@end

--@create-tb_crm_kpi_order_s1
create table tb_crm_kpi_order_s1 as
	SELECT 
		
		pc_id,
		session_id,
		min(server_time) server_time,
		sum(quantity) quantity,
		sum(price * quantity) revenue,
		count(distinct item_id) buyitemnum,
		count(distinct order_id) ordercount
	FROM
		tb_log_order_report
	GROUP BY pc_id, session_id;
--@end



--final funnel metric table

--@drop-tb_session_funnel
drop table if exists tb_session_funnel;
--@end

--@create-tb_session_funnel
create table tb_session_funnel as
select
	A.first_server_time first_view_time
	,B.server_time order_time
	,A.session_id
	,E.pc_id
	,A.device
	,A.first_reco_view
	,A.first_rc_code
	,A.last_reco_view
	,A.last_rc_code
	,A.view view_count
	,A.reco_view reco_view_count
	,A.itemvar item_var
	,A.period view_period
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count
	,C.reco_revenue 
	,C.reco_quantity
	
from tb_crm_kpi_view_s2 A
left join tb_crm_kpi_order_s1 B on A.session_id = B.session_id
left join tb_reco_revenue C on A.session_id = C.session_id
left join tb_log_session_pc_id E on A.session_id = E.session_id;
--@end

--@drop-tb_direct_session
drop table if exists tb_direct_session;
--@end

--@create-tb_direct_session
create table tb_direct_session as
select 
	a.date,
	a.direct_reco_session,
	b.direct_no_reco_session
from
	(select convert_timezone('KST', order_time)::date as date,
		count(session_id) as direct_reco_session
	from tb_session_funnel 
	where first_reco_view is not null and order_time is not null 
	group by 1
	order by 1) a
inner join
	(select convert_timezone('KST', order_time)::date as date, 
		count(session_id) direct_no_reco_session
	from tb_session_funnel 
	where first_reco_view is null and order_time is not null 
	group by 1 order by 1) b on a.date = b.date
order by 1;
--@end

--@drop-tb_reco_revenue_s3
drop table if exists tb_reco_revenue_s3;
--@end

--@create-tb_reco_revenue_s3
create table tb_reco_revenue_s3 as
select 
	B.date
	,A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code is not null then server_time end) server_time, session_id, item_id, max(case when rc_code is not null then 1 else 0 end) rc_num from tb_log_view_report group by session_id, item_id) A
inner join tb_log_order_report B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time
group by 1,2,3,4,5
order by 1;
--@end

--@drop-tb_reco_revenue_s4
drop table if exists tb_reco_revenue_s4;
--@end

--@create-tb_reco_revenue_s4
create table tb_reco_revenue_s4 as
select 
	date,
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s3
group by date,session_id
order by 1;
--@end

-- revenue

--@drop-tb_reco_revenue_funnel
drop table if exists tb_reco_revenue_funnel;
--@end

--@create-tb_reco_revenue_funnel
create table tb_reco_revenue_funnel as
select
	date
	,sum(reco_revenue) reco_revenue    
    
from tb_reco_revenue_s4
group by 1
order by 1;
--@end



####################################################################################################################################################################################

###############################################
--PC Funnel
###############################################

--@drop-tb_log_session_pc_id_pc
drop table if exists tb_log_session_pc_id_pc;
--@end

--@create-tb_log_session_pc_id_pc
CREATE TABLE tb_log_session_pc_id_pc as
select
	pc_id
	,session_id
from tb_log_view_report_pc
group by pc_id, session_id;
--@end

--@drop-tb_log_view_report_rank_s1_pc
drop table if exists tb_log_view_report_rank_s1_pc;
--@end

--@create-tb_log_view_report_rank_s1_pc
CREATE TABLE tb_log_view_report_rank_s1_pc as
select
	* 
	,rank() over (partition by session_id order by server_time)
from tb_log_view_report_pc;
--@end

--@drop-tb_log_view_report_rank_s2_pc
drop table if exists tb_log_view_report_rank_s2_pc;
--@end

--@create-tb_log_view_report_rank_s2_pc
CREATE TABLE tb_log_view_report_rank_s2_pc as
select
	session_id
	,min(rank) first_reco_view
	,max(rank) last_reco_view
from tb_log_view_report_rank_s1_pc
where rc_code is not null
group by session_id;
--@end

--@drop-tb_log_view_report_rank_s3_pc
drop table if exists tb_log_view_report_rank_s3_pc;
--@end

--@create-tb_log_view_report_rank_s3_pc
CREATE TABLE tb_log_view_report_rank_s3_pc as
select * from tb_log_view_report_rank_s1_pc where rc_code is not null;
--@end

--@drop-tb_log_view_report_rank_s4_pc
drop table if exists tb_log_view_report_rank_s4_pc;
--@end

--@create-tb_log_view_report_rank_s4_pc
CREATE TABLE tb_log_view_report_rank_s4_pc as
select 
A.session_id
,A.first_reco_view
,B.rc_code first_rc_code
,A.last_reco_view
from
tb_log_view_report_rank_s2_pc A
inner join tb_log_view_report_rank_s3_pc B 
on A.session_id = B.session_id and A.first_reco_view = B.rank;
--@end

--@drop-tb_log_view_report_rank_pc
drop table if exists tb_log_view_report_rank_pc;
--@end

--@create-tb_log_view_report_rank_pc
CREATE TABLE tb_log_view_report_rank_pc as
select 
A.session_id
,A.first_reco_view
,A.first_rc_code
,A.last_reco_view
,B.rc_code last_rc_code

from tb_log_view_report_rank_s4_pc A
inner join tb_log_view_report_rank_s3_pc B
on A.session_id = B.session_id and A.last_reco_view = B.rank;
--@end

--view

--@drop-tb_crm_kpi_view_s1_pc
drop table if exists tb_crm_kpi_view_s1_pc;
--@end

--@create-tb_crm_kpi_view_s1_pc
create table tb_crm_kpi_view_s1_pc as
select
	A.pc_id
	,A.device
	,A.session_id
	,count(*) as view
	,sum(case when A.rc_code is not null then 1 else 0 end) as reco_view
	,count(distinct A.item_id) as itemvar
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) first_server_time

from tb_log_view_report_pc A
group by A.pc_id, A.device, A.session_id;
--@end

--@drop-tb_crm_kpi_view_s2_pc
drop table if exists tb_crm_kpi_view_s2_pc;
--@end

--@create-tb_crm_kpi_view_s2_pc
create table tb_crm_kpi_view_s2_pc as
select
	A.*
	,B.first_reco_view
	,B.first_rc_code
	,B.last_reco_view
	,B.last_rc_code
from tb_crm_kpi_view_s1_pc A 
left join tb_log_view_report_rank_pc B on A.session_id = B.session_id;
--@end

--@drop-tb_reco_revenue_s1_pc
drop table if exists tb_reco_revenue_s1_pc;
--@end

--@create-tb_reco_revenue_s1_pc
create table tb_reco_revenue_s1_pc as
select 
	A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code is not null then server_time end) server_time, session_id, item_id, max(case when rc_code is not null then 1 else 0 end) rc_num from tb_log_view_report_pc group by session_id, item_id) A
inner join tb_log_order_report_pc B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time;
--@end

--@drop-tb_reco_revenue_pc
drop table if exists tb_reco_revenue_pc;
--@end

--@create-tb_reco_revenue_pc
create table tb_reco_revenue_pc as
select 
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s1_pc
group by session_id;
--@end

--@drop-tb_crm_kpi_order_s1_pc
drop table if exists tb_crm_kpi_order_s1_pc;
--@end

--@create-tb_crm_kpi_order_s1_pc
create table tb_crm_kpi_order_s1_pc as
	SELECT 
		
		pc_id,
		session_id,
		min(server_time) server_time,
		sum(quantity) quantity,
		sum(price * quantity) revenue,
		count(distinct item_id) buyitemnum,
		count(distinct order_id) ordercount
	FROM
		tb_log_order_report_pc
	GROUP BY pc_id, session_id;
--@end

--final funnel metric table

--@drop-tb_session_funnel_pc
drop table if exists tb_session_funnel_pc;
--@end

--@create-tb_session_funnel_pc
create table tb_session_funnel_pc as
select
	A.first_server_time first_view_time
	,B.server_time order_time
	,A.session_id
	,E.pc_id
	,A.device
	,A.first_reco_view
	,A.first_rc_code
	,A.last_reco_view
	,A.last_rc_code
	,A.view view_count
	,A.reco_view reco_view_count
	,A.itemvar item_var
	,A.period view_period
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count
	,C.reco_revenue 
	,C.reco_quantity
	
from tb_crm_kpi_view_s2_pc A
left join tb_crm_kpi_order_s1_pc B on A.session_id = B.session_id
left join tb_reco_revenue_pc C on A.session_id = C.session_id
left join tb_log_session_pc_id_pc E on A.session_id = E.session_id;
--@end

--@drop-tb_direct_session_pc
drop table if exists tb_direct_session_pc;
--@end

--@create-tb_direct_session_pc
create table tb_direct_session_pc as
select 
	a.date,
	a.direct_reco_session,
	b.direct_no_reco_session
from
	(select convert_timezone('KST', order_time)::date as date,
		count(session_id) as direct_reco_session
	from tb_session_funnel_pc
	where first_reco_view is not null and order_time is not null 
	group by 1
	order by 1) a
inner join
	(select convert_timezone('KST', order_time)::date as date, 
		count(session_id) direct_no_reco_session
	from tb_session_funnel_pc 
	where first_reco_view is null and order_time is not null 
	group by 1 order by 1) b on a.date = b.date
order by 1;
--@end

--@drop-tb_reco_revenue_s3_pc
drop table if exists tb_reco_revenue_s3_pc;
--@end

--@create-tb_reco_revenue_s3_pc
create table tb_reco_revenue_s3_pc as
select 
	B.date
	,A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code is not null then server_time end) server_time, session_id, item_id, max(case when rc_code is not null then 1 else 0 end) rc_num from tb_log_view_report_pc group by session_id, item_id) A
inner join tb_log_order_report_pc B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time
group by 1,2,3,4,5
order by 1;
--@end

--@drop-tb_reco_revenue_s4_pc
drop table if exists tb_reco_revenue_s4_pc;
--@end

--@create-tb_reco_revenue_s4_pc
create table tb_reco_revenue_s4_pc as
select 
	date,
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s3_pc
group by date,session_id
order by 1;
--@end

-- revenue

--@drop-tb_reco_revenue_funnel_pc
drop table if exists tb_reco_revenue_funnel_pc;
--@end

--@create-tb_reco_revenue_funnel_pc
create table tb_reco_revenue_funnel_pc as
select
	date
	,sum(reco_revenue) reco_revenue    
    
from tb_reco_revenue_s4_pc
group by 1
order by 1;
--@end


####################################################################################################################################################################################


###############################################
--MO Funnel
###############################################

--@drop-tb_log_session_pc_id_mo
drop table if exists tb_log_session_pc_id_mo;
--@end

--@create-tb_log_session_pc_id_mo
CREATE TABLE tb_log_session_pc_id_mo as
select
	pc_id
	,session_id
from tb_log_view_report_mo
group by pc_id, session_id;
--@end

--@drop-tb_log_view_report_rank_s1_mo
drop table if exists tb_log_view_report_rank_s1_mo;
--@end

--@create-tb_log_view_report_rank_s1_mo
CREATE TABLE tb_log_view_report_rank_s1_mo as
select
	* 
	,rank() over (partition by session_id order by server_time)
from tb_log_view_report_mo;
--@end

--@drop-tb_log_view_report_rank_s2_mo
drop table if exists tb_log_view_report_rank_s2_mo;
--@end

--@create-tb_log_view_report_rank_s2_mo
CREATE TABLE tb_log_view_report_rank_s2_mo as
select
	session_id
	,min(rank) first_reco_view
	,max(rank) last_reco_view
from tb_log_view_report_rank_s1_mo
where rc_code is not null
group by session_id;
--@end

--@drop-tb_log_view_report_rank_s3_mo
drop table if exists tb_log_view_report_rank_s3_mo;
--@end

--@create-tb_log_view_report_rank_s3_mo
CREATE TABLE tb_log_view_report_rank_s3_mo as
select * from tb_log_view_report_rank_s1_mo where rc_code is not null;
--@end

--@drop-tb_log_view_report_rank_s4_mo
drop table if exists tb_log_view_report_rank_s4_mo;
--@end

--@create-tb_log_view_report_rank_s4_mo
CREATE TABLE tb_log_view_report_rank_s4_mo as
select 
A.session_id
,A.first_reco_view
,B.rc_code first_rc_code
,A.last_reco_view
from
tb_log_view_report_rank_s2_mo A
inner join tb_log_view_report_rank_s3_mo B 
on A.session_id = B.session_id and A.first_reco_view = B.rank;
--@end

--@drop-tb_log_view_report_rank_mo
drop table if exists tb_log_view_report_rank_mo;
--@end

--@create-tb_log_view_report_rank_mo
CREATE TABLE tb_log_view_report_rank_mo as
select 
A.session_id
,A.first_reco_view
,A.first_rc_code
,A.last_reco_view
,B.rc_code last_rc_code

from tb_log_view_report_rank_s4_mo A
inner join tb_log_view_report_rank_s3_mo B
on A.session_id = B.session_id and A.last_reco_view = B.rank;
--@end

--@drop-tb_crm_kpi_view_s1_mo
drop table if exists tb_crm_kpi_view_s1_mo;
--@end

--@create-tb_crm_kpi_view_s1_mo
create table tb_crm_kpi_view_s1_mo as
select
	A.pc_id
	,A.device
	,A.session_id
	,count(*) as view
	,sum(case when A.rc_code is not null then 1 else 0 end) as reco_view
	,count(distinct A.item_id) as itemvar
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) first_server_time

from tb_log_view_report_mo A
group by A.pc_id, A.device, A.session_id;
--@end

--@drop-tb_crm_kpi_view_s2_mo
drop table if exists tb_crm_kpi_view_s2_mo;
--@end

--@create-tb_crm_kpi_view_s2_mo
create table tb_crm_kpi_view_s2_mo as
select
	A.*
	,B.first_reco_view
	,B.first_rc_code
	,B.last_reco_view
	,B.last_rc_code
from tb_crm_kpi_view_s1_mo A 
left join tb_log_view_report_rank_mo B on A.session_id = B.session_id;
--@end

--@drop-tb_reco_revenue_s1_mo
drop table if exists tb_reco_revenue_s1_mo;
--@end

--@create-tb_reco_revenue_s1_mo
create table tb_reco_revenue_s1_mo as
select 
	A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code is not null then server_time end) server_time, session_id, item_id, max(case when rc_code is not null then 1 else 0 end) rc_num from tb_log_view_report_mo group by session_id, item_id) A
inner join tb_log_order_report_mo B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time;
--@end

--@drop-tb_reco_revenue_mo
drop table if exists tb_reco_revenue_mo;
--@end

--@create-tb_reco_revenue_mo
create table tb_reco_revenue_mo as
select 
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s1_mo
group by session_id;
--@end

--@drop-tb_crm_kpi_order_s1_mo
drop table if exists tb_crm_kpi_order_s1_mo;
--@end

--@create-tb_crm_kpi_order_s1_mo
create table tb_crm_kpi_order_s1_mo as
	SELECT 
		
		pc_id,
		session_id,
		min(server_time) server_time,
		sum(quantity) quantity,
		sum(price * quantity) revenue,
		count(distinct item_id) buyitemnum,
		count(distinct order_id) ordercount
	FROM
		tb_log_order_report_mo
	GROUP BY pc_id, session_id;
--@end

--final funnel metric table

--@drop-tb_session_funnel_mo
drop table if exists tb_session_funnel_mo;
--@end

--@create-tb_session_funnel_mo
create table tb_session_funnel_mo as
select
	A.first_server_time first_view_time
	,B.server_time order_time
	,A.session_id
	,E.pc_id
	,A.device
	,A.first_reco_view
	,A.first_rc_code
	,A.last_reco_view
	,A.last_rc_code
	,A.view view_count
	,A.reco_view reco_view_count
	,A.itemvar item_var
	,A.period view_period
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count
	,C.reco_revenue 
	,C.reco_quantity
	
from tb_crm_kpi_view_s2_mo A
left join tb_crm_kpi_order_s1_mo B on A.session_id = B.session_id
left join tb_reco_revenue_mo C on A.session_id = C.session_id
left join tb_log_session_pc_id_mo E on A.session_id = E.session_id;
--@end

--@drop-tb_direct_session_mo
drop table if exists tb_direct_session_mo;
--@end

--@create-tb_direct_session_mo
create table tb_direct_session_mo as
select 
	a.date,
	a.direct_reco_session,
	b.direct_no_reco_session
from
	(select convert_timezone('KST', order_time)::date as date,
		count(session_id) as direct_reco_session
	from tb_session_funnel_mo
	where first_reco_view is not null and order_time is not null 
	group by 1
	order by 1) a
inner join
	(select convert_timezone('KST', order_time)::date as date, 
		count(session_id) direct_no_reco_session
	from tb_session_funnel_mo 
	where first_reco_view is null and order_time is not null 
	group by 1 order by 1) b on a.date = b.date
order by 1;
--@end

--@drop-tb_reco_revenue_s3_mo
drop table if exists tb_reco_revenue_s3_mo;
--@end

--@create-tb_reco_revenue_s3_mo
create table tb_reco_revenue_s3_mo as
select 
	B.date
	,A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code is not null then server_time end) server_time, session_id, item_id, max(case when rc_code is not null then 1 else 0 end) rc_num from tb_log_view_report_mo group by session_id, item_id) A
inner join tb_log_order_report_mo B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time
group by 1,2,3,4,5
order by 1;
--@end

--@drop-tb_reco_revenue_s4_mo
drop table if exists tb_reco_revenue_s4_mo;
--@end

--@create-tb_reco_revenue_s4_mo
create table tb_reco_revenue_s4_mo as
select 
	date,
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s3_mo
group by date,session_id
order by 1;
--@end

-- revenue

--@drop-tb_reco_revenue_funnel_mo
drop table if exists tb_reco_revenue_funnel_mo;
--@end

--@create-tb_reco_revenue_funnel_mo
create table tb_reco_revenue_funnel_mo as
select
	date
	,sum(reco_revenue) reco_revenue    
    
from tb_reco_revenue_s4_mo
group by 1
order by 1;
--@end


####################################################################################################################################################################################

###############################################
--Email Funnel
###############################################

--pc_id, session_id matching

--@drop-tb_log_session_pc_id_email
drop table if exists tb_log_session_pc_id_email;
--@end

--@create-tb_log_session_pc_id_email
CREATE TABLE tb_log_session_pc_id_email as
select
	pc_id
	,session_id
from tb_log_view_report
group by pc_id, session_id;
--@end


--first reco view

--@drop-tb_log_view_report_rank_s1_email
drop table if exists tb_log_view_report_rank_s1_email;
--@end

--@create-tb_log_view_report_rank_s1_email
CREATE TABLE tb_log_view_report_rank_s1_email as
select
	* 
	,rank() over (partition by session_id order by server_time)
from tb_log_view_report;
--@end

--@drop-tb_log_view_report_rank_s2_email
drop table if exists tb_log_view_report_rank_s2_email;
--@end

--@create-tb_log_view_report_rank_s2_email
CREATE TABLE tb_log_view_report_rank_s2_email as
select
	session_id
	,min(rank) first_reco_view
	,max(rank) last_reco_view
from tb_log_view_report_rank_s1_email
where rc_code like '%email%'
group by session_id;
--@end

--@drop-tb_log_view_report_rank_s3_email
drop table if exists tb_log_view_report_rank_s3_email;
--@end

--@create-tb_log_view_report_rank_s3_email
CREATE TABLE tb_log_view_report_rank_s3_email as
select * from tb_log_view_report_rank_s1_email where rc_code like '%email%';
--@end

--@drop-tb_log_view_report_rank_s4_email
drop table if exists tb_log_view_report_rank_s4_email;
--@end

--@create-tb_log_view_report_rank_s4_email
CREATE TABLE tb_log_view_report_rank_s4_email as
select 
A.session_id
,A.first_reco_view
,B.rc_code first_rc_code
,A.last_reco_view
from
tb_log_view_report_rank_s2_email A
inner join tb_log_view_report_rank_s3_email B 
on A.session_id = B.session_id and A.first_reco_view = B.rank;
--@end

--@drop-tb_log_view_report_rank_email
drop table if exists tb_log_view_report_rank_email;
--@end

--@create-tb_log_view_report_rank_email
CREATE TABLE tb_log_view_report_rank_email as
select 
A.session_id
,A.first_reco_view
,A.first_rc_code
,A.last_reco_view
,B.rc_code last_rc_code

from tb_log_view_report_rank_s4_email A
inner join tb_log_view_report_rank_s3_email B
on A.session_id = B.session_id and A.last_reco_view = B.rank;
--@end

--@drop-tb_crm_kpi_view_s1_email
drop table if exists tb_crm_kpi_view_s1_email;
--@end

--@create-tb_crm_kpi_view_s1_email
create table tb_crm_kpi_view_s1_email as
select
	A.pc_id
	,A.device
	,A.session_id
	,count(*) as view
	,sum(case when A.rc_code like '%email%' then 1 else 0 end) as reco_view
	,count(distinct A.item_id) as itemvar
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) first_server_time

from tb_log_view_report A
group by A.pc_id, A.device, A.session_id;
--@end

--@drop-tb_crm_kpi_view_s2_email
drop table if exists tb_crm_kpi_view_s2_email;
--@end

--@create-tb_crm_kpi_view_s2_email
create table tb_crm_kpi_view_s2_email as
select
	A.*
	,B.first_reco_view
	,B.first_rc_code
	,B.last_reco_view
	,B.last_rc_code
from tb_crm_kpi_view_s1_email A 
left join tb_log_view_report_rank_email B on A.session_id = B.session_id;
--@end

--@drop-tb_reco_revenue_s1_email
drop table if exists tb_reco_revenue_s1_email;
--@end

--@create-tb_reco_revenue_s1_email
create table tb_reco_revenue_s1_email as
select 
	A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code like '%email%' then server_time end) server_time, session_id, item_id, max(case when rc_code like '%email%' then 1 else 0 end) rc_num from tb_log_view_report group by session_id, item_id) A
inner join tb_log_order_report B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time;
--@end

--@drop-tb_reco_revenue_email
drop table if exists tb_reco_revenue_email;
--@end

--@create-tb_reco_revenue_email
create table tb_reco_revenue_email as
select 
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s1_email
group by session_id;
--@end

--@drop-tb_crm_kpi_order_s1_email
drop table if exists tb_crm_kpi_order_s1_email;
--@end

--@create-tb_crm_kpi_order_s1_email
create table tb_crm_kpi_order_s1_email as
	SELECT 
		
		pc_id,
		session_id,
		min(server_time) server_time,
		sum(quantity) quantity,
		sum(price * quantity) revenue,
		count(distinct item_id) buyitemnum,
		count(distinct order_id) ordercount
	FROM
		tb_log_order_report
	GROUP BY pc_id, session_id;
--@end

--@drop-tb_session_funnel_email
drop table if exists tb_session_funnel_email;
--@end

--@create-tb_session_funnel_email
create table tb_session_funnel_email as
select
	A.first_server_time first_view_time
	,B.server_time order_time
	,A.session_id
	,E.pc_id
	,A.device
	,A.first_reco_view
	,A.first_rc_code
	,A.last_reco_view
	,A.last_rc_code
	,A.view view_count
	,A.reco_view reco_view_count
	,A.itemvar item_var
	,A.period view_period
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count
	,C.reco_revenue 
	,C.reco_quantity
	
from tb_crm_kpi_view_s2_email A
left join tb_crm_kpi_order_s1_email B on A.session_id = B.session_id
left join tb_reco_revenue_email C on A.session_id = C.session_id
left join tb_log_session_pc_id_email E on A.session_id = E.session_id;
--@end

--@drop-tb_direct_session_email
drop table if exists tb_direct_session_email;
--@end

--@create-tb_direct_session_email
create table tb_direct_session_email as
select 
	a.date,
	a.direct_reco_session,
	b.direct_no_reco_session
from
	(select convert_timezone('KST', order_time)::date as date,
		count(session_id) as direct_reco_session
	from tb_session_funnel_email
	where first_reco_view is not null and order_time is not null
	group by 1
	order by 1) a
inner join
	(select convert_timezone('KST', order_time)::date as date, 
		count(session_id) direct_no_reco_session
	from tb_session_funnel_email 
	where first_reco_view is null and order_time is not null
	group by 1 order by 1) b on a.date = b.date
order by 1;
--@end

--@drop-tb_reco_revenue_s3_email
drop table if exists tb_reco_revenue_s3_email;
--@end

--@create-tb_reco_revenue_s3_email
create table tb_reco_revenue_s3_email as
select 
	B.date
	,A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code like '%email%' then server_time end) server_time, session_id, item_id, max(case when rc_code like '%email%' then 1 else 0 end) rc_num from tb_log_view_report group by session_id, item_id) A
inner join tb_log_order_report B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time
group by 1,2,3,4,5
order by 1;
--@end

--@drop-tb_reco_revenue_s4_email
drop table if exists tb_reco_revenue_s4_email;
--@end

--@create-tb_reco_revenue_s4_email
create table tb_reco_revenue_s4_email as
select 
	date,
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s3_email
group by date,session_id
order by 1;
--@end

-- revenue

--@drop-tb_reco_revenue_funnel_email
drop table if exists tb_reco_revenue_funnel_email;
--@end

--@create-tb_reco_revenue_funnel_email
create table tb_reco_revenue_funnel_email as
select
	date
	,sum(reco_revenue) reco_revenue    
    
from tb_reco_revenue_s4_email
group by 1
order by 1;
--@end



####################################################################################################################################################################################


###############################################
--Push Funnel
###############################################

--pc_id, session_id matching

--@drop-tb_log_session_pc_id_push
drop table if exists tb_log_session_pc_id_push;
--@end

--@create-tb_log_session_pc_id_push
CREATE TABLE tb_log_session_pc_id_push as
select
	pc_id
	,session_id
from tb_log_view_report
group by pc_id, session_id;
--@end


--first reco view

--@drop-tb_log_view_report_rank_s1_push
drop table if exists tb_log_view_report_rank_s1_push;
--@end

--@create-tb_log_view_report_rank_s1_push
CREATE TABLE tb_log_view_report_rank_s1_push as
select
	* 
	,rank() over (partition by session_id order by server_time)
from tb_log_view_report;
--@end

--@drop-tb_log_view_report_rank_s2_push
drop table if exists tb_log_view_report_rank_s2_push;
--@end

--@create-tb_log_view_report_rank_s2_push
CREATE TABLE tb_log_view_report_rank_s2_push as
select
	session_id
	,min(rank) first_reco_view
	,max(rank) last_reco_view
from tb_log_view_report_rank_s1_push
where rc_code like '%push%'
group by session_id;
--@end

--@drop-tb_log_view_report_rank_s3_push
drop table if exists tb_log_view_report_rank_s3_push;
--@end

--@create-tb_log_view_report_rank_s3_push
CREATE TABLE tb_log_view_report_rank_s3_push as
select * from tb_log_view_report_rank_s1_push where rc_code like '%push%';
--@end

--@drop-tb_log_view_report_rank_s4_push
drop table if exists tb_log_view_report_rank_s4_push;
--@end

--@create-tb_log_view_report_rank_s4_push
CREATE TABLE tb_log_view_report_rank_s4_push as
select 
A.session_id
,A.first_reco_view
,B.rc_code first_rc_code
,A.last_reco_view
from
tb_log_view_report_rank_s2_push A
inner join tb_log_view_report_rank_s3_push B 
on A.session_id = B.session_id and A.first_reco_view = B.rank;
--@end

--@drop-tb_log_view_report_rank_push
drop table if exists tb_log_view_report_rank_push;
--@end

--@create-tb_log_view_report_rank_push
CREATE TABLE tb_log_view_report_rank_push as
select 
A.session_id
,A.first_reco_view
,A.first_rc_code
,A.last_reco_view
,B.rc_code last_rc_code

from tb_log_view_report_rank_s4_push A
inner join tb_log_view_report_rank_s3_push B
on A.session_id = B.session_id and A.last_reco_view = B.rank;
--@end

--view

--@drop-tb_crm_kpi_view_s1_push
drop table if exists tb_crm_kpi_view_s1_push;
--@end

--@create-tb_crm_kpi_view_s1_push
create table tb_crm_kpi_view_s1_push as
select
	A.pc_id
	,A.device
	,A.session_id
	,count(*) as view
	,sum(case when A.rc_code like '%push%' then 1 else 0 end) as reco_view
	,count(distinct A.item_id) as itemvar
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) first_server_time

from tb_log_view_report A
group by A.pc_id, A.device, A.session_id;
--@end

--@drop-tb_crm_kpi_view_s2_push
drop table if exists tb_crm_kpi_view_s2_push;
--@end

--@create-tb_crm_kpi_view_s2_push
create table tb_crm_kpi_view_s2_push as
select
	A.*
	,B.first_reco_view
	,B.first_rc_code
	,B.last_reco_view
	,B.last_rc_code
from tb_crm_kpi_view_s1_push A 
left join tb_log_view_report_rank_push B on A.session_id = B.session_id;
--@end

--@drop-tb_reco_revenue_s1_push
drop table if exists tb_reco_revenue_s1_push;
--@end

--@create-tb_reco_revenue_s1_push
create table tb_reco_revenue_s1_push as
select 
	A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code like '%push%' then server_time end) server_time, session_id, item_id, max(case when rc_code like '%push%' then 1 else 0 end) rc_num from tb_log_view_report group by session_id, item_id) A
inner join tb_log_order_report B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time;
--@end

--@drop-tb_reco_revenue_push
drop table if exists tb_reco_revenue_push;
--@end

--@create-tb_reco_revenue_push
create table tb_reco_revenue_push as
select 
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s1_push
group by session_id;
--@end

--@drop-tb_crm_kpi_order_s1_push
drop table if exists tb_crm_kpi_order_s1_push;
--@end

--@create-tb_crm_kpi_order_s1_push
create table tb_crm_kpi_order_s1_push as
	SELECT 
		
		pc_id,
		session_id,
		min(server_time) server_time,
		sum(quantity) quantity,
		sum(price * quantity) revenue,
		count(distinct item_id) buyitemnum,
		count(distinct order_id) ordercount
	FROM
		tb_log_order_report
	GROUP BY pc_id, session_id;
--@end

--final funnel metric table

--@drop-tb_session_funnel_push
drop table if exists tb_session_funnel_push;
--@end

--@create-tb_session_funnel_push
create table tb_session_funnel_push as
select
	A.first_server_time first_view_time
	,B.server_time order_time
	,A.session_id
	,E.pc_id
	,A.device
	,A.first_reco_view
	,A.first_rc_code
	,A.last_reco_view
	,A.last_rc_code
	,A.view view_count
	,A.reco_view reco_view_count
	,A.itemvar item_var
	,A.period view_period
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count
	,C.reco_revenue 
	,C.reco_quantity
	
from tb_crm_kpi_view_s2_push A
left join tb_crm_kpi_order_s1_push B on A.session_id = B.session_id
left join tb_reco_revenue_push C on A.session_id = C.session_id
left join tb_log_session_pc_id_push E on A.session_id = E.session_id;
--@end

--@drop-tb_direct_session_push
drop table if exists tb_direct_session_push;
--@end

--@create-tb_direct_session_push
create table tb_direct_session_push as
select 
	a.date,
	a.direct_reco_session,
	b.direct_no_reco_session
from
	(select convert_timezone('KST', order_time)::date as date,
		count(session_id) as direct_reco_session
	from tb_session_funnel_push
	where first_reco_view is not null and order_time is not null
	group by 1
	order by 1) a
inner join
	(select convert_timezone('KST', order_time)::date as date, 
		count(session_id) direct_no_reco_session
	from tb_session_funnel_push 
	where first_reco_view is null and order_time is not null
	group by 1 order by 1) b on a.date = b.date
order by 1;
--@end

--@drop-tb_reco_revenue_s3_push
drop table if exists tb_reco_revenue_s3_push;
--@end

--@create-tb_reco_revenue_s3_push
create table tb_reco_revenue_s3_push as
select 
	B.date
	,A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code like '%push%' then server_time end) server_time, session_id, item_id, max(case when rc_code like '%push%' then 1 else 0 end) rc_num from tb_log_view_report group by session_id, item_id) A
inner join tb_log_order_report B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time
group by 1,2,3,4,5
order by 1;
--@end

--@drop-tb_reco_revenue_s4_push
drop table if exists tb_reco_revenue_s4_push;
--@end

--@create-tb_reco_revenue_s4_push
create table tb_reco_revenue_s4_push as
select 
	date,
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s3_push
group by date,session_id
order by 1;
--@end

-- revenue

--@drop-tb_reco_revenue_funnel_push
drop table if exists tb_reco_revenue_funnel_push;
--@end

--@create-tb_reco_revenue_funnel_push
create table tb_reco_revenue_funnel_push as
select
	date
	,sum(reco_revenue) reco_revenue    
    
from tb_reco_revenue_s4_push
group by 1
order by 1;
--@end


####################################################################################################################################################################################
