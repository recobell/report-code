
--revenue: 전체 매출
--reco_revenue: 경유 매출
--direct_reco_revenue: 추천매출
--visit: 열람 세션수
--reco_visit: 클릭 세션수
--reco_pv: 클릭수
--direct_reco_session: 추천 경유 구매 세션수
--direct_no_reco_session: 비추천 경유 구매 세션수


--@drop-total_lotte_report
drop table if exists total_lotte_report;
--@end

--@create-total_lotte_report
create table total_lotte_report as
select 
	a.date,   
	a.revenue,
	a.reco_revenue,
	(a.reco_revenue::float/a.revenue::float)*100 reco_revenue_ratio,
	e.reco_revenue as direct_reco_revenue,
	(e.reco_revenue::float/a.revenue::float)*100 direct_reco_revenue_ratio,
	b.visit,
	b.reco_visit,
	(b.reco_visit::real/b.visit::real)*100 reco_visit_ratio,
	c.reco_pv,
	d.direct_reco_session,
	(d.direct_reco_session::real/b.reco_visit::real)*100 direct_reco_session_ratio,
	d.direct_no_reco_session,
	(d.direct_no_reco_session::real/b.reco_visit::real)*100 direct_no_reco_session_ratio
from 
	tb_recommend_result_revenue_daily a
		left JOIN
	tb_recommend_result_visit_daily b on a.date= b.date
		left JOIN
	tb_recommend_result_pv_daily c on a.date = c.date
		left JOIN
	tb_direct_session d on a.date = d.date
		left JOIN
	tb_reco_revenue_funnel e on a.date = e.date
order by 1;
--@end


--@drop-pc_lotte_report
drop table if exists pc_lotte_report;
--@end

--@create-pc_lotte_report
create table pc_lotte_report as
select 
	a.date,   
	a.revenue,
	a.reco_revenue,
	(a.reco_revenue::float/a.revenue::float)*100 reco_revenue_ratio,
	e.reco_revenue as direct_reco_revenue,
	(e.reco_revenue::float/a.revenue::float)*100 direct_reco_revenue_ratio,
	b.visit,
	b.reco_visit,
	(b.reco_visit::real/b.visit::real)*100 reco_visit_ratio,
	c.reco_pv,
	d.direct_reco_session,
	(d.direct_reco_session::real/b.reco_visit::real)*100 direct_reco_session_ratio,
	d.direct_no_reco_session,
	(d.direct_no_reco_session::real/b.reco_visit::real)*100 direct_no_reco_session_ratio
from 
	tb_recommend_result_revenue_daily_pc a
		left JOIN
	tb_recommend_result_visit_daily_pc b on a.date= b.date
		left JOIN
	tb_recommend_result_pv_daily_pc c on a.date = c.date
		left JOIN
	tb_direct_session_pc d on a.date = d.date
		left JOIN
	tb_reco_revenue_funnel_pc e on a.date = e.date
order by 1;
--@end


--@drop-mp_lotte_report
drop table if exists mo_lotte_report;
--@end

--@create-mo_lotte_report
create table mo_lotte_report as
select 
	a.date,   
	a.revenue,
	a.reco_revenue,
	(a.reco_revenue::float/a.revenue::float)*100 reco_revenue_ratio,
	e.reco_revenue as direct_reco_revenue,
	(e.reco_revenue::float/a.revenue::float)*100 direct_reco_revenue_ratio,
	b.visit,
	b.reco_visit,
	(b.reco_visit::real/b.visit::real)*100 reco_visit_ratio,
	c.reco_pv,
	d.direct_reco_session,
	(d.direct_reco_session::real/b.reco_visit::real)*100 direct_reco_session_ratio,
	d.direct_no_reco_session,
	(d.direct_no_reco_session::real/b.reco_visit::real)*100 direct_no_reco_session_ratio
from 
	tb_recommend_result_revenue_daily_mo a
		left JOIN
	tb_recommend_result_visit_daily_mo b on a.date= b.date
		left JOIN
	tb_recommend_result_pv_daily_mo c on a.date = c.date
		left JOIN
	tb_direct_session_mo d on a.date = d.date
		left JOIN
	tb_reco_revenue_funnel_mo e on a.date = e.date
order by 1;
--@end


--@drop-email_lotte_report
drop table if exists email_lotte_report;
--@end

--@create-email_lotte_report
create table email_lotte_report as
select 
	a.date,   
	a.reco_revenue,
	(a.reco_revenue::float/a.revenue::float)*100 reco_revenue_ratio,
	e.reco_revenue as direct_reco_revenue,
	(e.reco_revenue::float/a.revenue::float)*100 direct_reco_revenue_ratio,
	b.reco_visit,
	(b.reco_visit::real/b.visit::real)*100 reco_visit_ratio,
	c.reco_pv,
	d.direct_reco_session,
	(d.direct_reco_session::real/b.reco_visit::real)*100 direct_reco_session_ratio
from 
	tb_recommend_result_revenue_daily_email a
		left JOIN
	tb_recommend_result_visit_daily_email b on a.date= b.date
		left JOIN
	tb_recommend_result_pv_daily_email c on a.date = c.date
		left JOIN
	tb_direct_session_email d on a.date = d.date
		left JOIN
	tb_reco_revenue_funnel_email e on a.date = e.date
order by 1;
--@end


--@drop-push_lotte_report
drop table if exists push_lotte_report;
--@end

--@create-push_lotte_report
create table push_lotte_report as
select 
	a.date,   
	a.reco_revenue,
	(a.reco_revenue::float/a.revenue::float)*100 reco_revenue_ratio,
	e.reco_revenue as direct_reco_revenue,
	(e.reco_revenue::float/a.revenue::float)*100 direct_reco_revenue_ratio,
	b.reco_visit,
	(b.reco_visit::real/b.visit::real)*100 reco_visit_ratio,
	c.reco_pv,
	d.direct_reco_session,
	(d.direct_reco_session::real/b.reco_visit::real)*100 direct_reco_session_ratio
from
	tb_recommend_result_revenue_daily_push a
		left JOIN
	tb_recommend_result_visit_daily_push b on a.date= b.date
		left JOIN
	tb_recommend_result_pv_daily_push c on a.date = c.date
		left JOIN
	tb_direct_session_push d on a.date = d.date
		left JOIN
	tb_reco_revenue_funnel_push e on a.date = e.date
order by 1;
--@end