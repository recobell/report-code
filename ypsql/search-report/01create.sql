
--@drop-tmp_log_view_unrefine
drop table if exists tmp_log_view_unrefine;
--@end

--@create-tmp_log_view_unrefine
CREATE TABLE tmp_log_view_unrefine (
	cuid varchar(200),
	type varchar(200),
	server_time timestamp NULL,
	ip_address varchar(200),
	user_agent varchar(2000),
	api_version varchar(200),
	domain varchar(200),
	device varchar(200),
	pc_id varchar(200),
	session_id varchar(200),
	ad_id varchar(200),
	push_id varchar(200),
	gc_id varchar(200),
	user_id varchar(1000),
	ab_group varchar(200),
	rc_code varchar(200),
	ad_code varchar(200),
	locale varchar(200),
	item_id varchar(200),
	search_term varchar(200),
	plan_id varchar(200)
);
--@end


--@drop-tmp_log_search_unrefine
drop table if exists tmp_log_search_unrefine;
--@end
--@create-tmp_log_search_unrefine
create table tmp_log_search_unrefine (
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(200),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
search_term varchar(200),
search_result varchar(200)
);
--@end


--@drop-tmp_log_order_unrefine
drop table if exists tmp_log_order_unrefine;
--@end

--@create-tmp_log_order_unrefine
CREATE TABLE tmp_log_order_unrefine (
	cuid varchar(200),
	type varchar(200),
	server_time timestamp NULL,
	ip_address varchar(200),
	user_agent varchar(2000),
	api_version varchar(200),
	domain varchar(200),
	device varchar(200),
	pc_id varchar(200),
	session_id varchar(200),
	ad_id varchar(200),
	push_id varchar(200),
	gc_id varchar(200),
	user_id varchar(200),
	ab_group varchar(200),
	rc_code varchar(200),
	ad_code varchar(200),
	locale varchar(200),
	item_id varchar(200),
	search_term varchar(200),
	order_id varchar(200),
	order_price float4,
	price float4,
	quantity int8
);
--@end
