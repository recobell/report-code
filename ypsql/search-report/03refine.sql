--#var $NOW = datenow(UTC)
--#var $date = datesub($NOW, 1, days)
--#var $DATE_STR = dateformat($date, yyyy-MM-dd)
--#var $TIMEZONE = springproperty(recobell.site.timezone)

--@drop-tb_log_view_report
drop table if exists tb_log_view_report;
--@end

--@create-tb_log_view_report
CREATE TABLE tb_log_view_report as
select
	server_time,
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	case 
	when rc_code like '%Detail%' then 'detail'
	when rc_code like '%Basket%' then 'basket'
	when rc_code like '%Search%' then 'search' 
	when rc_code is not null then 'main'
	else rc_code
	end as rc_code,
	ad_code,
	item_id, 
	search_term
	,row_number() over (partition by convert_timezone('$TIMEZONE', server_time)::date,session_id order by (case when rc_code is not null then 0 else 1 end),server_time) rc_rank
from 
tmp_log_view_unrefine
where 
convert_timezone('$TIMEZONE', server_time)::date=convert_timezone('kst',sysdate)::date-1;
--@end

--@drop-tb_log_order_report
drop table if exists tb_log_order_report;
--@end
--@create-tb_log_order_report
CREATE TABLE tb_log_order_report as
select
	server_time,
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	pc_id, 
	session_id, 
	rc_code,
	item_id, 
	order_id, 
	order_price,
	price, 
	quantity
from 
tmp_log_order_unrefine
where 
convert_timezone('$TIMEZONE', server_time)::date=convert_timezone('kst',sysdate)::date-1;
--@end

--@drop-tb_log_search_report
drop table if exists tb_log_search_report;
--@end
--@create-tb_log_search_report
CREATE TABLE tb_log_search_report as
select
	server_time,
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	pc_id, 
	session_id, 
	search_term
from 
tmp_log_order_unrefine
where 
convert_timezone('$TIMEZONE', server_time)::date=convert_timezone('kst',sysdate)::date-1;
--@end



--@drop-tmp_session_id_view
drop table if exists tmp_session_id_view;
--@end
--@create-tmp_session_id_view
CREATE TABLE tmp_session_id_view as
select
	session_id 
	,max(device) device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,count(distinct rc_code) rc_code_num
	,max(case when rc_rank = 1 and rc_code is not null then rc_code end) as first_rc_code
from tb_log_view_report
group by 1;
--@end

--@drop-tmp_session_id_order
drop table if exists tmp_session_id_order;
--@end
--@create-tmp_session_id_order
CREATE TABLE tmp_session_id_order as
select
	
	a.session_id session_id
	--VIEW
	,max(a.device) device
	,max(a.view) as view
	,max(a.reco_view) reco_view
	,max(first_rc_code) first_rc_code

	--ORDER
	,sum(b.price * b.quantity) revenue
	,sum(case when a.reco_view > 0 then b.price * b.quantity end) reco_revenue
	,sum(case when a.view > 0 and a.reco_view = 0 then b.price * b.quantity end) no_reco_revenue
	,sum(case when a.session_id is null then b.price * b.quantity end) no_click_revenue
	
	,sum(b.quantity) quantity
	,sum(case when a.reco_view > 0 then b.quantity end) reco_quantity
	,sum(case when a.reco_view = 0 then b.quantity end) no_reco_quantity
	
	,count(distinct b.item_id) item_count
	,count(distinct case when a.reco_view > 0 then b.item_id end) reco_item_count
	,count(distinct case when a.reco_view = 0 then b.item_id end) no_reco_item_count
	
	,count(distinct b.order_id) frequency
	,count(distinct case when a.reco_view > 0 then b.order_id end) reco_frequency
	,count(distinct case when a.reco_view = 0 then b.order_id end) no_reco_frequency

from tmp_session_id_view a
left join tb_log_order_report b on a.session_id = b.session_id
group by 1;
--@end
