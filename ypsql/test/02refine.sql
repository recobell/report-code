--refine


--@drop-tb_product
drop table if exists tb_product;
--@end

--@create-tb_product:export
create table tb_product as
	select
		item_id,
		max(item_name) item_name,
		max(item_image) item_image,
		max(item_url) item_url,
		max(original_price) original_price,
		--max(sale_price) sale_price,
		(case when max(sale_price) = 0 then max(original_price) else max(sale_price) end) as sale_price,
		max(COALESCE(category3, category2, category1, '')) category,
		coalesce(max(category1), '') category1,
		coalesce(max(category2), '') category2,
		coalesce(max(category3), '') category3,
		coalesce(max(category4), '') category4,
		coalesce(max(category5), '') category5,
		
		--max(brand_id) brand_id,
		--max(brand_name) brand_name,
		
		min(reg_date) reg_date,
		min(update_date) update_date,
		min(expire_date) expire_date,
		min(stock) stock,
		min(state) state,
		max(description) description, 
		max(extra_image) extra_image,
		max(locale) locale
	from
		tmp_product 
	where
		original_price is not null and original_price > 0 --and sale_price > 0
	group by item_id;
--@end




