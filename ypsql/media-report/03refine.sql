--#var $timezone = springproperty(recobell.site.timezone)
--#var $NOW = datenow(UTC)
--#var $date = datesub($NOW, 1, days)
--#var $DATE_STR = dateformat($date, yyyy-MM-dd)

--@drop-tb_log_view_report
drop table if exists tb_log_view_report;
--@end

--@create-tb_log_view_report
create table tb_log_view_report as
select 
  server_time
  ,device
  ,pc_id
  ,session_id
  ,rc_code
  ,item_id
  ,search_term
  ,row_number() over (partition by convert_timezone('$timezone', server_time)::date,session_id order by (case when rc_code is not null then 0 else 1 end),server_time) rc_rank
from tmp_log_view_unrefine
where convert_timezone('$timezone',server_time)::date = convert_timezone('kst',sysdate)::date-1;
--@end

--@drop-tmp_session_id_view
drop table if exists tmp_session_id_view;
--@end
--@create-tmp_session_id_view
CREATE TABLE tmp_session_id_view as
select
	session_id 
	,max(device) device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,count(distinct rc_code) rc_code_num
	,max(case when rc_rank = 1 and rc_code is not null then rc_code end) as first_rc_code
from tb_log_view_report
group by 1;
--@end
