
--@drop-tb_media_index_1
drop table if exists tb_media_index_1;
--@end
--@create-tb_media_index_1
CREATE TABLE tb_media_index_1 as
select 
  device
  ,case when reco_view>0 then 2 else 1 end as is_rec
  ,case when rc_code_num = 0 then 'na' when rc_code_num=1 then first_rc_code else 'multiple_rc' end as rc_code
  ,count(*) session_count
  ,sum(view) total_session_pv_count
from tmp_session_id_view 
group by 1,2,3;
--@end

--@drop-tb_media_index_2
drop table if exists tb_media_index_2;
--@end
--@create-tb_media_index_2
CREATE TABLE tb_media_index_2 as
select 
  device
  ,case when rc_code is not null then 2 else 1 end as is_rec
  ,case when rc_code is not null then rc_code else 'na' end rc_code
  ,count(*) pv_count
from tb_log_view_report 
group by 1,2,3;
--@end


--@drop-tb_media_index
drop table if exists tb_media_index;
--@end
--@create-tb_media_index
CREATE TABLE tb_media_index as
select 
  a.device
  ,a.is_rec
  ,a.rc_code
  ,b.pv_count
  ,a.session_count
  ,a.total_session_pv_count
from tb_media_index_1 a, tb_media_index_2 b
where a.device = b.device and a.is_rec=b.is_rec and a.rc_code = b.rc_code;
--@end
