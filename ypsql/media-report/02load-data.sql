--#var $AWS_ACCESS_KEY_ID = springproperty(aws.access-key-id)
--#var $AWS_SECRET_ACCESS_KEY = springproperty(aws.secret-access-key)
--#var $CUID = springproperty(recobell.site.cuid)
--#var $NOW = datenow(UTC)

--@copy-tmp_log_view_unrefine
--#foreach $i in (1...48)
--#var $date = datesub($NOW, $i, hours)
--#var $DATE_STR = dateformat($date, yyyy/MM/dd)
--#var $TIME_STR = dateformat($date, yyyyMMddHH)
copy tmp_log_view_unrefine
from 's3://rb-logs-apne1-rep-use1/$CUID/view/$DATE_STR/view_$TIME_STR.json.gz'
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
FORMAT AS JSON 's3://rb-logic-rec-conf-apne1-rep-use1/media/view.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto';
--@end
