--@drop-tb_uv_daily
drop table if exists tb_uv_daily;
--@end
--@create-tb_uv_daily
CREATE TABLE tb_uv_daily AS
select
	date
	,device
	,count(distinct pc_id) uv
	,count(distinct case when reco_view > 0 then pc_id end) reco_uv
	,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
	,round( count(distinct case when reco_view > 0 then pc_id end) :: real / count(distinct pc_id) :: real , 4) * 100 reco_ratio
from tmp_pc_id_view_daily
group by 1,2;
--@end

--@drop-tb_uv_monthly
drop table if exists tb_uv_monthly;
--@end
--@create-tb_uv_monthly
CREATE TABLE tb_uv_monthly AS
select
	
	device
	,count(distinct pc_id) uv
	,count(distinct case when reco_view > 0 then pc_id end) reco_uv
	,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
	,round( count(distinct case when reco_view > 0 then pc_id end) :: real / count(distinct pc_id) :: real , 4) * 100 reco_ratio
from tmp_pc_id_view_daily
group by 1;
--@end


--@drop-tb_visit_daily
drop table if exists tb_visit_daily;
--@end

--@create-tb_visit_daily
CREATE TABLE tb_visit_daily AS
select
	date
	,device
	,count(distinct session_id) visit
	,count(distinct case when reco_view > 0 then session_id end) reco_visit  
	,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,round(count(distinct case when reco_view > 0 then session_id end) :: float / count(distinct session_id) :: float , 4) reco_ratio
from tmp_session_id_view_daily
group by 1,2;
--@end

--@drop-tb_visit_monthly
drop table if exists tb_visit_monthly;
--@end

--@create-tb_visit_monthly
CREATE TABLE tb_visit_monthly AS
select
	device
	,count(distinct session_id) visit
	,count(distinct case when reco_view > 0 then session_id end) reco_visit  
	,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,round(count(distinct case when reco_view > 0 then session_id end) :: float / count(distinct session_id) :: float , 4) reco_ratio
from tmp_session_id_view_daily
group by 1;
--@end

--@drop-tb_pv_daily
drop table if exists tb_pv_daily;
--@end
--@create-tb_pv_daily
CREATE TABLE tb_pv_daily AS
select
	date
	,device
	,sum(view) pv
	,sum(case when reco_view > 0 then view else 0 end) reco_pv
	,sum(case when reco_view = 0 then view else 0 end) no_reco_pv
	,round( sum(case when reco_view > 0 then view else 0 end) :: float / sum(view) :: float , 4) reco_ratio
	
from tmp_session_id_view_daily
group by 1,2;
--@end

--@drop-tb_pv_monthly
drop table if exists tb_pv_monthly;
--@end
--@create-tb_pv_monthly
CREATE TABLE tb_pv_monthly AS
select
	device
	,sum(view) pv
	,sum(case when reco_view > 0 then view else 0 end) reco_pv
	,sum(case when reco_view = 0 then view else 0 end) no_reco_pv
	,round( sum(case when reco_view > 0 then view else 0 end) :: float / sum(view) :: float , 4) reco_ratio
	
from tmp_session_id_view_daily
group by 1;
--@end

--@drop-tb_frequency_daily
drop table if exists tb_frequency_daily;
--@end
--@create-tb_frequency_daily
CREATE TABLE tb_frequency_daily AS
select
	date
	,device
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency 
	,sum(no_reco_frequency) no_reco_frequency 
	,round( sum(reco_frequency) :: float / sum(frequency) :: float , 4) reco_ratio
    
from tmp_session_id_order_daily A
group by 1,2;
--@end

--@drop-tb_frequency_monthly
drop table if exists tb_frequency_monthly;
--@end
--@create-tb_frequency_monthly
CREATE TABLE tb_frequency_monthly AS
select
	device
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency 
	,sum(no_reco_frequency) no_reco_frequency 
	,round( sum(reco_frequency) :: float / sum(frequency) :: float , 4) reco_ratio
    
from tmp_session_id_order_daily A
group by 1;
--@end

--@drop-tb_revenue_daily
drop table if exists tb_revenue_daily;
--@end
--@create-tb_revenue_daily
CREATE TABLE tb_revenue_daily AS
select
	date
	,device
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
	,sum(no_reco_revenue) no_reco_revenue 
    ,round( sum(reco_revenue) :: float / sum(revenue) :: float, 4) reco_ratio
from tmp_session_id_order_daily A
group by 1,2;
--@end


--@drop-tb_revenue_monthly
drop table if exists tb_revenue_monthly;
--@end
--@create-tb_revenue_monthly
CREATE TABLE tb_revenue_monthly AS
select
	device
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
	,sum(no_reco_revenue) no_reco_revenue 
    ,round( sum(reco_revenue) :: float / sum(revenue) :: float, 4) reco_ratio
from tmp_session_id_order_daily A
group by 1;
--@end


--@drop-tb_ct_daily
drop table if exists tb_ct_daily;
--@end
--@create-tb_ct_daily
CREATE TABLE tb_ct_daily AS
select
	date
	,device
	,sum(revenue) revenue
	,count(distinct case when revenue>0 then session_id end) session_count
	,sum(revenue)/count(distinct session_id) ct
	,sum(case when reco_revenue>0 then revenue end) reco_revenue
	,count(distinct case when reco_revenue>0 then session_id end) reco_session_count
    ,sum(case when reco_revenue>0 then revenue end)/count(distinct case when reco_revenue>0 then session_id end) reco_ct
    ,sum(case when reco_revenue=0 then revenue end) no_reco_revenue
    ,count(distinct case when reco_revenue=0 and revenue>0 then session_id end) no_reco_session_count
    ,sum(case when reco_revenue=0 and revenue>0  then revenue end)/count(distinct case when reco_revenue=0 and revenue>0  then session_id end) no_reco_ct
from tmp_session_id_order_daily
group by 1,2;
--@end


--@drop-tb_ct_monthly
drop table if exists tb_ct_monthly;
--@end
--@create-tb_ct_monthly
CREATE TABLE tb_ct_monthly AS
select
	device
	,sum(revenue) revenue
	,count(distinct case when revenue>0 then session_id end) session_count
	,sum(revenue)/count(distinct session_id) ct
	,sum(case when reco_revenue>0 then revenue end) reco_revenue
	,count(distinct case when reco_revenue>0 then session_id end) reco_session_count
    ,sum(case when reco_revenue>0 then revenue end)/count(distinct case when reco_revenue>0 then session_id end) reco_ct
    ,sum(case when reco_revenue=0 then revenue end) no_reco_revenue
    ,count(distinct case when reco_revenue=0 and revenue>0 then session_id end) no_reco_session_count
    ,sum(case when reco_revenue=0 and revenue>0  then revenue end)/count(distinct case when reco_revenue=0 and revenue>0  then session_id end) no_reco_ct
from tmp_session_id_order_daily
group by 1;
--@end


--@drop-tb_item_daily
drop table if exists tb_item_daily;
--@end
--@create-tb_item_daily
CREATE TABLE tb_item_daily AS
select
	date
	,device
	,sum(item_count) item_count
	,sum(frequency) order_count
	,sum(item_count)/sum(frequency) avg_item_count
	,sum(case when reco_revenue>0 then item_count end) reco_item_count
	,sum(case when reco_revenue>0 then frequency end) reco_order_count
    ,sum(case when reco_revenue>0 then item_count end)/sum(case when reco_revenue>0 then frequency end) reco_avg_item_count
    ,sum(case when reco_revenue=0 then item_count end) no_reco_item_count
    ,sum(case when reco_revenue=0 then frequency end) no_reco_order_count
    ,sum(case when reco_revenue=0 then item_count end)/sum(case when reco_revenue=0 then frequency end) no_reco_avg_item_count
from tmp_session_id_order_daily
group by 1,2;
--@end



--@drop-tb_item_monthly
drop table if exists tb_item_monthly;
--@end
--@create-tb_item_monthly
CREATE TABLE tb_item_daily AS
select
	device
	,sum(item_count) item_count
	,sum(frequency) order_count
	,sum(item_count)/sum(frequency) avg_item_count
	,sum(case when reco_revenue>0 then item_count end) reco_item_count
	,sum(case when reco_revenue>0 then frequency end) reco_order_count
    ,sum(case when reco_revenue>0 then item_count end)/sum(case when reco_revenue>0 then frequency end) reco_avg_item_count
    ,sum(case when reco_revenue=0 then item_count end) no_reco_item_count
    ,sum(case when reco_revenue=0 then frequency end) no_reco_order_count
    ,sum(case when reco_revenue=0 then item_count end)/sum(case when reco_revenue=0 then frequency end) no_reco_avg_item_count
from tmp_session_id_order_daily
group by 1;
--@end


--@drop-tb_click_daily
drop table if exists tb_click_daily;
--@end
--@create-tb_click_daily
CREATE TABLE tb_click_daily AS 
SELECT 
	date
	,device
	,sum(view) as view
	,count(distinct session_id) session_count
	,sum(view)/count(distinct session_id) click
	,sum(case when reco_view>0 then view end) reco_view
	,count(distinct case when reco_view>0 then session_id end) reco_session_count
    ,sum(case when reco_view>0 then view end)/count(distinct case when reco_view>0 then session_id end) reco_click
    ,sum(case when reco_view=0 then view end) no_reco_view
    ,count(distinct case when reco_view=0 then session_id end) no_reco_session_count
    ,sum(case when reco_view=0 then view end)/count(distinct case when reco_view=0 then session_id end) no_reco_click
FROM
    tmp_session_id_view_daily
group by 1,2;
--@end


--@drop-tb_click_monthly
drop table if exists tb_click_monthly;
--@end
--@create-tb_click_monthly
CREATE TABLE tb_click_monthly AS 
SELECT 
	device
	,sum(view) as view
	,count(distinct session_id) session_count
	,sum(view)/count(distinct session_id) click
	,sum(case when reco_view>0 then view end) reco_view
	,count(distinct case when reco_view>0 then session_id end) reco_session_count
    ,sum(case when reco_view>0 then view end)/count(distinct case when reco_view>0 then session_id end) reco_click
    ,sum(case when reco_view=0 then view end) no_reco_view
    ,count(distinct case when reco_view=0 then session_id end) no_reco_session_count
    ,sum(case when reco_view=0 then view end)/count(distinct case when reco_view=0 then session_id end) no_reco_click
FROM
    tmp_session_id_view_daily
group by 1;
--@end

--@drop-tb_cvr_daily
drop table if exists tb_cvr_daily;
--@end
--@create-tb_cvr_daily
CREATE TABLE tb_cvr_daily AS 
select 
	date
	,device
	,count(distinct case when revenue <> 0 then session_id end) cv_session_count
	,count(distinct session_id) session_count
	,count(distinct case when revenue <> 0 then session_id end)/count(distinct session_id) cvr
	,count(distinct case when reco_revenue>0 then session_id end) reco_cv_session_count
	,count(distinct case when reco_view>0 then session_id end) reco_session_count
	,count(distinct case when reco_revenue>0 then session_id end)/count(distinct case when reco_view>0 then session_id end) reco_cvr
	,count(distinct case when reco_revenue=0 and revenue > 0 then session_id end) no_reco_cv_session_count
	,count(distinct case when reco_view=0 and revenue > 0 then session_id end) no_reco_session_count
	,count(distinct case when reco_revenue=0 and revenue > 0 then session_id end)/count(distinct case when reco_view=0 and revenue > 0 then session_id end) no_reco_cvr
from tmp_session_id_order_daily
group by 1,2;
--@end


--@drop-tb_cvr_monthly
drop table if exists tb_cvr_monthly;
--@end
--@create-tb_cvr_monthly
CREATE TABLE tb_cvr_monthly AS 
select 
	device
	,count(distinct case when revenue <> 0 then session_id end) cv_session_count
	,count(distinct session_id) session_count
	,count(distinct case when revenue <> 0 then session_id end)/count(distinct session_id) cvr
	,count(distinct case when reco_revenue>0 then session_id end) reco_cv_session_count
	,count(distinct case when reco_view>0 then session_id end) reco_session_count
	,count(distinct case when reco_revenue>0 then session_id end)/count(distinct case when reco_view>0 then session_id end) reco_cvr
	,count(distinct case when reco_revenue=0 and revenue > 0 then session_id end) no_reco_cv_session_count
	,count(distinct case when reco_view=0 and revenue > 0 then session_id end) no_reco_session_count
	,count(distinct case when reco_revenue=0 and revenue > 0 then session_id end)/count(distinct case when reco_view=0 and revenue > 0 then session_id end) no_reco_cvr
from tmp_session_id_order_daily
group by 1;
--@end
