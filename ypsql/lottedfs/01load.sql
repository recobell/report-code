--#var $AWS_ACCESS_KEY_ID = springproperty(aws.access-key-id)
--#var $AWS_SECRET_ACCESS_KEY = springproperty(aws.secret-access-key)
--#var $CUID = springproperty(recobell.site.cuid)
--#var $REPORTYEAR = springproperty(recobell.site.report-year)
--#var $REPORTMONTH = springproperty(recobell.site.report-month)
--#var $NOW = datenow(UTC)

--@drop-tmp_log_view_unrefine
drop table if exists tmp_log_view_unrefine;
--@end

--@create-tmp_log_view_unrefine
CREATE TABLE tmp_log_view_unrefine (
	cuid varchar(200),
	type varchar(200),
	server_time timestamp NULL,
	ip_address varchar(200),
	user_agent varchar(2000),
	api_version varchar(200),
	domain varchar(200),
	device varchar(200),
	pc_id varchar(200),
	session_id varchar(200),
	ad_id varchar(200),
	push_id varchar(200),
	gc_id varchar(200),
	user_id varchar(1000),
	ab_group varchar(200),
	rc_code varchar(200),
	ad_code varchar(200),
	locale varchar(200),
	item_id varchar(200),
	search_term varchar(200),
	plan_id varchar(200)
);
--@end


--@copy-tmp_log_view_unrefine
copy tmp_log_view_unrefine
from 's3://rb-logs-apne1-rep-use1/$CUID/view/$REPORTYEAR/$REPORTMONTH'
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
FORMAT AS JSON 's3://rb-logic-rec-conf-apne1-rep-use1/commerce/view.jsonpaths'
compupdate ON STATUPDATE OFF maxerror 100000 timeformat 'auto';
--@end


--@drop-tmp_log_order_unrefine
drop table if exists tmp_log_order_unrefine;
--@end

--@create-tmp_log_order_unrefine
CREATE TABLE tmp_log_order_unrefine (
	cuid varchar(200),
	type varchar(200),
	server_time timestamp NULL,
	ip_address varchar(200),
	user_agent varchar(2000),
	api_version varchar(200),
	domain varchar(200),
	device varchar(200),
	pc_id varchar(200),
	session_id varchar(200),
	ad_id varchar(200),
	push_id varchar(200),
	gc_id varchar(200),
	user_id varchar(200),
	ab_group varchar(200),
	rc_code varchar(200),
	ad_code varchar(200),
	locale varchar(200),
	item_id varchar(200),
	search_term varchar(200),
	order_id varchar(200),
	order_price float4,
	price float4,
	quantity int8
);
--@end

--@copy-tmp_log_order_unrefine
copy tmp_log_order_unrefine
from 's3://rb-logs-apne1-rep-use1/$CUID/order/$REPORTYEAR/$REPORTMONTH' 
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
FORMAT AS JSON 's3://rb-logic-rec-conf-apne1-rep-use1/commerce/order.jsonpaths'
compupdate ON STATUPDATE OFF maxerror 100000 timeformat 'auto';
--@end

--@drop-tmp_product
drop table if exists tmp_product;
--@end

--@create-tmp_product
create table tmp_product (
cuid varchar(200),
type varchar(200),
item_id varchar(200),
item_name varchar(2000),
item_image varchar(200),
item_url varchar(2000),
original_price real,
sale_price real,
category1 varchar(200),
category2 varchar(200),
category3 varchar(200),
category4 varchar(200),
category5 varchar(200),
reg_date varchar(200),
update_date timestamp,
expire_date timestamp,
stock bigint,
state varchar(200),
description varchar(200),
extra_image varchar(200),
locale varchar(200)
);
--@end

--@copy-tmp_product
copy tmp_product
from 's3://rb-logs-apne1-rep-use1/$CUID/product/$REPORTYEAR/$REPORTMONTH'
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP FORMAT AS JSON 's3://rb-logic-rec-conf-apne1-rep-use1/commerce/product.jsonpaths'
compupdate ON STATUPDATE OFF maxerror 100000 timeformat 'auto';
--@end