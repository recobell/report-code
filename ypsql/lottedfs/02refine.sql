--refine
--#var $REPORTYEAR = springproperty(recobell.site.report-year)
--#var $REPORTMONTH = springproperty(recobell.site.report-month)

--@drop-tb_log_view_report
drop table if exists tb_log_view_report;
--@end

--@create-tb_log_view_report
CREATE TABLE tb_log_view_report as
select
	server_time,
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	ad_code,
	item_id, 
	search_term
	,row_number() over (partition by convert_timezone('KST', server_time)::date,session_id order by (case when rc_code is not null then 0 else 1 end),server_time) rc_rank
from 
tmp_log_view_unrefine
where 
extract(month from convert_timezone('KST', server_time))=$REPORTMONTH
and extract(year from convert_timezone('KST', server_time))=$REPORTYEAR;
--@end

--@drop-tb_log_order_report
drop table if exists tb_log_order_report;
--@end
--@create-tb_log_order_report
CREATE TABLE tb_log_order_report as
select
	server_time,
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term, 
	order_id, 
	order_price,
	price, 
	quantity
from 
tmp_log_order_unrefine
where 
extract(month from convert_timezone('KST', server_time))=$REPORTMONTH
and extract(year from convert_timezone('KST', server_time))=$REPORTYEAR;
--@end

--@drop-tmp_pc_id_view_daily
drop table if exists tmp_pc_id_view_daily;
--@end
--@create-tmp_pc_id_view_daily
CREATE TABLE tmp_pc_id_view_daily as
select 
	convert_timezone('KST', server_time)::date as date
	,pc_id 
	,max(device) device
	,count(*) as view
	,sum(case when rc_code is not null  then 1 else 0 end) as reco_view
	,sum(case when rc_code is null  then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by 1,2;
--@end
--@drop-tmp_session_id_view_daily
drop table if exists tmp_session_id_view_daily;
--@end
--@create-tmp_session_id_view_daily
CREATE TABLE tmp_session_id_view_daily as
select
	convert_timezone('KST', server_time)::date as date
	,session_id 
	,max(device) device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
	,max(case when rc_rank = 1 and rc_code is not null then rc_code end) as first_rc_code
from tb_log_view_report
group by 1,2;
--@end

--@drop-tmp_session_id_order_daily
drop table if exists tmp_session_id_order_daily;
--@end
--@create-tmp_session_id_order_daily
CREATE TABLE tmp_session_id_order_daily as
select
	a.date date
	,a.session_id session_id
	--VIEW
	,max(a.device) device
	,max(a.view) as view
	,max(a.reco_view) reco_view
	,max(a.no_reco_view) no_reco_view
	,max(first_rc_code) first_rc_code

	--ORDER
	,sum(b.price * b.quantity) revenue
	,sum(case when a.reco_view > 0 then b.price * b.quantity end) reco_revenue
	,sum(case when a.view > 0 and a.reco_view = 0 then b.price * b.quantity end) no_reco_revenue
	,sum(case when a.session_id is null then b.price * b.quantity end) no_click_revenue
	
	,sum(b.quantity) quantity
	,sum(case when a.reco_view > 0 then b.quantity end) reco_quantity
	,sum(case when a.reco_view = 0 then b.quantity end) no_reco_quantity
	
	,count(distinct b.item_id) item_count
	,count(distinct case when a.reco_view > 0 then b.item_id end) reco_item_count
	,count(distinct case when a.reco_view = 0 then b.item_id end) no_reco_item_count
	
	,count(distinct b.order_id) frequency
	,count(distinct case when a.reco_view > 0 then b.order_id end) reco_frequency
	,count(distinct case when a.reco_view = 0 then b.order_id end) no_reco_frequency

from tmp_session_id_view_daily a
left join tb_log_order_report b on a.session_id = b.session_id and a.date = convert_timezone('KST', b.server_time)::date
group by 1,2;
--@end

