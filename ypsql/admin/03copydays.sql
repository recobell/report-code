--#var $AWS_ACCESS_KEY_ID = springproperty(aws.access-key)
--#var $AWS_SECRET_ACCESS_KEY = springproperty(aws.secret-access-key)
--#var $CUID = springproperty(recobell.rec.cuid)
--#var $NOW = datenow(UTC)

--@copy-tmp_product_info
--#foreach $i in (0...5)
--#var $date = dateadd($NOW, $i, days)
--#var $DATE_STR = dateformat($date, 2016/02/dd)
copy tmp_product_info
from 's3://rb-logs-apne1/$CUID/product/$DATE_STR'
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';
--@end

--@copy-tb_log_view
--#foreach $i in (0...5)
--#var $date = dateadd($NOW, $i, days)
--#var $DATE_STR = dateformat($date, 2016/02/dd)
copy tb_log_view
from 's3://rb-logs-apne1/$CUID/view/$DATE_STR'
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data_lotte.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';
--@end

--@copy-tb_log_order
--#foreach $i in (0...5)
--#var $date = dateadd($NOW, $i, days)
--#var $DATE_STR = dateformat($date, 2016/02/dd)
copy tb_log_order
from 's3://rb-logs-apne1-merge/$CUID/order/$DATE_STR'
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';
--@end

--@copy-tb_log_search
--#foreach $i in (0...5)
--#var $date = dateadd($NOW, $i, days)
--#var $DATE_STR = dateformat($date, 2016/02/dd)
copy tb_log_search
from 's3://rb-logs-apne1-merge/$CUID/search/$DATE_STR'
CREDENTIALS 'aws_access_key_id=$AWS_ACCESS_KEY_ID;aws_secret_access_key=$AWS_SECRET_ACCESS_KEY'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';
--@end