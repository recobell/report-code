
--#######
--# 집계 #
--#######
--to get the beauty point merge from mapping table to offline customer table (a.ucstmid = beauty point)
drop table if exists tb_log_offline_cst_01;
create table tb_log_offline_cst_01 as 
select 
	b.untycstmid,
	b.nationfl of_nationfl,
	b.gender of_gender,
	b.gnrt of_gnrt,
	b.CSTMID,
	case when a.ucstmid is not null then 'Y' else 'N' end of_ucstmid,
	b.homezip1 of_homezip1,
	b.homezip2 of_homezip2,
	b.addr1 of_addr1,
	b.emailsendfl of_emailsendfl,
	b.smssendfl of_smssendfl,
	b.dmsendfl of_dmsendfl,
	b.cstmusefl of_cstmusefl,
	b.mbrfl of_mbrfl,
	b.apempfl of_apempfl,
	b.delfl of_delfl,
	b.prtnrid of_prtnrid,
	b.fnlpurdate of_fnlpurdate,
	b.fnlmlgdate of_fnlmlgdate,
	b.joindate of_joindate,
	b.cstmlvl of_cstmlvl,
	b.bday of_bday
from 
	tb_log_offline_cst b
	left outer join
	tb_log_offline_mapping a on b.cstmid = pcstmid;

--to get the beauty point from the online user table (a.ucstmid = beauty point)
drop table if exists tb_log_user_01;
create table tb_log_user_01 as 
select 
	a.pc_id,
	a.user_id,
	a.user_id on_user_id,
	a.gender on_gender,
	a.birth_date,
	case when a.ucstmid is not null then 'Y' else 'N' end on_ucstmid,
	(extract(year from sysdate) - extract(year from a.birth_date)) + 1 on_gnrt,
	a.class on_class,
	a.signup_date on_signup_date,
	a.order_amount on_order_amount
from 
	tb_log_user a;

--merging online and offline user table and if the beauty point is 'Y' then ucstmid = 'Y'
drop table if exists tmp_customer_profiling_jensen_s1;
create table tmp_customer_profiling_jensen_s1 as 
select	
	a.pc_id,
	a.user_id,
	a.on_user_id,
	a.on_gender,
	a.birth_date,
	a.on_ucstmid,
	b.of_ucstmid,
	(case when a.on_ucstmid = 'Y' and b.of_ucstmid = 'Y' then 'Y'
		when a.on_ucstmid = 'Y' and b.of_ucstmid = 'N' then 'Y' 
		when a.on_ucstmid = 'N' and b.of_ucstmid = 'Y' then 'Y' 
	else 'N' end) ucstmid,	
	a.on_gnrt,
	a.on_class,
	a.on_signup_date,
	a.on_order_amount,
	b.untycstmid,
	b.CSTMID,
	b.of_nationfl,
	b.of_gender,
	b.of_gnrt,
	b.of_homezip1,
	b.of_homezip2,
	b.of_addr1,
	b.of_emailsendfl,
	b.of_smssendfl,
	b.of_dmsendfl,
	b.of_cstmusefl,
	b.of_mbrfl,
	b.of_apempfl,
	b.of_delfl,
	b.of_prtnrid,
	b.of_fnlpurdate,
	b.of_fnlmlgdate,
	b.of_joindate,
	b.of_cstmlvl,
	b.of_bday,
	row_number() over (order by on_signup_date) as seq
from 
	tb_log_user_01 a 	
	full outer join	
	tb_log_offline_cst_01 b on a.PC_ID = b.UNTYCSTMID;
--where a.pc_id is null and b.CSTMID is not null and a.user_id is null --case 1
--where a.pc_id is not null and b.CSTMID is not null and a.user_id is not null --case 2
--where a.pc_id is null and b.CSTMID is not null and a.user_id is null --case 3

--tb_log_order
drop table if exists tmp_customer_profiling_s00_jensen;
create table tmp_customer_profiling_s00_jensen as 
select 
	pc_id,
	user_id,
	count(distinct order_id) as on_ord_cnt,
	count(distinct case when device = 'PW' then order_id end) on_ord_cnt_p,
	count(distinct case when device <> 'PW' then order_id end) on_ord_cnt_m,
	count(distinct case when oncp_code is not null then order_id end) on_cu_on_cnt,
	count(distinct case when ofcp_code is not null then order_id end) on_cu_of_cnt,
	count(distinct oncp_code) on_cu_on_unicnt,
	count(distinct ofcp_code) on_cu_of_unicnt,
	sum(quantity) on_quantity,
	sum(order_price) on_ord_amt,
	count(distinct sap_code) on_item_cd_unicnt,
	min(server_time) on_f_sale_dt,
	max(server_time) on_l_sale_dt
from
	tb_log_order
group by pc_id,user_id;

--tb_log_offline_sales
drop table if exists tmp_customer_profiling_s01_jensen;
create table tmp_customer_profiling_s01_jensen as 
select 
	cust_id,
	count(case when coupon_cd is not null then 1 else 0 end) tmp_of_COUPON_CD_order_cnt,
	count(case when campaign_cd is not null then 1 else 0 end) tmp_of_CAMPAIGN_CD_order_cnt
-- 	case when coupon_cd is not null then count(distinct STOR_CD||SALE_DT||POS_NO||BILL_NO) end tmp_of_COUPON_CD_order_cnt,
-- 	case when campaign_cd is not null then count(distinct STOR_CD||SALE_DT||POS_NO||BILL_NO) end tmp_of_campaign_cd_order_cnt
from
	tb_log_offline_sales 
group by 1;

drop table if exists tmp_customer_profiling_s02_jensen;
create table tmp_customer_profiling_s02_jensen as
select
	distinct c.cust_id,
	count(distinct c.STOR_CD||c.SALE_DT||c.POS_NO||c.BILL_NO) of_ord_cnt,
	sum(c.sale_qty) of_quantity,
	sum(c.sale_amt) of_SALE_AMT,
	sum(c.dc_amt) of_DC_AMT,
	sum(c.enr_amt) of_ENR_AMT,
	sum(c.grd_amt) of_ord_amt,
	count(distinct c.stor_cd) of_STOR_CD_unicnt,
	count(distinct case when c.coupon_cd>0 then c.STOR_CD||c.SALE_DT||c.POS_NO||c.BILL_NO end)  of_COUPON_CD_order_cnt,
	count(distinct case when c.campaign_cd>0 then c.STOR_CD||c.SALE_DT||c.POS_NO||c.BILL_NO end)  of_CAMPAIGN_CD_order_cnt,
	--sum(case when c.coupon_cd >0 then count(distinct c.STOR_CD||c.SALE_DT||c.POS_NO||c.BILL_NO) end) of_COUPON_CD_order_cnt,
	--sum(case when c.campaign_cd >0 then count(distinct c.STOR_CD||c.SALE_DT||c.POS_NO||c.BILL_NO) end) of_campaign_cd_order_cnt,
	count(distinct c.coupon_cd) of_COUPON_CD_unicnt,
	count(distinct c.campaign_cd) of_CAMPAIGN_CD_cnt,
	count(distinct c.item_cd) of_ITEM_CD_unicnt,
	min(c.sale_dt) of_f_SALE_DT,
	max(c.sale_dt) of_l_SALE_DT
from
	tb_log_offline_sales c
	full outer join
	tmp_customer_profiling_s01_jensen d on c.cust_id=d.cust_id
where substring(item_cd from 1 for 5) in ('61111','61112','17001')
group by c.cust_id;

--merge customer with online order and offline_sales
drop table if exists tb_customer_profiling_jensen;
create table tb_customer_profiling_jensen as 
select
	a.pc_id,
	a.user_id,
	a.seq,
	a.ucstmid,
	a.CSTMID,
	a.on_user_id,
	a.on_gender,
	a.birth_date,
	a.on_gnrt,
	a.on_class,
	a.untycstmid,
	a.of_nationfl,
	a.of_gender,
	a.of_gnrt,
	a.of_homezip1,
	a.of_homezip2,
	a.of_addr1,
	a.of_emailsendfl,
	a.of_smssendfl,
	a.of_dmsendfl,
	a.of_cstmusefl,
	a.of_mbrfl,
	a.of_apempfl,
	a.of_delfl,
	a.of_prtnrid,
	a.of_fnlpurdate,
	a.of_fnlmlgdate,
	a.of_joindate,
	a.of_cstmlvl,
	a.of_bday,
	a.of_ucstmid,
	a.on_signup_date,
	a.on_order_amount,
	b.on_ord_cnt,
	b.on_ord_cnt_p,
	b.on_ord_cnt_m,
	b.on_cu_on_cnt,
	b.on_cu_of_cnt,
	b.on_cu_on_unicnt,
	b.on_cu_of_unicnt,
	b.on_quantity,
	b.on_ord_amt,
	b.on_item_cd_unicnt,
	b.on_f_sale_dt,
	b.on_l_sale_dt,
	c.of_ord_cnt,
	c.of_quantity,
	c.of_SALE_AMT,
	c.of_DC_AMT,
	c.of_ENR_AMT,
	c.of_ord_amt,
	c.of_STOR_CD_unicnt,
	c.of_COUPON_CD_order_cnt,
	c.of_campaign_cd_order_cnt,
	c.of_COUPON_CD_unicnt,
	c.of_CAMPAIGN_CD_cnt,
	c.of_ITEM_CD_unicnt,
	c.of_f_SALE_DT,
	c.of_l_SALE_DT
from
	tmp_customer_profiling_jensen_s1 a
	full outer join
	tmp_customer_profiling_s00_jensen b on a.user_id = b.user_id and a.pc_id =b.pc_id
	full outer join 
	tmp_customer_profiling_s02_jensen c on c.CUST_ID = a.CSTMID; 




--검증
--#####################################################################################################################
select * from tb_customer_profiling_jensen limit 100;
select count(*) from tb_customer_profiling_jensen;
select count(*) from tb_customer_profiling_jensen where on_ord_cnt is not null and of_ord_cnt is not null order by seq;
select count(*) from tmp_customer_profiling_jensen_s1;
select * from tb_log_user limit 10;
select count(*) from tb_log_user;
--#####################################################################################################################

