--공통
select count(*)
from    (select * from tb_log_user where pc_id is not null) a
	inner join 
	(select * from tb_log_offline_cst where UNTYCSTMID is not null b on a.pc_id = b.UNTYCSTMID 
where a.gender like 'F' or  a.gender like 'M';


--#########
--## 통합 ##
--#########

--user
drop table if exists tb_uni_customer_jensen;
create table tb_uni_customer_jensen as
select 
	a.order_amount,
	a.class,
	a.gender,
	a.user_id,
	a.pc_id,
	b.untycstmid,
	a.signup_date,
	b.gnrt,
	b.trg_chk,
	b.eaifl,
	b.ucstmid,
	b.webid,
	b.cstmlvl,
	b.hjsendfl,	
	b.mlgfl,
	b.prtnrid,
	b.delfl,
	b.apempfl,
	b.delmlgfl,
	b.mbrfl,
	b.cstmusefl,
	b.dmsendfl,
	b.smssendfl,
	b.emailsendfl,
	b.skintrblcd1,
	b.skintpcd,
	b.addr1,
	b.homezip2,
	b.homezip1,
	b.gender as gender_2,
	b.nationfl,
	b.routecd,
	b.cstmid,
	b.bday,
	b.joindate,
	b.fnlmlgdate,
	b.fnlpurdate	
from    (select * from tb_log_user) a
	full outer join 
	(select * from tb_log_offline_cst) b on a.pc_id = b.UNTYCSTMID;

drop table if exists tb_only_online_customer_jensen;
create table tb_only_online_customer_jensen as
select *
from tb_uni_customer_jensen
where cstmid is null;

drop table if exists tb_only_onoffline_customer_jensen;
create table tb_only_omni_customer_jensen as
select *
from tb_uni_customer_jensen
where pc_id = UNTYCSTMID;

drop table if exists tb_only_offline_customer_jensen;
create table tb_only_offline_customer_jensen as
select *
from tb_uni_customer_jensen
where cstmid is not null and pc_id is null;

--#########
--##count##
--#########
--total customer: 840173
--omni customer: 124418
--only online customer: 37677
--only offline customer: 678078
--select 840173-124418-37677 = 678078

-- Revenue ratio distinguish by class

drop table if exists tb_ratio_revenue_distinct_by_class;
create table tb_ratio_revenue_distinct_by_class as
select h.class, h.revenue, j.total_revenue, (h.revenue/j.total_revenue)as ratio
from
(select class, sum(order_amount) as revenue
from tb_uni_customer_jensen
group by 1) h,
(select sum(order_amount) as total_revenue from tb_uni_customer_jensen) j 
where h.class is not null
group by 1,2,3;


--define class online & offline
drop table if exists tb_ratio_revenue_distinct_by_class_s0;
create table tb_ratio_revenue_distinct_by_class_s0 as
select 
	order_amount,
	class as class_on,
	CSTMLVL,
	UCSTMID,
	(case when CSTMLVL = 501 then 'MEMBERS'
	when CSTMLVL = 503 then 'VVIP'
	else NULL end) as class_off
from
	tb_uni_customer_jensen;

drop table if exists tb_ratio_revenue_distinct_by_class_s1;
create table tb_ratio_revenue_distinct_by_class_s1 as
select 
	a.order_amount,
	a.class_on,	
	a.class_off,
	a.CSTMLVL,
	a.UCSTMID,
	b.CUST_ID,
	b.sale_amt	
from
	tb_ratio_revenue_distinct_by_class_s0 a
	right outer join
	tb_log_offline_sales b on a.UCSTMID=b.CUST_ID;

select a.class_on, sum(a.order_amount) as online_revenue, sum(a.sale_amt) as offline_revenue

select 
	*
from
tb_ratio_revenue_distinct_by_class_s1 a 
inner join
tb_ratio_revenue_distinct_by_class_s1 b on a.class_on = b.class_off
group by a.class_on limit 10


select * from tb_product_info limit 100;
select * from tb_only_online_customer_jensen limit 100;
select * from tb_uni_customer_jensen limit 100;
select * from tb_only_online_customer_jensen limit 100;
select * from tb_log_order limit 100;
select * from tb_log_offline_sales limit 100;


select count(*)
from tb_log_order
where user_id is not null;

select count(distinct user_id) from tb_log_user where pc_id is null group by user_id limit 10;
select * from tb_log_user where pc_id is null limit 10;
/*
How to relate product with customer? 
1. The pattern of buying for specific customers
2. Specifying the customers (3 categories)
3. 
*/

























