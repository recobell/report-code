drop table if exists tb_offline_sales_cust_hist;
create table tb_offline_sales_cust_hist as
select  a.CUST_ID 
       ,a.seq
       ,a.SALE_DT
       ,nvl(b.SALE_DT,a.SALE_DT) SALE_DT2
       ,extract(day from a.SALE_DT - nvl(b.SALE_DT,a.SALE_DT)) as SALE_DT_gap
       ,a.BILL_CNT
       ,sum(a.BILL_CNT) over (partition by a.CUST_ID order by a.CUST_ID, a.SALE_DT rows unbounded preceding) as BILL_CNT_C
       ,a.SALE_QTY
       ,sum(a.SALE_QTY) over (partition by a.CUST_ID order by a.CUST_ID, a.SALE_DT rows unbounded preceding) as SALE_QTY_C
       ,a.DC_AMT
       ,sum(a.DC_AMT) over (partition by a.CUST_ID order by a.CUST_ID, a.SALE_DT rows unbounded preceding) as DC_AMT_C
       ,a.GRD_AMT
       ,sum(a.GRD_AMT) over (partition by a.CUST_ID order by a.CUST_ID, a.SALE_DT rows unbounded preceding) as GRD_AMT_C
from (select  CUST_ID 
						 ,SALE_DT
						 ,rank() over(partition by CUST_ID order by SALE_DT) as SEQ
					   ,count(DISTINCT STOR_CD||SALE_DT||POS_NO||BILL_NO) as BILL_CNT
             ,sum(SALE_QTY) as SALE_QTY
             ,sum(DC_AMT + ENR_AMT) as DC_AMT
             ,sum(GRD_AMT) as GRD_AMT  
       from tb_log_offline_sales
      group by CUST_ID, SALE_DT) a left outer join
      (select CUST_ID 
             ,SALE_DT
				  	 ,rank() over(partition by CUST_ID order by SALE_DT) as SEQ
           from tb_log_offline_sales 
          group by CUST_ID, SALE_DT) b on
( a.CUST_ID = b.CUST_ID
  and a.SEQ-1 = b.SEQ);



select  a.CUST_ID 
       ,a.BILL_ID
       ,a.SEQ
       ,a.SALE_DT as SALE_DT
       ,nvl(b.SALE_DT,a.SALE_DT) as SALE_DT_BF
       ,extract(day from a.SALE_DT - nvl(b.SALE_DT,a.SALE_DT)) as SALE_DT_gap
       ,a.BILL_CNT
       ,sum(a.BILL_CNT) over (partition by a.CUST_ID order by a.CUST_ID, a.SALE_DT rows unbounded preceding) as BILL_CNT_C
       ,a.SALE_QTY
       ,sum(a.SALE_QTY) over (partition by a.CUST_ID order by a.CUST_ID, a.SALE_DT rows unbounded preceding) as SALE_QTY_C
       ,a.DC_AMT
       ,sum(a.DC_AMT) over (partition by a.CUST_ID order by a.CUST_ID, a.SALE_DT rows unbounded preceding) as DC_AMT_C
       ,a.GRD_AMT
       ,sum(a.GRD_AMT) over (partition by a.CUST_ID order by a.CUST_ID, a.SALE_DT rows unbounded preceding) as GRD_AMT_C
from (select  CUST_ID
             ,STOR_CD||SALE_DT||POS_NO||BILL_NO as BILL_ID
             ,SALE_DT
             ,rank() over(partition by CUST_ID order by SALE_DT, BILL_ID) as SEQ
             ,count(DISTINCT STOR_CD||SALE_DT||POS_NO||BILL_NO) as BILL_CNT
             ,sum(SALE_QTY) as SALE_QTY
             ,sum(DC_AMT + ENR_AMT) as DC_AMT
             ,sum(GRD_AMT) as GRD_AMT  
       from tb_log_offline_sales
      where substring(ITEM_CD from 1 for 5) in ('61111','61112','17001') 
      group by CUST_ID, BILL_ID, SALE_DT) a left outer join
      (select CUST_ID 
             ,STOR_CD||SALE_DT||POS_NO||BILL_NO as BILL_ID
             ,SALE_DT
             ,rank() over(partition by CUST_ID order by SALE_DT, BILL_ID) as SEQ
           from tb_log_offline_sales 
          where substring(ITEM_CD from 1 for 5) in ('61111','61112','17001') 
          group by CUST_ID, BILL_ID, SALE_DT) b on
( a.CUST_ID = b.CUST_ID
  and a.SEQ-1 = b.SEQ);