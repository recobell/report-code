--공통
select count(*)
from    (select * from tb_log_user where pc_id is not null) a
	inner join 
	(select * from tb_log_offline_cst where UNTYCSTMID is not null b on a.pc_id = b.UNTYCSTMID 
where a.gender like 'F' or  a.gender like 'M';


--#########
--## 통합 ##
--#########

drop table if exists tb_uni_customer_jensen;
create table tb_uni_customer_jensen as
select 
	a.order_amount,
	a.class,
	a.gender,
	a.user_id,
	b.cstmid,
	a.pc_id,
	b.untycstmid,
	a.signup_date,
	b.gnrt,
	b.trg_chk,
	b.eaifl,
	b.ucstmid,
	b.webid,
	b.cstmlvl,
	b.hjsendfl,	
	b.mlgfl,
	b.prtnrid,
	b.delfl,
	b.apempfl,
	b.delmlgfl,
	b.mbrfl,
	b.cstmusefl,
	b.dmsendfl,
	b.smssendfl,
	b.emailsendfl,
	b.skintrblcd1,
	b.skintpcd,
	b.addr1,
	b.homezip2,
	b.homezip1,
	b.gender as gender_2,
	b.nationfl,
	b.routecd,
	b.bday,
	b.joindate,
	b.fnlmlgdate,
	b.fnlpurdate	
from    (select * from tb_log_user) a
	full outer join 
	(select * from tb_log_offline_cst) b on a.pc_id = b.UNTYCSTMID;

drop table if exists tb_only_online_customer_jensen;
create table tb_only_online_customer_jensen as
select *
from tb_uni_customer_jensen
where cstmid is null;

drop table if exists tb_only_offline_customer_jensen;
create table tb_only_offline_customer_jensen as
select *
from tb_uni_customer_jensen
where cstmid is not null;

--#########
--##count##
--#########
--total customer: 840173
--only online customer: 37677
--only offline customer: 802496




















select * from tb_log_product_info group by;
