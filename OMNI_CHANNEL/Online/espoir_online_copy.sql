
drop table if exists tb_log_coupon;
create table tb_log_coupon(
server_time timestamp,
oncp_code varchar(50),
cp_name varchar(100),
dc_type varchar(50),
dc_amount float,
start_date timestamp,
end_date timestamp
);


drop table if exists tb_log_order;
create table tb_log_order(
server_time timestamp,
order_id varchar(200),
pc_id varchar(50),
ucstmid varchar(200),
user_id varchar(200),
oncp_code varchar(50),
ofcp_code varchar(100),
item_id varchar(50),
sap_code varchar(50),
quantity int,
order_price float,
device varchar(10),
signup_date timestamp,
gender varchar(50),
class varchar(50)
);


drop table if exists tb_log_user;
create table tb_log_user(
pc_id varchar(200),
ucstmid varchar(2000),
user_id varchar(100),
birth_date timestamp,
gender varchar(50),
class varchar(50),
signup_date timestamp,
order_amount float
);


drop table if exists tb_log_product;
create table tb_log_product(
item_id varchar(50),
item_name varchar(200),
original_price float,
sale_price float,
dc_price float,
category1 varchar(50),
category2 varchar(50)
);

drop table if exists tb_product_info;
create table tb_product_info(
category1 varchar(50),
category2 varchar(50),
category3 varchar(50),
sap_code varchar(50),
item_name varchar(200),
item_name_detail varchar(200),
sale_price float,
str2014 varchar(50),
str2015 varchar(50)
);


COPY tb_log_coupon FROM 's3://rb-ds/espoir/on/coupon/2015' 
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
MAXERROR 10000
GZIP
csv
EMPTYASNULL
IGNOREHEADER 1;

COPY tb_log_order FROM 's3://rb-ds/espoir/on/order/2015' 
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
MAXERROR 10000
GZIP
csv
EMPTYASNULL
IGNOREHEADER 1;

COPY tb_log_user FROM 's3://rb-ds/espoir/on/user/2015' 
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
MAXERROR 10000
GZIP
csv
EMPTYASNULL
IGNOREHEADER 1;

COPY tb_log_product FROM 's3://rb-ds/espoir/on/product/2015' 
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
MAXERROR 10000
GZIP
csv
EMPTYASNULL
IGNOREHEADER 1;

COPY tb_product_info FROM 's3://rb-ds/espoir/on/product_info/2015' 
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
MAXERROR 10000
GZIP
csv
EMPTYASNULL
IGNOREHEADER 1;

