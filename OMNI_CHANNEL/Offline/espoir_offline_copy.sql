drop table if exists tb_log_offline_cst;
create table tb_log_offline_cst(
CSTMID varchar(200),
NATIONFL varchar(200),
GENDER varchar(50),
GNRT int,
HOMEZIP1 varchar(200),
HOMEZIP2 varchar(200),
ADDR1 varchar(200),
EMAILSENDFL varchar(50),
SMSSENDFL varchar(50),
DMSENDFL varchar(50),
CSTMUSEFL varchar(200),
MBRFL varchar(200),
APEMPFL varchar(200),
DELFL varchar(200),
PRTNRID varchar(200),
FNLPURDATE timestamp,
FNLMLGDATE timestamp,
JOINDATE timestamp,
RTBADDRFL varchar(200),
UNTYCSTMID varchar(200),
CSTMLVL varchar(50),
BDAY varchar(200),
UCSTMID varchar(200)
);

drop table if exists tb_log_offline_mapping;
create table tb_log_offline_mapping(
CSTMID varchar(200),
UCSTMID varchar(200),
PCSTMID varchar(200),
ETWEBID varchar(200) 
);

drop table if exists tb_log_offline_point;
create table tb_log_offline_point(
UCSTMID varchar(200),
TOTACCUMPT INT,
TOTRDMPT INT, 
TOTREMAINPT INT,
EAIFL varchar(50)
);

drop table if exists tb_log_offline_product;
create table tb_log_offline_product(
BRAND_CD varchar(200),
ITEM_CD varchar(200),
TRNSCTPRCLISTTP varchar(200),
STDUNITPRC BIGINT,
USE_YN varchar(50)
);

drop table if exists tb_log_offline_sales;
create table tb_log_offline_sales(
STOR_CD varchar(200),
SALE_DT timestamp,
POS_NO varchar(50),
BILL_NO varchar(200),
SEQ varchar(200),
SALE_DIV varchar(50),
SORD_TM timestamp,
DELIVER_TM timestamp,
SALE_TM timestamp,
ITEM_CD varchar(50),
MAIN_ITEM_CD varchar(200),
PACK_DIV varchar(50),
FREE_DIV int,
DC_DIV int,
SALE_PRC float,
SALE_QTY int,
SALE_AMT float,
DC_RATE float,
DC_AMT float,
ENR_AMT float,
GRD_AMT float,
NET_AMT float,
VAT_RATE float,
VAT_AMT float,
COUPON_CD varchar(200),
CAMPAIGN_CD varchar(200),
CAMPAIGN_CUST_TP int,
CAMPAIGN_TP int,
CAMPAIGN_TG_DIV int,
CAMPAIGN_DC_S int,
CAMPAIGN_SALE_QTY int,
CAMPAIGN_SALE_AMT float,
SALER_DT timestamp,
BARCODE varchar(200),
CUST_ID varchar(200)
);

drop table if exists tb_log_offline_campaign;
create table tb_log_offline_campaign(
EVTCD varchar(2000),
CSTMID varchar(200),
ISSUENO varchar(50),
USEDATE timestamp,
USEPRTNRID varchar(200),
TOTSALAMT float,
NETSALAMT float,
DCAMT float,
ADDMLG float,
PRTNRTPCD varchar(100),
SMSFL varchar(50),
STDATE timestamp,
EDDATE timestamp,
DELFL varchar(50),
UCSTMID varchar(200)
);

drop table if exists tb_log_offline_point_detail;
create table tb_log_offline_point_detail(
USEDATE timestamp,
UCSTMID varchar(200),
PTSEQ varchar(50),
CHCD varchar(50),
CHCSTMID varchar(200),
PRTNRID int,
PRTNRNM varchar(200),
PURQTY int,
PURAMT float,
ACCUMPT int,
RDMPT int,
PTCD varchar(200),
PTTP varchar(200),
PTRSN varchar(200),
CHSEQ int,
DPREMAINPT int,
USEREMAINPT int,
TRMNSCHDDT timestamp
);


copy tb_log_offline_cst
from 's3://rb-ds/espoir/off/customer/2015'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
MAXERROR 10000
csv
EMPTYASNULL
IGNOREHEADER 1;

copy tb_log_offline_mapping
from 's3://rb-ds/espoir/off/mapping/2015'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
MAXERROR 10000
csv
EMPTYASNULL
IGNOREHEADER 1;

copy tb_log_offline_point
from 's3://rb-ds/espoir/off/point/2015'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
csv
GZIP
MAXERROR 10000
EMPTYASNULL
IGNOREHEADER 1;

copy tb_log_offline_product
from 's3://rb-ds/espoir/off/product/2015'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
MAXERROR 10000
csv
EMPTYASNULL
IGNOREHEADER 1;

copy tb_log_offline_sales
from 's3://rb-ds/espoir/off/sale/2015'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
MAXERROR 10000
csv
EMPTYASNULL;

copy tb_log_offline_campaign
from 's3://rb-ds/espoir/off/campaign/2015'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
MAXERROR 10000
csv
EMPTYASNULL
IGNOREHEADER 1;

copy tb_log_offline_point_detail
from 's3://rb-ds/espoir/off/point_detail/2015'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
MAXERROR 10000
csv
EMPTYASNULL
IGNOREHEADER 1;







