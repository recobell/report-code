
drop table if exists tb_log_campaign_master;
create table tb_log_campaign_master (
BRAND_CD varchar(200), 
CAMPAIGN_CD varchar(200), 
STOR_CD varchar(200), 
HQ_CAMPAIGN_DIV varchar(50), 
CAMPAIGN_NM varchar(200), 
CAMPAIGN_CUST_TP varchar(50), 
CAMPAIGN_CUST_GRADE varchar(200), 
HQ_RANK_YN varchar(50), 
CAMPAIGN_DIV varchar(50), 
CAMPAIGN_STAT varchar(50), 
START_DT timestamp, 
END_DT timestamp, 
CAMPAIGN_TP varchar(50), 
CAMPAIGN_TG_DIV varchar(50), 
CAMPAIGN_APP_DIV varchar(50), 
HPC_SAVE_DIV varchar(50), 
CAMPAIGN_AMT float, 
ADJ_METHOD varchar(50), 
CAMPAIGN_RANK int, 
CAMPAIGN_INFO varchar(200),
USE_YN varchar(50), 
DAY_MAX int, 
USE_CNT_LMT int , 
PUR_AMT_LMT float,
DC_TYPE  varchar(50));

Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000232', '000000', 'H', '(15년5월)쿠션30%_홍대점', 
    '1', '0000', 'Y', '1', 'P', 
    '20150515', '20150517', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 1000000);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000248', '000000', 'H', '(15년9월)바디케어 1만원', 
    '1', '0000', 'Y', '1', 'C', 
    '20150922', '20150930', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 10000000);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000247', '000000', 'H', '(15년9월)에스쁘아 빅세일', 
    '1', '0000', 'Y', '1', 'P', 
    '20150917', '20150920', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 1000000);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000239', '000000', 'H', '(15년6월)드로우센세이션 30%', 
    '1', '0000', 'Y', '1', 'C', 
    '20150622', '20150630', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 10000000);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000214', '000000', 'H', '12월_에스쁘아_BIG세일', 
    '2', '0000', 'Y', '1', 'C', 
    '20141218', '20141221', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000194', '000000', 'H', '9월_에스쁘아 BIG SALE_1', 
    '1', '0000', 'Y', '1', 'P', 
    '20140918', '20140921', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, HPC_SAVE_DIV, CAMPAIGN_AMT, 
    ADJ_METHOD, CAMPAIGN_RANK, USE_YN, DAY_MAX, USE_CNT_LMT, 
    PUR_AMT_LMT)
 Values
   ('EC01', '140000202', '000000', 'H', '9월_에스쁘아 BIG SALE_2', 
    'Y', '1', 'P', '20140918', '20140921', 
    '1', '2', '2', '1', 0, 
    '1', 0, 'Y', 0, 0, 
    0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000200', '000000', 'H', '프테파테+ANY미스트6000원_4호', 
    '2', '0000', 'Y', '1', 'P', 
    '20140922', '20140930', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 10000000);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000199', '000000', 'H', '프테파테+ANY미스트6000원_3호', 
    '2', '0000', 'Y', '1', 'P', 
    '20140922', '20140930', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 10000000);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000197', '000000', 'H', '프테파데+ANY미스트6000원_1호', 
    '1', '0000', 'Y', '1', 'P', 
    '20140922', '20140930', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 10000000);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000181', '000000', 'H', '3월_에스쁘아BigSALE', 
    '2', '0000', 'Y', '1', 'C', 
    '20140320', '20140323', '1', '3', '1', 
    '1', 1, '1', 0, 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, HPC_SAVE_DIV, 
    CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000179', '000000', 'H', 'test1', 
    '2', 'Y', '1', 'P', '20140314', 
    '20140314', '1', '1', '1', '1', 
    1, '1', 0, 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000231', '000000', 'H', '15년 5월 쿠션 30', 
    '1', '0000', 'Y', '1', 'C', 
    '20150515', '20150517', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 10000000);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000201', '000000', 'H', '프테파테+ANY미스트6000원_5호', 
    '2', '0000', 'Y', '1', 'P', 
    '20140922', '20140930', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 10000000);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '15465', '6', 'H', '(15년9월10월)코리아그랜드세일_추가', 
    'Y', '7', 'C', '20150907', '20151031', 
    0, 1, '(15년9월10월)코리아그랜드세일_추가', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '45464', '6', 'H', '(15년8,9월)서면지하상가 직원용', 
    'Y', '7', 'C', '20150830', '20150930', 
    0, 1, '(15년8,9월)서면지하상가 직원용', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '48473', '6', 'H', '(15년8월)립스틱데이', 
    'Y', '7', 'C', '20150810', '20150811', 
    0, 1, '(15년8월)립스틱데이', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '74646', '6', 'H', '(15년8월)외국인 40%', 
    'Y', '7', 'C', '20150810', '20150831', 
    0, 1, '(15년8월)외국인 40%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11879', '6', 'H', '(15년8월)임직원40%', 
    'Y', '7', 'C', '20150805', '20150811', 
    0, 1, '(15년8월)임직원40%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '78979', '6', 'H', '(15년7월)인증번호_가로수길 52,000', 
    'Y', '7', 'C', '20150702', '20150720', 
    0, 1, '(15년7월)인증번호_가로수길 52,000', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11789', '6', 'H', '(15년7월)러스터+쿠션+브라우마스터', 
    'Y', '7', 'C', '20150701', '20150731', 
    0, 1, '(15년7월)러스터+쿠션+브라우마스터', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11748', '6', 'H', '(15년7월)러스터+쿠션+스틱섀도우', 
    'Y', '7', 'C', '20150701', '20150731', 
    0, 1, '(15년7월)러스터+쿠션+스틱섀도우', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000240', '6', 'H', '15년6월_온리원이벤트', 
    '0000', 'Y', '6', 'C', '20150625', 
    '20150628', 0, 1, '15년6월_온리원이벤트', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, USE_YN, DAY_MAX, USE_CNT_LMT, 
    PUR_AMT_LMT)
 Values
   ('EC01', '11794', '6', 'H', '(15년5월)강남점_누드핏+핏브러쉬', 
    'Y', '7', 'C', '20150521', '20150521', 
    0, 1, 'Y', 0, 0, 
    0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '18444', '6', 'H', '(15년5월)강남점_누드핏+글로우라이저', 
    'Y', '7', 'C', '20150520', '20150531', 
    0, 1, '(15년5월)강남점_누드핏+글로우라이저', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000230', '6', 'H', '(15년5월)쿠션30%_대전', 
    '0000', 'Y', '6', 'C', '20150515', 
    '20150517', 0, 1, '(15년5월)쿠션30%_대전', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000225', '6', 'H', '15년5월_10%할인쿠폰(신규고객대상)_테스트', 
    '0000', 'Y', '6', 'C', '20150506', 
    '20150531', 0, 1, '15년5월_10%할인쿠폰(신규고객대상)_테스트', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11843', '6', 'H', '(15년4월)누드핏+노웨어,꾸뛰르', 
    'Y', '7', 'C', '20150401', '20150430', 
    0, 1, '(15년4월)누드핏+노웨어,꾸뛰르=48000', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11351', '6', 'H', '(15년2월)섀도우4개+쿼드', 
    'Y', '7', 'C', '20150130', '20150228', 
    0, 1, '(15년2월)섀도우4개+쿼드=35,000', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11531', '6', 'H', '(15년1월)리퀴드파데+브러쉬(4만원)_2', 
    'Y', '7', 'C', '20150114', '20150131', 
    0, 1, '(15년1월)리퀴드파데+브러쉬(4만원)_2', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000215', '6', 'H', '1월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20150122', 
    '20150125', 0, 1, '1월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '13413', '6', 'H', '(15년1월)프테파데+틴트_외국인상권X10', 
    'Y', '7', 'C', '20141231', '20150131', 
    0, 1, '(15년1월)프테파데+틴트_외국인상권X10', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '43111', '6', 'H', '(15년1월)프테파데+틴트_외국인상권X5', 
    'Y', '7', 'C', '20141231', '20150131', 
    0, 1, '(15년1월)프테파데+틴트_외국인상권X5', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11354', '6', 'H', '(15년1월)프테쿠션+틴트_외국인상권X5', 
    'Y', '7', 'C', '20141231', '20150131', 
    0, 1, '(15년1월)프테쿠션+틴트_외국인상권X5', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11141', '6', 'H', '(15년1월)외국인상권_스머프세트5개', 
    'Y', '7', 'C', '20141231', '20150131', 
    0, 1, '(15년1월)외국인상권_스머프세트5', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '12343', '6', 'H', '(14년12월)코리안그랜드세일행사', 
    'Y', '7', 'C', '20141201', '20150222', 
    0, 1, '(14년12월)코리안그랜드세일행사', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11164', '6', 'H', '(14년12월)프테쿠션+틴트', 
    'Y', '7', 'C', '20141129', '20141231', 
    0, 1, '(14년12월)프테쿠션+틴트', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11144', '6', 'H', '(14년12월)프테쿠션+틴트립트리트먼트', 
    'Y', '7', 'C', '20141128', '20141231', 
    0, 1, '(14년12월)프테쿠션+틴트립트리트먼트', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11168', '6', 'H', '(14년12월)프로에디션세트', 
    'Y', '7', 'C', '20141201', '20141231', 
    0, 1, '(14년12월)프로에디션세트', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '15434', '6', 'H', '(14년11월)쿠션전품목20%할인', 
    'Y', '7', 'C', '20141128', '20141215', 
    0, 1, '(14년11월) 쿠션전품목20%할인', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11145', '6', 'H', '나잇아웃세트1', 
    'Y', '7', 'C', '20141101', '20141112', 
    0, 1, '나잇아웃세트1 _ ( 립플루이드+프테쿠션+스카프)_ 48,000', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000193', '6', 'H', 'DT.C 20% 할인쿠폰', 
    '0000', 'Y', '6', 'C', '20140807', 
    '20140831', 0, 1, 'DT.C 20% 할인쿠폰(고객응대용)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21446', '6', 'H', '리퀴드파우더세트', 
    'Y', '7', 'C', '20140519', '20140630', 
    0, 1, '리퀴드파우더세트', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21449', '6', 'H', '아이브로우바10%추가인증', 
    'Y', '7', 'C', '20140417', '20140531', 
    0, 1, '아이브로우바10%추가인증', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000182', '6', 'H', '4월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20140425', 
    '20140427', 0, 1, '4월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21431', '6', 'H', '노웨어세트1', 
    'Y', '7', 'C', '20140228', '20140309', 
    0, 1, '노웨어세트1', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14203', '6', 'H', '졸업입학세트(노웨어+누쿠)', 
    'Y', '7', 'C', '20140201', '20140228', 
    0, 1, '졸업입학세트(노웨어+누쿠)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14201', '6', 'H', '프로인텐스기획세트', 
    'Y', '7', 'C', '20140201', '20140213', 
    0, 1, '프로 인텐스 클렌징 오일, 프로인텐스 크림,  프로 인텐스 아이&래쉬 세럼 + 프로 클렌징 포어 클리어 브러쉬 
', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000156', '6', 'H', '브라우마스터할인쿠폰(OR)', 
    '0501', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '브라우마스터 할인쿠폰(ORANGE)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '48978', '6', 'H', '(15년10월)코리아그랜드세일', 
    'Y', '7', 'C', '20150930', '20151031', 
    0, 1, '(15년10월)코리아그랜드세일', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000198', '000000', 'H', '프테파테+ANY미스트6000원_2호', 
    '1', '0000', 'Y', '1', 'P', 
    '20140922', '20140930', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 10000000);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000203', '000000', 'H', '9월_에스쁘아_BIG세일', 
    '2', '0000', 'Y', '1', 'C', 
    '20140918', '20140921', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000235', '000000', 'H', '(15년5월)누드핏세트1(누드핏+핏브러쉬)', 
    '1', '0000', 'Y', '1', 'P', 
    '20150520', '20150531', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000191', '000000', 'H', 'Last Chance_노웨어50%', 
    '2', '0000', 'Y', '1', 'C', 
    '20140721', '20140819', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000180', '000000', 'H', 'TEST_K', 
    '2', '0000', 'Y', '1', 'P', 
    '20140318', '20140320', '1', '3', '1', 
    '1', 1, '1', 0, 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000238', '000000', 'H', '(15년6월)에스쁘아 빅세일', 
    '1', '0000', 'Y', '1', 'C', 
    '20150618', '20150621', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 10000000);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '15464', '6', 'H', '(15년9월10월)그랜드포럼40%', 
    'Y', '7', 'C', '20150930', '20151031', 
    0, 1, '(15년9월10월)그랜드포럼40%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11787', '6', 'H', '(15년9월)타임스퀘어개점행사', 
    'Y', '7', 'C', '20150924', '20150930', 
    0, 1, '(15년9월)타임스퀘어개점행사:틴트마커 2개 이상 20%

', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '01243', '6', 'H', '(15년9월)대학로_10%', 
    'Y', '7', 'C', '20150907', '20150930', 
    0, 1, '(15년9월)대학로_10%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000246', '6', 'H', '(15년9월)카운트다운이벤트 5000원할인쿠폰', 
    '0000', 'Y', '6', 'C', '20150903', 
    '20150917', 0, 1, '(15년9월)카운트다운이벤트 5000원할인쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '12141', '6', 'H', '(15년8월)쿼드+더니트3개', 
    'Y', '7', 'C', '20150820', '20150831', 
    0, 1, '(15년8월)쿼드+더니트3개', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000242', '6', 'H', '(15년8월)8월온리원이벤트', 
    '0000', 'Y', '6', 'C', '20150820', 
    '20150823', 0, 1, '(15년8월)8월온리원이벤트', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '78942', '6', 'H', '(15년6월)임직원_쿠션본품', 
    'Y', '7', 'C', '20150622', '20150630', 
    0, 1, '(15년6월)임직원_쿠션본품', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11187', '6', 'H', '(15년6월)이대점_색조3만원구매시 10%', 
    'Y', '7', 'C', '20150622', '20150630', 
    0, 1, '(15년6월)이대점_색조3만원 이상 구매시 10% off
', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11176', '6', 'H', '(15년6월)이대점_쿠션6만원이상1만원할인', 
    'Y', '7', 'C', '20150622', '20150630', 
    0, 1, '(15년6월)이대점_쿠션6만원이상1만원할인', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '48945', '6', 'H', '인증번호 테스트', 
    'Y', '7', 'C', '20150615', '20150615', 
    0, 1, '인증번호 테스트', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000237', '6', 'H', '15년6월_10%할인쿠폰(신규고객대상)', 
    '0000', 'Y', '6', 'C', '20150605', 
    '20150630', 0, 1, '15년6월_10%할인쿠폰(신규고객대상)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000236', '6', 'H', '15년5월_10%할인쿠폰(신규고객대상)_테스트', 
    '0000', 'Y', '6', 'C', '20150605', 
    '20150630', 0, 1, '15년5월_10%할인쿠폰(신규고객대상)_테스트', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11746', '6', 'H', '(15년6월)가로수길_세트B', 
    'Y', '7', 'C', '20150604', '20150720', 
    0, 1, '(15년6월)가로수길_세트B', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11481', '6', 'H', '(15년6월)가로수길점_세트 A', 
    'Y', '7', 'C', '20150604', '20150720', 
    0, 1, '(15년6월)가로수길점_세트 A_ 듀이+스틱섀도우+노웨어', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11749', '6', 'H', '(15년6월)강남점_세트', 
    'Y', '7', 'C', '20150603', '20150630', 
    0, 1, '(15년6월)강남점_세트: 프라이머+쿠션AD/ 누드핏', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '12344', '6', 'H', '(15년2월,3월)립플루이드25%OFF', 
    'Y', '7', 'C', '20150227', '20150331', 
    0, 1, '(15년2월,3월)립플루이드25%OFF', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21321', '6', 'H', '(15년2월,3월)립플루이드20%OFF', 
    'Y', '7', 'C', '20150227', '20150331', 
    0, 1, '(15년2월,3월)립플루이드20%OFF', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '13451', '6', 'H', '(15년1월)스머프세트_추가3', 
    'Y', '7', 'C', '20150109', '20150131', 
    0, 1, '(15년1월)스머프세트_추가3', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '140000212', '6', 'H', '(15년)회원생일20%할인쿠폰', 
    '0000', 'Y', '6', 'C', '20150101', 
    '20151231', 0, 1, '(15년)회원생일20%할인쿠폰_생일당일발급', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '140000211', '6', 'H', '(15년)생일M-up쿠폰', 
    '0000', 'Y', '6', 'C', '20150101', 
    '20151231', 0, 1, '(15년)생일M-up쿠폰_생일당일 발급', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '140000210', '6', 'H', '(15년)회원생일20%할인쿠폰', 
    '0000', 'Y', '6', 'C', '20150101', 
    '20151231', 0, 1, '(15년)회원생일20%할인쿠폰_생일 당일 발급', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '140000209', '6', 'H', '(15년)M-up시연쿠폰(VVIP)', 
    '0503', 'Y', '6', 'C', '20150101', 
    '20151231', 0, 1, '(15년)M-up시연쿠폰(VVIP)_2개월에 1회 발급_VVIP, VVIP(DB)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '140000208', '6', 'H', '(15년)VVIP KIT증정쿠폰', 
    '0503', 'Y', '6', 'C', '20150101', 
    '20151231', 0, 1, '(15년)VVIP KIT증정쿠폰_누적금액 30만원 달성 시 - VVIP, VVIP DB 모두 포함', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11348', '6', 'H', '(14년11월)리퀴드파데+미스트', 
    'Y', '7', 'C', '20141114', '20141130', 
    0, 1, '(14년11월)리퀴드파데+미스트_타임스퀘어점 (40,000)
', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '75453', '6', 'H', '(14년11월)나잇아웃세트_교육팀', 
    'Y', '7', 'C', '20141111', '20141118', 
    0, 1, '(14년11월)나잇아웃세트_교육팀_97,000', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11331', '6', 'H', '(14년11월)프테라인쉐어링_20%', 
    'Y', '7', 'C', '20141117', '20141123', 
    0, 1, '(14년11월)프테라인쉐어링_20%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '25135', '6', 'H', '섀도우2+노웨어G+공용기', 
    'Y', '7', 'C', '20140820', '20140930', 
    0, 1, '섀도우2개+노웨어G +공용기 (39,000D원)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '25111', '6', 'H', '온리원_고객응대용', 
    'Y', '7', 'C', '20140523', '20140531', 
    0, 1, '온리원_고객클레임응대용', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21448', '6', 'H', '3월컬렉션정기교육키트2', 
    'Y', '7', 'C', '20140414', '20140430', 
    0, 1, '3월컬렉션정기교육키트2(여유)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21441', '6', 'H', '리퀴드파우더세트', 
    'Y', '7', 'C', '20140409', '20140430', 
    0, 1, '리퀴드파우더세트(리퀴드+부스터+브러쉬)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '24142', '6', 'H', '노웨어M,S+누드쿠션', 
    'Y', '7', 'C', '20140310', '20140310', 
    0, 1, '노웨어M,S+누드쿠션2', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21435', '6', 'H', '누드쿠션쉐어링40%OFF', 
    'Y', '7', 'C', '20140228', '20140309', 
    0, 1, '누드쿠션쉐어링40%OFF', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000176', '6', 'H', '누드쿠션30%OFF(신부평)', 
    '0000', 'Y', '6', 'C', '20140217', 
    '20140223', 0, 1, '누드쿠션30%할인쿠폰(신부평점)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000175', '6', 'H', '누드쿠션30%OFF(신부평)', 
    '0000', 'Y', '6', 'C', '20140217', 
    '20140223', 0, 1, '누드쿠션30%할인쿠폰(신부평점)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000174', '6', 'H', '누드쿠션30%OFF(신부평)', 
    '0000', 'Y', '6', 'C', '20140217', 
    '20140217', 0, 1, '누드쿠션30%할인쿠폰(신부평점)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000173', '6', 'H', '신부평점20%할인쿠폰', 
    '0000', 'Y', '6', 'C', '20140218', 
    '20140223', 0, 1, '신부평점_전제품20%할인쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14204', '6', 'H', '노웨어세트', 
    'Y', '7', 'C', '20140201', '20140228', 
    0, 1, '노웨어세트(노웨어+페슬+프로인텐스크림)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000170', '6', 'H', '1월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20140124', 
    '20140126', 0, 1, '1월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14111', '6', 'H', '(15년1월)스머프세트_외국인상권X10', 
    'Y', '7', 'C', '20141231', '20150131', 
    0, 1, '(15년1월)스머프세트_외국인상권X10', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11119', '6', 'H', '(15년1월)5만원구매시,파우치2000원', 
    'Y', '7', 'C', '20141231', '20150131', 
    0, 1, '(15년1월)5만원구매시,파우치2000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11138', '6', 'H', '(15년1월)리퀴드파데+틴트글로우', 
    'Y', '7', 'C', '20141229', '20150131', 
    0, 1, '(15년1월)리퀴드파데+틴트글로우_48,000→43,000', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11153', '6', 'H', '(15년1월)프테쿠션+틴트글로우', 
    'Y', '7', 'C', '20141229', '20151231', 
    0, 1, '(15년1월)프테쿠션+틴트글로우_51,000→43,000', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '12431', '6', 'H', '(15년1월)스머프 세트', 
    'Y', '7', 'C', '20141229', '20141229', 
    0, 1, '(15년1월)스머프 세트_틴트글로우+3색 팔레트, 파우치 (53,000→42,000)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '140000213', '6', 'H', '(15년)신규회원웰컴기프트', 
    '0000', 'Y', '6', 'C', '20150101', 
    '20151231', 0, 1, '신규회원 첫 구매시 증정_ 발급일 + 30일 이내 사용', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, USE_YN, DAY_MAX, USE_CNT_LMT, 
    PUR_AMT_LMT)
 Values
   ('EC01', '32547', '6', 'H', '시럽 쿠폰_2', 
    'Y', '7', 'C', '20141202', '20141202', 
    0, 1, 'Y', 0, 0, 
    0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11169', '6', 'H', '(14년12월)프로에디션세트', 
    'Y', '7', 'C', '20141129', '20141231', 
    0, 1, '(14년12월)프로에디션세트', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11311', '6', 'H', '(14년10월)섀도우2+노웨어G+공용기_2', 
    'Y', '7', 'C', '20141007', '20141031', 
    0, 1, '(14년10월)섀도우2+노웨어G+공용기_2=39,000', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11123', '6', 'H', '(14년10월)프테쿠션세트', 
    'Y', '7', 'C', '20141006', '20141006', 
    0, 1, '프테쿠션+쿠션리필+퍼프4매- 48,000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '15212', '6', 'H', '섀도우2+노웨어G+공용기_10개x20', 
    'Y', '7', 'C', '20140828', '20140828', 
    0, 1, '섀도우2+노웨어G+공용기_10개x20(39,000x10)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000196', '6', 'H', '8월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20140828', 
    '20140831', 0, 1, '8월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000195', '6', 'H', '8월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20140828', 
    '20140831', 0, 1, '8월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '25997', '6', 'H', '누리파SummerSet2', 
    'Y', '7', 'C', '20140731', '20140731', 
    0, 1, '누리파SummerSet2
(리퀴드파우더+슈퍼데피니션브러쉬+글로우픽스미스트30ml)
', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '25987', '6', 'H', '누리파SummerSet1', 
    'Y', '7', 'C', '20140731', '20140731', 
    0, 1, '누리파SummerSet1
(리퀴드파우더+슈퍼데시피니션브러쉬+UV선미스트)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000190', '6', 'H', '픽스미스트증정쿠폰(누리파구매)', 
    '0000', 'Y', '6', 'C', '20140714', 
    '20140731', 0, 1, '픽스미스트증정쿠폰(누리파구매시)
_누리파재구매event', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '24762', '6', 'H', '비치밤쉘LiteSet', 
    'Y', '7', 'C', '20140714', '20140731', 
    0, 1, '비치밤쉘LiteSet
(스틱섀도우+립스테인)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000189', '6', 'H', '7월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20140725', 
    '20140727', 0, 1, '7월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000188', '6', 'H', 'DT.C_20%쿠폰', 
    '0000', 'Y', '6', 'C', '20140708', 
    '20140731', 0, 1, 'DT.C_20%쿠폰(클레임응대용)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '61719', '6', 'H', '아이브로우바10%', 
    'Y', '7', 'C', '20140630', '20140630', 
    0, 1, '아이브로우바10%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '61716', '6', 'H', '7월_외국인상권20%', 
    'Y', '7', 'C', '20140630', '20140630', 
    0, 1, '7월_외국인상권20%
(황고미)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '24665', '6', 'H', '비치밤쉘LiteSet*10', 
    'Y', '7', 'C', '20140625', '20140630', 
    0, 1, '비치밤쉘LiteSet*10
(스틱섀도우+립스테인)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21412', '6', 'H', '2014_아이브로우바10%쿠폰', 
    'Y', '7', 'C', '20140528', '20141231', 
    0, 1, '2014_아이브로우바10%쿠폰', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000183', '6', 'H', '5월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20140523', 
    '20140525', 0, 1, '5월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21455', '6', 'H', '온리원30%여유인증번호', 
    'Y', '7', 'C', '20140328', '20140331', 
    0, 1, '온리원30%여유인증번호', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21440', '6', 'H', '유네스코40%', 
    'Y', '7', 'C', '20140320', '20140331', 
    0, 1, '유네스코40%(인증번호1개)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21451', '6', 'H', '휴면고객50%여유인증번호', 
    'Y', '7', 'C', '20140314', '20140331', 
    0, 1, '휴면고객50%여유인증번호', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14105', '6', 'H', '명동30%할인쿠폰', 
    'Y', '7', 'C', '20140102', '20140131', 
    0, 1, '명동30%할인쿠폰', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14103', '6', 'H', '명동10%할인쿠폰', 
    'Y', '7', 'C', '20140102', '20140131', 
    0, 1, '명동10%할인쿠폰', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14102', '6', 'H', '프로인텐스기획세트', 
    'Y', '7', 'C', '20140102', '20140131', 
    0, 1, '프로인텐스기획세트', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '20014', '6', 'H', 'M-up레슨20%할인쿠폰', 
    'Y', '7', 'C', '20140101', '20141231', 
    0, 1, 'M-up레슨20%할인쿠폰', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000165', '6', 'H', 'Make-up쿠폰(B-day)', 
    '0000', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '회원 생일(B-day)기념 메이크업쿠폰
(생일당일발급_사용기간:발급일+30일)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000164', '6', 'H', '회원 생일(B-day)쿠폰', 
    '0000', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '회원 생일(B-day)쿠폰(생일당일발급_사용기간:발급일+30일)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000163', '6', 'H', '브로우바 서비스쿠폰(BL)', 
    '0000', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '브로우바 서비스쿠폰(BLACK)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000158', '6', 'H', '15만원gift증정쿠폰(BL)', 
    '0000', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '누적구매금액:15만원달성_gift증정쿠폰(BLACK_DB 리스트업)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000157', '6', 'H', '15만원gift증정쿠폰(PP)', 
    '0502', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '누적구매금액:15만원달성_gift증정쿠폰(PURPLE)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000153', '6', 'H', '페이스슬립하이드레이팅컴팩트1만원할인쿠폰', 
    '0000', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '페이스슬립하이드레이팅컴팩트1만원할인쿠폰', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '13144', '6', 'H', '(15년3월)외국인상권40%OFF', 
    'Y', '7', 'C', '20150313', '20150331', 
    0, 1, '(15년3월)외국인상권40%OFF', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '00001', '6', 'H', '(15년2월)부진재고40%OFF', 
    'Y', '7', 'C', '20150225', '20150228', 
    0, 1, '(15년2월)부진재고40%OFF', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11185', '6', 'H', '(15년2월)립플루이드20% 완불', 
    'Y', '7', 'C', '20150216', '20150228', 
    0, 1, '(15년2월)립플루이드20% 완불_', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11186', '6', 'H', '(15년2월)립플루이드 2개이상20%', 
    'Y', '7', 'C', '20150209', '20150228', 
    0, 1, '(15년2월)립플루이드 2개이상20%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '13111', '6', 'H', '5만원구매시_파우치2000원', 
    'Y', '7', 'C', '20150108', '20150131', 
    0, 1, '5만원구매시_파우치2000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11383', '6', 'H', '(15년1월)외국인상권_30%', 
    'Y', '7', 'C', '20150107', '20150131', 
    0, 1, '(15년1월)외국인상권_30%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '13411', '6', 'H', '(15년1월)5만원구매시_스머프파우치2000원', 
    'Y', '7', 'C', '20150107', '20150131', 
    0, 1, '(15년1월)5만원구매시_스머프파우치2000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '13202', '6', 'H', '(15년1월)외국인상권_나잇아웃50%', 
    'Y', '7', 'C', '20150106', '20150131', 
    0, 1, '(15년1월)외국인상권_나잇아웃50%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '15131', '6', 'H', '(15년1월)스머프세트_추가2', 
    'Y', '7', 'C', '20150105', '20150131', 
    0, 1, '(15년1월)스머프세트_아이섀도우트리오+틴트글로우+파우치
=42,000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11154', '6', 'H', '(15년1월)스머프세트', 
    'Y', '7', 'C', '20150102', '20150131', 
    0, 1, '(15년1월)스머프세트_아이섀도우트리오+틴트글로우+파우치 =42,000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11556', '6', 'H', '(15년1월)프테쿠션+틴트_외국인상권X10', 
    'Y', '7', 'C', '20141231', '20150131', 
    0, 1, '(15년1월)프테쿠션+틴트_외국인상권X10', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '140000207', '6', 'H', '(15년)신규회원 할인쿠폰', 
    '0000', 'Y', '6', 'C', '20150101', 
    '20151231', 0, 1, '(15년)신규회원 할인쿠폰_첫 구매 시 10% 할인쿠폰', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11151', '6', 'H', '프테라인쉐어링_30%', 
    'Y', '7', 'C', '20141107', '20141123', 
    0, 1, '프테라인쉐어링_30%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000205', '6', 'H', '10월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20141023', 
    '20141026', 0, 1, '10월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '15231', '6', 'H', '8월컬렉션_정기교육키트(핸드크림세트)_2', 
    'Y', '7', 'C', '20141001', '20141005', 
    0, 1, '8월컬렉션_정기교육키트(핸드크림세트)_2
', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11143', '6', 'H', '8월_정기교육KIT', 
    'Y', '7', 'C', '20140922', '20140930', 
    0, 1, '8월_정기교육KIT', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11116', '6', 'H', '프테파데+미스트_25,000', 
    'Y', '7', 'C', '20140922', '20140930', 
    0, 1, '프테파데+미스트_25,000', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11114', '6', 'H', '프테파데+미스트_12,000원', 
    'Y', '7', 'C', '20140922', '20140930', 
    0, 1, '프테파데+미스트_12,000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '25121', '6', 'H', '오늘셋트', 
    'Y', '7', 'C', '20140805', '20140805', 
    0, 1, '(주)코인트서비스 영업총괄팀/신규영업파트/대리 정 대 겸 
서울시 서초구 방배2동 980-43 정빌딩2층 
TEL:02)522-4851~2 FAX:02)522-4279 
MOBILE:010-9057-2805 E-mail:haewon23@hanmail.net', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21475', '6', 'H', 'Artist20%할인쿠폰', 
    'Y', '7', 'C', '20140707', '20140731', 
    0, 1, 'Artist20%할인쿠폰(7월)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '61713', '6', 'H', '7월_명동상권30%', 
    'Y', '7', 'C', '20140627', '20140731', 
    0, 1, '7월_명동상권30%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21649', '6', 'H', '리퀴드파우더세트4', 
    'Y', '7', 'C', '20140623', '20140731', 
    0, 1, '리퀴드파우더세트4(리퀴드+부스터+브러쉬)
추가발급', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21547', '6', 'H', '리퀴드파우더세트*10', 
    'Y', '7', 'C', '20140620', '20140630', 
    0, 1, '리퀴드파우더세트*10', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21546', '6', 'H', '리퀴드파우더세트*5', 
    'Y', '7', 'C', '20140620', '20140630', 
    0, 1, '리퀴드파우더세트*5', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21646', '6', 'H', '비치밤쉘FullSet*5', 
    'Y', '7', 'C', '20140620', '20140630', 
    0, 1, '비치밤쉘FullSet*5', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '24662', '6', 'H', '비치밤쉘LiteSet', 
    'Y', '7', 'C', '20140611', '20140630', 
    0, 1, '비치밤쉘LiteSet
(스틱섀도우+립스테인)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '24661', '6', 'H', '비치밤쉘FullSet', 
    'Y', '7', 'C', '20140611', '20140611', 
    0, 1, '비치밤쉘FullSet
(스틱섀도우+누쿠+립스테인)+파우치(판촉)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21462', '6', 'H', '他제품구매시,섀도우2품목5천원(분당서현)', 
    'Y', '7', 'C', '20140602', '20140602', 
    0, 1, '他제품구매시,섀도우2품목5천원에판매
(75%할인)_분당서현', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21461', '6', 'H', '리퀴드파우더세트(분당서현점)', 
    'Y', '7', 'C', '20140602', '20140630', 
    0, 1, '리퀴드파우더세트(분당서현점)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21460', '6', 'H', '누리파+슈퍼데피니션(6천원할인)2', 
    'Y', '7', 'C', '20140602', '20140602', 
    0, 1, '리퀴드파우더구매시슈퍼데피니션6천원에할인판매
(6월연장)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000178', '6', 'H', '3월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20140328', 
    '20140330', 0, 1, '3월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000172', '6', 'H', '2월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20140221', 
    '20140223', 0, 1, '2월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000162', '6', 'H', 'Make-up쿠폰(BL)', 
    '0000', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '블랙회원 승급시 매월 1일(승급월로부터 매월1일자 발급)_메이크업쿠폰(발급일+30일 사용가능)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000161', '6', 'H', '30만원gift증정쿠폰(BL)', 
    '0000', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '누적구매금액:30만원달성_gift증정쿠폰(BLACK)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000160', '6', 'H', '브라우마스터 할인쿠폰(PP)', 
    '0502', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '브라우마스터 할인쿠폰(PURPLE)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000159', '6', 'H', '브로우바 서비스쿠폰(PP)', 
    '0502', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '브로우바 서비스쿠폰(PURPLE)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000234', '6', 'H', '5월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20150528', 
    '20150531', 0, 1, '5월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000233', '6', 'H', '(15년5월)온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20150528', 
    '20150531', 0, 1, '(15년5월)온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11484', '6', 'H', '(15년5월)쿠션30%OFF_홍대점', 
    'Y', '7', 'C', '20150515', '20150517', 
    0, 1, '(15년5월)쿠션30%OFF_홍대점', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '44316', '6', 'H', '(15년5월)AK수원_꾸뛰르20%OFF', 
    'Y', '7', 'C', '20150507', '20150510', 
    0, 1, '(15년5월)AK수원_꾸뛰르20%OFF', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000226', '6', 'H', '15년5월_10%할인쿠폰(신규고객대상)', 
    '0000', 'Y', '6', 'C', '20150506', 
    '20150531', 0, 1, '15년5월_10%할인쿠폰(신규고객대상)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000224', '6', 'H', '15년5월_10%할인쿠폰(신규고객대상)', 
    '0000', 'Y', '6', 'C', '20150506', 
    '20150531', 0, 1, '15년5월_10%할인쿠폰(신규고객대상)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000223', '6', 'H', '15년5월_10%할인쿠폰(신규고객대상)_테스트', 
    '0000', 'Y', '6', 'C', '20150506', 
    '20150531', 0, 1, '15년5월_10%할인쿠폰(신규고객대상)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000222', '6', 'H', '15년5월_10%할인쿠폰(신규고객대상)', 
    '0000', 'Y', '6', 'C', '20150506', 
    '20150531', 0, 1, '15년5월_10%할인쿠폰(신규고객대상)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000221', '6', 'H', '15년5월_10%할인쿠폰(신규고객대상)', 
    '0000', 'Y', '6', 'C', '20150506', 
    '20150531', 0, 1, '15년5월_10%할인쿠폰(신규고객대상)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11179', '6', 'H', '(15년 5월)바디세트 30%', 
    'Y', '7', 'C', '20150501', '20150518', 
    0, 1, '(15년 5월)바디세트 30%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11146', '6', 'H', '(15년5월)명동_8만원이상 구매시 10% 할인', 
    'Y', '7', 'C', '20150501', '20150506', 
    0, 1, '(15년5월)명동_8만원이상 구매시 10% 할인', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11198', '6', 'H', '(15년4월)선크림쉐어링20%', 
    'Y', '7', 'C', '20150423', '20150426', 
    0, 1, '(15년4월)선크림쉐어링20%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11197', '6', 'H', '(15년4월)선크림쉐어링20%', 
    'Y', '7', 'C', '20150423', '20150423', 
    0, 1, '(15년4월)선크림쉐어링20%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11195', '6', 'H', '(15년4월)선크림2개40%', 
    'Y', '7', 'C', '20150422', '20150426', 
    0, 1, '(15년4월)선크림2개40%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11194', '6', 'H', '(15년4월)선크림쉐어링20%', 
    'Y', '7', 'C', '20150422', '20150426', 
    0, 1, '(15년4월)선크림쉐어링20%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000220', '6', 'H', '4월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20150423', 
    '20150426', 0, 1, '4월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11443', '6', 'H', '(15년3월)외국인상권40%OFF', 
    'Y', '7', 'C', '20150319', '20150331', 
    0, 1, '(15년3월)외국인상권40%OFF', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11140', '6', 'H', '(15년3월)2개이상 꾸뛰르20%OFF', 
    'Y', '7', 'C', '20150319', '20150331', 
    0, 1, '(15년3월)2개이상 꾸뛰르20%OFF_빅세일', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14124', '6', 'H', '(14년11월)명동상권40%', 
    'Y', '7', 'C', '20141105', '20141130', 
    0, 1, '(14년11월)명동상권40%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '12456', '6', 'H', '(14년10월)[8월 정기교육KIT]_3', 
    'Y', '7', 'C', '20141022', '20141026', 
    0, 1, '(14년10월)[8월 정기교육KIT]_3 12,000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14564', '6', 'H', '(14년10월)[8월 정기교육KIT]', 
    'Y', '7', 'C', '20141020', '20141026', 
    0, 1, '(14년10월)[8월 정기교육KIT_2]_64,000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11131', '6', 'H', '섀도우4개+공용기', 
    'Y', '7', 'C', '20140929', '20140929', 
    0, 1, '섀도우4개+공용기 (35,000원)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11122', '6', 'H', '프테파데+프로인텐스크림', 
    'Y', '7', 'C', '20141001', '20141031', 
    0, 1, '프테파데+프로인텐스크림 (48,000원)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '12313', '6', 'H', '8월컬렉션_정기교육키트(핸드크림세트)_1', 
    'Y', '7', 'C', '20140924', '20140930', 
    0, 1, '8월컬렉션_정기교육키트(핸드크림세트)_1 12000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11117', '6', 'H', '프테파데+미스트_22,000원', 
    'Y', '7', 'C', '20140922', '20140930', 
    0, 1, '프테파데+미스트_22,000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11115', '6', 'H', '프테파데+미스트_15,000원', 
    'Y', '7', 'C', '20140922', '20140930', 
    0, 1, '프테파데+미스트_15,000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11112', '6', 'H', '8월퍼스널레슨시상품', 
    'Y', '7', 'C', '20140915', '20140930', 
    0, 1, '8월퍼스널레슨시상품', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '55431', '6', 'H', '8월컬렉션_정기교육키트(핸드크림세트)', 
    'Y', '7', 'C', '20140903', '20140914', 
    0, 1, '8월컬렉션_정기교육키트(핸드크림세트)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '68432', '6', 'H', '8월컬렉션_정기교육키트', 
    'Y', '7', 'C', '20140903', '20140914', 
    0, 1, '8월컬렉션_정기교육키트 (프테파데+브러쉬+핸드크림)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21456', '6', 'H', '他제품+섀도우2품목5천원판매3', 
    'Y', '7', 'C', '20140523', '20140531', 
    0, 1, '他제품구매시,섀도우2품목5천원에판매(75%할인)3', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21550', '6', 'H', '他제품+섀도우2품목5천원판매', 
    'Y', '7', 'C', '20140523', '20140523', 
    0, 1, '他제품구매시,섀도우2품목5천원에판매(75%할인)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21450', '6', 'H', '他제품+섀도우2품목5천원판매', 
    'Y', '7', 'C', '20140514', '20140531', 
    0, 1, '他제품구매시,섀도우2품목5천원에판매(75%할인)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21459', '6', 'H', '리퀴드파우더+슈퍼데피니션(6천원)할인', 
    'Y', '7', 'C', '20140507', '20140531', 
    0, 1, '리퀴드파우더구매시슈퍼데피니션6천원에할인판매', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21443', '6', 'H', '아이브로우바10%추가인증', 
    'Y', '7', 'C', '20140320', '20140331', 
    0, 1, '아이브로우바10%추가인증', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21442', '6', 'H', '할인행사여유인증번호(30%)', 
    'Y', '7', 'C', '20140320', '20140323', 
    0, 1, '할인행사여유인증번호(30%)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '24141', '6', 'H', '프라이머리세럼+누드쿠션', 
    'Y', '7', 'C', '20140310', '20140331', 
    0, 1, '프라이머리세럼+누드쿠션', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000171', '6', 'H', 'NEW노웨어립카드증정쿠폰', 
    '0000', 'Y', '6', 'C', '20140205', 
    '20140228', 0, 1, 'NEW노웨어립카드증정쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14107', '6', 'H', 'New페슬하이드레이팅_신규회원', 
    'Y', '7', 'C', '20140102', '20140131', 
    0, 1, 'New페슬하이드레이팅_신규회원구매시(8천원할인권)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14106', '6', 'H', '아이브로우바10%_1월', 
    'Y', '7', 'C', '20140102', '20140131', 
    0, 1, '아이브로우바10%_1월', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14104', '6', 'H', '명동10%할인쿠폰', 
    'Y', '7', 'C', '20140102', '20140131', 
    0, 1, '명동10%할인쿠폰', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14101', '6', 'H', '페이스슬립기획세트', 
    'Y', '7', 'C', '20140102', '20140102', 
    0, 1, '페이스슬립기획세트(페슬(new)+언더레이어스틱+슬림핏브러쉬)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000169', '6', 'H', 'New멤버쉽런칭(하이드레이팅3만원)', 
    '0000', 'Y', '6', 'C', '20140102', 
    '20140131', 0, 1, 'New멤버쉽런칭(하이드레이팅3만원)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11199', '6', 'H', '(15년4월)선크림쉐어링2개40%', 
    'Y', '7', 'C', '20150423', '20150426', 
    0, 1, '(15년4월)선크림쉐어링2개40%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11345', '6', 'H', '(15년3월)외국인상권30%', 
    'Y', '7', 'C', '20150311', '20150331', 
    0, 1, '(15년3월)외국인상권30%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000219', '6', 'H', '14년VVIP_온라인전용_5만원할인쿠폰_최종', 
    '0000', 'Y', '6', 'C', '20150310', 
    '20150430', 0, 1, '14년VVIP_온라인전용_5만원할인쿠폰_최종', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000218', '6', 'H', '14년VVIP_온라인전용_5만원할인쿠폰', 
    '0000', 'Y', '6', 'C', '20150309', 
    '20150430', 0, 1, '14년VVIP_온라인전용_5만원할인쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '15666', '6', 'H', '(15년3월)이대점10%', 
    'Y', '7', 'C', '20150304', '20150331', 
    0, 1, '(15년3월)이대점10%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000216', '6', 'H', '2월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20150226', 
    '20150301', 0, 1, '2월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11189', '6', 'H', '(15년1월)리퀴드파데+핏브러쉬(4만원)', 
    'Y', '7', 'C', '20150109', '20150121', 
    0, 1, '(15년1월)리퀴드파데+핏브러쉬(4만원)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '13431', '6', 'H', '(14년11월)섀도우4개+쿼드(강남점)', 
    'Y', '7', 'C', '20141124', '20141130', 
    0, 1, '(14년11월)섀도우4개+쿼드(강남점)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000206', '6', 'H', '11월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20141128', 
    '20141130', 0, 1, '11월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11134', '6', 'H', '(14년11월)섀도우4개+쿼드', 
    'Y', '7', 'C', '20141119', '20141130', 
    0, 1, '(14년11월)섀도우4개+쿼드(35000원)_ 명동 3호점
', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14113', '6', 'H', '(14년10월)[8월 정기교육KIT]', 
    'Y', '7', 'C', '20141021', '20141031', 
    0, 1, '(14년10월)[8월 정기교육KIT]_64,000', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11413', '6', 'H', '[8월_정기교육KIT] _2', 
    'Y', '7', 'C', '20141017', '20141026', 
    0, 1, '프로테일러 파운데이션 / 핏페이스브러쉬 (2가지)
', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '12143', '6', 'H', '[8월_정기교육KIT]_1', 
    'Y', '7', 'C', '20141017', '20141026', 
    0, 1, '프로테일러 파운데이션 / 핏페이스브러쉬 / 핸드크림세트(3가지)
', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '12341', '6', 'H', '[8월_정기교육KIT]_2', 
    'Y', '7', 'C', '20141016', '20141016', 
    0, 1, '[8월_정기교육KIT]_2
프로테일러 파운데이션 / 핏페이스브러쉬 (2가지)
', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11341', '6', 'H', '[8월_정기교육KIT]_1', 
    'Y', '7', 'C', '20141017', '20141026', 
    0, 1, '[8월_정기교육KIT]
프로테일러 파운데이션 / 핏페이스브러쉬 / 핸드크림세트(3가지)
', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11118', '6', 'H', '(14년10월)프테쿠션세트', 
    'Y', '7', 'C', '20141013', '20141031', 
    0, 1, '(14년10월)프테쿠션세트= 프테쿠션+리필+퍼프 4매 (48,000원)
', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000204', '6', 'H', '9월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20140925', 
    '20140928', 0, 1, '9월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '12354', '6', 'H', '프.테파데+핏브러쉬+부스터', 
    'Y', '7', 'C', '20140820', '20140930', 
    0, 1, '프.테파데+핏브러쉬+부스터(56000원)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '31546', '6', 'H', '프.테리퀴드파데+핏브러쉬_1', 
    'Y', '7', 'C', '20140820', '20140930', 
    0, 1, '프.테리퀴드파데 구매시 핏브러쉬 증정 (38,000원)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '61714', '6', 'H', '7월_40%할인쿠폰', 
    'Y', '7', 'C', '20140627', '20140731', 
    0, 1, '7월_40%할인쿠폰', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '61712', '6', 'H', '7월_명동상권20%', 
    'Y', '7', 'C', '20140627', '20140731', 
    0, 1, '7월_명동상권20%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '61711', '6', 'H', '7월_명동상권10%', 
    'Y', '7', 'C', '20140627', '20140731', 
    0, 1, '7월_명동상권10%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21647', '6', 'H', '비치밤쉘FullSet*10', 
    'Y', '7', 'C', '20140620', '20140620', 
    0, 1, '비치밤쉘FullSet*10', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000187', '6', 'H', '6월_온리원이벤트쿠폰', 
    '0000', 'Y', '6', 'C', '20140627', 
    '20140629', 0, 1, '6월_온리원이벤트쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '85000', '6', 'H', '프로에디션C', 
    'Y', '7', 'C', '20140613', '20140613', 
    0, 1, '프로에디션C(85000)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '84000', '6', 'H', '프로에디션B', 
    'Y', '7', 'C', '20140613', '20140613', 
    0, 1, '프로에디션B(84000)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '87000', '6', 'H', '프로에디션A', 
    'Y', '7', 'C', '20140613', '20140630', 
    0, 1, '프로에디션A(87000)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000186', '6', 'H', '온.오프통합쿠폰TEST', 
    '0000', 'Y', '6', 'C', '20140611', 
    '20140614', 0, 1, '온.오프통합쿠폰TEST', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21463', '6', 'H', '누드쿠션30%할인권', 
    'Y', '7', 'C', '20140603', '20140608', 
    0, 1, '누드쿠션30%할인권
(부산3개점_ES1005/ES1006/ES1014)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21484', '6', 'H', '아이브로우바(New)', 
    'Y', '7', 'C', '20140425', '20140531', 
    0, 1, '아이브로우바(New)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '24134', '6', 'H', '누드쿠션쉐어링20%OFF', 
    'Y', '7', 'C', '20140228', '20140309', 
    0, 1, '누드쿠션쉐어링20%OFF', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '24133', '6', 'H', '노웨어세트2-2', 
    'Y', '7', 'C', '20140310', '20140331', 
    0, 1, '노웨어세트2-2', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21432', '6', 'H', '노웨어세트2', 
    'Y', '7', 'C', '20140310', '20140331', 
    0, 1, '노웨어세트2', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21477', '6', 'H', '신부평오픈기념20%할인', 
    'Y', '7', 'C', '20140218', '20140223', 
    0, 1, '신부평오픈기념20%할인', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '22277', '6', 'H', '2월컬렉션정기교육키트', 
    'Y', '7', 'C', '20140217', '20140228', 
    0, 1, '2월컬렉션정기교육키트', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000155', '6', 'H', '브로우바 서비스쿠폰(OR)', 
    '0501', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '브로우바 서비스쿠폰(ORANGE)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '130000154', '6', 'H', '신규회원 M-up쿠폰', 
    '0000', 'Y', '6', 'C', '20140101', 
    '20201231', 0, 1, '신규회원가입시 즉시발급_메이크업 쿠폰(발급일+30일 사용가능)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11898', '6', 'H', '(15년7,8월)노웨어2개이상 20%', 
    'Y', '7', 'C', '20150730', '20150831', 
    0, 1, '(15년7,8월)노웨어2개이상 20%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '41944', '6', 'H', '(15년6월)외국인 40%', 
    'Y', '7', 'C', '20150723', '20150731', 
    0, 1, '(15년6월)외국인 40%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11494', '6', 'H', '(15년7월)강남점_세트B', 
    'Y', '7', 'C', '20150709', '20150709', 
    0, 1, '(15년7월)강남점_세트B: 노웨어 립스틱+ 립 트리트먼트 에센스 + 립 브러쉬', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11648', '6', 'H', '(15년7월)강남점_세트 A', 
    'Y', '7', 'C', '20150709', '20150720', 
    0, 1, '쿠션/ 누드핏+ 퍼펙트오일 컨트롤 프레스드 파우더 67000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '48974', '6', 'H', '(15년7월)인증번호_가로수길_54000', 
    'Y', '7', 'C', '20150702', '20150702', 
    0, 1, '(15년7월)인증번호_가로수길_54000
파데+노웨어', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '44947', '6', 'H', '(15년6월)임직원_강남_리필_50만원', 
    'Y', '7', 'C', '20150626', '20150630', 
    0, 1, '(15년6월)임직원_강남_리필_50만원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '44644', '6', 'H', '(15년6월)임직원쿠션_50만원', 
    'Y', '7', 'C', '20150626', '20150630', 
    0, 1, '(15년6월)임직원쿠션_50만원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11745', '6', 'H', '(15년6월)임직원쿠션_30만원', 
    'Y', '7', 'C', '20150626', '20150630', 
    0, 1, '(15년6월)임직원쿠션_30만원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14849', '6', 'H', '(15년6월) 향수 30% (현대신촌)', 
    'Y', '7', 'C', '20150626', '20150628', 
    0, 1, '(15년6월) 향수 30% (현대신촌)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11477', '6', 'H', '(15년6월)아이펜슬2개 20%', 
    'Y', '7', 'C', '20150622', '20150630', 
    0, 1, '(15년6월)아이펜슬2개 20%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11149', '6', 'H', '(15년5월)누드핏+글로우라이저', 
    'Y', '7', 'C', '20150521', '20150531', 
    0, 1, '(15년5월)누드핏+글로우라이저', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11778', '6', 'H', '(15년5월)강남점_누드핏+핏브러쉬', 
    'Y', '7', 'C', '20150520', '20150531', 
    0, 1, '(15년5월)강남점_누드핏+핏브러쉬', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000229', '6', 'H', '(15년5월)쿠션30%_대전', 
    '0000', 'Y', '6', 'C', '20150515', 
    '20150517', 0, 1, '(15년5월)쿠션30%_대전', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000228', '6', 'H', '(15년5월)대전점_쿠션30%', 
    '0000', 'Y', '6', 'C', '20150514', 
    '20150514', 0, 1, '(15년5월)대전점_쿠션30%', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000227', '6', 'H', 'Make-up쿠폰', 
    '0000', 'Y', '6', 'C', '20150513', 
    '20150517', 0, 1, 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, USE_YN, DAY_MAX, USE_CNT_LMT, 
    PUR_AMT_LMT)
 Values
   ('EC01', '78456', '6', 'H', '시럽 다운로드 이벤트', 
    'Y', '7', 'C', '20141125', '20150131', 
    0, 1, 'Y', 0, 0, 
    0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '12111', '6', 'H', '(14년10월-대학로점)프테파데+핏브러쉬', 
    'Y', '7', 'C', '20141016', '20141019', 
    0, 1, '(14년10월-대학로점)프테파데+핏브러쉬_38,000원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11113', '6', 'H', '프테파데+브러쉬+부스터_완불10%_2', 
    'Y', '7', 'C', '20140905', '20140918', 
    0, 1, '프테파데+브러쉬+부스터_완불10%_2 (52,800원)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11511', '6', 'H', '프테파데+브러쉬_완불10%_2', 
    'Y', '7', 'C', '20140905', '20140918', 
    0, 1, '프테파데+브러쉬_완불10%_2(34,800원)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '12113', '6', 'H', '프테파데_완불10%할인_2', 
    'Y', '7', 'C', '20140905', '20140918', 
    0, 1, '프테파데_완불10%할인_2 (28,800원)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '13255', '6', 'H', '섀도우2+노웨어G+공용기_1', 
    'Y', '7', 'C', '20140820', '20140930', 
    0, 1, '섀도우2+노웨어G+공용기 (39,000원)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '64772', '6', 'H', '누리파+브러쉬SET2', 
    'Y', '7', 'C', '20140717', '20140731', 
    0, 1, '리퀴드파우더구매시슈퍼데피니션6천원에할인판매
(6월추가생성)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '61718', '6', 'H', '7월_외국인상권40%', 
    'Y', '7', 'C', '20140630', '20140731', 
    0, 1, '7월_외국인상권40%
(황고미)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '61717', '6', 'H', '7월_외국인상권30%', 
    'Y', '7', 'C', '20140630', '20140630', 
    0, 1, '7월_외국인상권30%
(황고미)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '61715', '6', 'H', '7월_외국인상권10%', 
    'Y', '7', 'C', '20140630', '20140731', 
    0, 1, '7월_외국인상권10%
(황고미)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '66677', '6', 'H', '5월컬렉션정기교육키트', 
    'Y', '7', 'C', '20140630', '20140707', 
    0, 1, '5월컬렉션정기교육키트', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '24664', '6', 'H', '비치밤쉘LiteSet*5', 
    'Y', '7', 'C', '20140625', '20140625', 
    0, 1, '비치밤쉘LiteSet*5
(스틱섀도우+립스테인)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21469', '6', 'H', '리퀴드파우더+슈퍼데피니션(6천원)할인판매', 
    'Y', '7', 'C', '20140609', '20140630', 
    0, 1, '리퀴드파우더+슈퍼데피니션(6천원)할인판매
_분당서현점200', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000185', '6', 'H', '픽스미스트증정쿠폰', 
    '0000', 'Y', '6', 'C', '20140601', 
    '20140630', 0, 1, '픽스미스트증정쿠폰
(누리파구매기대상자)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21426', '6', 'H', '프로인텐스클렌징토너30%', 
    'Y', '7', 'C', '20140603', '20140603', 
    0, 1, '프로인텐스클렌징토너30%
(500개 한정_6월3일 원데이특가)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000184', '6', 'H', '증정(대상상품有)테슷흐', 
    '0000', 'Y', '6', 'C', '20140529', 
    '20140529', 0, 1, '증정(대상상품有)테슷흐', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21483', '6', 'H', '30%할인쿠폰', 
    'Y', '7', 'C', '20140424', '20140531', 
    0, 1, '30%할인쿠폰(tngh)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21482', '6', 'H', '20%할인쿠폰', 
    'Y', '7', 'C', '20140424', '20140424', 
    0, 1, '20%할인쿠폰(tngh)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21481', '6', 'H', '10%할인쿠폰', 
    'Y', '7', 'C', '20140424', '20140424', 
    0, 1, '10%할인쿠폰(tngh)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21445', '6', 'H', '리퀴드파우더세트2', 
    'Y', '7', 'C', '20140410', '20140430', 
    0, 1, '리퀴드파우더세트2(리퀴드+피니쉬+브러쉬)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21447', '6', 'H', '3월컬렉션정기교육키트', 
    'Y', '7', 'C', '20140414', '20140423', 
    0, 1, '3월컬렉션정기교육키트', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '24106', '6', 'H', '아이브로우바10%_3월~', 
    'Y', '7', 'C', '20140303', '20140331', 
    0, 1, '아이브로우바10%_3월~', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14250', '6', 'H', '50%핸드크림300', 
    'Y', '7', 'C', '20140217', '20140223', 
    0, 1, '50%핸드크림300', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '32541', '6', 'H', '다음 마이원 IOS런칭 이벤트', 
    'Y', '7', 'C', '20140205', '20140331', 
    0, 1, '다음 마이원 ios런칭 3000원 할인쿠폰', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14208', '6', 'H', '명동30%할인쿠폰', 
    'Y', '7', 'C', '20140201', '20140228', 
    0, 1, '명동30%할인쿠폰', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14202', '6', 'H', '페이스슬립기획세트', 
    'Y', '7', 'C', '20140201', '20140213', 
    0, 1, '페이스슬립기획세트', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14155', '6', 'H', '럭셔리BODY(캄인도쿄)할인쿠폰', 
    'Y', '7', 'C', '20140116', '20140131', 
    0, 1, '럭셔리BODY(캄인도쿄)할인쿠폰', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000168', '6', 'H', '1월_프로인텐스5000원멤버쉽할인쿠폰', 
    '0000', 'Y', '6', 'C', '20140102', 
    '20140131', 0, 1, '1월_프로인텐스5000원멤버쉽할인쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000245', '6', 'H', '(15년9월)카운트다운5000원', 
    '0000', 'Y', '6', 'C', '20150903', 
    '20150917', 0, 1, '(15년9월)카운트다운5000원', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000244', '6', 'H', '(15년9월)시크릿m-up쿠폰', 
    '0000', 'Y', '6', 'C', '20150901', 
    '20150930', 0, 1, '(15년9월)시크릿m-up쿠폰 참석자 10000원 할인', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000243', '6', 'H', '(15년9월)시크릿M-up고객 10000원할인쿠폰', 
    '0000', 'Y', '6', 'C', '20150901', 
    '20150930', 0, 1, '(15년9월)시크릿M-up고객 10000원할인쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11488', '6', 'H', '(15년8월~10월)코리아그랜드세일_1', 
    'Y', '7', 'C', '20150820', '20151031', 
    0, 1, '(15년8월~10월)코리아그랜드세일', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11876', '6', 'H', '(15년8월~10월)코리아그랜드세일', 
    'Y', '7', 'C', '20150820', '20151031', 
    0, 1, '(15년8월~10월)코리아그랜드세일', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11641', '6', 'H', '(15년8월)코리아그랜드_30%', 
    'Y', '7', 'C', '20150813', '20151031', 
    0, 1, '(15년8월)코리아그랜드_30%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '13545', '6', 'H', '(15년8월)코리아그랜드_20%', 
    'Y', '7', 'C', '20150813', '20151031', 
    0, 1, '(15년8월)코리아그랜드_20%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '64668', '6', 'H', '(15년8월)코리아그랜드세일_10%', 
    'Y', '7', 'C', '20150813', '20151031', 
    0, 1, '(15년8월)코리아그랜드세일_10%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11489', '6', 'H', '(15년7월)섬머 룩세트+파우치', 
    'Y', '7', 'C', '20150730', '20150831', 
    0, 1, '(15년7월)섬머 룩세트+파우치', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000241', '6', 'H', '(15년7월)7월온리원이벤트', 
    '0000', 'Y', '6', 'C', '20150723', 
    '20150726', 0, 1, '(15년7월)7월온리원이벤트', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '74645', '6', 'H', '(15년7월)서울시10%', 
    'Y', '7', 'C', '20150714', '20150731', 
    0, 1, '(15년7월)서울시10%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '48949', '6', 'H', '(15년6월)외국인 40%', 
    'Y', '7', 'C', '20150703', '20150930', 
    0, 1, '(15년6월)외국인 40%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11487', '6', 'H', '(15년7월)서울시10%', 
    'Y', '7', 'C', '20150703', '20150731', 
    0, 1, '(15년7월)서울시10%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '48494', '6', 'H', '(15년6월)임직원_강남점_리필_30만원', 
    'Y', '7', 'C', '20150626', '20150630', 
    0, 1, '(15년6월)임직원_강남점_리필_30만원', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '41313', '6', 'H', '(15년6월)향수30%', 
    'Y', '7', 'C', '20150626', '20150628', 
    0, 1, '(15년6월)향수30%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '48431', '6', 'H', '(15년6월)외국인 40%', 
    'Y', '7', 'C', '20150625', '20150630', 
    0, 1, '(15년6월)외국인 40%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11150', '6', 'H', '(15년6월)임직원_쿠션 리필', 
    'Y', '7', 'C', '20150622', '20150630', 
    0, 1, '(15년6월)임직원_쿠션 리필', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11148', '6', 'H', '(15년6월)임직원_쿠션본품', 
    'Y', '7', 'C', '20150622', '20150630', 
    0, 1, '(15년6월)임직원_쿠션본품', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11476', '6', 'H', '(15년5월)정기교육 키트 지급', 
    'Y', '7', 'C', '20150527', '20150607', 
    0, 1, '(15년5월)정기교육 키트 지급', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, USE_YN, DAY_MAX, USE_CNT_LMT, 
    PUR_AMT_LMT)
 Values
   ('EC01', '11196', '6', 'H', '(15년3월)외국인상권40%OFF_1', 
    'Y', '7', 'C', '20150313', '20150331', 
    0, 1, 'Y', 0, 0, 
    0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '16150', '6', 'H', '(15년3월)외국인상권30%OFF_1', 
    'Y', '7', 'C', '20150312', '20150331', 
    0, 1, '(15년3월)외국인상권30%OFF_1', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11167', '6', 'H', '(15년3월)AK수원_향수30%OFF', 
    'Y', '7', 'C', '20150311', '20150313', 
    0, 1, '(15년3월)AK수원_향수30%OFF', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '150000217', '6', 'H', '14년VVIP_온라인전용_5만원할인쿠폰', 
    '0000', 'Y', '6', 'C', '20150309', 
    '20150430', 0, 1, '14년VVIP_온라인전용_5만원할인쿠폰', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11184', '6', 'H', '(15년2월)꾸뛰르 2개 이상 20% OFF_2', 
    'Y', '7', 'C', '20150223', '20150309', 
    0, 1, '(15년2월)꾸뛰르 2개 이상 20% OFF_2', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '13461', '6', 'H', '(14년11월)명동상권30%', 
    'Y', '7', 'C', '20141105', '20141130', 
    0, 1, '(14년11월)명동상권30%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11135', '6', 'H', '나잇아웃세트2', 
    'Y', '7', 'C', '20141101', '20141112', 
    0, 1, '나잇아웃세트2_나잇아웃 노웨어+향수+스카프 48,000', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11121', '6', 'H', '프테파데+부스터', 
    'Y', '7', 'C', '20140913', '20140917', 
    0, 1, '프테파데+부스터 ( 48,000원 )', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '12244', '6', 'H', '프테파데+부스터+브러쉬_완불10%할인', 
    'Y', '7', 'C', '20140904', '20140918', 
    0, 1, '프테파데+부스터+브러쉬_완불10%할인_52,800원
', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '11234', '6', 'H', '프테파데+브러쉬_완불용10%할인', 
    'Y', '7', 'C', '20140904', '20140918', 
    0, 1, '프테파데+브러쉬_완불용10%할인(34,800원)

', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21315', '6', 'H', '프테파데_완불용10%할인', 
    'Y', '7', 'C', '20140904', '20140918', 
    0, 1, '프테파데_완불용10%할인 (28,800)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '35464', '6', 'H', '프.테파데+핏브러쉬_10개x6개_2', 
    'Y', '7', 'C', '20140901', '20140930', 
    0, 1, '프.테파데+핏브러쉬_10개x6개_2', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '13548', '6', 'H', '프.테리퀴드파데+핏브러쉬_10개', 
    'Y', '7', 'C', '20140820', '20140930', 
    0, 1, '프.테리퀴드파데+핏브러쉬 10개 (38,000x10 =380,000)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '13654', '6', 'H', '프로클렌징오일+리필50%', 
    'Y', '7', 'C', '20140824', '20140824', 
    0, 1, '프로클렌징오일+리필50% (38,000)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '25631', '6', 'H', '프로클렌징오일+리필50%', 
    'Y', '7', 'C', '20140820', '20140930', 
    0, 1, '프로클렌징오일+리필50%', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '21325', '6', 'H', '프.테파데+핏브러쉬+부스터', 
    'Y', '7', 'C', '20140820', '20140930', 
    0, 1, '프.테파데+핏브러쉬+부스터(56,000원)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '32156', '6', 'H', '프.테리퀴드파데+핏브러쉬', 
    'Y', '7', 'C', '20140820', '20140930', 
    0, 1, '프.테 리퀴드 파데 구매시 핏브러쉬 증정 (38,000원)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000192', '6', 'H', '누쿠_온.오프통합테슷흐쿠폰(나래뀌)', 
    '0000', 'Y', '6', 'C', '20140801', 
    '20140803', 0, 1, '누쿠_온.오프통합테슷흐쿠폰(나래뀌)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '90000', '6', 'H', '프로에디션E', 
    'Y', '7', 'C', '20140613', '20140630', 
    0, 1, '프로에디션E(90000)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '89000', '6', 'H', '프로에디션D', 
    'Y', '7', 'C', '20140613', '20140630', 
    0, 1, '프로에디션D(89000)', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '140000177', '6', 'H', '3월_온리원50%쿠폰', 
    '0000', 'Y', '6', 'C', '20140312', 
    '20140331', 0, 1, '3월_온리원50%쿠폰(휴면고객대상발급)', 'Y', 
    0, 0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14206', '6', 'H', '명동10%할인쿠폰', 
    'Y', '7', 'C', '20140201', '20140228', 
    0, 1, '명동10%할인쿠폰', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14205', '6', 'H', '명동10%할인쿠폰', 
    'Y', '7', 'C', '20140201', '20140228', 
    0, 1, '명동10%할인쿠폰', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT)
 Values
   ('EC01', '14108', '6', 'H', '신규10%할인쿠폰', 
    'Y', '7', 'C', '20140102', '20140131', 
    0, 1, '신규10%할인쿠폰', 'Y', 0, 
    0, 0);
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '15461', '6', 'H', '(15년10월)전주점_노웨어2개 이상 20%', 
    'Y', '7', 'C', '20151008', '20151011', 
    0, 1, '(15년10월)전주점_노웨어2개 이상 20%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000262', '000000', 'H', '(15년12월)에스쁘아 빅세일', 
    '1', '0000', 'Y', '1', 'P', 
    '20151215', '20151215', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 1000000, '10');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000263', '000000', 'H', '(15년12월)에스쁘아 빅세일', 
    '2', '0000', 'Y', '1', 'C', 
    '20151215', '20151215', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 1000000, '10');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '14784', '6', 'H', '(15년12월)스크래치_20%(홍대점)', 
    'Y', '7', 'C', '20151230', '20151231', 
    0, 1, '(15년12월)스크래치_20%(홍대점)', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '18494', '6', 'H', '(15년12월)체스파우치 5000원', 
    'Y', '7', 'C', '20151230', '20151231', 
    0, 1, '(15년12월)체스파우치 5000원', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '15489', '6', 'H', '(16년1월)외국인20%', 
    'Y', '7', 'C', '20160104', '20160131', 
    0, 1, '(16년1월)외국인20% _한국관광공사 제휴', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11878', '6', 'H', '(16년1월)유통사임직원40%', 
    'Y', '7', 'C', '20160129', '20160131', 
    0, 1, '(16년1월)유통사임직원40%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11654', '6', 'H', '(16년2월)외국인_20%', 
    'Y', '7', 'C', '20160218', '20160630', 
    0, 1, '(16년2월)외국인_20%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '74879', '6', 'H', '(15년10월)서현점_노웨어2개이상20%', 
    'Y', '7', 'C', '20151015', '20151018', 
    0, 1, '(15년10월)서현점_노웨어2개이상20%(1주년행사)', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11897', '6', 'H', '(15년11월)전주고사2개이상20%', 
    'Y', '7', 'C', '20151112', '20151115', 
    0, 1, '(15년11월)전주고사2개이상20%,노웨어 주년행사', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000253', '6', 'H', '(15년11월)11월온리원이벤트', 
    '0000', 'Y', '6', 'C', '20151119', 
    '20151122', 0, 1, '(15년11월)11월온리원이벤트', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000258', '6', 'H', '(16년)생일회원20%할인쿠폰', 
    '0000', 'Y', '6', 'C', '20151125', 
    '20161231', 0, 1, '(16년)생일회원20%할인쿠폰', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '13876', '6', 'H', '(15년12월) 폐기대상품목 70%', 
    'Y', '7', 'C', '20151209', '20151231', 
    0, 1, '(15년12월) 폐기대상품목 70%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11848', '6', 'H', '(15년12월)임직원할인', 
    'Y', '7', 'C', '20151222', '20151223', 
    0, 1, '(15년12월)임직원할인', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11874', '6', 'H', '(16년 1월) 홍대점_50,000원', 
    'Y', '7', 'C', '20160109', '20160109', 
    0, 1, '(16년 1월) 홍대점_50,000원', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '160000268', '6', 'H', '000', 
    '0000', 'Y', '6', 'C', '20160201', 
    '20160201', 0, 1, 'Y', 0, 
    0, 0, '10');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '160000273', '000000', 'H', '(16년2월)향수40%', 
    '2', '0000', 'Y', '1', 'C', 
    '20160211', '20160214', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 1000000, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11867', '6', 'H', '(16년2월~)임직원 30%', 
    'Y', '7', 'C', '20160223', '20160630', 
    0, 1, '(16년2월~)임직원 30%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '160000274', '6', 'H', '16년2월_온리원이벤트', 
    '0000', 'Y', '6', 'C', '20160225', 
    '20160228', 0, 1, '16년2월_온리원이벤트', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, USE_YN, DAY_MAX, USE_CNT_LMT, 
    PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '13484', '6', 'H', '(15년11월)립스틱+프테EX라인', 
    'Y', '7', 'C', '20151103', '20151130', 
    0, 1, 'Y', 0, 0, 
    0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11849', '6', 'H', '(15년11월)립스틱+프테EX라인(18000원)', 
    'Y', '7', 'C', '20151103', '20151130', 
    0, 1, '(15년11월)립스틱+프테EX라인(18000원)', 'Y', 0, 
    0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '41164', '6', 'H', '(15년11월)AK수원점_메이크업쇼', 
    'Y', '7', 'C', '20151103', '20151130', 
    0, 1, '(15년11월)AK수원점_메이크업쇼', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '76466', '6', 'H', '(15년11월)대전은행_쿠션30%', 
    'Y', '7', 'C', '20151116', '20151122', 
    0, 1, '(15년11월)대전은행_쿠션30%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000254', '000000', 'H', '(15년11월)스킨케어50%', 
    '1', '0000', 'Y', '1', 'C', 
    '20151119', '20151122', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 1000000, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000259', '6', 'H', '(16년)VVIP_샘플킷 증정', 
    '0503', 'Y', '6', 'C', '20151125', 
    '20151231', 0, 1, '(16년)VVIP_샘플킷 증정_매월 1일', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '35564', '6', 'H', '(15년12월)강남점 10만원', 
    'Y', '7', 'C', '20151201', '20151231', 
    0, 1, '(15년12월)강남점 10만원', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000260', '6', 'H', '(15년12월)강남VVIP_5만원', 
    '0000', 'Y', '6', 'C', '20151204', 
    '20151231', 0, 1, '(15년12월)강남VVIP_5만원', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000261', '6', 'H', '(15년12월)강남VVIP_5만원', 
    '0000', 'Y', '6', 'C', '20151204', 
    '20151231', 0, 1, '(15년12월)강남VVIP_5만원', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '18789', '6', 'H', '(15년12월)강남점_20%(스크래치)', 
    'Y', '7', 'C', '20151204', '20151231', 
    0, 1, '(15년12월)강남점_20%(스크래치)', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000264', '000000', 'H', '(15년12월)에스쁘아 빅세일', 
    '2', '0000', 'Y', '1', 'C', 
    '20151217', '20151220', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 1000000, '10');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11215', '6', 'H', '(16년3월)카카오페이3000원권', 
    'Y', '7', 'C', '20160202', '20160331', 
    0, 1, '(16년3월)카카오페이3000원권', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '160000270', '6', 'H', '(16년2월)복지쿠폰_350,000', 
    '0000', 'Y', '6', 'C', '20160204', 
    '20160229', 0, 1, '(16년2월)복지쿠폰_350,000', 'Y', 
    0, 0, 0, '50');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '160000271', '6', 'H', '(16년2월)복지쿠폰_350,000', 
    '0000', 'Y', '6', 'C', '20160204', 
    '20160229', 0, 1, '(16년2월)복지쿠폰_350,000', 'Y', 
    0, 0, 0, '50');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '160000272', '6', 'H', '(16년2월)복지쿠폰_450,000', 
    '0000', 'Y', '6', 'C', '20160204', 
    '20160229', 0, 1, '(16년2월)복지쿠폰_450,000', 'Y', 
    0, 0, 0, '50');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000249', '6', 'H', '(15년10월)10월온리원이벤트', 
    '0000', 'Y', '6', 'C', '20151022', 
    '20151025', 0, 1, '(15년10월)10월온리원이벤트', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11687', '6', 'H', '(15년11월)', 
    'Y', '7', 'C', '20151102', '20151130', 
    0, 1, '외국인 10만원 이상 30만원 미만 30% (일부품목)', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11431', '6', 'H', '(16년1월~) 임직원 30%', 
    'Y', '7', 'C', '20151231', '20161231', 
    0, 1, '(16년1월~) 임직원 30%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000265', '6', 'H', '(16년1월)체스앤피스 3000원권', 
    '0000', 'Y', '6', 'C', '20151231', 
    '20160131', 0, 1, '(16년1월)체스앤피스 3000원권', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '15746', '6', 'H', '(16년2월)외국인20%', 
    'Y', '7', 'C', '20160202', '20160331', 
    0, 1, '(16년2월)외국인20% _한국관광공사 제휴', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11467', '6', 'H', '(16년2월)폐기자재80%', 
    'Y', '7', 'C', '20160211', '20160229', 
    0, 1, '(16년2월)폐기자재80%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '14844', '6', 'H', '(15년10월)임직원30%', 
    'Y', '7', 'C', '20151002', '20151031', 
    0, 1, '(15년10월)임직원30%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '48678', '6', 'H', '(15년10월)향수30%', 
    'Y', '7', 'C', '20151011', '20151031', 
    0, 1, '(15년10월)향수30%_현대신촌', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000250', '000000', 'H', '(15년10월)꾸뛰르터치30%', 
    '1', '0000', 'Y', '1', 'P', 
    '20151022', '20151031', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 1000000, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000251', '000000', 'H', '(15년10월)꾸뛰르터치30%', 
    '1', '0000', 'Y', '1', 'P', 
    '20151022', '20151031', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 1000000, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000252', '000000', 'H', '(15년10월)꾸뛰르터치30%', 
    '1', '0000', 'Y', '1', 'C', 
    '20151022', '20151031', '1', '2', '2', 
    '1', 0, '1', 0, 'N', 
    0, 0, 1000000, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '78944', '6', 'H', '(15년11월12월)3000원할인쿠폰', 
    'Y', '7', 'C', '20151116', '20151231', 
    0, 1, '(15년11월12월)3000원할인쿠폰', 'Y', 0, 
    0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000255', '6', 'H', '(16년)신규회원10%할인쿠폰', 
    '0000', 'Y', '6', 'C', '20151125', 
    '20161231', 0, 1, '(16년)신규회원10%할인쿠폰_ 가입 시 10% 할인', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000256', '6', 'H', '(16년)VVIP키트 증정쿠폰', 
    '0503', 'Y', '6', 'C', '20151125', 
    '20161231', 0, 1, '(16년)VVIP키트 증정쿠폰', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '150000257', '6', 'H', '(16년)VVIP_M-up쿠폰', 
    '0503', 'Y', '6', 'C', '20151125', 
    '20161231', 0, 1, '(16년)VVIP_M-up쿠폰 ( 3개월에 1회)', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '11846', '6', 'H', '(15년12월)외국인상권_20%', 
    'Y', '7', 'C', '20151202', '20151231', 
    0, 1, '(15년12월)외국인상권_20%', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, END_DT, 
    CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, DAY_MAX, 
    USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '10131', '6', 'H', '(16년1월)2개이상20%', 
    'Y', '7', 'C', '20151230', '20151230', 
    0, 1, '(16년1월)2개이상20%_체스피스, 노웨어', 'Y', 0, 
    0, 0, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_TP, CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, 
    START_DT, END_DT, CAMPAIGN_TP, CAMPAIGN_TG_DIV, CAMPAIGN_APP_DIV, 
    HPC_SAVE_DIV, CAMPAIGN_AMT, ADJ_METHOD, CAMPAIGN_RANK, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '160000266', '000000', 'H', '(16년1월)클렌징30%(쇼타임)', 
    '2', '0000', 'Y', '1', 'C', 
    '20160121', '20160124', '1', '2', '2', 
    '1', 0, '1', 0, 'Y', 
    0, 0, 1000000, '30');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '160000267', '6', 'H', '16년1월_온리원이벤트', 
    '0000', 'Y', '6', 'C', '20160121', 
    '20160124', 0, 1, '16년1월_온리원이벤트', 'Y', 
    0, 0, 0, '20');
Insert into tb_log_campaign_master
   (BRAND_CD, CAMPAIGN_CD, STOR_CD, HQ_CAMPAIGN_DIV, CAMPAIGN_NM, 
    CAMPAIGN_CUST_GRADE, HQ_RANK_YN, CAMPAIGN_DIV, CAMPAIGN_STAT, START_DT, 
    END_DT, CAMPAIGN_AMT, CAMPAIGN_RANK, CAMPAIGN_INFO, USE_YN, 
    DAY_MAX, USE_CNT_LMT, PUR_AMT_LMT, DC_TYPE)
 Values
   ('EC01', '160000269', '6', 'H', '(16년2월)복지쿠폰', 
    '0000', 'Y', '6', 'C', '20160201', 
    '20160229', 0, 1, '16년2월)복지쿠폰', 'Y', 
    0, 0, 0, '50');
COMMIT;



drop table if exists tb_log_campaign_package_master;
create table tb_log_campaign_package_master (
BRAND_CD varchar(200), 
CAMPAIGN_CD varchar(200), 
CAMPAIGN_STAT varchar(50), 
CAMPAIGN_NM varchar(200), 
START_DT timestamp, 
END_DT timestamp, 
CAMPAIGN_DIV varchar(50), 
DC_RATE float, 
DC_AMT float, 
CAMPAIGN_CUST_TP varchar(50), 
CAMPAIGN_CUST_GRADE varchar(200), 
CAMPAIGN_TG_DIV varchar(50), 
PUR_AMT_LMT float, 
CAMPAIGN_APP_DIV varchar(50), 
USE_CNT_LMT float,
USE_YN varchar(50), 
OFFLINE_ONLY_YN varchar(50), 
PACKAGE_SET_CNT varchar(50),
DC_TYPE varchar(50)
);

Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000005', 'P', '(15년2월)프테쿠션+노웨어=45000(54000원)', '20150223', 
    '20150228', '2', 0, 9000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000008', 'P', '(15년2월)프테쿠션리필+노웨어=34000(30000원)', '20150210', 
    '20150212', '2', 0, 4000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT, DC_TYPE)
 Values
   ('EC01', '150000038', 'C', '(15년12월,1월)프로EX+오일50%', '20151214', 
    '20160131', '2', 0, 19000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2', '30');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT, DC_TYPE)
 Values
   ('EC01', '160000040', 'C', '(16년2월)퓨어래디언스+글로우픽스미스트', '20160201', 
    '20160229', '2', 20, 10000, '2', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2', '30');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000021', 'C', '(15년5월)바디세트30%', '20150501', 
    '20150518', '1', 30, 0, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '1');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000007', 'P', '(15년2월)프테쿠션+노웨어=45000(54000원)', '20150210', 
    '20150212', '2', 0, 9000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000024', 'C', '(15년6월)쿠션듀이+글로우미스트', '20150601', 
    '20150610', '2', 30, 6600, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000025', 'C', '(15년6월)쿠션+퓨어래디언스', '20150601', 
    '20150610', '2', 30, 8400, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000028', 'P', '(15년7월)러스터+쿠션+브라우마스터', '20150629', 
    '20150731', '2', 0, 10000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '3');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000029', 'P', '(15년7월)브라우마스터+러스터+쿠션', '20150629', 
    '20150731', '2', 0, 10000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '3');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000030', 'C', '(15년9월)컨실러15000원', '20150921', 
    '20150930', '2', 0, 3000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000033', 'C', '(15년9월)컨실러25000원', '20150921', 
    '20150930', '2', 0, 5000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT, DC_TYPE)
 Values
   ('EC01', '160000039', 'C', '(16년1월)프로EX+모이스쳐스틱=54000', '20160108', 
    '20160131', '2', 0, 16000, '1', 
    '0000', '0', 1000000, '0', 0, 
    'Y', 'N', '2', '30');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000006', 'P', '(15년2월)프테쿠션+립플루이드=45,000', '20150209', 
    '20150228', '2', 0, 9000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000011', 'P', '(15년4월)누드핏+피니쉬=52000', '20150331', 
    '20150430', '2', 0, 8000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000012', 'P', '(15년4월)누드핏+피니쉬+블렌더=62,000', '20150401', 
    '20150430', '2', 0, 8000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '1');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000015', 'C', '(15년4월)누드핏+블렌더=40,000원', '20150430', 
    '20150530', '2', 0, 5000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000016', 'C', '(15년4월)누드핏+글로우라이저=54,600', '20150420', 
    '20150430', '2', 0, 8400, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000018', 'C', '(15년4월)누드핏+블렌더+오일젤', '20150421', 
    '20150430', '2', 0, 9000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '3');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000019', 'C', '(15년4월)누드핏+블렌더+글로우라이저', '20150421', 
    '20150430', '2', 0, 8400, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '3');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000020', 'C', '(15년5월)로즈+꾸뛰르', '20150501', 
    '20150518', '2', 0, 14000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000022', 'P', '(15년5월)쿠션30%OFF', '20150515', 
    '20150517', '1', 30, 0, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '1');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000023', 'C', '(15년6월)누드쿠션+롱웨어미스트', '20150601', 
    '20150610', '2', 30, 6600, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT, DC_TYPE)
 Values
   ('EC01', '150000037', 'P', '(15년12월)프테ex+인스턴트50%', '20151201', 
    '20151213', '2', 0, 12500, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2', '30');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000001', 'P', '(15년1월)리퀴드파데+브러쉬', '20150115', 
    '20150121', '2', 0, 12000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000002', 'P', '(15년2월)섀도우4개+퀴드=35,000(50,000원)', '20150128', 
    '20150212', '2', 0, 15000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '5');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000003', 'C', '(15년2월)섀도우+블러쉬+노웨어', '20150301', 
    '20150331', '2', 0, 13000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '3');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000014', 'C', '(15년4월)누드핏+피니쉬=52000', '20150401', 
    '20150430', '2', 0, 8000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT, DC_TYPE)
 Values
   ('EC01', '150000034', 'P', '(15년10월)프테쿠션구매시,브러쉬50%', '20151001', 
    '20151031', '2', 0, 10000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2', '30');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT, DC_TYPE)
 Values
   ('EC01', '150000035', 'C', '(15년10월)프테쿠션+리필50%', '20151102', 
    '20151102', '2', 0, 8000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2', '30');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000009', 'C', '(15년2월)프테쿠션+립플루이드=45,000', '20150301', 
    '20150331', '2', 0, 9000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000010', 'P', '(15년4월)누드핏+블렌더=40,000원', '20150330', 
    '20150430', '2', 0, 5000, '1', 
    '0000', '0', 100000000, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000004', 'C', '(15년2월)섀도우4개+퀴드=35,000(50,000원)', '20150128', 
    '20150212', '2', 0, 15000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '5');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000013', 'C', '(15년4월)누드핏+피니쉬+블렌더=62,000', '20150401', 
    '20150430', '2', 0, 8000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '3');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000017', 'C', '(15년4월)누드핏+오일젤', '20150420', 
    '20150430', '2', 0, 9000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000026', 'C', '(15년7월)노웨어러스터+쿠션', '20150803', 
    '20150831', '2', 0, 8000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000027', 'P', '(15년7월)러스터+쿠션+섀도우', '20150629', 
    '20150731', '2', 0, 11000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '3');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000031', 'C', '(15년9월)컨실러16000원', '20150921', 
    '20150930', '2', 0, 3200, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT)
 Values
   ('EC01', '150000032', 'C', '(15년9월)컨실러23000원', '20150921', 
    '20150930', '2', 0, 4600, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2');
Insert into tb_log_campaign_package_master
   (BRAND_CD, CAMPAIGN_CD, CAMPAIGN_STAT, CAMPAIGN_NM, START_DT, 
    END_DT, CAMPAIGN_DIV, DC_RATE, DC_AMT, CAMPAIGN_CUST_TP, 
    CAMPAIGN_CUST_GRADE, CAMPAIGN_TG_DIV, PUR_AMT_LMT, CAMPAIGN_APP_DIV, USE_CNT_LMT, 
    USE_YN, OFFLINE_ONLY_YN, PACKAGE_SET_CNT, DC_TYPE)
 Values
   ('EC01', '150000036', 'P', '(15년11월,12월)프테 ex라인 구매시 브러쉬50%', '20151123', 
    '20151231', '2', 0, 10000, '1', 
    '0000', '0', 0, '0', 0, 
    'Y', 'N', '2', '30');
COMMIT;





drop table if exists tb_log_campaign_detail;
create table tb_log_campaign_detail (
EVTCD varchar(200), 
EVTTYPE varchar(200), 
EVTNM varchar(200), 
ISSUEDT timestamp, 
ISSUECNT int, 
EVTDETAIL varchar(200), 
TGCUSGRD varchar(200), 
TGCUSGRP varchar(200), 
TGPRDFL varchar(200), 
USETYPE varchar(200), 
USEDETAIL float, 
USECON float, 
USELMT float, 
AUTOFL varchar(50), 
STDATE timestamp, 
EDDATE timestamp, 
SMSFL varchar(50),
DELFL varchar(50),
CUSTINSCNT int, 
ITEMINSCNT int, 
CHKUSEFL varchar(50), 
OFF_ONLY_YN varchar(50), 
ONLY_ONE_YN varchar(50));

select * from tb_log_campaign_detail;

Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, ITEMINSCNT, 
    CHKUSEFL, OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000169', 'CP', 'New멤버쉽런칭(하이드레이팅3만원)', '20140102', 1, 
    'New멤버쉽런칭(하이드레이팅3만원)', '0000', '0905', '1', '10000002', 
    8000, 1, 1000000, '2', '20140102', 
    '20140131', 'N', '1', 1, 5, 
    '2', 'Y', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000173', 'CP', '신부평점20%할인쿠폰', '20140217', 1, 
    '신부평점_전제품20%할인쿠폰', '0000', '0905', '2', '10000001', 
    20, 1, 1000000, '2', '20140218', 
    '20140223', 'N', '1', 6223, '2', 
    'Y', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000174', 'CP', '누드쿠션30%OFF(신부평)', '20140217', 1, 
    '누드쿠션30%할인쿠폰(신부평점)', '0000', '0905', '2', '10000001', 
    30, 1, 1000000, '2', '20140217', 
    '20140217', 'N', '2', '2', 'Y', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000175', 'CP', '누드쿠션30%OFF(신부평)', '20140217', 1, 
    '누드쿠션30%할인쿠폰(신부평점)', '0000', '0901', '2', '10000001', 
    30, 1, 1000000, '2', '20140217', 
    '20140223', 'N', '2', '2', 'Y', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000176', 'CP', '누드쿠션30%OFF(신부평)', '20140217', 1, 
    '누드쿠션30%할인쿠폰(신부평점)', '0000', '0905', '1', '10000001', 
    30, 1, 1000000, '2', '20140217', 
    '20140223', 'N', '2', 6223, '2', 
    'Y', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000205', 'CP', '10월_온리원이벤트쿠폰', '20141013', 1, 
    '10월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20141023', 
    '20141102', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000216', 'CP', '2월_온리원이벤트쿠폰', '20150223', 1, 
    '2월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 0, '2', '20150226', 
    '20150301', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000218', 'CP', '14년VVIP_온라인전용_5만원할인쿠폰', '20150309', 1, 
    '14년VVIP_온라인전용_5만원할인쿠폰', '0000', '0905', '2', '10000002', 
    50000, 1, 0, '2', '20150309', 
    '20150430', 'N', '2', 1, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000219', 'CP', '14년VVIP_온라인전용_5만원할인쿠폰_최종', '20150310', 1, 
    '14년VVIP_온라인전용_5만원할인쿠폰_최종', '0000', '0905', '2', '10000002', 
    50000, 1, 0, '2', '20150310', 
    '20150430', 'N', '1', 46, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('130000154', 'CP', '신규회원 M-up쿠폰', '20131206', 1, 
    '신규회원가입시 즉시발급_메이크업 쿠폰(발급일+30일 사용가능)', '0000', '0903', '2', '30000001', 
    0, 0, 0, '2', '20131206', 
    '20201231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('130000155', 'CP', '브로우바 서비스쿠폰(OR)', '20131206', 1, 
    '브로우바 서비스쿠폰(ORANGE)', '0501', '0901', '2', '30000001', 
    0, 0, 0, '2', '20140101', 
    '20201231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, ITEMINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('130000156', 'CP', '브라우마스터할인쿠폰(OR)', '20131206', 1, 
    '브라우마스터 할인쿠폰(ORANGE)', '0501', '0901', '1', '10000001', 
    30, 17000, 10000000, '2', '20140101', 
    '20201231', 'Y', '1', 7, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, ITEMINSCNT, 
    CHKUSEFL, OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000168', 'CP', '1월_프로인텐스5000원멤버쉽할인쿠폰', '20140102', 1, 
    '1월_프로인텐스5000원멤버쉽할인쿠폰', '0000', '0905', '1', '10000002', 
    5000, 1, 1000000, '2', '20140102', 
    '20140131', 'N', '2', 1, 3, 
    '2', 'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000187', 'CP', '6월_온리원이벤트쿠폰', '20140620', 1, 
    '6월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20140627', 
    '20140629', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000206', 'CP', '11월_온리원이벤트쿠폰', '20141121', 1, 
    '11월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20141128', 
    '20141130', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000220', 'CP', '4월_온리원이벤트쿠폰', '20150417', 1, 
    '4월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20150423', 
    '20150426', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000221', 'CP', '15년5월_10%할인쿠폰(신규고객대상)', '20150506', 1, 
    '15년5월_10%할인쿠폰(신규고객대상)', '0000', '0901', '2', '10000001', 
    10, 1, 10000000, '2', '20150506', 
    '20150531', 'N', '2', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000222', 'CP', '15년5월_10%할인쿠폰(신규고객대상)', '20150506', 1, 
    '15년5월_10%할인쿠폰(신규고객대상)', '0000', '0905', '2', '10000001', 
    10, 1, 10000000, '2', '20150506', 
    '20150531', 'N', '2', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000223', 'CP', '15년5월_10%할인쿠폰(신규고객대상)_테스트', '20150506', 1, 
    '15년5월_10%할인쿠폰(신규고객대상)', '0000', '0905', '2', '10000001', 
    10, 1, 0, '2', '20150506', 
    '20150531', 'N', '2', 1, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000224', 'CP', '15년5월_10%할인쿠폰(신규고객대상)', '20150506', 1, 
    '15년5월_10%할인쿠폰(신규고객대상)', '0000', '0905', '2', '10000001', 
    10, 1, 10000000, '2', '20150506', 
    '20150531', 'N', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, USEDETAIL, 
    USECON, USELMT, AUTOFL, STDATE, EDDATE, 
    SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000227', 'CP', 'Make-up쿠폰', '20150513', 1, 
    '0000', '0905', '2', '10000001', 40, 
    1, 1000000, '2', '20150513', '20150517', 
    'N', '2', 1, '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, ITEMINSCNT, 
    CHKUSEFL, OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000228', 'CP', '(15년5월)대전점_쿠션30%', '20150514', 1, 
    '(15년5월)대전점_쿠션30%', '0000', '0905', '1', '10000001', 
    30, 1, 10000000, '2', '20150514', 
    '20150514', 'N', '1', 1, 45, 
    '2', 'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, ITEMINSCNT, 
    CHKUSEFL, OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000229', 'CP', '(15년5월)쿠션30%_대전', '20150514', 1, 
    '(15년5월)쿠션30%_대전', '0000', '0905', '1', '10000001', 
    30, 1, 10000000, '2', '20150515', 
    '20150517', 'N', '2', 9671, 45, 
    '2', 'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000236', 'CP', '15년5월_10%할인쿠폰(신규고객대상)_테스트', '20150605', 1, 
    '15년5월_10%할인쿠폰(신규고객대상)_테스트', '0000', '0905', '2', '10000001', 
    10, 1, 10000000, '2', '20150605', 
    '20150630', 'N', '1', 2, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000237', 'CP', '15년6월_10%할인쿠폰(신규고객대상)', '20150605', 1, 
    '15년6월_10%할인쿠폰(신규고객대상)', '0000', '0905', '2', '10000001', 
    10, 1, 10000000, '2', '20150605', 
    '20150630', 'N', '1', 9999, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000253', 'CP', '(15년11월)11월온리원이벤트', '20151117', 1, 
    '(15년11월)11월온리원이벤트', '0000', '0901', '2', '10000001', 
    30, 1, 1000000, '2', '20151119', 
    '20151122', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000258', 'CP', '(16년)생일회원20%할인쿠폰', '20151125', 1, 
    '(16년)생일회원20%할인쿠폰', '0000', '0902', '2', '10000001', 
    20, 1, 1000000, '2', '20151125', 
    '20170131', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, USEDETAIL, 
    USECON, USELMT, AUTOFL, STDATE, EDDATE, 
    SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('160000268', 'CP', '000', '20160201', 1, 
    '0000', '0901', '2', '10000001', 30, 
    1, 0, '2', '20160201', '20160201', 
    'N', '2', '1', 'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('160000274', 'CP', '16년2월_온리원이벤트', '20160223', 1, 
    '16년2월_온리원이벤트', '0000', '0901', '2', '10000001', 
    30, 1, 1000000, '2', '20160225', 
    '20160228', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('130000159', 'CP', '브로우바 서비스쿠폰(PP)', '20131206', 1, 
    '브로우바 서비스쿠폰(PURPLE)', '0502', '0901', '2', '30000001', 
    0, 0, 0, '2', '20140101', 
    '20201231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, ITEMINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('130000160', 'CP', '브라우마스터 할인쿠폰(PP)', '20131206', 1, 
    '브라우마스터 할인쿠폰(PURPLE)', '0502', '0901', '1', '10000001', 
    50, 17000, 10000000, '2', '20140101', 
    '20201231', 'Y', '1', 7, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('130000161', 'CP', '30만원gift증정쿠폰(BL)', '20131206', 1, 
    '누적구매금액:30만원달성_gift증정쿠폰(BLACK)', '0000', '0901', '2', '30000001', 
    0, 0, 0, '2', '20140101', 
    '20201231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('130000162', 'CP', 'Make-up쿠폰(BL)', '20131206', 1, 
    '신규회원가입시 즉시발급_메이크업 쿠폰(발급일+30일 사용가능)', '0000', '0901', '2', '30000001', 
    0, 0, 0, '2', '20140101', 
    '20201231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000177', 'CP', '3월_온리원50%쿠폰', '20140312', 1, 
    '3월_온리원50%쿠폰(휴면고객대상발급)', '0000', '0905', '2', '10000001', 
    50, 1, 10000000, '2', '20140312', 
    '20140331', 'N', '1', 1, '2', 
    'N', 'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, ITEMINSCNT, 
    CHKUSEFL, OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000184', 'CP', '증정(대상상품有)테슷흐', '20140529', 1, 
    '증정(대상상품有)테슷흐', '0000', '0905', '1', '30000001', 
    0, 0, 100000, '2', '20140529', 
    '20140529', 'N', '1', 1, 1, 
    '2', 'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, ITEMINSCNT, 
    CHKUSEFL, OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000185', 'CP', '픽스미스트증정쿠폰', '20140529', 1, 
    '픽스미스트증정쿠폰
(누리파구매기대상자)', '0000', '0905', '1', '30000001', 
    0, 0, 1000000, '2', '20140601', 
    '20140630', 'N', '1', 2403, 1, 
    '2', 'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000188', 'CP', 'DT.C_20%쿠폰', '20140708', 1, 
    'DT.C_20%쿠폰(클레임응대용)', '0000', '0905', '2', '10000001', 
    20, 1, 10000000, '2', '20140708', 
    '20140731', 'N', '1', 1, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000189', 'CP', '7월_온리원이벤트쿠폰', '20140714', 1, 
    '7월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20140725', 
    '20140727', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, ITEMINSCNT, 
    CHKUSEFL, OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000190', 'CP', '픽스미스트증정쿠폰(누리파구매)', '20140714', 1, 
    '픽스미스트증정쿠폰(누리파구매시)
_누리파재구매event', '0000', '0905', '1', '30000001', 
    0, 32000, 10000000, '2', '20140714', 
    '20140731', 'N', '1', 2492, 2, 
    '2', 'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000196', 'CP', '8월_온리원이벤트쿠폰', '20140825', 1, 
    '8월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20140828', 
    '20140831', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000225', 'CP', '15년5월_10%할인쿠폰(신규고객대상)_테스트', '20150506', 1, 
    '15년5월_10%할인쿠폰(신규고객대상)_테스트', '0000', '0905', '2', '10000001', 
    10, 1, 10000000, '2', '20150506', 
    '20150531', 'N', '1', 2, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000226', 'CP', '15년5월_10%할인쿠폰(신규고객대상)', '20150506', 1, 
    '15년5월_10%할인쿠폰(신규고객대상)', '0000', '0905', '2', '10000001', 
    10, 1, 0, '2', '20150506', 
    '20150531', 'N', '2', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000234', 'CP', '5월_온리원이벤트쿠폰', '20150519', 1, 
    '5월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 0, '2', '20150528', 
    '20150531', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000242', 'CP', '(15년8월)8월온리원이벤트', '20150818', 1, 
    '(15년8월)8월온리원이벤트', '0000', '0901', '2', '10000001', 
    30, 1, 1000000, '2', '20150820', 
    '20150823', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, ITEMINSCNT, 
    CHKUSEFL, OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000245', 'CP', '(15년9월)카운트다운5000원', '20150902', 1, 
    '(15년9월)카운트다운5000원', '0000', '0905', '1', '10000002', 
    5000, 38000, 1000000, '2', '20150903', 
    '20150917', 'N', '2', 4949, 8, 
    '2', 'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000259', 'CP', '(16년)VVIP_샘플킷 증정', '20151125', 1, 
    '(16년)VVIP_샘플킷 증정_매월 1일', '0503', '0901', '2', '30000001', 
    0, 0, 0, '2', '20151125', 
    '20151231', 'N', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000260', 'CP', '(15년12월)강남VVIP_5만원', '20151204', 1, 
    '(15년12월)강남VVIP_5만원', '0000', '0901', '2', '10000002', 
    50000, 1, 1000000, '2', '20151204', 
    '20151231', 'N', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000261', 'CP', '(15년12월)강남VVIP_5만원', '20151204', 1, 
    '(15년12월)강남VVIP_5만원', '0000', '0905', '2', '10000002', 
    50000, 1, 1000000, '2', '20151204', 
    '20151231', 'N', '1', 21, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('160000270', 'CP', '(16년2월)복지쿠폰_350,000', '20160204', 1, 
    '(16년2월)복지쿠폰_350,000', '0000', '0901', '2', '10000002', 
    350000, 1, 1000000, '2', '20160204', 
    '20160229', 'N', '2', '2', 'Y', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('160000271', 'CP', '(16년2월)복지쿠폰_350,000', '20160204', 1, 
    '(16년2월)복지쿠폰_350,000', '0000', '0905', '2', '10000002', 
    350000, 1, 1000000, '2', '20160204', 
    '20160229', 'N', '1', 1, '2', 
    'Y', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('160000272', 'CP', '(16년2월)복지쿠폰_450,000', '20160204', 1, 
    '(16년2월)복지쿠폰_450,000', '0000', '0905', '2', '10000002', 
    450000, 1, 1000000, '2', '20160204', 
    '20160229', 'N', '1', 1, '2', 
    'Y', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('130000157', 'CP', '15만원gift증정쿠폰(PP)', '20131206', 1, 
    '누적구매금액:15만원달성_gift증정쿠폰(PURPLE)', '0502', '0901', '2', '30000001', 
    0, 0, 0, '2', '20140101', 
    '20201231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('130000158', 'CP', '15만원gift증정쿠폰(BL)', '20131206', 1, 
    '누적구매금액:15만원달성_gift증정쿠폰(BLACK_DB 리스트업)', '0000', '0901', '2', '30000001', 
    0, 0, 0, '2', '20140101', 
    '20201231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('130000163', 'CP', '브로우바 서비스쿠폰(BL)', '20131206', 1, 
    '브로우바 서비스쿠폰(BLACK)', '0000', '0901', '2', '30000001', 
    0, 0, 0, '2', '20140101', 
    '20201231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('130000164', 'CP', '회원 생일(B-day)쿠폰', '20131206', 1, 
    '회원 생일(B-day)쿠폰(생일당일발급_사용기간:발급일+30일)', '0000', '0902', '2', '10000001', 
    20, 1, 10000000, '2', '20140101', 
    '20201231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('130000165', 'CP', 'Make-up쿠폰(B-day)', '20131206', 1, 
    '회원 생일(B-day)기념 메이크업쿠폰
(생일당일발급_사용기간:발급일+30일)', '0000', '0902', '2', '30000001', 
    0, 0, 0, '2', '20140101', 
    '20201231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000170', 'CP', '1월_온리원이벤트쿠폰', '20140120', 1, 
    '1월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20140124', 
    '20140126', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000178', 'CP', '3월_온리원이벤트쿠폰', '20140313', 1, 
    '3월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20140328', 
    '20140330', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000182', 'CP', '4월_온리원이벤트쿠폰', '20140417', 1, 
    '4월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20140425', 
    '20140427', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000193', 'CP', 'DT.C 20% 할인쿠폰', '20140807', 1, 
    'DT.C 20% 할인쿠폰(고객응대용)', '0000', '0905', '2', '10000001', 
    20, 1, 10000000, '2', '20140807', 
    '20140831', 'N', '1', 1, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000195', 'CP', '8월_온리원이벤트쿠폰', '20140820', 1, 
    '8월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20140828', 
    '20140831', 'N', '2', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000204', 'CP', '9월_온리원이벤트쿠폰', '20140920', 1, 
    '9월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20140925', 
    '20140928', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000207', 'CP', '(15년)신규회원 할인쿠폰', '20141215', 1, 
    '(15년)신규회원 할인쿠폰_첫 구매 시 10% 할인쿠폰', '0000', '0903', '2', '10000001', 
    10, 1, 10000000, '2', '20150101', 
    '20151231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000208', 'CP', '(15년)VVIP KIT증정쿠폰', '20141215', 1, 
    '(15년)VVIP KIT증정쿠폰_누적금액 30만원 달성 시 - VVIP, VVIP DB 모두 포함', '0503', '0901', '2', '30000001', 
    0, 0, 0, '2', '20150101', 
    '20151231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000209', 'CP', '(15년)M-up시연쿠폰(VVIP)', '20141215', 1, 
    '(15년)M-up시연쿠폰(VVIP)_2개월에 1회 발급_VVIP, VVIP(DB)', '0503', '0901', '2', '30000001', 
    0, 0, 0, '2', '20150101', 
    '20151231', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000210', 'CP', '(15년)회원생일20%할인쿠폰', '20141215', 1, 
    '(15년)회원생일20%할인쿠폰_생일 당일 발급', '0000', '0901', '2', '10000001', 
    20, 1, 100000000, '2', '20150101', 
    '20151231', 'Y', '2', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000211', 'CP', '(15년)생일M-up쿠폰', '20141215', 1, 
    '(15년)생일M-up쿠폰_생일당일 발급', '0000', '0902', '2', '30000001', 
    0, 0, 0, '2', '20150101', 
    '20160131', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000212', 'CP', '(15년)회원생일20%할인쿠폰', '20141215', 1, 
    '(15년)회원생일20%할인쿠폰_생일당일발급', '0000', '0902', '2', '10000001', 
    20, 1, 10000000, '2', '20150101', 
    '20160131', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000215', 'CP', '1월_온리원이벤트쿠폰', '20150113', 1, 
    '1월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 0, '2', '20150122', 
    '20150125', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000241', 'CP', '(15년7월)7월온리원이벤트', '20150720', 1, 
    '(15년7월)7월온리원이벤트', '0000', '0901', '2', '10000001', 
    30, 1, 1000000, '2', '20150723', 
    '20150726', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000246', 'CP', '(15년9월)카운트다운이벤트 5000원할인쿠폰', '20150903', 1, 
    '(15년9월)카운트다운이벤트 5000원할인쿠폰', '0000', '0905', '2', '10000002', 
    5000, 38000, 1000000, '2', '20150903', 
    '20150917', 'N', '1', 4949, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000249', 'CP', '(15년10월)10월온리원이벤트', '20151021', 1, 
    '(15년10월)10월온리원이벤트', '0000', '0901', '2', '10000001', 
    30, 1, 1000000, '2', '20151022', 
    '20151025', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000265', 'CP', '(16년1월)체스앤피스 3000원권', '20151231', 1, 
    '(16년1월)체스앤피스 3000원권', '0000', '0905', '2', '10000002', 
    3000, 1, 1000000, '2', '20151231', 
    '20160131', 'N', '1', 2059, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, ITEMINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('130000153', 'CP', '[누드쿠션1만원할인쿠폰]', '20131206', 1, 
    '[누드쿠션1만원할인쿠폰]', '0000', '0903', '1', '10000002', 
    10000, 1, 10000000, '2', '20140101', 
    '20201231', 'Y', '1', 31, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, ITEMINSCNT, 
    CHKUSEFL, OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000171', 'CP', 'NEW노웨어립카드증정쿠폰', '20140205', 1, 
    'NEW노웨어립카드증정쿠폰', '0000', '0905', '2', '30000001', 
    0, 0, 0, '2', '20140205', 
    '20140228', 'N', '1', 355, 1, 
    '2', 'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000172', 'CP', '2월_온리원이벤트쿠폰', '20140214', 1, 
    '2월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20140221', 
    '20140223', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000183', 'CP', '5월_온리원이벤트쿠폰', '20140516', 1, 
    '5월_온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 10000000, '2', '20140523', 
    '20140525', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000186', 'CP', '온.오프통합쿠폰TEST', '20140611', 1, 
    '온.오프통합쿠폰TEST', '0000', '0905', '2', '10000001', 
    10, 1, 10000000, '2', '20140611', 
    '20140614', 'N', '1', 1, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, ITEMINSCNT, 
    CHKUSEFL, OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('140000192', 'CP', '누쿠_온.오프통합테슷흐쿠폰(나래뀌)', '20140801', 1, 
    '누쿠_온.오프통합테슷흐쿠폰(나래뀌)', '0000', '0905', '1', '10000002', 
    10000, 1, 1000000, '2', '20140801', 
    '20140803', 'N', '1', 1, 35, 
    '2', 'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('140000213', 'CP', '(15년)신규회원웰컴기프트', '20141216', 1, 
    '신규회원 첫 구매시 증정_ 발급일 + 30일 이내 사용', '0000', '0903', '2', '30000001', 
    0, 0, 0, '2', '20150101', 
    '20160131', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000217', 'CP', '14년VVIP_온라인전용_5만원할인쿠폰', '20150309', 1, 
    '14년VVIP_온라인전용_5만원할인쿠폰', '0000', '0901', '2', '10000002', 
    50000, 1, 0, '2', '20150309', 
    '20150430', 'N', '2', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, ITEMINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000230', 'CP', '(15년5월)쿠션30%_대전', '20150514', 1, 
    '(15년5월)쿠션30%_대전', '0000', '0905', '1', '10000001', 
    30, 1, 10000000, '2', '20150515', 
    '20150517', 'N', '1', 45, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000233', 'CP', '(15년5월)온리원이벤트쿠폰', '20150519', 1, 
    '(15년5월)온리원이벤트쿠폰', '0000', '0901', '2', '10000001', 
    30, 1, 0, '2', '20150528', 
    '20150531', 'N', '2', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000240', 'CP', '15년6월_온리원이벤트', '20150623', 1, 
    '15년6월_온리원이벤트', '0000', '0901', '2', '10000001', 
    30, 1, 1000000, '2', '20150625', 
    '20150628', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000243', 'CP', '(15년9월)시크릿M-up고객 10000원할인쿠폰', '20150831', 1, 
    '(15년9월)시크릿M-up고객 10000원할인쿠폰', '0000', '0905', '1', '10000002', 
    10000, 38000, 1000000, '2', '20150901', 
    '20150930', 'N', '2', 76, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('150000244', 'CP', '(15년9월)시크릿m-up쿠폰', '20150831', 1, 
    '(15년9월)시크릿m-up쿠폰 참석자 10000원 할인', '0000', '0905', '2', '10000002', 
    10000, 38000, 1000000, '1', '20150901', 
    '20150930', 'N', '1', 78, '2', 
    'N', 'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000255', 'CP', '(16년)신규회원10%할인쿠폰', '20151125', 1, 
    '(16년)신규회원10%할인쿠폰_ 가입 시 10% 할인', '0000', '0903', '2', '10000001', 
    10, 1, 10000000, '2', '20151125', 
    '20170131', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000256', 'CP', '(16년)VVIP키트 증정쿠폰', '20151125', 1, 
    '(16년)VVIP키트 증정쿠폰', '0503', '0901', '2', '30000001', 
    0, 0, 0, '2', '20151125', 
    '20170131', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('150000257', 'CP', '(16년)VVIP_M-up쿠폰', '20151125', 1, 
    '(16년)VVIP_M-up쿠폰 ( 3개월에 1회)', '0503', '0901', '2', '30000001', 
    0, 0, 0, '2', '20151125', 
    '20170131', 'Y', '1', '2', 'N', 
    'N');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CHKUSEFL, OFF_ONLY_YN, 
    ONLY_ONE_YN)
 Values
   ('160000267', 'CP', '16년1월_온리원이벤트', '20160119', 1, 
    '16년1월_온리원이벤트', '0000', '0901', '2', '10000001', 
    30, 1, 1000000, '2', '20160121', 
    '20160124', 'N', '1', '2', 'N', 
    'Y');
Insert into tb_log_campaign_detail
   (EVTCD, EVTTYPE, EVTNM, ISSUEDT, ISSUECNT, 
    EVTDETAIL, TGCUSGRD, TGCUSGRP, TGPRDFL, USETYPE, 
    USEDETAIL, USECON, USELMT, AUTOFL, STDATE, 
    EDDATE, SMSFL, DELFL, CUSTINSCNT, CHKUSEFL, 
    OFF_ONLY_YN, ONLY_ONE_YN)
 Values
   ('160000269', 'CP', '(16년2월)복지쿠폰', '20160201', 1, 
    '16년2월)복지쿠폰', '0000', '0905', '2', '10000002', 
    300000, 1, 1000000, '2', '20160201', 
    '20160229', 'N', '1', 4, '2', 
    'Y', 'N');
COMMIT;

