tb_log_offline_cst
tb_log_offline_mapping
tb_log_offline_point
tb_log_offline_product
tb_log_offline_sales
tb_log_campaign

select * from tb_log_offline_cst limit 100;
select * from tb_log_offline_mapping limit 100;
select * from tb_log_offline_point limit 100;
select * from tb_log_offline_product limit 100;
select * from tb_log_offline_sales limit 100;
select * from tb_log_campaign limit 100;

drop table if exists tb_log_offline_sales_report_jensen;
create table tb_log_offline_sales_report_jensen as
select 
	stor_cd,
	sale_dt,
	pos_no,
	bill_no,
	seq,
	sale_div,
	extract(hour from sord_tm)||':'||extract(Minute from sord_tm)||':'||extract(second from sord_tm) as sord_time,
	extract(hour from deliver_tm)||':'||extract(Minute from deliver_tm)||':'||extract(second from deliver_tm) as deliver_time,
	extract(hour from sale_tm)||':'||extract(Minute from sale_tm)||':'||extract(second from sale_tm) as sale_time,
	item_cd,
	main_item_cd,
	pack_div,
	free_div,
	dc_div,
	sale_prc as sale_price,
	sale_qty,  
	sale_amt,
	dc_rate,
	dc_amt, 
	enr_amt,
	grd_amt,
	net_amt,
	vat_rate,
	vat_amt,
	coupon_cd,
	campaign_cd,
	campaign_cust_tp,
	campaign_tp,
	campaign_tg_div,
	campaign_dc_s,
	campaign_sale_qty,
	campaign_sale_amt,
	saler_dt,
	barcode,
	cust_id	
from tb_log_offline_sales;

select * from tb_log_offline_sales_report_jensen limit 100;