--##online

drop table if exists tb_log_coupon;
create table tb_log_coupon(
server_time timestamp,
oncp_code varchar(50),
cp_name varchar(100),
dc_type varchar(50),
dc_amount float,
start_date timestamp,
end_date timestamp
);

drop table if exists tb_log_order;
create table tb_log_order(
server_time timestamp,
pc_id varchar(50),
user_id varchar(200),
oncp_code varchar(50),
ofcp_code varchar(100),
item_id varchar(50),
sap_code varchar(50),
quantity int,
order_price float,
device varchar(10)
);

drop table if exists tb_log_user;
create table tb_log_user(
pc_id varchar(50),
user_id varchar(100),
gender varchar(50),
class varchar(50),
signup_date timestamp,
order_amount float
);

drop table if exists tb_log_product;
create table tb_log_product(
item_id varchar(50),
item_name varchar(200),
original_price float,
sale_price float,
dc_price float,
category1 varchar(50),
category2 varchar(50)
);

drop table if exists tb_product_info;tb_log_order
create table tb_product_info(
item_name varchar(200),
sap_code varchar(50),
sale_price float,
category1 varchar(50),
category2 varchar(50),
category3 varchar(50)
);


COPY tb_log_coupon FROM '/Volumes/RYANKIM/SI_consulting/espoir/coupon.csv' DELIMITER ',' CSV HEADER;
COPY tb_log_order FROM '/Volumes/RYANKIM/SI_consulting/espoir/order.csv' DELIMITER ',' CStb_log_userV HEADER;tb_log_product
COPY tb_log_user FROM '/Volumes/RYANKIM/SI_consulting/espoir/user.csv' DELIMITER ',' CSV HEADER;
COPY tb_log_product FROM '/Volumes/RYANKIM/SI_consulting/espoir/product.csv' DELIMITER ',' CSV HEADER;
COPY tb_product_info FROM '/Volumes/RYANKIM/SI_consulting/espoir/product_info.csv' DELIMITER ',' CSV HEADER;tb_product_info




--##offline
drop table if exists tb_log_offline_cst;
create table tb_log_offline_cst(
CSTMID varchar(200),
ROUTECD varchar(50),
NATIONFL varchar(200),
GENDER varchar(50),
GNRT int,
HOMEZIP1 varchar(200),
HOMEZIP2 varchar(200),
ADDR1 varchar(200),
SKINTPCD varchar(50),
SKINTRBLCD1 varchar(50),
EMAILSENDFL varchar(50),
SMSSENDFL varchar(50),
DMSENDFL varchar(50),
CSTMUSEFL varchar(200),
MBRFL varchar(200),
DELMLGFL varchar(200),
APEMPFL varchar(200),
DELFL varchar(200),
PRTNRID varchar(200),
FNLPURDATE timestamp,
FNLMLGDATE timestamp,
JOINDATE timestamp,
MLGFL varchar(200),
UNTYCSTMID varchar(200),
HJSENDFL varchar(200),
CSTMLVL varchar(50),
WEBID varchar(200),
BDAY timestamp,
UCSTMID varchar(200),
EAIFL varchar(2000),
TRG_CHK varchar(100)
);

drop table if exists tb_log_offline_product;
create table tb_log_offline_product(
BRAND_CD varchar(200),
ITEM_CD varchar(200),
TRNSCTPRCLISTTP varchar(200),
STDUNITPRC INT,
USE_YN varchar(50)
);

drop table if exists tb_log_offline_mapping;
create table tb_log_offline_mapping(
CSTMID varchar(200),
UCSTMID varchar(200),
PCSTMID varchar(200),
ETWEBID varchar(200) 
);

drop table if exists tb_log_offline_point;
create table tb_log_offline_point(
UCSTMID varchar(200),
TOTACCUMPT INT,
TOTRDMPT INT, 
TOTREMAINPTT INT,
EAIFL varchar(50)
);

drop table if exists tb_log_offline_sales;
create table tb_log_offline_sales(
STOR_CD varchar(200),
SALE_DT timestamp,
POS_NO varchar(50),
BILL_NO varchar(200),
SEQ varchar(200),
SALE_DIV varchar(50),
SORD_TM timestamp,
DELIVER_TM timestamp,
SALE_TM timestamp,
ITEM_CD varchar(50),
MAIN_ITEM_CD varchar(200),
PACK_DIV varchar(50),
FREE_DIV int,
DC_DIV int,
SALE_PRC float,
SALE_QTY int,
SALE_AMT float,
DC_RATE float,
DC_AMT float,
ENR_AMT float,
GRD_AMT float,
NET_AMT float,
VAT_RATE float,
VAT_AMT float,
COUPON_CD varchar(200),
CAMPAIGN_CD varchar(200),
CAMPAIGN_CUST_TP int,
CAMPAIGN_TP int,
CAMPAIGN_TG_DIV int,
CAMPAIGN_DC_S int,
CAMPAIGN_SALE_QTY int,
CAMPAIGN_SALE_AMT float,
SALER_DT timestamp,
BARCODE varchar(200),
CUST_ID varchar(200)
);

drop table if exists tb_log_offline_campaign;
create table tb_log_offline_campaign(
EVTCD varchar(2000),
CSTMID varchar(200),
ISSUENO varchar(50),
USEDATE timestamp,
USEPRTNRID varchar(200),
TOTSALAMT float,
NETSALAMT float,
DCAMT float,
ADDMLG float,
PRTNRTPCD varchar(100),
SMSFL varchar(50),
STDATE timestamp,
EDDATE timestamp,
DELFL varchar(50),
UCSTMID varchar(200)
);

drop table if exists tb_log_offline_point_detail;
create table tb_log_offline_point_detail(
USEDATE timestamp,
UCSTMID varchar(200),
PTSEQ varchar(50),
CHCD varchar(50),
CHCSTMID varchar(200),
PRTNRID int,
PRTNRNM varchar(200),
PURQTY int,
PURAMT float,
ACCUMPT int,
RDMPT int,
PTCD varchar(200),
PTTP varchar(200),
PTRSN varchar(200),
CHSEQ int,
DPREMAINPT int,
USEREMAINPT int,
TRMNSCHDDT timestamp
);


COPY tb_log_offline_cst FROM '/Volumes/RYANKIM/SI_consulting/espoir_offline_new/offline_cst_new.csv' DELIMITER ',' CSV HEADER;
COPY tb_log_offline_product FROM '/Volumes/RYANKIM/SI_consulting/espoir_offline_new/offline_product_new.csv' DELIMITER ',' CSV HEADER;
COPY tb_log_offline_mapping FROM '/Volumes/RYANKIM/SI_consulting/espoir_offline_new/online_offline_mapping_new.csv' DELIMITER ',' CSV HEADER;
COPY tb_log_offline_point FROM '/Volumes/RYANKIM/SI_consulting/espoir_offline_new/offline_point_new.csv' DELIMITER ',' CSV HEADER;
COPY tb_log_offline_sales FROM '/Volumes/RYANKIM/SI_consulting/espoir_offline_new/offline_sale1.csv' DELIMITER ',' CSV HEADER;
COPY tb_log_offline_sales FROM '/Volumes/RYANKIM/SI_consulting/espoir_offline_new/offline_sale2.csv' DELIMITER ',' CSV HEADER;
COPY tb_log_offline_sales FROM '/Volumes/RYANKIM/SI_consulting/espoir_offline_new/offline_sale3.csv' DELIMITER ',' CSV HEADER;
COPY tb_log_offline_campaign FROM '/Volumes/RYANKIM/SI_consulting/offline_coupon1.csv' DELIMITER ',' CSV HEADER;

COPY tb_log_offline_point_detail FROM '/Volumes/RYANKIM/SI_consulting/espoir_offline_new/offline_point_detail1.csv' DELIMITER ',' CSV HEADER;
COPY tb_log_offline_point_detail FROM '/Volumes/RYANKIM/SI_consulting/espoir_offline_new/offline_point_detail2.csv' DELIMITER ',' CSV HEADER;
COPY tb_log_offline_point_detail FROM '/Volumes/RYANKIM/SI_consulting/espoir_offline_new/offline_point_detail3.csv' DELIMITER ',' CSV HEADER;








