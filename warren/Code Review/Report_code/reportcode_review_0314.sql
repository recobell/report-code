						###########################################################
						##					 리포트 지표 코드 리뷰					 ##
						###########################################################

- Refine Table 
Before making the 10 released indexes on our admin page, we refine a few tables first right after copying it from
S3 to Redshift. These tables helps us to easily combine/merge raw view, order, product_info and even search tables
together to get the outcome we want to.

########################################################################################################
########################################################################################################

###########
## DAILY ##
###########

drop table if exists tb_log_view_daily_report;
create table tb_log_view_daily_report as
select
    server_time,
    convert_timezone('KST', server_time)::date as date, 
    (case when device = 'PW' then 'PW' else 'MW' end) as device, 
    user_agent,
    pc_id, 
    session_id, 
    gc_id, 
    rc_code,
    item_id, 
    search_term
from 
tb_log_view;

drop table if exists tb_log_search_daily_report;
create table tb_log_search_daily_report as
select
    server_time,
    convert_timezone('KST', server_time)::date as date, 
    (case when device = 'PW' then 'PW' else 'MW' end) as device, 
    user_agent,
    pc_id, 
    session_id, 
    gc_id, 
    rc_code,
    search_term
from 
tb_log_search;

drop table if exists tb_log_order_daily_report;
create table tb_log_order_daily_report as
select
    server_time,
    convert_timezone('KST', server_time)::date as date, 
    (case when device = 'PW' then 'PW' 
	  when device = 'PC Web' then 'PW'
	  when device = 'pw' then 'PW' else 'MW' end) as device, 
    user_agent,
    pc_id, 
    session_id, 
    gc_id, 
    rc_code,
    item_id, 
    search_term, 
    order_id, 
    order_price,
    price, 
    quantity
from 
tb_log_order;

drop table if exists tb_product_info;
create table tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0
 group by item_id;

drop table if exists tb_product_info_report;
CREATE TABLE tb_product_info_report as
select
    item_id, 
    item_name, 
    original_price, 
    sale_price, 
    category1, 
    category2, 
    category3, 
    category4, 
    category5, 
    reg_date
    stock
from 
tb_product_info;

drop table if exists tmp_recommend_result_view;
CREATE TABLE tmp_recommend_result_view as
select 
    date
    ,pc_id 
    ,device
    ,count(*) as view
    ,sum(case when rc_code is not null then 1 else 0 end) as reco_view
    ,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date, pc_id, device;

drop table if exists tmp_recommend_result_order;
CREATE TABLE tmp_recommend_result_order as
select
    date
    ,pc_id 
    ,device
    
    ,sum(price*quantity) revenue
    ,sum(case when rc_code is not null then price*quantity else 0 end) reco_revenue
    ,sum(case when rc_code is null then price*quantity else 0 end) no_reco_revenue
    
    ,count(*) frequency
    ,count(distinct case when rc_code is not null then order_id else null end) reco_frequency
    ,count(distinct case when rc_code is null then order_id else null end) no_reco_frequency
    
from tb_log_order_daily_report
group by date, pc_id, device;

drop table if exists tmp_recommend_result_view_session;
CREATE TABLE tmp_recommend_result_view_session as
select
    date
    ,session_id 
    ,device
    ,count(*) as view
    ,sum(case when rc_code is not null then 1 else 0 end) as reco_view
    ,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date, session_id, device;

drop table if exists tmp_recommend_result_order_session;
CREATE TABLE tmp_recommend_result_order_session as
select
    A.date
    ,A.session_id 
    ,A.order_id
    ,A.device
    
    ,sum(A.price*A.quantity) revenue
    ,sum(case when B.reco_view > 0 then A.price*A.quantity else 0 end) reco_revenue
    ,sum(case when B.reco_view = 0 or B.reco_view is null then A.price*A.quantity else 0 end) no_reco_revenue
    
    ,sum(distinct A.order_price) actual_payment
    ,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
    ,sum(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_price else 0 end) no_reco_actual_payment
    
    ,sum(A.quantity) quantity
    ,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
    ,sum(case when B.reco_view = 0 or B.reco_view is null then A.quantity else 0 end) no_reco_quantity
    
    ,count(distinct A.item_id) itemvar
    ,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
    ,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.item_id end) no_reco_itemvar
    
    ,count(distinct A.order_id) frequency
    ,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
    ,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_id end) no_reco_frequency
    
from tb_log_order_daily_report A
left join tmp_recommend_result_view_session B on A.session_id = B.session_id and A.date = B.date
group by A.date,A.session_id,A.order_id,A.device;

drop table if exists tb_recommend_loyal_group;
CREATE TABLE tb_recommend_loyal_group AS SELECT * FROM
    tmp_recommend_result_view_session
WHERE
    view > 1;

drop table if exists tb_recommend_reco_group;
CREATE TABLE tb_recommend_reco_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
where reco_view > 0;

#############
## MONTHLY ##
#############

drop table if exists tb_log_view_monthly_report;
create table tb_log_view_monthly_report as
select
    server_time,
    convert_timezone('KST', server_time)::date - extract(day from convert_timezone('KST', server_time))+1 as date, 
    (case when device = 'PW' then 'PW' else 'MW' end) as device, 
    user_agent,
    pc_id, 
    session_id, 
    gc_id, 
    rc_code,
    item_id, 
    search_term
from 
tb_log_view;

drop table if exists tb_log_search_monthly_report;
create table tb_log_search_monthly_report as
select
    server_time,
    convert_timezone('KST', server_time)::date - extract(day from convert_timezone('KST', server_time))+1 as date, 
    (case when device = 'PW' then 'PW' else 'MW' end) as device,
    user_agent,
    pc_id, 
    session_id, 
    gc_id, 
    rc_code,
    search_term
from 
tb_log_search;

drop table if exists tb_log_order_monthly_report;
create table tb_log_order_monthly_report as
select
    server_time,
    convert_timezone('KST', server_time)::date - extract(day from convert_timezone('KST', server_time))+1 as date, 
    (case when device = 'PW' then 'PW' 
	  when device = 'PC Web' then 'PW'
	  when device = 'pw' then 'PW' else 'MW' end) as device,
    user_agent,
    pc_id, 
    session_id, 
    gc_id, 
    rc_code,
    item_id, 
    search_term, 
    order_id, 
    order_price,
    price, 
    quantity
from 
tb_log_order;

drop table if exists tb_product_info;
create table tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0
 group by item_id;

drop table if exists tb_product_info_report;
CREATE TABLE tb_product_info_report as
select
    item_id, 
    item_name, 
    original_price, 
    sale_price, 
    category1, 
    category2, 
    category3, 
    category4, 
    category5, 
    reg_date
    stock
from 
tb_product_info;

drop table if exists tmp_recommend_result_view;
CREATE TABLE tmp_recommend_result_view as
select 
    date
    ,pc_id 
    ,device
    ,count(*) as view
    ,sum(case when rc_code is not null then 1 else 0 end) as reco_view
    ,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_monthly_report
group by date, pc_id, device;

drop table if exists tmp_recommend_result_order;
CREATE TABLE tmp_recommend_result_order as
select
    date
    ,pc_id 
    ,device
    
    ,sum(price*quantity) revenue
    ,sum(case when rc_code is not null then price*quantity else 0 end) reco_revenue
    ,sum(case when rc_code is null then price*quantity else 0 end) no_reco_revenue
    
    ,count(*) frequency
    ,count(distinct case when rc_code is not null then order_id else null end) reco_frequency
    ,count(distinct case when rc_code is null then order_id else null end) no_reco_frequency
    
from tb_log_order_monthly_report
group by date, pc_id, device;

drop table if exists tmp_recommend_result_view_session;
CREATE TABLE tmp_recommend_result_view_session as
select
    date
    ,session_id 
    ,device
    ,count(*) as view
    ,sum(case when rc_code is not null then 1 else 0 end) as reco_view
    ,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_monthly_report
group by date,session_id, device;

drop table if exists tmp_recommend_result_order_session;
CREATE TABLE tmp_recommend_result_order_session as
select
    A.date
    ,A.session_id 
    ,A.order_id
    ,A.device
    
    ,sum(A.price*A.quantity) revenue
    ,sum(case when B.reco_view > 0 then A.price*A.quantity else 0 end) reco_revenue
    ,sum(case when B.reco_view = 0 or B.reco_view is null then A.price*A.quantity else 0 end) no_reco_revenue
    
    ,sum(distinct A.order_price) actual_payment
    ,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
    ,sum(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_price else 0 end) no_reco_actual_payment
    
    ,sum(A.quantity) quantity
    ,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
    ,sum(case when B.reco_view = 0 or B.reco_view is null then A.quantity else 0 end) no_reco_quantity
    
    ,count(distinct A.item_id) itemvar
    ,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
    ,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.item_id end) no_reco_itemvar
    
    ,count(distinct A.order_id) frequency
    ,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
    ,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_id end) no_reco_frequency
    
from tb_log_order_monthly_report A
left join tmp_recommend_result_view_session B on A.session_id = B.session_id and A.date = B.date
group by A.date,A.session_id,A.order_id,A.device;

drop table if exists tb_recommend_loyal_group;
CREATE TABLE tb_recommend_loyal_group AS SELECT * FROM
    tmp_recommend_result_view_session
WHERE
    view > 1;

drop table if exists tb_recommend_reco_group;
CREATE TABLE tb_recommend_reco_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
where reco_view > 0;

########################################################################################################
########################################################################################################


1. Quality Index
   quantity index는 추천 영역이 사이트 내에 얼마나 많은 영향을 미치는 지표로서, 사이트 내에서 추천 영역이 얼마나 많이 확보되었나를 의미하는 지표이다. 
   추천 노출이 많이 되어 클릭이 많이 될수록 해당 지표가 늘어나게 된다. 각 지표마다 추천영역 클릭과 연관된 지표들과 전체 지표들이 보여지고, 그 둘의 비율을 
   보여줌으로써, 추천 영역의 양적으로 얼마나 사이트에 영향을 미치고 있는지 확인할 수 있다.

UV (없애는 방향)
 -> def : user visit , 중복 포함하지 않는 유저 방문 수
 -> data
     -> date : 날짜 (월별일 경우 해당 월의 첫일(ex: 2015-11-01), 일별일 경우 해당 날짜)
     -> uv : 전체 유저 수
	 ->	reco_uv : 하루에 추천을 한번 이상 클릭한 유저 수
	 -> ratio : reco_uv/uv

########################################################################################################
########################################################################################################

###########
## DAILY ##
###########

drop table if exists tb_recommend_result_uv_daily_device;
CREATE TABLE tb_recommend_result_uv_daily_device AS
select
    date
    ,device
    ,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv
    
from tmp_recommend_result_view
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_uv_daily_total;
CREATE TABLE tb_recommend_result_uv_daily_total AS
select
    date
    ,'AL' device
    ,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv
from tmp_recommend_result_view
group by 1
order by 1;

drop table if exists tb_recommend_result_uv_daily_s1;
CREATE TABLE tb_recommend_result_uv_daily_s1 AS
select 
	* from tb_recommend_result_uv_daily_device
union all
select 
	* from tb_recommend_result_uv_daily_total;

drop table if exists tb_recommend_result_uv_daily;
CREATE TABLE tb_recommend_result_uv_daily AS
select
    *
    ,(case when uv <> 0 then round( reco_uv :: float / uv :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_uv_daily_s1;

#############
## MONTHLY ##
#############

drop table if exists tb_recommend_result_uv_monthly_device;
CREATE TABLE tb_recommend_result_uv_monthly_device AS
select
    date
    ,device
    ,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv    
from tmp_recommend_result_view
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_uv_monthly_total;
CREATE TABLE tb_recommend_result_uv_monthly_total AS
select
    date
    ,'AL' device
    ,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv
from tmp_recommend_result_view
group by 1
order by 1;

drop table if exists tb_recommend_result_uv_monthly_s1;
CREATE TABLE tb_recommend_result_uv_monthly_s1 AS
select 
	* from tb_recommend_result_uv_monthly_device
union all
select 
	* from tb_recommend_result_uv_monthly_total;

drop table if exists tb_recommend_result_uv_monthly;
CREATE TABLE tb_recommend_result_uv_monthly AS
select
    *
    ,(case when uv <> 0 then round( reco_uv :: float / uv :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_uv_monthly_s1;

########################################################################################################
########################################################################################################

Visit
 -> def : session visit , 중복 포함하는 유저 방문 수 (세션 수) 
 ->	data
	 ->	date : 날짜 (월별일 경우 해당 월의 첫일(ex: 2015-11-01), 일별일 경우 해당 날짜)
	 -> visit : 전체 세션 방문 수
	 ->	reco_visit : 세션 중 추천을 한번 이상 클릭한 세션의 수
	 -> ratio : reco_visit/ visit

########################################################################################################
########################################################################################################

###########
## DAILY ##
###########

drop table if exists tb_recommend_result_visit_daily_device;
CREATE TABLE tb_recommend_result_visit_daily_device AS
select
    date
    ,device
    ,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
    ,count(distinct case when view > 1 then session_id end) loyal_visit  
    
    
from tmp_recommend_result_view_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_visit_daily_total;
CREATE TABLE tb_recommend_result_visit_daily_total AS
select
    date
    ,'AL' device
    ,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
    ,count(distinct case when view > 1 then session_id end) loyal_visit  
    
    
from tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_visit_daily_s1;
CREATE TABLE tb_recommend_result_visit_daily_s1 AS
select 
	* from tb_recommend_result_visit_daily_device
union all
select 
	* from tb_recommend_result_visit_daily_total;

drop table if exists tb_recommend_result_visit_daily;
CREATE TABLE tb_recommend_result_visit_daily AS
select
    *
    ,(case when visit <> 0 then round( reco_visit :: float / visit :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_visit_daily_s1;

#############
## MONTHLY ##
#############

drop table if exists tb_recommend_result_visit_monthly_device;
CREATE TABLE tb_recommend_result_visit_monthly_device AS
select
    date
    ,device
    ,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
    ,count(distinct case when view > 1 then session_id end) loyal_visit  
    
    
from tmp_recommend_result_view_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_visit_monthly_total;
CREATE TABLE tb_recommend_result_visit_monthly_total AS
select
    date
    ,'AL' device
    ,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
    ,count(distinct case when view > 1 then session_id end) loyal_visit  
from tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_visit_monthly_s1;
CREATE TABLE tb_recommend_result_visit_monthly_s1 AS
select 
	* from tb_recommend_result_visit_monthly_device
union all
select 
	* from tb_recommend_result_visit_monthly_total;

drop table if exists tb_recommend_result_visit_monthly;
CREATE TABLE tb_recommend_result_visit_monthly AS
select
    *
    ,(case when visit <> 0 then round( reco_visit :: float / visit :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_visit_monthly_s1;

########################################################################################################
########################################################################################################

PV 
 -> def : page view , 다른 사이트와는 다르게, 사이트 내 '상품' 클릭 수를 나타냄
 -> data
	 ->	date : 날짜 (월별일 경우 해당 월의 첫일(ex: 2015-11-01), 일별일 경우 해당 날짜)
	 ->	pv : 전체 상품 클릭 수
	 ->	reco_pv : 전체 추천 영역에 나타난 상품 클릭 수 
	 ->	ratio : pv/reco_pv

########################################################################################################
########################################################################################################

###########
## DAILY ##
###########

drop table if exists tb_recommend_result_pv_daily_device;
CREATE TABLE tb_recommend_result_pv_daily_device AS
select
    date
    ,device
    ,sum(view) pv
    ,sum(reco_view) reco_pv
    ,sum(no_reco_view) no_reco_pv
    
from tmp_recommend_result_view_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_pv_daily_total;
CREATE TABLE tb_recommend_result_pv_daily_total AS
select
    date
    ,'AL' device
    ,sum(view) pv
    ,sum(reco_view) reco_pv
    ,sum(no_reco_view) no_reco_pv
    
from tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_pv_daily_s1;
CREATE TABLE tb_recommend_result_pv_daily_s1 AS
select 
	* from tb_recommend_result_pv_daily_device
union all
select 
	* from tb_recommend_result_pv_daily_total;

drop table if exists tb_recommend_result_pv_daily;
CREATE TABLE tb_recommend_result_pv_daily AS
select
    *
    ,(case when pv <> 0 then round( reco_pv :: float / pv :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_pv_daily_s1;

#############
## MONTHLY ##
#############

drop table if exists tb_recommend_result_pv_monthly_device;
CREATE TABLE tb_recommend_result_pv_monthly_device AS
select
    date
    ,device
    ,sum(view) pv
    ,sum(reco_view) reco_pv
    ,sum(no_reco_view) no_reco_pv
    
from tmp_recommend_result_view_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_pv_monthly_total;
CREATE TABLE tb_recommend_result_pv_monthly_total AS
select
    date
    ,'AL' device
    ,sum(view) pv
    ,sum(reco_view) reco_pv
    ,sum(no_reco_view) no_reco_pv    
from tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_pv_monthly_s1;
CREATE TABLE tb_recommend_result_pv_monthly_s1 AS
select 
	* from tb_recommend_result_pv_monthly_device
union all
select 
	* from tb_recommend_result_pv_monthly_total;

drop table if exists tb_recommend_result_pv_monthly;
CREATE TABLE tb_recommend_result_pv_monthly AS
select
    *
    ,(case when pv <> 0 then round( reco_pv :: float / pv :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_pv_monthly_s1;

########################################################################################################
########################################################################################################

frequency
-> def : 구매 건수
-> data
-> date : 날짜 (월별일 경우 해당 월의 첫일(ex: 2015-11-01), 일별일 경우 해당 날짜)
-> frequency : 전체 상품 구매건수
-> reco_frequency : 전체 추천 영역에 상품 구매 건수 
-> ratio : frequency/reco_frequency

########################################################################################################
########################################################################################################

###########
## DAILY ##
###########

drop table if exists tb_recommend_result_frequency_daily_device;
CREATE TABLE tb_recommend_result_frequency_daily_device AS
select
    date
    ,device
    ,sum(frequency) frequency
    ,sum(reco_frequency) reco_frequency 
    ,sum(no_reco_frequency) no_reco_frequency         
from tmp_recommend_result_order_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_frequency_daily_total;
CREATE TABLE tb_recommend_result_frequency_daily_total AS
select
    date
    ,'AL' device
    ,sum(frequency) frequency
    ,sum(reco_frequency) reco_frequency 
    ,sum(no_reco_frequency) no_reco_frequency 
from tmp_recommend_result_order_session
group by 1
order by 1;

drop table if exists tb_recommend_result_frequency_daily_s1;
CREATE TABLE tb_recommend_result_frequency_daily_s1 AS
select 
	* from tb_recommend_result_frequency_daily_device
union all
select 
	* from tb_recommend_result_frequency_daily_total;

drop table if exists tb_recommend_result_frequency_daily;
CREATE TABLE tb_recommend_result_frequency_daily AS
select
    *
    ,(case when frequency <> 0 then round( reco_frequency :: float / frequency :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_frequency_daily_s1;

#############
## MONTHLY ##
#############

drop table if exists tb_recommend_result_frequency_monthly_device;
CREATE TABLE tb_recommend_result_frequency_monthly_device AS
select
    date
    ,device
    ,sum(frequency) frequency
    ,sum(reco_frequency) reco_frequency 
    ,sum(no_reco_frequency) no_reco_frequency 
from tmp_recommend_result_order_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_frequency_monthly_total;
CREATE TABLE tb_recommend_result_frequency_monthly_total AS
select
    date
    ,'AL' device
    ,sum(frequency) frequency
    ,sum(reco_frequency) reco_frequency 
    ,sum(no_reco_frequency) no_reco_frequency 
from tmp_recommend_result_order_session
group by 1
order by 1;

drop table if exists tb_recommend_result_frequency_monthly_s1;
CREATE TABLE tb_recommend_result_frequency_monthly_s1 AS
select 
	* from tb_recommend_result_frequency_monthly_device
union all
select 
	* from tb_recommend_result_frequency_monthly_total;

drop table if exists tb_recommend_result_frequency_monthly;
CREATE TABLE tb_recommend_result_frequency_monthly AS
select
    *
    ,(case when frequency <> 0 then round( reco_frequency :: float / frequency :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_frequency_monthly_s1;

########################################################################################################
########################################################################################################

Revenue
 -> def: 매출
 -> data
	 ->	date : 날짜 (월별일 경우 해당 월의 첫일(ex: 2015-11-01), 일별일 경우 해당 날짜)
	 ->	revenue: 전체 매출
	 ->	reco_revenue: 세션 중 추천 상품을 한번 이상 클릭한 세션에서 일어나는 매출
	 ->	ratio: reco_revenue/ revenue

########################################################################################################
########################################################################################################

###########
## DAILY ##
###########

drop table if exists tb_recommend_result_revenue_daily_device;
CREATE TABLE tb_recommend_result_revenue_daily_device AS
select
    date
    ,device
    ,sum(revenue) revenue
    ,sum(reco_revenue) reco_revenue    
    ,sum(no_reco_revenue) no_reco_revenue 
    ,sum(case when session_id is not null then revenue end) loyal_revenue    
    
from tmp_recommend_result_order_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_revenue_daily_total;
CREATE TABLE tb_recommend_result_revenue_daily_total AS
select
    date
    ,'AL' device
    ,sum(revenue) revenue
    ,sum(reco_revenue) reco_revenue    
    ,sum(no_reco_revenue) no_reco_revenue 
    ,sum(case when session_id is not null then revenue end) loyal_revenue    
    
from tmp_recommend_result_order_session
group by 1
order by 1;

drop table if exists tb_recommend_result_revenue_daily_s1;
CREATE TABLE tb_recommend_result_revenue_daily_s1 AS
select 
	* from tb_recommend_result_revenue_daily_device
union all
select 
	* from tb_recommend_result_revenue_daily_total;

drop table if exists tb_recommend_result_revenue_daily;
CREATE TABLE tb_recommend_result_revenue_daily AS
select
    *
    ,(case when revenue <> 0 then round( reco_revenue :: float / revenue :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_revenue_daily_s1;

#############
## MONTHLY ##
#############

drop table if exists tb_recommend_result_revenue_monthly_device;
CREATE TABLE tb_recommend_result_revenue_monthly_device AS
select
    date
    ,device
    ,sum(revenue) revenue
    ,sum(reco_revenue) reco_revenue    
    ,sum(no_reco_revenue) no_reco_revenue 
    ,sum(case when session_id is not null then revenue end) loyal_revenue    
    
from tmp_recommend_result_order_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_revenue_monthly_total;
CREATE TABLE tb_recommend_result_revenue_monthly_total AS
select
    date
    ,'AL' device
    ,sum(revenue) revenue
    ,sum(reco_revenue) reco_revenue    
    ,sum(no_reco_revenue) no_reco_revenue 
    ,sum(case when session_id is not null then revenue end) loyal_revenue    
    
from tmp_recommend_result_order_session
group by 1
order by 1;

drop table if exists tb_recommend_result_revenue_monthly_s1;
CREATE TABLE tb_recommend_result_revenue_monthly_s1 AS
select 
	* from tb_recommend_result_revenue_monthly_device
union all
select 
	* from tb_recommend_result_revenue_monthly_total;

drop table if exists tb_recommend_result_revenue_monthly;
CREATE TABLE tb_recommend_result_revenue_monthly AS
select
    *
    ,(case when revenue <> 0 then round( reco_revenue :: float / revenue :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_revenue_monthly_s1;

########################################################################################################
########################################################################################################

2. Quality Index
   quality index는 추천 영역이 사이트 UI/UX 및 이동 경로를 최적화시켜 좋은방향으로 얼마나 향상시켰나를 보여주는 지표로써, 추천 서비스가 사이트에 
   얼마나 긍정적인 영향을 미치고 있는 보여주는 지표이다. 해당 지표가 좋아질 수록 추천 서비스의 질이 좋아진다고 볼 수 있다. 각 지표마다 추천영역 클릭과 
   연관된 지표들과 전체 지표들이 보여지고, 그 둘의 비율을 보여줌으로써, 추천 영역의 질적으로 얼마나 사이트에 영향을 미치고 있는지 확인할 수 있다.

Prevenue
 -> def: 객단가 (주문당 평균 구매 금액)
 -> data
	 ->	date : 날짜 (월별일 경우 해당 월의 첫일(ex: 2015-11-01), 일별일 경우 해당 날짜)
	 ->	prevenue : 전체 주문에 대한 평균 구매 금액 
	 ->	reco_prevenue : 추천 상품을 클릭한 세션에 대해서 전체 주문에 대한 평균 구매 금액
	 ->	no_reco_prevenue : 추천 상품을 한번도 클릭하지 않은 세션에 대해서 전체 주문에 대한 평균 구매 금액
	 ->	reco_up_ratio : reco_prevenue/ prevenue

########################################################################################################
########################################################################################################

###########
## DAILY ##
###########

drop table if exists tb_recommend_result_prevenue_daily_s1_device;
CREATE TABLE tb_recommend_result_prevenue_daily_s1_device AS
select
    A.date
    ,A.device
    ,round(sum(A.revenue)/sum(A.frequency),2) prevenue
    ,round(sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end),2) reco_prevenue
    ,round(sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end),2) no_reco_prevenue
    ,round(sum(case when B.session_id is not null then A.revenue end)/sum(case when B.session_id is not null  then A.frequency end),2) loyal_prevenue
    
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_prevenue_daily_s1_total;
CREATE TABLE tb_recommend_result_prevenue_daily_s1_total AS
select
    A.date
    ,'AL' device
    
    ,round(sum(A.revenue)/sum(A.frequency),2) prevenue
    ,round(sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end),2) reco_prevenue
    ,round(sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end),2) no_reco_prevenue
    ,round(sum(case when B.session_id is not null then A.revenue end)/sum(case when B.session_id is not null  then A.frequency end),2) loyal_prevenue
    
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_prevenue_daily_s1;
CREATE TABLE tb_recommend_result_prevenue_daily_s1 AS
select 
	* from tb_recommend_result_prevenue_daily_s1_device
union all
select 
	* from tb_recommend_result_prevenue_daily_s1_total;


drop table if exists tb_recommend_result_prevenue_daily;
CREATE TABLE tb_recommend_result_prevenue_daily AS SELECT *,
    (case when prevenue <> 0  then round(reco_prevenue :: float / (prevenue :: float), 4) * 100 else null end) as reco_up_ratio
FROM tb_recommend_result_prevenue_daily_s1;

#############
## MONTHLY ##
#############

drop table if exists tb_recommend_result_prevenue_monthly_s1_device;
CREATE TABLE tb_recommend_result_prevenue_monthly_s1_device AS
select
    A.date
    ,A.device
    ,round(sum(A.revenue)/sum(A.frequency),2) prevenue
    ,round(sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end),2) reco_prevenue
    ,round(sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end),2) no_reco_prevenue
    ,round(sum(case when B.session_id is not null then A.revenue end)/sum(case when B.session_id is not null  then A.frequency end),2) loyal_prevenue
    
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_prevenue_monthly_s1_total;
CREATE TABLE tb_recommend_result_prevenue_monthly_s1_total AS
select
    A.date
    ,'AL' device
    ,round(sum(A.revenue)/sum(A.frequency),2) prevenue
    ,round(sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end),2) reco_prevenue
    ,round(sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end),2) no_reco_prevenue
    ,round(sum(case when B.session_id is not null then A.revenue end)/sum(case when B.session_id is not null  then A.frequency end),2) loyal_prevenue
    
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_prevenue_monthly_s1;
CREATE TABLE tb_recommend_result_prevenue_monthly_s1 AS
select 
	* from tb_recommend_result_prevenue_monthly_s1_device
union all
select 
	* from tb_recommend_result_prevenue_monthly_s1_total;

drop table if exists tb_recommend_result_prevenue_monthly;
CREATE TABLE tb_recommend_result_prevenue_monthly AS SELECT *,
    (case when prevenue <> 0  then round(reco_prevenue :: float / (prevenue :: float), 4) * 100 else null end) as reco_up_ratio
FROM tb_recommend_result_prevenue_monthly_s1;

########################################################################################################
########################################################################################################

Quantity
 -> def: 주문당 평균구매수량
 -> data
	 ->	date : 날짜 (월별일 경우 해당 월의 첫일(ex: 2015-11-01), 일별일 경우 해당 날짜)
	 ->	quantity : 전체 세션에 대한 주문당 평균구매수량
	 ->	reco_quantity : 추천 클릭한 세션에 대한 주문당 평균구매수량
	 ->	no_reco_quantity : 추천 클릭을 한번도 하지 않은 세션에 대한 주문당 평균구매수량
	 ->	reco_up_ratio : reco_quantity/ quantity

########################################################################################################
########################################################################################################

###########
## DAILY ##
###########

drop table if exists tb_recommend_result_quantity_daily_s1_device;
CREATE TABLE tb_recommend_result_quantity_daily_s1_device AS
select
    A.date, A.device
    ,round(avg(A.quantity :: float),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: float end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: float end),3) no_reco_quantity
    ,round(avg(case when B.session_id is not null then A.quantity :: float end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_quantity_daily_s1_total;
CREATE TABLE tb_recommend_result_quantity_daily_s1_total AS
select
    A.date
    ,'AL' device
    ,round(avg(A.quantity :: float),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: float end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: float end),3) no_reco_quantity
    ,round(avg(case when B.session_id is not null then A.quantity :: float end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_quantity_daily_s1;
CREATE TABLE tb_recommend_result_quantity_daily_s1 AS
select 
	* from tb_recommend_result_quantity_daily_s1_device
union all
select 
	* from tb_recommend_result_quantity_daily_s1_total;


drop table if exists tb_recommend_result_quantity_daily;
CREATE TABLE tb_recommend_result_quantity_daily AS SELECT *,
    (case when quantity <> 0  then round(reco_quantity :: float / (quantity :: float), 4) * 100 else null end) as reco_up_ratio
FROM tb_recommend_result_quantity_daily_s1;

#############
## MONTHLY ##
#############

drop table if exists tb_recommend_result_quantity_monthly_s1_device;
CREATE TABLE tb_recommend_result_quantity_monthly_s1_device AS
select
    A.date, A.device
    ,round(avg(A.quantity :: float),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: float end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: float end),3) no_reco_quantity
    ,round(avg(case when B.session_id is not null then A.quantity :: float end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_quantity_monthly_s1_total;
CREATE TABLE tb_recommend_result_quantity_monthly_s1_total AS
select
    A.date
    ,'AL' device
    ,round(avg(A.quantity :: float),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: float end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: float end),3) no_reco_quantity
    ,round(avg(case when B.session_id is not null then A.quantity :: float end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_quantity_monthly_s1;
CREATE TABLE tb_recommend_result_quantity_monthly_s1 AS
select 
	* from tb_recommend_result_quantity_monthly_s1_device
union all
select 
	* from tb_recommend_result_quantity_monthly_s1_total;

drop table if exists tb_recommend_result_quantity_monthly;
CREATE TABLE tb_recommend_result_quantity_monthly AS SELECT *,
    (case when quantity <> 0  then round(reco_quantity :: float / (quantity :: float), 4) * 100 else null end) as reco_up_ratio
FROM tb_recommend_result_quantity_monthly_s1;

########################################################################################################
########################################################################################################

Click
 -> def: 세션당 평균 조회 상품수(중복포함)
 -> data
	 ->	date : 날짜 (월별일 경우 해당 월의 첫일(ex: 2015-11-01), 일별일 경우 해당 날짜)
	 ->	click : 전체 세션에 대한 평균 상품 클릭 수
	 ->	reco_click : 추천 상품을 클릭한 세션에 대하여 조회한 상품수
	 ->	no_reco_click : 추천 상품을 한번도 클릭하지 않은 세션에 대하여 조회한 상품수 
	 ->	reco_up_ratio : reco_click/ click

########################################################################################################
########################################################################################################

###########
## DAILY ##
###########

drop table if exists tb_recommend_result_click_daily_s1_device;
CREATE TABLE tb_recommend_result_click_daily_s1_device AS 
SELECT 
    date, device
    ,round(AVG(view :: float),3) click
    ,round(AVG(case when reco_view>0 then view :: float end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: float end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: float  end),3) loyal_click
FROM
    tmp_recommend_result_view_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_click_daily_s1_total;
CREATE TABLE tb_recommend_result_click_daily_s1_total AS 
SELECT 
    date,'AL' device
    ,round(AVG(view :: float),3) click
    ,round(AVG(case when reco_view>0 then view :: float end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: float end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: float  end),3) loyal_click
FROM
    tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_click_daily_s1;
CREATE TABLE tb_recommend_result_click_daily_s1 AS 
select 
	* from tb_recommend_result_click_daily_s1_device
union all
select 
	* from tb_recommend_result_click_daily_s1_total;

drop table if exists tb_recommend_result_click_daily;
CREATE TABLE tb_recommend_result_click_daily AS SELECT *,
    (case when click <> 0 then round(reco_click / (click), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_click_daily_s1;   

#############
## MONTHLY ##
#############

drop table if exists tb_recommend_result_click_monthly_s1_device;
CREATE TABLE tb_recommend_result_click_monthly_s1_device AS 
SELECT 
    date, device
    ,round(AVG(view :: float),3) click
    ,round(AVG(case when reco_view>0 then view :: float end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: float end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: float  end),3) loyal_click
FROM
    tmp_recommend_result_view_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_click_monthly_s1_total;
CREATE TABLE tb_recommend_result_click_monthly_s1_total AS 
SELECT 
    date,'AL' device
    ,round(AVG(view :: float),3) click
    ,round(AVG(case when reco_view>0 then view :: float end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: float end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: float  end),3) loyal_click
FROM
    tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_click_monthly_s1;
CREATE TABLE tb_recommend_result_click_monthly_s1 AS 
select 
	* from tb_recommend_result_click_monthly_s1_device
union all
select 
	* from tb_recommend_result_click_monthly_s1_total;

drop table if exists tb_recommend_result_click_monthly;
CREATE TABLE tb_recommend_result_click_monthly AS SELECT *,
    (case when click <> 0 then round(reco_click / (click), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_click_monthly_s1;   

########################################################################################################
########################################################################################################

CVR
 -> def: 구매전환율
 -> data
	 ->	date : 날짜 (월별일 경우 해당 월의 첫일(ex: 2015-11-01), 일별일 경우 해당 날짜)
	 ->	cvr : 전체 구매전환율
	 ->	reco_cvr : 추천 상품을 클릭한 세션에 대하여 구매전환율
	 ->	no_reco_cvr : 추천 상품을 한번도 클릭하지 않은 세션에 대하여 구매전환율
	 ->	reco_up_ratio : reco_cvr/ cvr

########################################################################################################
########################################################################################################

###########
## DAILY ##
###########

drop table if exists tb_recommend_result_cvr_daily_s1_device;
CREATE TABLE tb_recommend_result_cvr_daily_s1_device AS 
select 
    A.date, A.device
    
    ,round(sum(B.frequency :: float)/count(distinct A.session_id),4)*100 cvr
    ,round(sum(case when A.reco_view >0 then B.frequency :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),4)*100 reco_cvr
    ,round(sum(case when A.reco_view =0 then B.frequency :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),4)*100 no_reco_cvr
    ,round(sum(case when A.view >1 then B.frequency :: float end)/count(distinct case when A.view >1 then A.session_id end),4)*100 loyal_cr
    
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date  and A.session_id = B.session_id
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_cvr_daily_s1_total;
CREATE TABLE tb_recommend_result_cvr_daily_s1_total AS 
select 
    A.date, 'AL' device
    
    ,round(sum(B.frequency :: float)/count(distinct A.session_id),4)*100 cvr
    ,round(sum(case when A.reco_view >0 then B.frequency :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),4)*100 reco_cvr
    ,round(sum(case when A.reco_view =0 then B.frequency :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),4)*100 no_reco_cvr
    ,round(sum(case when A.view >1 then B.frequency :: float end)/count(distinct case when A.view >1 then A.session_id end),4)*100 loyal_cr
    
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date  and A.session_id = B.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_cvr_daily_s1;
CREATE TABLE tb_recommend_result_cvr_daily_s1 AS 
select 
	* from tb_recommend_result_cvr_daily_s1_device
union all
select 
	* from tb_recommend_result_cvr_daily_s1_total;

drop table if exists tb_recommend_result_cvr_daily;
CREATE TABLE tb_recommend_result_cvr_daily AS SELECT *,
    (case when cvr <> 0 then round(reco_cvr / (cvr), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_cvr_daily_s1;   

#############
## MONTHLY ##
#############

drop table if exists tb_recommend_result_cvr_monthly_s1_device;
CREATE TABLE tb_recommend_result_cvr_monthly_s1_device AS 
select 
    A.date, A.device
    ,round(sum(B.frequency :: float)/count(distinct A.session_id),4)*100 cvr
    ,round(sum(case when A.reco_view >0 then B.frequency :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),4)*100 reco_cvr
    ,round(sum(case when A.reco_view =0 then B.frequency :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),4)*100 no_reco_cvr
    ,round(sum(case when A.view >1 then B.frequency :: float end)/count(distinct case when A.view >1 then A.session_id end),4)*100 loyal_cr
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date  and A.session_id = B.session_id
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_cvr_monthly_s1_total;
CREATE TABLE tb_recommend_result_cvr_monthly_s1_total AS 
select 
    A.date, 'AL' device
    ,round(sum(B.frequency :: float)/count(distinct A.session_id),4)*100 cvr
    ,round(sum(case when A.reco_view >0 then B.frequency :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),4)*100 reco_cvr
    ,round(sum(case when A.reco_view =0 then B.frequency :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),4)*100 no_reco_cvr
    ,round(sum(case when A.view >1 then B.frequency :: float end)/count(distinct case when A.view >1 then A.session_id end),4)*100 loyal_cr
    from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date  and A.session_id = B.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_cvr_monthly_s1;
CREATE TABLE tb_recommend_result_cvr_monthly_s1 AS 
select 
	* from tb_recommend_result_cvr_monthly_s1_device
union all
select 
	* from tb_recommend_result_cvr_monthly_s1_total;

drop table if exists tb_recommend_result_cvr_monthly;
CREATE TABLE tb_recommend_result_cvr_monthly AS SELECT *,
    (case when cvr <> 0 then round(reco_cvr / (cvr), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_cvr_monthly_s1;   

########################################################################################################
########################################################################################################

SA
 -> def: 세션당 평균 매출
 -> data
	 ->	date : 날짜 (월별일 경우 해당 월의 첫일(ex: 2015-11-01), 일별일 경우 해당 날짜)
	 ->	sa : 전체 세션에 대해서 세션당 평균 매출
	 ->	reco_sa : 추천 상품을 클릭한 세션에 대해서 세션당 평균 매출
	 ->	no_reco_sa : 추천 상품을 한번도 클릭하지 않은 세션에 대하여 세션당 평균 매출
	 ->	reco_up_ratio : reco_visit/ visit 

########################################################################################################
########################################################################################################

###########
## DAILY ##
###########

drop table if exists tb_recommend_result_sa_daily_s1_device;
CREATE TABLE tb_recommend_result_sa_daily_s1_device AS 
select 
    A.date, A.device   
    ,round(sum(B.revenue :: float)/count(distinct A.session_id),3) sa
    ,round(sum(case when A.reco_view >0 then B.revenue :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
    ,round(sum(case when A.reco_view =0 then B.revenue :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
    ,round(sum(case when A.view >1 then B.revenue :: float end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_sa
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.session_id = B.session_id
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_sa_daily_s1_total;
CREATE TABLE tb_recommend_result_sa_daily_s1_total AS 
select 
    A.date, 'AL' device   
    ,round(sum(B.revenue :: float)/count(distinct A.session_id),3) sa
    ,round(sum(case when A.reco_view >0 then B.revenue :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
    ,round(sum(case when A.reco_view =0 then B.revenue :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
    ,round(sum(case when A.view >1 then B.revenue :: float end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_sa  
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.session_id = B.session_id
group by 1
order by 1;

#############
## MONTHLY ##
#############

drop table if exists tb_recommend_result_sa_monthly_s1_device;
CREATE TABLE tb_recommend_result_sa_monthly_s1_device AS 
select 
    A.date, A.device
    ,round(sum(B.revenue :: float)/count(distinct A.session_id),3) sa
    ,round(sum(case when A.reco_view >0 then B.revenue :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
    ,round(sum(case when A.reco_view =0 then B.revenue :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
    ,round(sum(case when A.view >1 then B.revenue :: float end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_sa
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.session_id = B.session_id
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_sa_monthly_s1_total;
CREATE TABLE tb_recommend_result_sa_monthly_s1_total AS 
select 
    A.date, 'AL' device   
    ,round(sum(B.revenue :: float)/count(distinct A.session_id),3) sa
    ,round(sum(case when A.reco_view >0 then B.revenue :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
    ,round(sum(case when A.reco_view =0 then B.revenue :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
    ,round(sum(case when A.view >1 then B.revenue :: float end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_sa   
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.session_id = B.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_sa_monthly_s1;
CREATE TABLE tb_recommend_result_sa_monthly_s1 AS 
select 
	* from tb_recommend_result_sa_monthly_s1_device
union all
select 
	* from tb_recommend_result_sa_monthly_s1_total;

drop table if exists tb_recommend_result_sa_monthly;
CREATE TABLE tb_recommend_result_sa_monthly AS SELECT *,
    (case when sa <> 0  then round(reco_sa / (sa), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_sa_monthly_s1; 

########################################################################################################
########################################################################################################
