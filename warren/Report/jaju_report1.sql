drop table if exists rec_click;
CREATE TABLE rec_click AS
SELECT 
    avg(case when device = 'PW' then view end)::float pc_click
    ,avg(case when device <> 'PW' then view end)::float mo_click

    ,avg(case when device = 'PW' and reco_view>0 then view end)::float pc_reco_click
    ,avg(case when device <> 'PW' and reco_view>0 then view end)::float mo_reco_click

    ,avg(case when device = 'PW' and reco_view=0 and no_reco_view > 4 then view end)::float pc_no_reco_five_click
    ,avg(case when device <> 'PW' and reco_view=0 and no_reco_view > 4 then view end)::float mo_no_reco_five_click
    
    ,avg(case when device = 'PW' and reco_view=0 and no_reco_view > 3 then view end)::float pc_no_reco_four_click
    ,avg(case when device <> 'PW' and reco_view=0 and no_reco_view > 3 then view end)::float mo_no_reco_four_click

    ,avg(case when device = 'PW' and reco_view=0 and no_reco_view > 2 then view end)::float pc_no_reco_three_click
    ,avg(case when device <> 'PW' and reco_view=0 and no_reco_view > 2 then view end)::float mo_no_reco_three_click

    ,avg(case when device = 'PW' and reco_view=0 and no_reco_view > 1 then view end)::float pc_no_reco_two_click
    ,avg(case when device <> 'PW' and reco_view=0 and no_reco_view > 1 then view end)::float mo_no_reco_two_click

    ,avg(case when device = 'PW' and reco_view=0 and no_reco_view > 0 then view end)::float pc_no_reco_one_click
    ,avg(case when device <> 'PW' and reco_view=0 and no_reco_view > 0 then view end)::float mo_no_reco_one_click

FROM
    tmp_recommend_result_view_session;


SELECT *,
    round(pc_reco_click / pc_no_reco_one_click, 4)::float as pc_click_up_ratio,
    round(mo_reco_click / mo_no_reco_one_click, 4)::float as mo_click_up_ratio FROM
    rec_click;   
    
drop table if exists tb_uv;
CREATE TABLE tb_uv AS
select
	extract(month from date) as month,
    count(case when device = 'PW' then session_id end) pc_uv
    ,count(case when device <> 'PW' then session_id end) mo_uv
    ,count(case when device = 'PW' and reco_view > 0 then session_id end) pc_reco_uv
    ,count(case when device <> 'PW' and reco_view > 0 then session_id end) mo_reco_uv
    ,count(case when device = 'PW' and reco_view = 0 and no_reco_view > 0 then session_id end) pc_no_reco_uv
    ,count(case when device <> 'PW' and reco_view = 0 and no_reco_view > 0 then session_id end) mo_no_reco_uv
from tmp_recommend_result_view_session
group by extract(month from date);


drop table if exists tb_freq;
CREATE TABLE tb_freq AS
select
    extract(month from date) as month,
    sum(case when device = 'PW' then frequency end) pc_freq
    ,sum(case when device <> 'PW' then frequency end) mo_freq
    ,sum(case when device = 'PW' then reco_frequency end) pc_reco_freq
    ,sum(case when device <> 'PW' then reco_frequency end) mo_reco_freq    
    ,sum(case when device = 'PW' then no_reco_frequency end) pc_no_reco_freq
    ,sum(case when device <> 'PW' then no_reco_frequency end) mo_no_reco_freq
from tmp_recommend_result_order_session
group by 1
order by 1;


drop table if exists tb_freq;
CREATE TABLE tb_freq AS SELECT
    round(SUM(CASE WHEN device = 'PW' THEN revenue END)::float / sum(CASE WHEN  device = 'PW' THEN frequency END)::float,0) pc_prevenue
    ,round(SUM(CASE WHEN device <> 'PW' THEN revenue END)::float / sum(CASE WHEN device <> 'PW' THEN frequency END)::float,0) mo_prevenue
    ,round(SUM(CASE WHEN device = 'PW' THEN reco_revenue END)::float / sum(CASE WHEN device = 'PW'  THEN reco_frequency END)::float,0) pc_reco_prevenue
    ,round(SUM(CASE WHEN device <> 'PW' THEN reco_revenue END)::float / sum(CASE WHEN device <> 'PW'  THEN reco_frequency END)::float,0) mo_reco_prevenue
    ,round(SUM(CASE WHEN device = 'PW' THEN no_reco_revenue END)::float / sum(CASE WHEN device = 'PW'  THEN no_reco_frequency END)::float,0) pc_no_reco_prevenue
    ,round(SUM(CASE WHEN device <> 'PW' THEN no_reco_revenue END)::float / sum(CASE WHEN device <> 'PW'  THEN no_reco_frequency END)::float,0) mo_no_reco_prevenue
    FROM
    tmp_recommend_result_order_session;


drop table if exists tb_freq_01;
CREATE TABLE tb_freq_01 AS SELECT 
    A.*
    ,round(pc_reco_prevenue / pc_no_reco_prevenue,4) pc_prevenue_up_ratio
    ,round(mo_reco_prevenue / mo_no_reco_prevenue,4) mo_prevenue_up_ratio FROM
    tb_freq A;

#추천을 클릭한 사람과 클릭하지 않은 사람 사이의 구매전환율 비교(모바일/pc 분리)
drop table if exists tb_cr_s1;
CREATE TABLE tb_cr_s1 AS
select 
	A.month,
    round(B.pc_freq/NULLIF(A.pc_uv,0),4) pc_cr
    ,round(B.mo_freq/NULLIF(A.mo_uv,0),4) mo_cr
    ,round(B.pc_reco_freq/NULLIF(A.pc_reco_uv,0),4) pc_reco_cr
    ,round(B.mo_reco_freq/NULLIF(A.mo_reco_uv,0),4) mo_reco_cr
    ,round(B.pc_no_reco_freq/NULLIF(A.pc_no_reco_uv,0),4) pc_no_reco_cr
    ,round(B.mo_no_reco_freq/NULLIF(A.mo_no_reco_uv,0),4) mo_no_reco_cr
from tb_uv A
	left join
	tb_freq B on a.month = b.month;

drop table if exists tbs_consult.tb_recommend_territory_result_cr;
CREATE TABLE tbs_consult.tb_recommend_territory_result_cr AS
select 
    *, round(pc_reco_cr/NULLIF(pc_no_reco_cr,0),4) pc_cr_up_ratio
    , round(mo_reco_cr/NULLIF(mo_no_reco_cr,0),4) mo_cr_up_ratio
from tb_cr_s1;

drop table if exists tb_loyal_s1;
CREATE TABLE tb_loyal_s1 AS 
SELECT 
    date
    ,pc_id
    ,device
    ,count(case when rc_code is not null then pc_id end) loyal_count
    ,count(case when rc_code is null then pc_id end) no_loyal_count
FROM
tb_log_view_monthly_report
group by date, pc_id, device;

drop table if exists tb_loyal_group_S2;
CREATE TABLE tb_loyal_group_S2 AS
SELECT 
    date
    ,pc_id
    ,device
    ,count(price*quantity) frequency
    ,sum(price*quantity) revenue
    ,sum(case when rc_code is null then price*quantity end) no_reco_revenue
    ,sum(case when rc_code is not null then price*quantity end) reco_revenue
    ,count(case when rc_code is not null then pc_id end) loyal_count
FROM
tb_log_order_monthly_report
group by date, pc_id, device;

drop table if exists tb_loyal_group;
CREATE TABLE tb_loyal_group AS SELECT * FROM
    tb_loyal_s1
WHERE
    loyal_count=0
    AND
    no_loyal_count > 1;

drop table if exists tbs_consult.tb_recommend_territory_result_loyal_prevenue;
CREATE TABLE tbs_consult.tb_recommend_territory_result_loyal_prevenue AS
SELECT 
    round(avg(case when A.device = 'PW' then B.revenue end)) PC_loyal_prevenue
    ,round(avg(case when A.device <>'PW' then B.revenue end)) MO_loyal_prevenue
FROM
    (SELECT * from tb_loyal_group) A
        inner JOIN
    (select * from tb_loyal_group_S2) B ON A.date = B.date AND A.pc_id = B.pc_id;
    
