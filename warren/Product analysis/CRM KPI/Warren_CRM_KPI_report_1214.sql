--@CRM KPI 
--@CRM KPI based on Warren data
--@RFM 지수

--********
--monthly
--********

drop table if exists tb_member;
create table tb_member as
    SELECT 
        pc_id pc_id
        ,ad_id ad_id
        ,(CASE
                WHEN user_agent LIKE '%iPhone%' THEN 'iPhone'
                ELSE 'Android'
            END) AS platform
    FROM
        tb_log_view 
    GROUP BY 1,2,3; 
    


drop table if exists tb_order;
create table tb_order as
    SELECT 
        a.pc_id,
        FLOOR(SUM(a.price)) total_revenue,
        FLOOR(AVG(a.price)) avg_revenue,
        COUNT(*) frequency,
        SUM((CASE WHEN b.rc_code IS NOT NULL THEN a.price ELSE 0 END)) reco_revenue, 
        SUM((CASE WHEN b.ad_code IS NOT NULL THEN a.price ELSE 0 END)) adcode_revenue
    
    FROM
        tb_log_order a
    INNER JOIN
    tb_log_view b ON a.pc_id = b.pc_id AND a.item_id = b.item_id
    GROUP BY a.pc_id
    ORDER BY a.pc_id;



--drop table if exists tb_p_fav_cat;
--create table tb_p_fav_cat as
--SELECT 
--        a.pc_id,
--            MAX(CASE
--                WHEN rank = 1 THEN category
--                ELSE NULL
--            END) main_favorite_p_category,
--            ROUND(MAX(CASE
--                WHEN rank = 1 THEN gross_revenue
--               ELSE NULL
--            END) / MAX(num_orders), 4) main_favorite_p_category_ratio,
--            MAX(CASE
--                WHEN rank = 2 THEN category
--                ELSE NULL
--            END) second_favorite_p_category,
--            ROUND(MAX(CASE
--                WHEN rank = 2 THEN gross_revenue
--                ELSE NULL
--           END) / MAX(num_orders), 4) second_favorite_p_category_ratio,
--            MAX(rank) p_category_diversity
--    FROM
--        (SELECT 
--        a.*,
--            @p_fav_cat_rank:=(IF(pcid = @p_fav_cat_prev_member, @p_fav_cat_current_member_cnt:=@p_fav_cat_current_member_cnt + 1, @p_fav_cat_current_member_cnt:=1)) AS rank,
--            @p_fav_cat_num_orders:=(IF(pcid = @p_fav_cat_prev_member, @p_fav_cat_current_main_deal_cnt:=@p_fav_cat_current_main_deal_cnt + gross_revenue, @p_fav_cat_current_main_deal_cnt:=gross_revenue)) AS num_orders,
--            @p_fav_cat_prev_member:=pcid AS tmp_member
--    FROM
--        (SELECT 
--        a.pc_id,
--            b.category1 category,
--            SUM(b.price * b.quantity) gross_revenue
--    FROM
--        tb_log_order a
--    LEFT JOIN tmp_product_info b ON a.item_id = b.item_id
--    GROUP BY 1 , 2
--    ORDER BY 1 , 3 DESC) a) a
--    GROUP BY 1;


drop table if exists tb_p_fav_cat;
create table tb_p_fav_cat as
SELECT 
        a.pc_id,
        MAX(CASE
                WHEN rank = 1 THEN category
                ELSE NULL
                END) main_favorite_p_category,
        MAX(CASE
                WHEN rank = 1 THEN gross_revenue
                ELSE NULL 
        END)::real/sum(gross_revenue)::real main_favorite_p_category_ratio,
        MAX(CASE
                WHEN rank = 2 THEN category
                ELSE NULL
                END) second_favorite_p_category,
        MAX(CASE
                WHEN rank = 2 THEN gross_revenue
                ELSE NULL
                END)::real/sum(gross_revenue)::real second_favorite_p_category_ratio,
        MAX(rank) p_category_diversity
FROM
    (SELECT
        *, row_number() over(partition by pc_id ORDER BY gross_revenue DESC) AS rank
    FROM
        (SELECT 
            a.pc_id,
            b.category1 category,
            SUM(a.price * a.quantity) gross_revenue
        FROM
            tb_log_order a
            LEFT JOIN tmp_product_info b ON a.item_id = b.item_id
            GROUP BY 1 , 2) 

        ORDER BY rank ASC) a
GROUP BY 1;



drop table if exists tb_c_fav_cat;
create table tb_c_fav_cat as
SELECT 
        a.pc_id,
        MAX(CASE
                WHEN rank = 1 THEN category
                ELSE NULL
                END) main_favorite_c_category,
        MAX(CASE
                WHEN rank = 1 THEN view_count
                ELSE NULL 
                END)::real/sum(view_count)::real main_favorite_c_category_ratio,  
        MAX(CASE
                WHEN rank = 2 THEN category
                ELSE NULL
                END) second_favorite_c_category,
        MAX(CASE
                WHEN rank = 2 THEN view_count
                ELSE NULL
                END)::real/sum(view_count)::real second_favorite_c_category_ratio,    
        MAX(rank) c_category_diversity
 
   FROM(SELECT
        *, row_number() over(partition by pc_id ORDER BY view_count DESC) AS rank

    FROM
        (SELECT 
            a.pc_id, b.category1 category, COUNT(*) view_count
        FROM
            tb_log_view a
            INNER JOIN tb_product_info b ON a.item_id = b.item_id
        GROUP BY 1 , 2
    ORDER BY 1 , 3 DESC) a) a
GROUP BY 1;




drop table if exists tb_churn;
create table tb_churn as
SELECT 
        server_time,
            pc_id,
            MIN(sday) first_purchase,
            MAX(next_sday) last_purchase,
            DATEDIFF(DAY, MAX(next_sday), CURRENT_DATE) recency,
            COUNT(*) frequency,
            AVG(purchase_interval) avg_purchase_interval,
            MIN(purchase_interval) min_purchase_interval,
            MAX(purchase_interval) max_purchase_interval,
            (CASE
                WHEN DATEDIFF(DAY, MAX(next_sday), CURRENT_DATE) >= MAX(purchase_interval) * 1.5 THEN 5
                WHEN DATEDIFF(DAY, MAX(next_sday), CURRENT_DATE) >= MAX(purchase_interval) THEN 4
                WHEN DATEDIFF(DAY, MAX(next_sday), CURRENT_DATE) >= AVG(purchase_interval) THEN 3
                WHEN DATEDIFF(DAY, MAX(next_sday), CURRENT_DATE) < AVG(purchase_interval) THEN 2
                WHEN DATEDIFF(DAY, MAX(next_sday), CURRENT_DATE) < 7 THEN 1
                ELSE 0
            END) churn_level
    FROM
        (SELECT 
        a.server_time,
        a.pc_id,
        a.sday,
        MIN(b.next_sday) next_sday,
        DATEDIFF(DAY, a.sday, b.next_sday) purchase_interval
    FROM
        (SELECT 
            server_time, pc_id, server_time sday
        FROM
            tb_log_order
        ORDER BY pc_id, server_time DESC) a
            INNER JOIN (SELECT 
                    server_time, pc_id, server_time next_sday
                    FROM
                    tb_log_order
        ORDER BY pc_id,server_time DESC) b ON a.pc_id = b.pc_id
        WHERE
            a.sday < b.next_sday
        GROUP BY a.server_time, b.next_sday, a.pc_id , a.sday) a
    GROUP BY server_time, pc_id
HAVING (COUNT(*) >= 2);
       
    
drop table if exists tb_time_slot;
create table tb_time_slot AS
SELECT
    A.pc_id
    ,COUNT(DISTINCT TIME_SLOT) time_slot_diversity
    ,MAX(CASE 
        WHEN rank = 1 THEN time_slot 
        ELSE NULL 
        END) best_time_slot
    ,MAX(CASE
                WHEN rank = 2 THEN BUY_MAIN_DEAL
                ELSE NULL
                END)::real/sum(BUY_MAIN_DEAL)::real time_slot_ratio
FROM
    (SELECT
        A.*, row_number() over(partition by pc_id order by BUY_MAIN_DEAL desc) as rank

    FROM
        (SELECT 
            a.pc_id pc_id,
            (CASE
                WHEN a.p_hour IN (02 , 03, 04, 05, 06, 07) THEN 'T_02'
                WHEN a.p_hour IN (08 , 09) THEN 'T_08'
                WHEN a.p_hour IN (10 , 11, 12) THEN 'T_10'
                WHEN a.p_hour IN (13 , 14, 15) THEN 'T_13'
                WHEN a.p_hour IN (16 , 17) THEN 'T_16'
                WHEN a.p_hour IN (18 , 19, 20, 21, 22) THEN 'T_18'
                WHEN a.p_hour IN (23 , 00, 01) THEN 'T_23'
            END) TIME_SLOT
            ,count(*) BUY_MAIN_DEAL
    FROM
        (SELECT 
            pc_id,
            EXTRACT(HOUR FROM convert_timezone('KST', server_time)) as p_hour
        FROM
            tb_log_order
        ORDER BY pc_id) a
        group by 1,2
order by 1,3 desc) A) A 
GROUP BY 1;
    
    
    
drop table if exists tb_last_click;
create table tb_last_click as  
SELECT
    a.pc_id, 
    MAX((CASE WHEN number = 1 THEN a.item_id ELSE NULL END)) recent_click_product, 
    COUNT(DISTINCT number) number_click_product
    
FROM
    (SELECT
        a.*,
        ROW_NUMBER() OVER(PARTITION BY pc_id ORDER BY item_id DESC) AS number
    --@recent_click_num_prd:=(if(a.pcid=@recent_click_prev_pid, @recent_click_current_pid_cnt:=@recent_click_current_pid_cnt+1, @recent_click_current_pid_cnt:=1)) as number, 
    --@recent_click_prev_pid:=a.pcid as tmp_pid 
    FROM
        (SELECT
            pc_id,  
            item_id
        FROM tb_log_view
--@group by pcid
        --where server_time >= CURRENT_DATE + INTERVAL '-1 day'
        ORDER BY pc_id,server_time DESC) a)a
GROUP BY 1;

   


drop table if exists tb_last_purchase;
create table tb_last_purchase as 
SELECT
    a.pc_id, 
    MAX((CASE WHEN number = 1 THEN a.item_id ELSE NULL END)) recent_purchase_product, 
    COUNT(DISTINCT number) number_purchase_product
    
FROM
    (SELECT
        a.*,
        ROW_NUMBER() OVER(PARTITION BY pc_id ORDER BY item_id DESC) AS number
    --@recent_purchase_num_prd:=(if(a.pcid=@recent_purchase_prev_pid, @recent_purchase_current_pid_cnt:=@recent_purchase_current_pid_cnt+1, @recent_purchase_current_pid_cnt:=1)) as number, 
    --@recent_purchase_prev_pid:=a.pcid as tmp_pid 
    FROM
        (SELECT
            a.server_time,
            a.pc_id pc_id,  
            a.item_id
        FROM tb_log_order a
        INNER JOIN  tb_product_info b
        ON a.item_id = b.item_id
--#FROM mcr_rblog.tb_log_order_item
--#group by pcid
--where a.server_time >= @TIME_CUT_OFF_1M
ORDER BY a.pc_id,a.server_time DESC) a)a
GROUP BY 1;




--@OUTPUT TABLE  


drop table if exists tb_crm_kpi;
create table tb_crm_kpi as 
SELECT 
    S_MEMBER.pc_id pcid,
    S_MEMBER.ad_id adid,
    S_MEMBER.platform,
    
    
    (CASE WHEN S_ORDER.total_revenue IS NULL THEN 0 ELSE S_ORDER.total_revenue END) total_revenue,
    (CASE WHEN S_ORDER.avg_revenue IS NULL THEN 0 ELSE S_ORDER.avg_revenue END) avg_revenue,
    (CASE WHEN S_ORDER.frequency IS NULL THEN 0 ELSE S_ORDER.frequency END) frequency,
    (CASE WHEN S_ORDER.reco_revenue IS NULL THEN 0 ELSE S_ORDER.reco_revenue END) reco_revenue,
    (CASE WHEN S_ORDER.adcode_revenue IS NULL THEN 0 ELSE S_ORDER.adcode_revenue END) adcode_revenue,
    --#S_ORDER.total_revenue,
    --#S_ORDER.avg_revenue,
    --#S_ORDER.frequency,`
    --#S_ORDER.reco_revenue,
    --#S_ORDER.adcode_revenue,
    
    S_P_FAV_CAT.main_favorite_p_category,
    S_P_FAV_CAT.main_favorite_p_category_ratio,
    S_P_FAV_CAT.second_favorite_p_category,
    S_P_FAV_CAT.second_favorite_p_category_ratio,
    S_P_FAV_CAT.p_category_diversity,
    
    
    S_C_FAV_CAT.main_favorite_c_category,
    S_C_FAV_CAT.main_favorite_c_category_ratio,
    S_C_FAV_CAT.second_favorite_c_category,
    S_C_FAV_CAT.second_favorite_c_category_ratio,
    S_C_FAV_CAT.c_category_diversity,
    
    
    S_CHURN.first_purchase,
    S_CHURN.last_purchase,
    S_CHURN.recency,
    S_CHURN.avg_purchase_interval,
    S_CHURN.min_purchase_interval,
    S_CHURN.max_purchase_interval,
    S_CHURN.churn_level,
    
    
    S_TIME_SLOT.time_slot_diversity,
    S_TIME_SLOT.best_time_slot,
    S_TIME_SLOT.time_slot_ratio,
    
    S_LAST_CLICK.recent_click_product, 
    S_LAST_CLICK.number_click_product,
    
    S_LAST_PURCHASE.recent_purchase_product, 
    S_LAST_PURCHASE.number_purchase_product
    
FROM tb_member S_MEMBER
LEFT JOIN tb_order S_ORDER ON S_MEMBER.pc_id = S_ORDER.pc_id
LEFT JOIN tb_p_fav_cat S_P_FAV_CAT ON S_ORDER.pc_id = S_P_FAV_CAT.pc_id
LEFT JOIN tb_c_fav_cat S_C_FAV_CAT ON S_ORDER.pc_id = S_C_FAV_CAT.pc_id
LEFT JOIN tb_churn S_CHURN ON S_MEMBER.pc_id = S_CHURN.pc_id
LEFT JOIN tb_time_slot S_TIME_SLOT on S_MEMBER.pc_id = S_TIME_SLOT.pc_id
LEFT JOIN tb_last_click S_LAST_CLICK on S_MEMBER.pc_id = S_LAST_CLICK.pc_id
LEFT JOIN tb_last_purchase S_LAST_PURCHASE on S_MEMBER.pc_id = S_LAST_PURCHASE.pc_id;


