
drop table if exists tb_log_view_201601;
create table tb_log_view_201601 as
select
	pc_id
	,session_id
	,item_id
	,user_id
	,device
	,convert_timezone('KST',server_time) server_time
	,rc_code
from tb_log_view
where extract(month from convert_timezone('KST',server_time)) = 1;

select case when rc_code is not null then 1 else 0 end, count(*) from tb_log_view_201601 group by 1;

select rc_code, count(*) from (select session_id, max(case when rc_code is not null then 1 else 0 end) rc_code from tb_log_view_201601 group by 1) group by 1;

drop table if exists tb_log_order_201601;
create table tb_log_order_201601 as
select
	convert_timezone('KST',server_time) server_time
	,pc_id
	,session_id
	,device
	,user_id
	,order_id
	,order_price
	,item_id
	,price
	,quantity
from tb_log_order
where extract(month from convert_timezone('KST',server_time)) = 1;

drop table tb_order_stat;
create table tb_order_stat as
select
	order_id
	,sum(quantity) order_item_count
	,sum(price * quantity) order_price
from tb_log_order
where extract(month from convert_timezone('KST',server_time)) = 1
group by 1;

--#############
--##   24    ##
--#############

drop table if exists tb_order_item_within_24h;
create table tb_order_item_within_24h as
select 
	a.pc_id
	,a.session_id
	,a.device
	,a.rc_code
	,a.item_id
	,a.server_time view_time
	,b.server_time order_time
	,b.order_id
	,b.price
	,b.quantity
from tb_log_view_201601 a
join tb_log_order_201601 b on a.pc_id = b.pc_id and a.item_id = b.item_id
where datediff (minute, a.server_time, b.server_time ) < 1440 and datediff(minute, a.server_time, b.server_time) >= 0;

drop table if exists tb_view_order_24h;
create table tb_view_order_24h as
select a.pc_id, a.session_id, b.device, a.rc_code, a.item_id, a.server_time as view_time, b.order_time, b.order_id,b.price,b.quantity
from tb_log_view_201601 a
left join tb_order_item_within_24h b on a.pc_id = b.pc_id and a.item_id = b.item_id and a.session_id = b.session_id and a.server_time = b.view_time;

drop table if exists tb_view_order_24h_s0;
create table tb_view_order_24h_s0 as
select 
	device
	,pc_id
	,order_id
	,max(case when rc_code is not null then 1 else 0 end) reco
	,max(price) order_price
from tb_view_order_24h
group by 1,2,3;

drop table if exists tb_view_order_24h_s1;
create table tb_view_order_24h_s1 as
select 
	a.device
	,a.pc_id
	,a.order_id
	,b.reco
	,count(distinct a.item_id) item_num
from tb_log_order_201601 a
inner join tb_view_order_24h_s0 b on a.order_id = b.order_id and a.pc_id = b.pc_id
group by 1,2,3,4;

drop table if exists tb_view_order_24h_s2;
create table tb_view_order_24h_s2 as
select 
	device
	,pc_id
	,item_id
	,order_id
	,max(case when rc_code is not null then 1 else 0 end) reco
	,max(price) price
	,max(quantity) quantity
	
from tb_order_item_within_24h
group by 1,2,3,4,5;

--추천 직접 매출 -> 객단가/구매건수
drop table if exists tb_view_order_24h_s00;
create table tb_view_order_24h_s00 as
select device, reco, avg(order_price) ct, count(*) freq from tb_view_order_24h_s0 group by device,reco;

--추천 구매전환율 ->
drop table if exists tb_view_order_24h_s01;
create table tb_view_order_24h_s01 as
select 
	pc_id
	,case when device= 'PW' then 'PW' else 'MW' end device
	,session_id
	,item_id
	,case when rc_code is not null then 1 else 0 end as reco
from tb_log_view_201601
group by 1,2,3,4,5;

drop table if exists tb_view_order_24h_s02;
create table tb_view_order_24h_s02 as
select 
	a.device
	,a.reco
	,a.item_id
	,sum(case when b.session_id is not null then 1 else 0 end)/sum(case when a.session_id is not null then 1 else 0 end) cv
	,sum(case when b.session_id is not null then 1 else 0 end) order_item
	,sum(case when a.session_id is not null then 1 else 0 end) view_item
	,avg(case when b.price is not null then b.price else null end) avg_price
from tb_view_order_24h_s01 a
left join tb_view_order_24h_s2 b on a.item_id = b.item_id and a.pc_id = b.pc_id
group by 1,2,3;


drop table if exists tb_view_order_24h_s3;
create table tb_view_order_24h_s3 as
select 
	device,
	reco, 
	round(sum(order_item::real)/sum(view_item::real),4)*100 cv, 
	sum(avg_price * order_item)/sum(order_item) avg_price 
from 
	tb_view_order_24h_s02 
group by device,reco;


--==============================
--#	     OUTPUT
--==============================

--CVR based on Reco
select * from tb_view_order_24h_s3 order by device,reco;


--CVR based on total
select 
	device, 
	round(sum(order_item::real)/sum(view_item::real),4)*100 cv, 
	sum(avg_price * order_item)/sum(order_item) avg_price 
from 
	tb_view_order_24h_s02 
where order_item >0 
group by device;

drop table if exists tb_recommend_result_quantity_monthly_s1_device;
CREATE TABLE tb_recommend_result_quantity_monthly_s1_device AS
select
	A.date, A.device
	,round(avg(A.quantity :: float),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: float end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: float end),3) no_reco_quantity
    ,round(avg(case when B.session_id is not null then A.quantity :: float end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_quantity_monthly_s1_total;
CREATE TABLE tb_recommend_result_quantity_monthly_s1_total AS
select
	A.date
	,'AL' device
	,round(avg(A.quantity :: float),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: float end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: float end),3) no_reco_quantity
    ,round(avg(case when B.session_id is not null then A.quantity :: float end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_quantity_monthly_s1;
CREATE TABLE tb_recommend_result_quantity_monthly_s1 AS
select * from tb_recommend_result_quantity_monthly_s1_device
union all
select * from tb_recommend_result_quantity_monthly_s1_total;

drop table if exists tb_recommend_result_quantity_monthly;
CREATE TABLE tb_recommend_result_quantity_monthly AS SELECT *,
    (case when quantity <> 0  then round(reco_quantity :: float / (quantity :: float), 4) * 100 else null end) as reco_up_ratio
FROM tb_recommend_result_quantity_monthly_s1;



--########################
--##    24시간 output    ##
--#######################

--rc_code별로 click per visit
select
	case when rc_code>0 then '1' else '0' end rc_code,
	device,
	round(avg(view::real),3) click
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	count(*) as view
from
tb_view_order_24h
group by 1,device)
group by 1,2
order by 2,1; 

--total
select
	device,
	round(avg(view::real),3) click
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	count(*) as view
from
tb_view_order_24h 
group by 1,device)
group by 1
order by 1;

--rc_code별로 quantity per purchase
select
	case when rc_code>0 then '1' else '0' end rc_code,
	device,
	round(avg(quantity :: float),3) quantity
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	sum(quantity) as quantity
from
tb_view_order_24h 
group by 1,device)
group by 1,2
order by 2,1; 

--total
select
	device,
	round(avg(quantity :: float),3) quantity
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	sum(quantity) as quantity
from
tb_view_order_24h 
group by 1,device)
group by 1
order by 1;


--###############
--##   1시간    ##
--###############


drop table if exists tb_order_item_within_1h;
create table tb_order_item_within_1h as
select 
	a.pc_id
	,a.session_id
	,a.device
	,a.rc_code
	,a.item_id
	,a.server_time view_time
	,b.server_time order_time
	,b.order_id
	,b.price
	,b.quantity
from tb_log_view_201601 a
join tb_log_order_201601 b on a.pc_id = b.pc_id and a.item_id = b.item_id
where datediff (minute, a.server_time, b.server_time ) < 60 and datediff(minute, a.server_time, b.server_time) >= 0;

drop table if exists tb_view_order_1h;
create table tb_view_order_1h as
select a.pc_id, a.session_id, b.device, a.rc_code, a.item_id, a.server_time as view_time, b.order_time, b.order_id,b.price,b.quantity
from tb_log_view_201601 a
left join tb_order_item_within_1h b on a.pc_id = b.pc_id and a.item_id = b.item_id and a.session_id = b.session_id and a.server_time = b.view_time;

--#####################
--##   1시간 output   ##
--#####################

--rc_code별로 click per visit
select
	case when rc_code>0 then '1' else '0' end rc_code,
	device,
	round(avg(view::real),3) click
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	count(*) as view
from
tb_view_order_1h
group by 1,device)
group by 1,2
order by 2,1; 

--total
select
	device,
	round(avg(view::real),3) click
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	count(*) as view
from
tb_view_order_1h 
group by 1,device)
group by 1
order by 1;

--rc_code별로 quantity per purchase
select
	case when rc_code>0 then '1' else '0' end rc_code,
	device,
	round(avg(quantity :: float),3) quantity
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	sum(quantity) as quantity
from
tb_view_order_1h 
group by 1,device)
group by 1,2
order by 2,1; 

--total
select
	device,
	round(avg(quantity :: float),3) quantity
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	sum(quantity) as quantity
from
tb_view_order_1h 
group by 1,device)
group by 1
order by 1;


--###############
--##   48시간    ##
--###############


drop table if exists tb_order_item_within_48h;
create table tb_order_item_within_48h as
select 
	a.pc_id
	,a.session_id
	,a.device
	,a.rc_code
	,a.item_id
	,a.server_time view_time
	,b.server_time order_time
	,b.order_id
	,b.price
	,b.quantity
from tb_log_view_201601 a
join tb_log_order_201601 b on a.pc_id = b.pc_id and a.item_id = b.item_id
where datediff (minute, a.server_time, b.server_time ) < 2880 and datediff(minute, a.server_time, b.server_time) >= 0;

drop table if exists tb_view_order_48h;
create table tb_view_order_48h as
select a.pc_id, a.session_id, b.device, a.rc_code, a.item_id, a.server_time as view_time, b.order_time, b.order_id,b.price,b.quantity
from tb_log_view_201601 a
left join tb_order_item_within_48h b on a.pc_id = b.pc_id and a.item_id = b.item_id and a.session_id = b.session_id and a.server_time = b.view_time;

--######################
--##   48시간 output   ##
--######################

--rc_code별로 click per visit
select
	case when rc_code>0 then '1' else '0' end rc_code,
	device,
	round(avg(view::real),3) click
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	count(*) as view
from
tb_view_order_48h
group by 1,device)
group by 1,2
order by 2,1; 

--total
select
	device,
	round(avg(view::real),3) click
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	count(*) as view
from
tb_view_order_48h 
group by 1,device)
group by 1
order by 1;

--rc_code별로 quantity per purchase
select
	case when rc_code>0 then '1' else '0' end rc_code,
	device,
	round(avg(quantity :: float),3) quantity
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	sum(quantity) as quantity
from
tb_view_order_48h
group by 1,device) 
group by 1,2
order by 2,1; 

--total
select
	device,
	round(avg(quantity :: float),3) quantity
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	sum(quantity) as quantity
from
tb_view_order_48h 
group by 1,device) 
group by 1
order by 1;


--####################
--##   session단위   ##
--####################


drop table if exists tb_order_item_session;
create table tb_order_item_session as
select 
	a.session_id
	,a.device
	,a.rc_code
	,a.item_id
	,a.server_time view_time
	,b.server_time order_time
	,b.order_id
	,b.price
	,b.quantity
from tb_log_view_201601 a
join tb_log_order_201601 b on a.pc_id = b.pc_id and a.item_id = b.item_id;

drop table if exists tb_view_order_session;
create table tb_view_order_session as
select a.session_id, b.device, a.rc_code, a.item_id, a.server_time as view_time, b.order_time, b.order_id,b.price,b.quantity
from tb_log_view_201601 a
left join tb_order_item_session b on a.item_id = b.item_id and a.session_id = b.session_id and a.server_time = b.view_time;


--########################
--##   session output   ##
--########################

--rc_code별로 click per visit
select
	case when rc_code>0 then '1' else '0' end rc_code,
	device,
	round(avg(view::real),3) click
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	count(*) as view
from
tb_view_order_session
group by 1,device)
group by 1,2
order by 2,1; 

--total
select
	device,
	round(avg(view::real),3) click
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	count(*) as view
from
tb_view_order_session 
group by 1,device)
group by 1
order by 1;

--rc_code별로 quantity per purchase
select
	case when rc_code>0 then '1' else '0' end rc_code,
	device,
	round(avg(quantity :: float),3) quantity
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	sum(quantity) as quantity
from
tb_view_order_session
group by 1,device) 
group by 1,2
order by 2,1; 

--total
select
	device,
	round(avg(quantity :: float),3) quantity
from
(select 
	session_id,
	case when device= 'PW' then 'PW' else 'MW' end device,
	sum(case when rc_code is not null then 1 else 0 end) rc_code,
	sum(quantity) as quantity
from
tb_view_order_session 
group by 1,device) 
group by 1
order by 1;



















