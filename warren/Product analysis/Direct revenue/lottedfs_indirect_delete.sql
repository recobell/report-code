drop table if exists tmp_recommend_result_view_session_daily;
CREATE TABLE tmp_recommend_result_view_session_daily as
select
	year
	,month
	,day
	,session_id 
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_report_01
group by year,month,day,session_id;




drop table if exists tmp_recommend_result_order_session_daily;
CREATE TABLE tmp_recommend_result_order_session_daily as
select
	
	A.year
	,A.month
	,A.day
	,A.device
	,A.session_id
	,A.order_id
	,sum(A.price * A.quantity) revenue
	,sum(case when B.reco_view > 0 then A.price * A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 then A.price * A.quantity else 0 end) no_reco_revenue
	
	,sum(case when B.session_id is not null then A.price * A.quantity else 0 end) click_revenue
	,sum(case when B.session_id is null then A.price * A.quantity else 0 end) no_click_revenue
	

	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 then A.order_id end) no_reco_frequency

	,count(distinct case when B.session_id is not null then A.order_id end) session_frequency
	,count(distinct case when B.session_id is null then A.order_id end) no_session_frequency
	
from tb_log_order_report_01 A
left join tmp_recommend_result_view_session_daily B on A.session_id = B.session_id and a.year = b.year and a.month = b.month and a.day=b.day
group by A.year,A.month,A.day,A.session_id,A.device,A.order_id;

select * from tmp_recommend_result_order_session_daily limit 100;


drop table if exists tmp_recommend_result_order_session_daily_delete_drows;
CREATE TABLE tmp_recommend_result_order_session_daily_delete_drows as
select *, dense_rank() over (partition by session_id order by day,device) as rank
from tmp_recommend_result_order_session_daily a
where (select count(*) from tmp_recommend_result_order_session_daily b
	where a.order_id=b.order_id and a.revenue = b.revenue and a.session_id = b.session_id) > 1 
order by session_id, year,month,day;


drop table if exists tmp_recommend_result_order_session_daily_s0;
CREATE TABLE tmp_recommend_result_order_session_daily_s0 as
select year,month,day,session_id,rank from tmp_recommend_result_order_session_daily_delete_drows
where rank=1;


delete 
from tmp_recommend_result_order_session_daily 
	USING tmp_recommend_result_order_session_daily_s0 B
    where (tmp_recommend_result_order_session_daily.session_id = B.session_id and 
	   tmp_recommend_result_order_session_daily.year = B.year and 
	   tmp_recommend_result_order_session_daily.month = B.month and 
	   tmp_recommend_result_order_session_daily.day = B.day);








select * from tmp_recommend_result_order_session_daily a
where (select count(*) from tmp_recommend_result_order_session_daily b
where a.order_id=b.order_id and a.revenue = b.revenue and a.session_id = b.session_id) > 1 
order by session_id, year,month,day;