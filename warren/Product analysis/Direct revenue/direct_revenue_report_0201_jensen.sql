
--************************************************************************
--Direct Revenue -1
--************************************************************************

--************************************************************************
--pc_id, session_id matching
--************************************************************************

drop table if exists tb_log_session_pc_id;
CREATE TABLE tb_log_session_pc_id as
select
	pc_id
	,session_id
from tb_log_view_report
where device = 'PW'
group by pc_id, session_id;


--************************************************************************
--first reco view
--************************************************************************
drop table if exists tb_log_view_report_rank_s1;
CREATE TABLE tb_log_view_report_rank_s1 as
select
	* 
	,rank() over (partition by session_id order by server_time)
from tb_log_view_report where device = 'PW';

drop table if exists tb_log_view_report_rank_s2;
CREATE TABLE tb_log_view_report_rank_s2 as
select
	session_id
	,min(rank) first_reco_view
	,max(rank) last_reco_view
from tb_log_view_report_rank_s1
where rc_code is not null
group by session_id;

drop table if exists tb_log_view_report_rank_s3;
CREATE TABLE tb_log_view_report_rank_s3 as
select * from tb_log_view_report_rank_s1 where rc_code is not null;

drop table if exists tb_log_view_report_rank_s4;
CREATE TABLE tb_log_view_report_rank_s4 as
select 
A.session_id
,A.first_reco_view
,B.rc_code first_rc_code
,A.last_reco_view
from
tb_log_view_report_rank_s2 A
inner join tb_log_view_report_rank_s3 B 
on A.session_id = B.session_id and A.first_reco_view = B.rank;

drop table if exists tb_log_view_report_rank;
CREATE TABLE tb_log_view_report_rank as
select 
A.session_id
,A.first_reco_view
,A.first_rc_code
,A.last_reco_view
,B.rc_code last_rc_code

from tb_log_view_report_rank_s4 A
inner join tb_log_view_report_rank_s3 B
on A.session_id = B.session_id and A.last_reco_view = B.rank;

--************************************************************************
--first four view
--************************************************************************
drop table if exists tb_log_view_first_four;
CREATE TABLE tb_log_view_first_four as 
select 
	session_id
	,max(case when rank = 1 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 1 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) first_view
	,max(case when rank = 2 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 2 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) second_view
	,max(case when rank = 3 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 3 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) third_view
	,max(case when rank = 4 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 4 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) fourth_view
	,max(case when rank = 1 and a.item_id is not null then b.category1 end) first_category
	,max(case when rank = 2 and a.item_id is not null then b.category1 end) second_category
	,max(case when rank = 3 and a.item_id is not null then b.category1 end) third_category
	,max(case when rank = 4 and a.item_id is not null then b.category1 end) fourth_category
from tb_log_view_report_rank_s1 a
left join tb_product_info b on a.item_id = b.item_id
group by session_id;

--************************************************************************
--view
--************************************************************************

drop table if exists tb_crm_kpi_view_s1;
create table tb_crm_kpi_view_s1 as
select
	A.session_id
	,count(*) as view
	,sum(case when A.rc_code is not null then 1 else 0 end) as reco_view
	,count(distinct A.item_id) as itemvar
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) first_server_time

from tb_log_view_report A
where device = 'PW'
group by A.session_id;



drop table if exists tb_crm_kpi_view_s2;
create table tb_crm_kpi_view_s2 as
select
	A.*
	,B.first_reco_view
	,B.first_rc_code
	,B.last_reco_view
	,B.last_rc_code
from tb_crm_kpi_view_s1 A 
left join tb_log_view_report_rank B on A.session_id = B.session_id;


--************************************************************************
--reco order
--************************************************************************
drop table if exists tb_reco_revenue_s1;
create table tb_reco_revenue_s1 as
select 
	A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code is not null then server_time end) server_time, session_id, item_id, max(case when rc_code is not null then 1 else 0 end) rc_num from tb_log_view_report where device = 'PW' group by session_id, item_id) A
inner join tb_log_order_report B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time and B.device = 'PW';

drop table if exists tb_reco_revenue;
create table tb_reco_revenue as
select 
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s1
group by session_id;

--##########################################################################################
--view and order
--order (frequency, quantity, revenue)
--##########################################################################################
drop table if exists tb_crm_kpi_order_s1;
create table tb_crm_kpi_order_s1 as
	SELECT 
		session_id,
		min(server_time) server_time,
		sum(quantity) quantity,
        sum(price * quantity) revenue,
		count(distinct item_id) buyitemnum,
		count(distinct order_id) ordercount
    FROM
        tb_log_order_report
	where device = 'PW'
    GROUP BY session_id;



--##########################################################################################
--final funnel metric table
--##########################################################################################

drop table if exists tb_session_funnel;
create table tb_session_funnel as
select
	A.first_server_time first_view_time
	,B.server_time order_time
	,A.session_id
	,A.first_reco_view
	,A.first_rc_code
	,A.last_reco_view
	,A.last_rc_code
	,A.view view_count
	,A.reco_view reco_view_count
	,A.itemvar item_var
	,A.period view_period
	,D.first_view
	,D.second_view
	,D.third_view
	,D.fourth_view
	,D.first_category
	,D.second_category
	,D.third_category
	,D.fourth_category
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count
	,C.reco_revenue 
	,C.reco_quantity
	
from tb_crm_kpi_view_s2 A
left join tb_crm_kpi_order_s1 B on A.session_id = B.session_id
left join tb_reco_revenue C on A.session_id = C.session_id
left join tb_log_view_first_four D on A.session_id = D.session_id;


select * from tb_session_funnel;


select
	(case when first_rc_code like '%ProductDetail_product%' then 'pw_detail'
	when first_rc_code like '%BasketView_product%' then 'pw_basket'
	when first_rc_code like '%Search_product%' then 'pw_search' 
	--when first_rc_code like '%RECENT%' and b.device = 'PW' then 'pw_recent'
	--when first_rc_code like '%RECENT%' and b.device <> 'PW' then 'mw_recent'
	when first_rc_code like '%RECENT%' then 'pw_recent'
	when first_rc_code like '%REO%' then 'pw_main'
	when first_rc_code like '%RECO%' then 'pw_reco'
	--when a.first_rc_code like '%ProductDetail_m_product%' then 'mw_detail'
	--when a.first_rc_code like '%BasketView_m_product%' then 'mw_basket'
	--when a.first_rc_code like '%Search_m_product%' then 'mw_search' 
	--when a.first_rc_code like '%TODAY%' then 'mw_main' 
	--when a.first_rc_code like '%RECO%' and b.device <> 'PW' then 'mw_reco'
	else first_rc_code
	end) rc_code
	,sum(order_revenue) revenue
from tb_session_funnel 
group by 1 
order by 2 desc;


--************************************************************************
--Direct Revenue -2
--************************************************************************


drop table if exists tb_reco_revenue_s1;
create table tb_reco_revenue_s1 as
select session_id, device, item_id,rc_code, server_time from tb_log_view_report where rc_code is not null group by 1,2,3,4,5;

drop table if exists tb_reco_revenue_s2;
create table tb_reco_revenue_s2 as
select 
	A.session_id
	,A.item_id
	,A.device
	,A.rc_code
	,B.order_id
	,max(A.server_time) servertime
	,max(B.price) price
	,max(B.quantity) quantity
from tb_reco_revenue_s1 A
inner join tb_log_order_report B on A.session_id = B.session_id  and A.item_id = B.item_id
--where datediff(minute,a.server_time, b.server_time)>0 and datediff(minute,a.server_time, b.server_time)<30
group by 1,2,3,4,5;

drop table if exists tb_reco_revenue_last_click_s3;
create table tb_reco_revenue_last_click_s3 as
select
	*
	,row_number() over (partition by session_id, item_id, order_id order by servertime desc) as rank
from tb_reco_revenue_s2;

drop table if exists tb_reco_revenue_last_click_s4;
create table tb_reco_revenue_last_click_s4 as
select 
	session_id
	,item_id
	,device
	,rc_code
	,order_id
	,servertime
	,price
	,quantity
from tb_reco_revenue_last_click_s3
where rank = 1;


drop table if exists tb_reco_revenue_last_click;
create table tb_reco_revenue_last_click as
select
	(case when rc_code like '%ProductDetail_product%' then 'pw_detail'
	when rc_code like '%BasketView_product%' then 'pw_basket'
	when rc_code like '%Search_product%' then 'pw_search' 
	when rc_code like '%RECENT%' then 'mw_recent'
	when rc_code like '%REO%' then 'pw_main'
	when rc_code like '%RECO%' and device = 'PW' then 'pw_reco'
	when rc_code like '%ProductDetail_m_product%' then 'mw_detail'
	when rc_code like '%BasketView_m_product%' then 'mw_basket'
	when rc_code like '%Search_m_product%' then 'mw_search' 
	when rc_code like '%TODAY%' then 'mw_main' 
	when rc_code like '%RECO%' and device <> 'PW' then 'mw_reco'
	else rc_code
	end) rc_code
	,sum(price * quantity) revenue
	,count(*) frequency
from tb_reco_revenue_last_click_s4
group by 1 
order by 2 desc;


drop table if exists tb_reco_revenue_first_click_s3;
create table tb_reco_revenue_first_click_s3 as
select
	*
	,row_number() over (partition by session_id, item_id, order_id order by servertime) as rank
from tb_reco_revenue_s2;

drop table if exists tb_reco_revenue_first_click_s4;
create table tb_reco_revenue_first_click_s4 as
select 
	session_id
	,item_id
	,device
	,rc_code
	,order_id
	,servertime
	,price
	,quantity
from tb_reco_revenue_first_click_s3
where rank = 1;


drop table if exists tb_reco_revenue_first_click;
create table tb_reco_revenue_first_click as
select
	(case when rc_code like '%ProductDetail_product%' then 'pw_detail'
	when rc_code like '%BasketView_product%' then 'pw_basket'
	when rc_code like '%Search_product%' then 'pw_search' 
	when rc_code like '%RECENT%' and device = 'PW' then 'pw_recent'
	when rc_code like '%RECENT%' and device <> 'PW' then 'mw_recent'
	when rc_code like '%REO%' then 'pw_main'
	when rc_code like '%RECO%' and device = 'PW' then 'pw_reco'
	when rc_code like '%ProductDetail_m_product%' then 'mw_detail'
	when rc_code like '%BasketView_m_product%' then 'mw_basket'
	when rc_code like '%Search_m_product%' then 'mw_search' 
	when rc_code like '%TODAY%' then 'mw_main' 
	when rc_code like '%RECO%' and device <> 'PW' then 'mw_reco'
	else rc_code
	end) rc_code
	,sum(price * quantity) revenue
	,count(*) frequency
from tb_reco_revenue_first_click_s4
group by 1 
order by 2 desc;


select * from tb_log_view_report where rc_code like '%RECENT%' and device <> 'PW' limit 10;

select * from tb_reco_revenue_last_click order by rc_code;
select * from tb_reco_revenue_first_click order by rc_code;

select sum(revenue) from tb_reco_revenue_first_click;
select sum(price * quantity) from tb_log_order_report;
select count(distinct order_id) from tb_log_order_report;
