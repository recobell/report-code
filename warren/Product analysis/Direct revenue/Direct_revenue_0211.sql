select convert_timezone('kst', server_time)::date from tb_log_view_report group by 1 order by 1;

drop table if exists tb_log_view_report_01;
create table tb_log_view_report_01 as
select * from tb_log_view_report  where month = 01;

select convert_timezone('kst', server_time)::date from tb_log_view_report_01 group by 1 order by 1;

drop table if exists tb_log_order_report_01;
create table tb_log_order_report_01 as
select * from tb_log_order_report  where month = 01;

select convert_timezone('kst', server_time)::date from tb_log_order_report_01 group by 1 order by 1;



drop table if exists tmp_recommend_result_view_session_monthly;
CREATE TABLE tmp_recommend_result_view_session_monthly as
select
	year
	,month
	,session_id 
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
	,sum(case when ad_code is not null then 1 else 0 end) as ad_view
	
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) as first_click
	,max(ad_code) as ad_code
from tb_log_view_report_01
group by year,month,  session_id;




drop table if exists tmp_recommend_result_order_session_monthly;
CREATE TABLE tmp_recommend_result_order_session_monthly as
select
	A.year
	,A.month
	,A.session_id 
	
	,min(B.first_click) frist_click
	,max(B.ad_code) ad_code
	
	,sum(A.price * A.quantity) revenue

	,sum(case when B.reco_view > 0 then A.price * A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 then A.price * A.quantity else 0 end) no_reco_revenue

	,sum(case when B.ad_view > 0 then A.price * A.quantity else 0 end) ad_revenue
	,sum(distinct case when B.ad_view > 0 then A.order_price else 0 end) ad_revenue2
	
	,sum(case when B.session_id is not null then A.price * A.quantity else 0 end) click_revenue
	,sum(case when B.session_id is null then A.price * A.quantity else 0 end) no_click_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 then A.order_price else 0 end) no_reco_actual_payment

	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 then A.order_id end) no_reco_frequency

	,count(distinct case when B.session_id is not null then A.order_id end) session_frequency
	,count(distinct case when B.session_id is null then A.order_id end) no_session_frequency
	
from tb_log_order_report_01 A
left join tmp_recommend_result_view_session_monthly B on A.session_id = B.session_id and a.year = b.year and a.month = b.month
group by A.year,A.month,A.session_id;



-- revenue
drop table if exists tb_recommend_result_revenue_monthly;
CREATE TABLE tb_recommend_result_revenue_monthly AS
select
	A.year
	,A.month
	
	,sum(A.revenue) revenue
	,sum(A.reco_revenue) reco_revenue    
	,sum(A.no_reco_revenue) no_reco_revenue 
    ,round( sum(A.reco_revenue) :: real / sum(A.revenue) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_order_session_monthly A
group by 1,2
order by 1,2;

--funnel

drop table if exists tb_session_view_rank;
CREATE TABLE tb_session_view_rank as
select
	* 
	,row_number() over (partition by session_id order by server_time) as rank
from tb_log_view_report_01;

drop table if exists tb_session_view_rank_minmax;
CREATE TABLE tb_session_view_rank_minmax as
select
	session_id
	,min(rank) first_reco_view
	,max(rank) last_reco_view
from tb_session_view_rank
where rc_code is not null
group by session_id;

drop table if exists tb_session_view_rank_reco;
CREATE TABLE tb_session_view_rank_reco as
select * from tb_log_view_report_rank_s1 where rc_code is not null;

drop table if exists tb_session_view_rank_first;
CREATE TABLE tb_session_view_rank_first as
select 
A.session_id
,A.first_reco_view
,B.rc_code first_rc_code
,A.last_reco_view
from
tb_session_view_rank_minmax A
inner join tb_session_view_rank_reco B 
on A.session_id = B.session_id and A.first_reco_view = B.rank;

drop table if exists tb_session_view_rank_first_last;
CREATE TABLE tb_session_view_rank_first_last as
select 
A.session_id
,A.first_reco_view
,A.first_rc_code
,A.last_reco_view
,B.rc_code last_rc_code

from tb_session_view_rank_first A
inner join tb_session_view_rank_reco B
on A.session_id = B.session_id and A.last_reco_view = B.rank;



--************************************************************************
--first four view
--************************************************************************
drop table if exists tb_session_view_first_four;
CREATE TABLE tb_session_view_first_four as 
select 
	session_id
	,max(case when rank = 1 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 1 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) first_view
	,max(case when rank = 2 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 2 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) second_view
	,max(case when rank = 3 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 3 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) third_view
	,max(case when rank = 4 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 4 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) fourth_view
	,max(case when rank = 1 and a.item_id is not null then b.category1 end) first_category
	,max(case when rank = 2 and a.item_id is not null then b.category1 end) second_category
	,max(case when rank = 3 and a.item_id is not null then b.category1 end) third_category
	,max(case when rank = 4 and a.item_id is not null then b.category1 end) fourth_category
from tb_session_view_rank a
left join tb_product_info b on a.item_id = b.item_id
group by session_id;

--************************************************************************
--view
--************************************************************************

drop table if exists tb_session_view_stat;
create table tb_session_view_stat as
select
	session_id
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as eco_view
	,count(distinct item_id) as itemvar
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) first_server_time
from tb_log_view_report_01
group by session_id;
--raw data


drop table if exists tb_session_view_stat_reco;
create table tb_session_view_stat_reco as
select
	A.*
	,B.first_reco_view
	,B.first_rc_code
	,B.last_reco_view
	,B.last_rc_code
from tb_session_view_stat A 
left join tb_session_view_rank_first_last B on A.session_id = B.session_id;


--##########################################################################################
--view and order
--order (frequency, quantity, revenue)
--##########################################################################################
drop table if exists tb_session_order_stat;
create table tb_session_order_stat as
	SELECT 
		session_id,
		min(server_time) server_time,
		sum(quantity) quantity,
        sum(price * quantity) revenue,
		count(distinct item_id) uyitemnum,
		count(distinct order_id) order count
    FROM
        tb_log_order_report_01
    GROUP BY session_id;



--##########################################################################################
--final funnel metric table
--##########################################################################################

drop table if exists tb_session_funnel;
create table tb_session_funnel as
select
	A.first_server_time first_view_time
	,B.server_time order_time
	,A.session_id
	,A.first_reco_view
	,A.first_rc_code
	,A.last_reco_view
	,A.last_rc_code
	,A.view view_count
	,A.reco_view eco_view_count
	,A.itemvar item_var
	,A.period view_period
	,D.first_view
	,D.second_view
	,D.third_view
	,D.fourth_view
	,D.first_category
	,D.second_category
	,D.third_category
	,D.fourth_category
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count
	
from tb_session_view_stat_reco A
left join tb_session_order_stat B on A.session_id = B.session_id
left join tb_log_view_first_four D on A.session_id = D.session_id;


select
	(case when first_rc_code like '%ProductDetail_product%' then 'pw_detail'
	when first_rc_code like '%BasketView_product%' then 'pw_basket'
	when first_rc_code like '%Search_product%' then 'pw_search' 
	when first_rc_code like '%RECENT%' then 'pw_recent'
	--when first_rc_code like '%RECENT%' then 'mw_recent'
	--when a.first_rc_code like '%RECENT%' then 'pw_recent'
	when first_rc_code like '%REO%' then 'pw_main'
	when first_rc_code like '%RECO%' then 'pw_reco'
	when a.first_rc_code like '%ProductDetail_m_product%' then 'mw_detail'
	when a.first_rc_code like '%BasketView_m_product%' then 'mw_basket'
	when a.first_rc_code like '%Search_m_product%' then 'mw_search' 
	when a.first_rc_code like '%TODAY%' then 'mw_main' 
	when a.first_rc_code like '%RECO%' then 'mw_reco'
	else first_rc_code
	end) rc_code
	,sum(order_revenue) revenue
from tb_session_funnel a
group by 1 
order by 2 desc;



select * from tb_recommend_result_revenue_monthly;
select sum(order_revenue) from tb_session_funnel where order_revenue is not null and reco_view_count>0 limit 10;