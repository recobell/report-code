
drop table if exists tmp_recommend_result_view_session;
CREATE TABLE tmp_recommend_result_view_session as
select
	date
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_monthly_report
group by date,session_id, device;

drop table if exists tmp_recommend_result_order_session;
CREATE TABLE tmp_recommend_result_order_session as
select
	A.date
	,A.session_id 
	,A.order_id
	,A.device
	
	,sum(A.price*A.quantity) revenue
	,sum(case when B.reco_view > 0 then A.price*A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.price*A.quantity else 0 end) no_reco_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_price else 0 end) no_reco_actual_payment
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_id end) no_reco_frequency
	
from tb_log_order_monthly_report A
left join tmp_recommend_result_view_session B on A.session_id = B.session_id and A.date = B.date
group by A.date,A.session_id,A.order_id,A.device;


select
	sum(case when reco_view>1 then revenue end) as reco_revenue,
	sum(case when reco_view = 0 then revenue end) as revenue
from
(select 
	A.date,
	A.session_id,
	A.order_id,
	nvl(B.reco_view,0),
	sum(A.revenue) as revenue,
	sum(A.reco_revenue) as reco_revenue,
	sum(A.quantity) as quantity,
	sum(A.reco_quantity) as reco_quantity
from
	tmp_recommend_result_order_session a
	left join
	tmp_recommend_result_view_session b on A.session_id = B.session_id and A.date = B.date
where A.revenue is not null and A.date > '2016-02-29' and A.date < '2016-04-01' group by 1,2,3,4 order by 1);


drop table if exists tb_recommend_result_revenue_monthly_device;
CREATE TABLE tb_recommend_result_revenue_monthly_device AS
select
	date
	,device
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
	,sum(no_reco_revenue) no_reco_revenue 
	,sum(case when session_id is not null then revenue end) loyal_revenue    
    
from tmp_recommend_result_order_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_revenue_monthly_total;
CREATE TABLE tb_recommend_result_revenue_monthly_total AS
select
	date
	,'AL' device
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
	,sum(no_reco_revenue) no_reco_revenue 
	,sum(case when session_id is not null then revenue end) loyal_revenue    
    
from tmp_recommend_result_order_session
group by 1
order by 1;

drop table if exists tb_recommend_result_revenue_monthly_s1;
CREATE TABLE tb_recommend_result_revenue_monthly_s1 AS
select * from tb_recommend_result_revenue_monthly_device
union all
select * from tb_recommend_result_revenue_monthly_total;

drop table if exists tb_recommend_result_revenue_monthly;
CREATE TABLE tb_recommend_result_revenue_monthly AS
select
	*
	,(case when revenue <> 0 then round( reco_revenue :: float / revenue :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_revenue_monthly_s1;
