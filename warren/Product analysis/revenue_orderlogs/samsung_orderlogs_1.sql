
drop table if exists tb_log_view;
create table tb_log_view (	
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(1000),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
item_id varchar(200),
search_term varchar(200)
);

drop table if exists tb_log_order;
create table tb_log_order (
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(200),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
item_id varchar(200),
search_term varchar(200),
order_id varchar(200),
order_price float,
price float,
quantity bigint
);

drop table if exists tmp_product_info;
create table tmp_product_info (
b70be3b7-83e5-4819-b4c5-ea690d83ea88 varchar(200),
type varchar(200),
item_id varchar(200),
item_name varchar(2000),
item_image varchar(200),
item_url varchar(2000),
original_price float,
sale_price float,
category1 varchar(200),
category2 varchar(200),
category3 varchar(200),
category4 varchar(200),
category5 varchar(200),
reg_date varchar(200),
update_date timestamp,
expire_date timestamp,
stock int,
state varchar(200),
description varchar(200),
extra_image varchar(200),
locale varchar(200)
);

copy tb_log_view
from 's3://rb-logs-apne1/b70be3b7-83e5-4819-b4c5-ea690d83ea88/view/2016/02/29'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/b70be3b7-83e5-4819-b4c5-ea690d83ea88/view/2016/03'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/b70be3b7-83e5-4819-b4c5-ea690d83ea88/order/2016/02/29'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/b70be3b7-83e5-4819-b4c5-ea690d83ea88/order/2016/03'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/b70be3b7-83e5-4819-b4c5-ea690d83ea88/product/2016/02/29'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
maxerror 100000
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/b70be3b7-83e5-4819-b4c5-ea690d83ea88/product/2016/03'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
maxerror 100000
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

select
	user_id,
	count(*)
from
(select
	a.user_id,
	b.order_id,
	case when a.rc_code is not null then a.rc_code end,
	max(convert_timezone('KST', a.server_time)) as time,
	a.pc_id,
	b.quantity,
	(b.price* b.quantity) as revenue
from
	tb_log_view a
	left join
	tb_log_order b on a.pc_id = b.pc_id and a.session_id = b.session_id
where a.server_time > '2016-02-29 23:59:59' and a.server_time < '2016-04-01 00:00:00' and b.quantity is not null and a.user_id is not null
group by 1,2,3,5,6,7
order by 4)
group by 1 having count(*)>1;
	


drop table if exists tb_log_view_daily_report;
create table tb_log_view_daily_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
	(case when device = 'PW' then 'PW' else 'MW' end) as device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term
from 
tb_log_view
;

drop table if exists tb_log_search_daily_report;
create table tb_log_search_daily_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
	(case when device = 'PW' then 'PW' else 'MW' end) as device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	search_term
from 
tb_log_search
;

drop table if exists tb_log_order_daily_report;
create table tb_log_order_daily_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
	(case when device = 'PW' then 'PW' else 'MW' end) as device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term, 
	order_id, 
	order_price,
	price, 
	quantity
from 
tb_log_order
;

drop table if exists tb_log_view_monthly_report;
create table tb_log_view_monthly_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
-- 	convert_timezone('KST', server_time)::date - extract(day from convert_timezone('KST', server_time))+1 as date, 
	(case when device = 'PW' then 'PW' else 'MW' end) as device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term
from 
tb_log_view
;

drop table if exists tb_log_order_monthly_report;
create table tb_log_order_monthly_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
-- 	convert_timezone('KST', server_time)::date - extract(day from convert_timezone('KST', server_time))+1 as date, 
	(case when device = 'PW' then 'PW' else 'MW' end) as device,
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term, 
	order_id, 
	order_price,
	price, 
	quantity
from 
tb_log_order
;

drop table if exists tb_product_info;
create table tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0
 group by item_id;

drop table if exists tb_product_info_report;
CREATE TABLE tb_product_info_report as
select
	item_id, 
	item_name, 
	original_price, 
	sale_price, 
	category1, 
	category2, 
	category3, 
	category4, 
	category5, 
	reg_date
	stock
from 
tb_product_info;

drop table if exists tmp_recommend_result_view;
CREATE TABLE tmp_recommend_result_view as
select 
	date
	,pc_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date , pc_id, device;

drop table if exists tmp_recommend_result_order;
CREATE TABLE tmp_recommend_result_order as
select
	date
	,pc_id 
	,device
	
	,sum(price*quantity) revenue
	,sum(case when rc_code is not null then price*quantity else 0 end) reco_revenue
	,sum(case when rc_code is null then price*quantity else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(distinct case when rc_code is not null then order_id else null end) reco_frequency
	,count(distinct case when rc_code is null then order_id else null end) no_reco_frequency
	
from tb_log_order_daily_report
group by date, pc_id, device;



drop table if exists tmp_recommend_result_view_session;
CREATE TABLE tmp_recommend_result_view_session as
select
	date
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date,session_id, device;

drop table if exists tmp_recommend_result_order_session;
CREATE TABLE tmp_recommend_result_order_session as
select
	A.date
	,A.session_id 
	,A.order_id
	,A.device
	
	,sum(A.price*A.quantity) revenue
	,sum(case when B.reco_view > 0 then A.price*A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.price*A.quantity else 0 end) no_reco_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_price else 0 end) no_reco_actual_payment
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_id end) no_reco_frequency
	
from tb_log_order_daily_report A
left join tmp_recommend_result_view_session B on A.session_id = B.session_id and A.date = B.date
group by A.date,A.session_id,A.order_id,A.device;



drop table if exists tmp_recommend_result_order_session_s2;
CREATE TABLE tmp_recommend_result_order_session_s2 as
select
	 A.date::date
	,A.pc_id
	,A.session_id
	,A.order_id
	,B.rc_code
	
	,A.price*A.quantity revenue


	,A.quantity quantity
	
from tb_log_order_daily_report A
left join tb_log_view_daily_report B on A.pc_id=B.pc_id and A.session_id = B.session_id and A.date = B.date
where A.date > '2016-02-29' and A.date < '2016-04-01'
group by A.date,A.pc_id,A.session_id,A.order_id,B.rc_code,6,A.quantity
order by 1;

drop table if exists tmp_recommend_result_order_session_s1;
CREATE TABLE tmp_recommend_result_order_session_s1 as
select
-- 	date,
-- 	pc_id,
-- 	session_id,
	order_id,
	sum(case when rc_code is not null then 1 else 0 end) as reco_view,
	sum(revenue) revenue,
	sum(quantity) quantity
from
	tmp_recommend_result_order_session_s2
where revenue is not null
group by 1
order by 1;

select * from tb_log_view_daily_report where pc_id = 1454292442572248812 order by date; and rc_code is not null order by date;
select * from tb_log_order_daily_report where pc_id = 1454292442572248812 order by date;
select * from tb_log_order_daily_report where pc_id = 1457956693383568875;order_id ='201603156850796';
select * from tmp_recommend_result_order_session_s1 where order_id='201512316185111'
select * from tmp_recommend_result_order_session_s1 where rc_code is not null;
select order_id, count(*) from tmp_recommend_result_order_session where  pc_id = 1457956693383568875 group by 1 
select * from tmp_recommend_result_order_session where  pc_id = 1457956693383568875 order by date;

select
	order_id,
	count(*)
from
	tmp_recommend_result_order_session_s1
where revenue is not null
group by 1
having count(*) > 1;

select
	sum(case when reco_view>1 then revenue end) as reco_revenue,
	sum(case when reco_view = 0 then revenue end) as revenue
from 
	tmp_recommend_result_order_session_s1;









drop table if exists tb_recommend_loyal_group;
CREATE TABLE tb_recommend_loyal_group AS SELECT * FROM
    tmp_recommend_result_view_session
WHERE
	view > 1;

drop table if exists tb_recommend_reco_group;
CREATE TABLE tb_recommend_reco_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
where reco_view > 0;

drop table if exists tb_recommend_result_uv_daily_device;
CREATE TABLE tb_recommend_result_uv_daily_device AS
select
	date
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv
    
from tmp_recommend_result_view
group by 1
order by 1;

drop table if exists tb_recommend_result_uv_daily_total;
CREATE TABLE tb_recommend_result_uv_daily_total AS
select
	date
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv
from tmp_recommend_result_view
group by 1
order by 1;

drop table if exists tb_recommend_result_uv_daily_s1;
CREATE TABLE tb_recommend_result_uv_daily_s1 AS
select * from tb_recommend_result_uv_daily_device
union all
select * from tb_recommend_result_uv_daily_total;

drop table if exists tb_recommend_result_uv_daily;
CREATE TABLE tb_recommend_result_uv_daily AS
select
	*
	,(case when uv <> 0 then round( reco_uv :: float / uv :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_uv_daily_s1;

drop table if exists tb_recommend_result_visit_daily_device;
CREATE TABLE tb_recommend_result_visit_daily_device AS
select
	date
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,count(distinct case when view > 1 then session_id end) loyal_visit  
    
    
from tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_visit_daily_total;
CREATE TABLE tb_recommend_result_visit_daily_total AS
select
	date
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,count(distinct case when view > 1 then session_id end) loyal_visit  
    
    
from tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_visit_daily_s1;
CREATE TABLE tb_recommend_result_visit_daily_s1 AS
select * from tb_recommend_result_visit_daily_device
union all
select * from tb_recommend_result_visit_daily_total;

drop table if exists tb_recommend_result_visit_daily;
CREATE TABLE tb_recommend_result_visit_daily AS
select
	*
	,(case when visit <> 0 then round( reco_visit :: float / visit :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_visit_daily_s1;

drop table if exists tb_recommend_result_pv_daily_device;
CREATE TABLE tb_recommend_result_pv_daily_device AS
select
	date
	,sum(view) pv
    ,sum(reco_view) reco_pv
	,sum(no_reco_view) no_reco_pv
    
from tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_pv_daily_total;
CREATE TABLE tb_recommend_result_pv_daily_total AS
select
	date
	,sum(view) pv
    ,sum(reco_view) reco_pv
	,sum(no_reco_view) no_reco_pv
    
from tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_pv_daily_s1;
CREATE TABLE tb_recommend_result_pv_daily_s1 AS
select * from tb_recommend_result_pv_daily_device
union all
select * from tb_recommend_result_pv_daily_total;

drop table if exists tb_recommend_result_pv_daily;
CREATE TABLE tb_recommend_result_pv_daily AS
select
	*
	,(case when pv <> 0 then round( reco_pv :: float / pv :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_pv_daily_s1;

drop table if exists tb_recommend_result_frequency_daily_device;
CREATE TABLE tb_recommend_result_frequency_daily_device AS
select
	date
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency 
	,sum(no_reco_frequency) no_reco_frequency 
from tmp_recommend_result_order_session
group by 1
order by 1;

drop table if exists tb_recommend_result_frequency_daily_total;
CREATE TABLE tb_recommend_result_frequency_daily_total AS
select
	date
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency 
	,sum(no_reco_frequency) no_reco_frequency 
	
    
from tmp_recommend_result_order_session
group by 1
order by 1;

drop table if exists tb_recommend_result_frequency_daily_s1;
CREATE TABLE tb_recommend_result_frequency_daily_s1 AS
select * from tb_recommend_result_frequency_daily_device
union all
select * from tb_recommend_result_frequency_daily_total;

drop table if exists tb_recommend_result_frequency_daily;
CREATE TABLE tb_recommend_result_frequency_daily AS
select
	*
	,(case when frequency <> 0 then round( reco_frequency :: float / frequency :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_frequency_daily_s1;

drop table if exists tb_recommend_result_revenue_daily_device;
CREATE TABLE tb_recommend_result_revenue_daily_device AS
select
	date
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
	,sum(no_reco_revenue) no_reco_revenue 
	,sum(case when session_id is not null then revenue end) loyal_revenue    
    
from tmp_recommend_result_order_session
group by 1
order by 1;

drop table if exists tb_recommend_result_revenue_daily_total;
CREATE TABLE tb_recommend_result_revenue_daily_total AS
select
	date
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
	,sum(no_reco_revenue) no_reco_revenue 
	,sum(case when session_id is not null then revenue end) loyal_revenue    
from tmp_recommend_result_order_session
group by 1
order by 1;

drop table if exists tb_recommend_result_revenue_daily_s1;
CREATE TABLE tb_recommend_result_revenue_daily_s1 AS
select * from tb_recommend_result_revenue_daily_device
union all
select * from tb_recommend_result_revenue_daily_total;

drop table if exists tb_recommend_result_revenue_daily;
CREATE TABLE tb_recommend_result_revenue_daily AS
select
	*
	,(case when revenue <> 0 then round( reco_revenue :: float / revenue :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_revenue_daily_s1;

select
	extract(month from date) as month,
	sum(revenue),
	sum(reco_revenue)
from
	tb_recommend_result_revenue_daily
group by 1;
	


drop table if exists tb_recommend_result_prevenue_daily_s1_device;
CREATE TABLE tb_recommend_result_prevenue_daily_s1_device AS
select
	A.date
	,round(sum(A.revenue)/sum(A.frequency),2) prevenue
    ,round(sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end),2) reco_prevenue
    ,round(sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end),2) no_reco_prevenue
    ,round(sum(case when B.session_id is not null then A.revenue end)/sum(case when B.session_id is not null  then A.frequency end),2) loyal_prevenue
    
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_prevenue_daily_s1_total;
CREATE TABLE tb_recommend_result_prevenue_daily_s1_total AS
select
	A.date
	,round(sum(A.revenue)/sum(A.frequency),2) prevenue
    ,round(sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end),2) reco_prevenue
    ,round(sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end),2) no_reco_prevenue
    ,round(sum(case when B.session_id is not null then A.revenue end)/sum(case when B.session_id is not null  then A.frequency end),2) loyal_prevenue
    
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_prevenue_daily_s1;
CREATE TABLE tb_recommend_result_prevenue_daily_s1 AS
select * from tb_recommend_result_prevenue_daily_s1_device
union all
select * from tb_recommend_result_prevenue_daily_s1_total;

drop table if exists tb_recommend_result_prevenue_daily;
CREATE TABLE tb_recommend_result_prevenue_daily AS SELECT *,
    (case when prevenue <> 0  then round(reco_prevenue :: float / (prevenue :: float), 4) * 100 else null end) as reco_up_ratio
FROM tb_recommend_result_prevenue_daily_s1;

drop table if exists tb_recommend_result_quantity_daily_s1_device;
CREATE TABLE tb_recommend_result_quantity_daily_s1_device AS
select
	A.date
	,round(avg(A.quantity :: float),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: float end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: float end),3) no_reco_quantity
    ,round(avg(case when B.session_id is not null then A.quantity :: float end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_quantity_daily_s1_total;
CREATE TABLE tb_recommend_result_quantity_daily_s1_total AS
select
	A.date
	,round(avg(A.quantity :: float),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: float end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: float end),3) no_reco_quantity
    ,round(avg(case when B.session_id is not null then A.quantity :: float end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_quantity_daily_s1;
CREATE TABLE tb_recommend_result_quantity_daily_s1 AS
select * from tb_recommend_result_quantity_daily_s1_device
union all
select * from tb_recommend_result_quantity_daily_s1_total;

drop table if exists tb_recommend_result_quantity_daily;
CREATE TABLE tb_recommend_result_quantity_daily AS SELECT *,
    (case when quantity <> 0  then round(reco_quantity :: float / (quantity :: float), 4) * 100 else null end) as reco_up_ratio
FROM tb_recommend_result_quantity_daily_s1;

drop table if exists tb_recommend_result_click_daily_s1_device;
CREATE TABLE tb_recommend_result_click_daily_s1_device AS 
SELECT 
	date
	,round(AVG(view :: float),3) click
    ,round(AVG(case when reco_view>0 then view :: float end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: float end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: float  end),3) loyal_click
FROM
    tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_click_daily_s1_total;
CREATE TABLE tb_recommend_result_click_daily_s1_total AS 
SELECT 
	date
	,round(AVG(view :: float),3) click
    ,round(AVG(case when reco_view>0 then view :: float end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: float end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: float  end),3) loyal_click
FROM
    tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_click_daily_s1;
CREATE TABLE tb_recommend_result_click_daily_s1 AS 
select * from tb_recommend_result_click_daily_s1_device
union all
select * from tb_recommend_result_click_daily_s1_total;

drop table if exists tb_recommend_result_click_daily;
CREATE TABLE tb_recommend_result_click_daily AS SELECT *,
    (case when click <> 0 then round(reco_click / (click), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_click_daily_s1;   

drop table if exists tb_recommend_result_cvr_daily_s1_device;
CREATE TABLE tb_recommend_result_cvr_daily_s1_device AS 
select 
	A.date
	,round(sum(B.frequency :: float)/count(distinct A.session_id),4)*100 cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),4)*100 reco_cvr
	,round(sum(case when A.reco_view =0 then B.frequency :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),4)*100 no_reco_cvr
	,round(sum(case when A.view >1 then B.frequency :: float end)/count(distinct case when A.view >1 then A.session_id end),4)*100 loyal_cr
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date  and A.session_id = B.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_cvr_daily_s1_total;
CREATE TABLE tb_recommend_result_cvr_daily_s1_total AS 
select 
	A.date
	
	,round(sum(B.frequency :: float)/count(distinct A.session_id),4)*100 cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),4)*100 reco_cvr
	,round(sum(case when A.reco_view =0 then B.frequency :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),4)*100 no_reco_cvr
	,round(sum(case when A.view >1 then B.frequency :: float end)/count(distinct case when A.view >1 then A.session_id end),4)*100 loyal_cr
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date  and A.session_id = B.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_cvr_daily_s1;
CREATE TABLE tb_recommend_result_cvr_daily_s1 AS 
select * from tb_recommend_result_cvr_daily_s1_device
union all
select * from tb_recommend_result_cvr_daily_s1_total;

drop table if exists tb_recommend_result_cvr_daily;
CREATE TABLE tb_recommend_result_cvr_daily AS SELECT *,
    (case when cvr <> 0 then round(reco_cvr / (cvr), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_cvr_daily_s1;   

drop table if exists tb_recommend_result_sa_daily_s1_device;
CREATE TABLE tb_recommend_result_sa_daily_s1_device AS 
select 
	A.date
	
	,round(sum(B.revenue :: float)/count(distinct A.session_id),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
	,round(sum(case when A.reco_view =0 then B.revenue :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
	,round(sum(case when A.view >1 then B.revenue :: float end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_sa
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.session_id = B.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_sa_daily_s1_total;
CREATE TABLE tb_recommend_result_sa_daily_s1_total AS 
select 
	A.date
	
	,round(sum(B.revenue :: float)/count(distinct A.session_id),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
	,round(sum(case when A.reco_view =0 then B.revenue :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
	,round(sum(case when A.view >1 then B.revenue :: float end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_sa
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.session_id = B.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_sa_daily_s1;
CREATE TABLE tb_recommend_result_sa_daily_s1 AS 
select * from tb_recommend_result_sa_daily_s1_device
union all
select * from tb_recommend_result_sa_daily_s1_total;

drop table if exists tb_recommend_result_sa_daily;
CREATE TABLE tb_recommend_result_sa_daily AS SELECT *,
    (case when sa <> 0  then round(reco_sa / (sa), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_sa_daily_s1; 

drop table if exists tb_recommend_result_daily;
CREATE TABLE tb_recommend_result_daily AS 
select
extract(year from a.date) as year,
extract(month from a.date) as month,
extract(day from a.date) as day,
a.visit session_visit_total,
a.reco_visit session_visit_reco, 

b.uv unique_visit_total,
b.reco_uv unique_visit_reco,

c.pv page_view_total,
c.reco_pv page_view_reco,
c.reco_ratio page_view_rate,

d.frequency purchase_count_total,
d.reco_frequency purchase_count_reco,

e.revenue revenue_total,
e.reco_revenue revenue_reco,

f.prevenue customer_transaction_avg_total,
f.reco_prevenue customer_transaction_avg_reco,

g.quantity purchase_item_count_avg_total,
g.reco_quantity purchase_item_count_avg_reco,

h.click visit_item_click_avg_total,
h.reco_click visit_item_click_avg_reco,

i.cvr purchase_conversion_rate_total,
i.reco_cvr purchase_conversion_rate_reco,

j.sa session_purchase_revenue_avg_total,
j.reco_sa session_purchase_revenue_avg_reco

from tb_recommend_result_visit_daily a
left join tb_recommend_result_uv_daily b on a.date = b.date
left join tb_recommend_result_pv_daily c on a.date = c.date
left join tb_recommend_result_frequency_daily d on a.date = d.date
left join tb_recommend_result_revenue_daily e on a.date = e.date
left join tb_recommend_result_prevenue_daily f on a.date = f.date
left join tb_recommend_result_quantity_daily g on a.date = g.date
left join tb_recommend_result_click_daily h on a.date = h.date
left join tb_recommend_result_cvr_daily i on a.date = i.date
left join tb_recommend_result_sa_daily j on a.date = j.date
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24;

select * from tb_recommend_result_daily order by year,month,day ;


