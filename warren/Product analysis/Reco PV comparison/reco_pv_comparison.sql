select 
extract(year from date) as year,
extract(month from date) as month,
device,
sum(case when rc_code like '%REO%' then 1 when rc_code like '%RECO%' then 1 when rc_code like '%RECENT%' then 1 else 0 end) search_pv,
count(*) pv,
sum(case when rc_code like '%REO%' then 1 when rc_code like '%RECO%' then 1 when rc_code like '%RECENT%' then 1 else 0 end)/count(*)::real as ratio
from tb_log_view_daily_report
where convert_timezone('kst', server_time)::date >= '2015-12-01'
group by 1,2,device
order by 1,2,3;