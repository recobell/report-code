--ellotte--

--APP PUSH RESULT
--@start

select A.reco, A.num_pcid, B.user_id, B.revenue, B.rev_per_user, round(B.user_id/A.num_pcid,4) conversion_rate
from
(
		-- APP PUSH VIEW
		-- 레코벨 개인화 마케팅 유입 성과
		select reco, sum(num_pcid) num_pcid
		from ellotte_report2.rt_stat_view
		where reco is not null
			and num_pcid >= 1000
		group by reco
) A inner join 
(
		-- APP PUSH PURCHASE
		-- 레코벨 개인화 마케팅 구매 성과
		-- device 1: PC, 2: mobile Web
		select reco
					, count(distinct user_id) user_id
					-- , sum(item_num) item_num
					, sum(revenue) revenue
					, round( sum(revenue) / count(distinct user_id) ) rev_per_user
		from
		( -- 아래와 같이 일단 중복 제거
				select reco, user_id, max(revenue) revenue
				from ellotte_rblog.tb_log_order
				where reco is not null
					and reco >= 20150701
				group by reco, user_id
		) A
		group by reco
		having count(distinct user_id) >= 100
) B on A.reco = B.reco;
--@end

--APP PUSH TOP 30
--@start

select B.item_id, B.item_name, count(*)*max(price) 
from
(
		select distinct order_id
		from ellotte_rblog.tb_log_order
		where reco is not null and reco >= 20150701
) A inner join 
(
		select *
		from ellotte_rblog.tb_log_order_item
) B
on A.order_id = B.order_id
group by B.item_id, B.item_name
order by count(*)*max(price)  desc limit 30;

--@end

--lottecom--

--APP PUSH RESULT
--@start

select A.reco, A.num_pcid, B.user_id, B.revenue, B.rev_per_user, round(B.user_id/A.num_pcid,4) conversion_rate
from
(
		-- APP PUSH VIEW
		-- 레코벨 개인화 마케팅 유입 성과
		select reco, sum(num_pcid) num_pcid
		from lottecom_report2.rt_stat_view
		where reco is not null
			and num_pcid >= 1000
		group by reco
) A inner join 
(
		-- APP PUSH PURCHASE
		-- 레코벨 개인화 마케팅 구매 성과
		-- device 1: PC, 2: mobile Web
		select reco
					, count(distinct user_id) user_id
					-- , sum(item_num) item_num
					, sum(revenue) revenue
					, round( sum(revenue) / count(distinct user_id) ) rev_per_user
		from
		( -- 아래와 같이 일단 중복 제거
				select reco, user_id, max(revenue) revenue
				from lottecom_rblog.tb_log_order
				where reco is not null
					and reco >= 20150701
				group by reco, user_id
		) A
		group by reco
		having count(distinct user_id) >= 100
) B on A.reco = B.reco;
--@end

--APP PUSH TOP 30
--@start

select B.item_id, B.item_name, count(*)*max(price) 
from
(
		select distinct order_id
		from lottecom_rblog.tb_log_order
		where reco is not null and reco >= 20150701
) A inner join 
(
		select *
		from lottecom_rblog.tb_log_order_item
) B
on A.order_id = B.order_id
group by B.item_id, B.item_name
order by count(*)*max(price)  desc limit 30;

