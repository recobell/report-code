drop table if exists tb_log_view;
create table tb_log_view (  
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(1000),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
item_id varchar(200),
search_term varchar(200)
);


copy tb_log_view
from 's3://rb-logs-apne1/@CUID/view/2016/01'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';


--Basic Tables
drop table if exists tb_log_view_daily_report;
create table tb_log_view_daily_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
	(case when device = 'PW' then 'PW' else 'MW' end) as device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term
from 
tb_log_view
where cuid = 'adafe461-0b59-430e-9504-a1252366a299'
;

select device, count(*) from tb_log_view_daily_report group by device;

drop table if exists tmp_recommend_result_view;
CREATE TABLE tmp_recommend_result_view as
select 
	date
	,pc_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date , pc_id, device;

drop table if exists tb_recommend_result_uv_daily_device;
CREATE TABLE tb_recommend_result_uv_daily_device AS
select
	date
	,device
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv
    
from tmp_recommend_result_view
group by 1,2
order by 1,2;


drop table if exists tb_recommend_result_uv_daily_total;

CREATE TABLE tb_recommend_result_uv_daily_total AS
select
	date
	,'AL' device
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv
from tmp_recommend_result_view
group by 1
order by 1;


drop table if exists tb_recommend_result_uv_daily_s1;
CREATE TABLE tb_recommend_result_uv_daily_s1 AS
select * from tb_recommend_result_uv_daily_device
union all
select * from tb_recommend_result_uv_daily_total;


drop table if exists tb_recommend_result_uv_daily;
CREATE TABLE tb_recommend_result_uv_daily AS
select
	*
	,(case when uv <> 0 then round( reco_uv :: float / uv :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_uv_daily_s1;

--pv
drop table if exists tmp_recommend_result_view_session;
CREATE TABLE tmp_recommend_result_view_session as
select
	date
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date,session_id, device;

drop table if exists tb_recommend_result_pv_daily_device;
CREATE TABLE tb_recommend_result_pv_daily_device AS
select
	date
	,device
	,sum(view) pv
    ,sum(reco_view) reco_pv
	,sum(no_reco_view) no_reco_pv
    
from tmp_recommend_result_view_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_pv_daily_total;
CREATE TABLE tb_recommend_result_pv_daily_total AS
select
	date
	,'AL' device
	,sum(view) pv
    ,sum(reco_view) reco_pv
	,sum(no_reco_view) no_reco_pv
    
from tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_pv_daily_s1;
CREATE TABLE tb_recommend_result_pv_daily_s1 AS
select * from tb_recommend_result_pv_daily_device
union all
select * from tb_recommend_result_pv_daily_total;

drop table if exists tb_recommend_result_pv_daily;
CREATE TABLE tb_recommend_result_pv_daily AS
select
	*
	,(case when pv <> 0 then round( reco_pv :: float / pv :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_pv_daily_s1;


--visit
drop table if exists tb_recommend_result_visit_daily_device;
CREATE TABLE tb_recommend_result_visit_daily_device AS
select
	date
	,device
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,count(distinct case when view > 1 then session_id end) loyal_visit  
    
    
from tmp_recommend_result_view_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_visit_daily_total;
CREATE TABLE tb_recommend_result_visit_daily_total AS
select
	date
	,'AL' device
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,count(distinct case when view > 1 then session_id end) loyal_visit  
    
    
from tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_visit_daily_s1;
CREATE TABLE tb_recommend_result_visit_daily_s1 AS
select * from tb_recommend_result_visit_daily_device
union all
select * from tb_recommend_result_visit_daily_total;

drop table if exists tb_recommend_result_visit_daily;
CREATE TABLE tb_recommend_result_visit_daily AS
select
	*
	,(case when visit <> 0 then round( reco_visit :: float / visit :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_visit_daily_s1;


--click

drop table if exists tb_recommend_result_click_daily_s1_device;
CREATE TABLE tb_recommend_result_click_daily_s1_device AS 
SELECT 
	date, device
	,round(AVG(view :: float),3) click
    ,round(AVG(case when reco_view>0 then view :: float end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: float end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: float  end),3) loyal_click
FROM
    tmp_recommend_result_view_session
group by 1,2
order by 1,2;

drop table if exists tb_recommend_result_click_daily_s1_total;
CREATE TABLE tb_recommend_result_click_daily_s1_total AS 
SELECT 
	date,'AL' device
	,round(AVG(view :: float),3) click
    ,round(AVG(case when reco_view>0 then view :: float end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: float end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: float  end),3) loyal_click
FROM
    tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_click_daily_s1;
CREATE TABLE tb_recommend_result_click_daily_s1 AS 
select * from tb_recommend_result_click_daily_s1_device
union all
select * from tb_recommend_result_click_daily_s1_total;

drop table if exists tb_recommend_result_click_daily;
CREATE TABLE tb_recommend_result_click_daily AS SELECT *,
    (case when click <> 0 then round(reco_click / (click), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_click_daily_s1;   



--Output table

select 
	extract(year from date) as year,
	extract(month from date) as month,
	extract(day from date) as day,
	device,
	uv,
	reco_uv,
	reco_ratio/100	
from tb_recommend_result_uv_daily order by device,date;


select 
	extract(year from date) as year,
	extract(month from date) as month,
	extract(day from date) as day,
	device,
	pv,
	reco_pv,
	reco_ratio/100	
from tb_recommend_result_pv_daily order by device,date;


select 
	extract(year from date) as year,
	extract(month from date) as month,
	extract(day from date) as day,
	device,
	visit,
	reco_visit,
	reco_ratio/100	
from tb_recommend_result_visit_daily order by device,date;

select 
	extract(year from date) as year,
	extract(month from date) as month,
	extract(day from date) as day,
	device,
	click,
	reco_click,
	reco_up_ratio/100	
from tb_recommend_result_click_daily order by device,date;



--Loyalty

--ki: ad62883d-db1d-40a7-b06d-c7fce25e20b6
--hg: e35abc33-705f-47e8-bb9b-eb03eef84bc6
--wp: 860a86e0-0d1e-4d44-b0f3-07ad9d488f29
--zd: adafe461-0b59-430e-9504-a1252366a299

select
	*
from 
	tb_log_view
where cuid = 'ad62883d-db1d-40a7-b06d-c7fce25e20b6'
limit 10

select rc_code, count(*) 
from tb_log_view 
where cuid = 'ad62883d-db1d-40a7-b06d-c7fce25e20b6' 
group by 1;


select 
	convert_timezone('KST', server_time)::date as date, 
	count(distinct session_id) as visit_daily
from
	tb_log_view
where cuid = 'ad62883d-db1d-40a7-b06d-c7fce25e20b6' and rc_code = 'lv'
group by date
order by date;



drop table if exists tb_log_view_daily;
CREATE TABLE tb_log_view_daily AS
select 
	convert_timezone('KST', server_time)::date as date,
	extract(hour from server_time - interval '9 hours') as hour,
	extract(minute from server_time) as minute,
	extract(second from server_time) as second,
	pc_id, 
	session_id,
	item_id,
	rc_code
from tb_log_view
where cuid = 'ad62883d-db1d-40a7-b06d-c7fce25e20b6'
order by hour,1;

drop table if exists tb_log_view_personal;
CREATE TABLE tb_log_view_personal AS
select 
	date,
	hour,
	minute,
	second,
	pc_id,
	session_id,
	rc_code
from
	tb_log_view_daily
group by 1,2,3,4,5,6,7

drop table if exists tb_log_view_personal_s1;
CREATE TABLE tb_log_view_personal_s1 AS
select 
	date,
	hour,
	minute,
	second,
	pc_id,
	(case when rc_code is not null then session_id end) as reco_personal,
	(case when rc_code is null then session_id end) as no_reco_personal
from tb_log_view_personal
order by 2,3,4,1;

select pc_id, 
	count(reco_personal) as rc_code_viewer, 
	count(no_reco_personal) as no_rc_code_viewer 
from tb_log_view_personal_s1 
group by 1;


select
	a.date,
	b.date,
	a.pc_id,
	b.pc_id,
	c.item_id,
	d.item_id
from
	tb_log_view_personal_s1 a
	inner join
	(select date, pc_id from tb_log_view_personal_s1) b on a.pc_id = b.pc_id
	inner join
	tb_log_view_daily c on a.date = c.date and a.pc_id = c.pc_id
	inner join
	tb_log_view_daily d on b.date = d.date and b.pc_id = d.pc_id
where a.date <> b.date
group by 1,2,3,4,5,6
order by 1,2,3,4,5,6
	






