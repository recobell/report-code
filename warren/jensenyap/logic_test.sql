select * from tb_product_view_best order by rank;  limit 1000;
select * from tb_product_order_best order by rank;

select count(*) from tmp_log_order_unrefine where item_id = '214162040';
select count(*) from tmp_log_view_unrefine where item_id = '214162040';
select * from tmp_log_order_unrefine limit 100;


--test

drop table if exists tb_test_jensen_01;
create table tb_test_jensen_01 as
select 
	a.item_id,
	a.view_best_score,
	a.order_best_score,
	a.product_best_score,
	a.view_rank,
	a.order_rank,
	a.product_rank,
	b.revenue,
	b.quantity
from
	(select
		NVL(a.item_id,b.item_id,c.item_id) as item_id,
		a.score as view_best_score,
		b.score as order_best_score,
		c.score as product_best_score,
		a.rank as view_rank,
		b.rank as order_rank,
		c.rank as product_rank
	from
		tb_product_view_best a 
		left join
		tb_product_order_best b on a.item_id = b.item_id
		left join
		tb_product_best c on a.item_id = c.item_id
	order by a.rank) a
	left join
	(select item_id, sum(price * quantity) as revenue, sum(quantity) as quantity from tmp_log_order_unrefine group by 1) b on a.item_id = b.item_id
where revenue is not null
order by b.revenue DESC;

--############################################################################################################################################################
--############################################################################################################################################################

select 
	a.item_id,
	a.score,
	a.rank,
	b.visit,
	b.reco_visit
from
	tb_product_view_best a
	left join
	(select 
		item_id,
		count(*) as visit,  
		count(case when rc_code is not null then session_id end) as reco_visit
	from tmp_log_view_unrefine group by 1) b on a.item_id = b.item_id
where b.reco_visit > 0
order by reco_visit
limit 1000;



select * from tb_asc_recmd_view_result_cur order by score DESC;



