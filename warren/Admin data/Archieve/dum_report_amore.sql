drop table if exists tb_log_view;
create table tb_log_view (	
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(1000),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
item_id varchar(200),
search_term varchar(200)
);

drop table if exists tb_log_search;
create table tb_log_search (
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(200),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
search_term varchar(200),
search_result varchar(200)
);



drop table if exists tb_log_order;
create table tb_log_order (
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(200),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
item_id varchar(200),
search_term varchar(200),
order_id varchar(200),
order_price float,
price float,
quantity bigint
);

drop table if exists tmp_product_info;
create table tmp_product_info (
cuid varchar(200),
type varchar(200),
item_id varchar(200),
item_name varchar(2000),
item_image varchar(200),
item_url varchar(2000),
original_price float,
sale_price float,
category1 varchar(200),
category2 varchar(200),
category3 varchar(200),
category4 varchar(200),
category5 varchar(200),
reg_date varchar(200),
update_date varchar(200),
expire_date timestamp,
stock int,
state varchar(200),
description varchar(200),
extra_image varchar(200),
locale varchar(200)
);

copy tb_log_view
from 's3://rb-logs-apne1/a3ef5e12-d59b-4807-a18f-8a715b6b4838/view/2015'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://rb-rec-report-conf/jsonpaths/tb_log_view.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/a3ef5e12-d59b-4807-a18f-8a715b6b4838/view/2016'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://rb-rec-report-conf/jsonpaths/tb_log_view.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/a3ef5e12-d59b-4807-a18f-8a715b6b4838/order/2016'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://rb-rec-report-conf/jsonpaths/tb_log_order.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/a3ef5e12-d59b-4807-a18f-8a715b6b4838/product/2015'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://rb-rec-report-conf/jsonpaths/tmp_product_info.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/a3ef5e12-d59b-4807-a18f-8a715b6b4838/product/2016'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://rb-rec-report-conf/jsonpaths/tmp_product_info.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';

select * from stl_load_errors limit 100;

select * from tmp_product_info limit 100;

drop table if exists tb_log_view_daily_report;
create table tb_log_view_daily_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
	(case when device = 'PW' then 'PW' else 'MW' end) as device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term
from 
tb_log_view
;

drop table if exists tb_log_view_monthly_report;
create table tb_log_view_monthly_report as
select
	server_time,
	convert_timezone('KST', server_time)::date - extract(day from convert_timezone('KST', server_time))+1 as date, 
	(case when device = 'PW' then 'PW' else 'MW' end) as device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term
from 
tb_log_view
;

drop table if exists tb_product_info;
create table tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0
 group by item_id;

drop table if exists tb_product_info_report;
CREATE TABLE tb_product_info_report as
select
	item_id, 
	item_name, 
	original_price, 
	sale_price, 
	category1, 
	category2, 
	category3, 
	category4, 
	category5, 
	reg_date
	stock
from 
tb_product_info;

drop table if exists tmp_recommend_result_view;
CREATE TABLE tmp_recommend_result_view as
select 
	date
	,pc_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date , pc_id, device;

drop table if exists tmp_recommend_result_order;
CREATE TABLE tmp_recommend_result_order as
select
	date
	,pc_id 
	,device
	
	,sum(price*quantity) revenue
	,sum(case when rc_code is not null then price*quantity else 0 end) reco_revenue
	,sum(case when rc_code is null then price*quantity else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(distinct case when rc_code is not null then order_id else null end) reco_frequency
	,count(distinct case when rc_code is null then order_id else null end) no_reco_frequency
	
from tb_log_order_daily_report
group by date, pc_id, device;

drop table if exists tmp_recommend_result_view_session;
CREATE TABLE tmp_recommend_result_view_session as
select
	date
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date,session_id, device;



drop table if exists tb_recommend_loyal_group;
CREATE TABLE tb_recommend_loyal_group AS SELECT * FROM
    tmp_recommend_result_view_session
WHERE
	view > 1;

drop table if exists tb_recommend_reco_group;
CREATE TABLE tb_recommend_reco_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
where reco_view > 0;

drop table if exists tb_recommend_result_uv_daily_device;
--@end
--@create-tb_recommend_result_uv_daily_device
CREATE TABLE tb_recommend_result_uv_daily_device AS
select
	date
	,device
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv
    
from tmp_recommend_result_view
group by 1,2
order by 1,2;
--@end

--@drop-tb_recommend_result_uv_daily_total
drop table if exists tb_recommend_result_uv_daily_total;
--@end
--@create-tb_recommend_result_uv_daily_total
CREATE TABLE tb_recommend_result_uv_daily_total AS
select
	date
	,'AL' device
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv
from tmp_recommend_result_view
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_uv_daily_s1
drop table if exists tb_recommend_result_uv_daily_s1;
--@end
--@create-tb_recommend_result_uv_daily_s1
CREATE TABLE tb_recommend_result_uv_daily_s1 AS
select * from tb_recommend_result_uv_daily_device
union all
select * from tb_recommend_result_uv_daily_total;
--@end

--@drop-tb_recommend_result_uv_daily
drop table if exists tb_recommend_result_uv_daily;
--@end
--@create-tb_recommend_result_uv_daily
CREATE TABLE tb_recommend_result_uv_daily AS
select
	*
	,(case when uv <> 0 then round( reco_uv :: float / uv :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_uv_daily_s1;
--@end

--@drop-tb_recommend_result_visit_daily_device
drop table if exists tb_recommend_result_visit_daily_device;
--@end
--@create-tb_recommend_result_visit_daily_device
CREATE TABLE tb_recommend_result_visit_daily_device AS
select
	date
	,device
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,count(distinct case when view > 1 then session_id end) loyal_visit  
    
    
from tmp_recommend_result_view_session
group by 1,2
order by 1,2;
--@end

--@drop-tb_recommend_result_visit_daily_total
drop table if exists tb_recommend_result_visit_daily_total;
--@end
--@create-tb_recommend_result_visit_daily_total
CREATE TABLE tb_recommend_result_visit_daily_total AS
select
	date
	,'AL' device
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,count(distinct case when view > 1 then session_id end) loyal_visit  
    
    
from tmp_recommend_result_view_session
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_visit_daily_s1
drop table if exists tb_recommend_result_visit_daily_s1;
--@end
--@create-tb_recommend_result_visit_daily_s1
CREATE TABLE tb_recommend_result_visit_daily_s1 AS
select * from tb_recommend_result_visit_daily_device
union all
select * from tb_recommend_result_visit_daily_total;
--@end


--@drop-tb_recommend_result_visit_daily
drop table if exists tb_recommend_result_visit_daily;
--@end
--@create-tb_recommend_result_visit_daily
CREATE TABLE tb_recommend_result_visit_daily AS
select
	*
	,(case when visit <> 0 then round( reco_visit :: float / visit :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_visit_daily_s1;
--@end


--@drop-tb_recommend_result_pv_daily_device
drop table if exists tb_recommend_result_pv_daily_device;
--@end
--@create-tb_recommend_result_pv_daily_device
CREATE TABLE tb_recommend_result_pv_daily_device AS
select
	date
	,device
	,sum(view) pv
    ,sum(reco_view) reco_pv
	,sum(no_reco_view) no_reco_pv
    
from tmp_recommend_result_view_session
group by 1,2
order by 1,2;
--@end


--@drop-tb_recommend_result_pv_daily_total
drop table if exists tb_recommend_result_pv_daily_total;
--@end
--@create-tb_recommend_result_pv_daily_total
CREATE TABLE tb_recommend_result_pv_daily_total AS
select
	date
	,'AL' device
	,sum(view) pv
    ,sum(reco_view) reco_pv
	,sum(no_reco_view) no_reco_pv
    
from tmp_recommend_result_view_session
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_pv_daily_s1
drop table if exists tb_recommend_result_pv_daily_s1;
--@end
--@create-tb_recommend_result_pv_daily_s1
CREATE TABLE tb_recommend_result_pv_daily_s1 AS
select * from tb_recommend_result_pv_daily_device
union all
select * from tb_recommend_result_pv_daily_total;
--@end

--@drop-tb_recommend_result_pv_daily
drop table if exists tb_recommend_result_pv_daily;
--@end
--@create-tb_recommend_result_pv_daily
CREATE TABLE tb_recommend_result_pv_daily AS
select
	*
	,(case when pv <> 0 then round( reco_pv :: float / pv :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_pv_daily_s1;
--@end



--@drop-tb_recommend_result_click_daily_s1_device
drop table if exists tb_recommend_result_click_daily_s1_device;
--@end
--@create-tb_recommend_result_click_daily_s1_device
CREATE TABLE tb_recommend_result_click_daily_s1_device AS 
SELECT 
	date, device
	,round(AVG(view :: float),3) click
    ,round(AVG(case when reco_view>0 then view :: float end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: float end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: float  end),3) loyal_click
FROM
    tmp_recommend_result_view_session
group by 1,2
order by 1,2;
--@end


--@drop-tb_recommend_result_click_daily_s1_total
drop table if exists tb_recommend_result_click_daily_s1_total;
--@end
--@create-tb_recommend_result_click_daily_s1_total
CREATE TABLE tb_recommend_result_click_daily_s1_total AS 
SELECT 
	date,'AL' device
	,round(AVG(view :: float),3) click
    ,round(AVG(case when reco_view>0 then view :: float end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: float end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: float  end),3) loyal_click
FROM
    tmp_recommend_result_view_session
group by 1
order by 1;
--@end

--@drop-tb_recommend_result_click_daily_s1
drop table if exists tb_recommend_result_click_daily_s1;
--@end
--@create-tb_recommend_result_click_daily_s1
CREATE TABLE tb_recommend_result_click_daily_s1 AS 
select * from tb_recommend_result_click_daily_s1_device
union all
select * from tb_recommend_result_click_daily_s1_total;
--@end
--@drop-tb_recommend_result_click_daily
drop table if exists tb_recommend_result_click_daily;
--@end
--@create-tb_recommend_result_click_daily
CREATE TABLE tb_recommend_result_click_daily AS SELECT *,
    (case when click <> 0 then round(reco_click / (click), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_click_daily_s1;   
--@end

select
a.date,
a.device,
a.visit session_visit_total,
a.reco_visit session_visit_reco, 
a.reco_ratio session_visit_rate,

b.uv unique_visit_total,
b.reco_uv unique_visit_reco,
b.reco_ratio unique_visit_rate,

c.pv page_view_total,
c.reco_pv page_view_reco,
c.reco_ratio page_view_rate,

NULL,
NULL,
NULL,

NULL,
NULL,
NULL,

NULL,
NULL,
NULL,

NULL,
NULL,
NULL,

h.click visit_item_click_avg_total,
h.reco_click visit_item_click_avg_reco,
h.reco_up_ratio visit_item_click_avg_result,

NULL,
NULL,
NULL,

NULL,
NULL,
NULL

from tb_recommend_result_visit_daily a
left join tb_recommend_result_uv_daily b on a.date = b.date and a.device = b.device
left join tb_recommend_result_pv_daily c on a.date = c.date and a.device = c.device

left join tb_recommend_result_click_daily h on a.date = h.date and a.device = h.device
order by date,device;


select * from tb_recommend_result_daily order by date,device;


