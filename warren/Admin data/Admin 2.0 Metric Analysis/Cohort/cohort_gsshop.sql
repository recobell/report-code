--Import take in all pcid
drop table if exists tb_log_pcid;
create table tb_log_pcid as
select
	convert_timezone('KST', server_time)::date as date,
	pc_id
from
	tb_log_view
group by 1,2
order by 1;

--D1~D3 revisitors
drop table if exists tb_log_revisit_D1_to_D3;
create table tb_log_revisit_D1_to_D3 as
select
	a.date,
	count(distinct a.pc_id) as revisit_from_D1_to_D3
from
tb_log_pcid a
inner join
tb_log_pcid b on a.pc_id = b.pc_id 
where datediff(day, b.date, a.date ) < 4 and datediff(day, b.date, a.date ) > 0
and a.date between '2016-01-21' and '2016-02-04'
group by a.date
order by 1;

--D4~D7 revisitors
drop table if exists tb_log_revisit_D4_to_D7;
create table tb_log_revisit_D4_to_D7 as
select
	a.date,
	count(distinct a.pc_id) as revisit_from_D4_to_D7
from
tb_log_pcid a
inner join
tb_log_pcid b on a.pc_id = b.pc_id 
where datediff(day, b.date, a.date ) < 8 and datediff(day, b.date, a.date ) > 3
and a.date between '2016-01-21' and '2016-02-04'
group by a.date
order by 1;

--D1~D7 revisitors
drop table if exists tb_log_revisit_D1_to_D7;
create table tb_log_revisit_D1_to_D7 as
select
	a.date,
	count(distinct a.pc_id) as revisit_from_D1_to_D7
from
tb_log_pcid a
inner join
tb_log_pcid b on a.pc_id = b.pc_id 
where datediff(day, b.date, a.date ) < 8 and datediff(day, b.date, a.date ) > 0
and a.date between '2016-01-21' and '2016-02-04'
group by a.date
order by 1;

--OUTPUT revisitors
select
	a.date,
	a.revisit_from_D1_to_D3,
	b.revisit_from_D4_to_D7,
	c.revisit_from_D1_to_D7
from
	tb_log_revisit_D1_to_D3 a
	inner join
	tb_log_revisit_D4_to_D7 b on a.date = b.date
	inner join
	tb_log_revisit_D1_to_D7 c on a.date = c.date
order by 1;
