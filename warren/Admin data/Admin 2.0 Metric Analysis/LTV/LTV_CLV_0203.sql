
--##################################
--#Customer Retention Rate(Monthly)#
--##################################

drop table if exists tb_total_customer_jensen;
create table tb_total_customer_jensen as
select 
	extract(year from date) as year,
	extract(month from date) as month,
	count(distinct pc_id) as total_customer,
	rank() over (order by year, month ASC) as rank
from 	
	tb_log_view_daily_report
group by 1,2
order by 4;

drop table if exists tb_log_total_new_customer_jensen;
create table tb_log_total_new_customer_jensen as
select
	extract(year from date) as year,
	extract(month from date) as month,
	count(pc_id) as new_customer
from
	(select
		distinct pc_id,
		min(date) as date
	from 
		tb_log_view_daily_report
	group by 1
	order by 2)
group by 1,2
order by 1,2;

drop table if exists tb_log_customer_retention_rate_monthly_jensen;
create table tb_log_customer_retention_rate_monthly_jensen as
select 
	c.year,c.month,
	c.period_end,
	a.new_customer,
	b.period_start,
	((c.period_end::real - a.new_customer::real)/b.period_start::real)*100 as CRR
from
	tb_log_total_new_customer_jensen a
	inner join
	(select year,month,rank,total_customer as period_start from tb_total_customer_jensen) b on a.year = b.year and a.month = b.month
	inner join
	(select year,month,rank,total_customer as period_end from tb_total_customer_jensen) c on b.rank = c.rank - 1
order by c.year,c.month;


--#######################################
--#RECO Customer Retention Rate(Monthly)#
--#######################################


drop table if exists tmp_recommend_result_view_session_jensen;
CREATE TABLE tmp_recommend_result_view_session_jensen as
select
	date
	,pc_id
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date,pc_id, session_id, device;


drop table if exists tmp_recommend_result_order_session_jensen;
CREATE TABLE tmp_recommend_result_order_session_jensen as
select
	A.date
	,A.session_id 
	,A.order_id
	,A.device
	
	,sum(A.price*A.quantity) revenue
	,sum(case when B.reco_view > 0 then A.price*A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.price*A.quantity else 0 end) no_reco_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_price else 0 end) no_reco_actual_payment
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_id end) no_reco_frequency
	
from tb_log_order_daily_report A
left join tmp_recommend_result_view_session_jensen B on A.session_id = B.session_id and A.date = B.date
group by A.date,A.session_id,A.order_id,A.device;


drop table if exists tb_total_reco_customer_jensen;
create table tb_total_reco_customer_jensen as
select 
	extract(year from date) as year,
	extract(month from date) as month,
	count(distinct pc_id) as total_reco_customer,
	rank() over (order by year, month ASC) as rank
from 	
	tmp_recommend_result_view_session_jensen
where reco_view > 0 and no_reco_view = 0
group by 1,2
order by 4;

drop table if exists tb_log_total_new_reco_customer_jensen_s1;
create table tb_log_total_new_reco_customer_jensen_s1 as
select 
	distinct pc_id, 
	min(date), 
	view, 
	reco_view 
from tmp_recommend_result_view_session_jensen 
where reco_view>0 
group by 1,date,3,4
order by date; 

drop table if exists tb_log_total_new_reco_customer_jensen;
create table tb_log_total_new_reco_customer_jensen as
select
	extract(year from date) as year,
	extract(month from date) as month,
	count(pc_id) as new_reco_customer
from
	(select
		distinct pc_id,
		min(date) as date
	from 
		tmp_recommend_result_view_session_jensen 
	where reco_view > 0 and no_reco_view = 0
	group by 1
	order by 2)
group by 1,2
order by 1,2;

drop table if exists tb_log_reco_customer_retention_rate_monthly_jensen;
create table tb_log_reco_customer_retention_rate_monthly_jensen as
select 
	c.year,c.month,
	c.period_end,
	a.new_reco_customer,
	b.period_start,
	((c.period_end::real - a.new_reco_customer::real)/b.period_start::real)*100 as CRR
	
from
	tb_log_total_new_reco_customer_jensen a
	inner join
	(select year,month,rank,total_reco_customer as period_start from tb_total_reco_customer_jensen) b on a.year = b.year and a.month = b.month
	inner join
	(select year,month,rank,total_reco_customer as period_end from tb_total_reco_customer_jensen) c on b.rank = c.rank - 1
order by c.year,c.month;



--##########################################
--#NO RECO Customer Retention Rate(Monthly)#
--##########################################

drop table if exists tb_total_no_reco_customer_jensen;
create table tb_total_no_reco_customer_jensen as
select 
	extract(year from date) as year,
	extract(month from date) as month,
	count(distinct pc_id) as total_no_reco_customer,
	rank() over (order by year, month ASC) as rank
from 	
	tmp_recommend_result_view_session_jensen
where reco_view = 0 and no_reco_view > 0
group by 1,2
order by 4;

drop table if exists tb_log_total_new_no_reco_customer_jensen;
create table tb_log_total_new_no_reco_customer_jensen as
select
	extract(year from date) as year,
	extract(month from date) as month,
	count(pc_id) as new_no_reco_customer
from
	(select
		distinct pc_id,
		min(date) as date
	from 
		tmp_recommend_result_view_session_jensen
	where reco_view = 0 and no_reco_view > 0
	group by 1
	order by 2)
group by 1,2
order by 1,2;

drop table if exists tb_log_no_reco_customer_retention_rate_monthly_jensen;
create table tb_log_no_reco_customer_retention_rate_monthly_jensen as
select 
	c.year,c.month,
	c.period_end,
	a.new_no_reco_customer,
	b.period_start,
	((c.period_end::real - a.new_no_reco_customer::real)/b.period_start::real)*100 as CRR
from
	tb_log_total_new_no_reco_customer_jensen a
	inner join
	(select year,month,rank,total_no_reco_customer as period_start from tb_total_no_reco_customer_jensen) b on a.year = b.year and a.month = b.month
	inner join
	(select year,month,rank,total_no_reco_customer as period_end from tb_total_no_reco_customer_jensen) c on b.rank = c.rank - 1
order by c.year,c.month;

--#########################################
--#Customer Retention Rate Output(Monthly)#
--#########################################

select 
	a.year,
	a.month,
	b.period_end,
	b.new_reco_customer,
	b.period_start,
	c.period_end,
	c.new_no_reco_customer,
	c.period_start,
	a.period_end,
	a.new_customer,
	a.period_start,
	round(b.CRR,2) as reco_CRR,
	round(c.CRR,2) as no_reco_CRR,
	round(a.CRR,2) as total_CRR,
	round(b.CRR/a.CRR,2)ratio
from tb_log_customer_retention_rate_monthly_jensen a
	inner join
     tb_log_reco_customer_retention_rate_monthly_jensen b on a.year=b.year and a.month=b.month
	inner join
     tb_log_no_reco_customer_retention_rate_monthly_jensen c on a.year=c.year and a.month = c.month
order by year,month;


select * from tb_log_reco_customer_retention_rate_monthly_jensen order by year,month;
select * from tb_log_no_reco_customer_retention_rate_monthly_jensen order by year,month;

