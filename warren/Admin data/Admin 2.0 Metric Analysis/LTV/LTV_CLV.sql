
--#################################
--#Customer Expenditures Per Visit#
--#################################

drop table if exists tb_log_expenditures_per_visit_s1;
create table tb_log_expenditures_per_visit_s1 as
SELECT
	A.date,
	B.pc_id,
	A.session_id,
	A.revenue as visit_revenue,
	A.reco_revenue as visit_reco_revenue
FROM
	tmp_recommend_result_order_session A
	INNER join	
	tb_log_order_daily_report B on a.date= b.date and A.session_id = B.session_id
order by 1;

drop table if exists tb_log_expenditures_per_visit;
create table tb_log_expenditures_per_visit as
SELECT
	sum(visit_revenue)/count(session_id) as avg_revenue_per_visit
FROM 	
	tb_log_expenditures_per_visit_s1;

--################
--#Purchase cycle#
--################

drop table if exists tb_log_purchase_cycle_s1;
create table tb_log_purchase_cycle_s1 as
SELECT
	date,
	pc_id,
	session_id
from
	tb_log_view_daily_report
group by 1,2,3
order by 1;	

drop table if exists tb_log_purchase_cycle_s2;
create table tb_log_purchase_cycle_s2 as
select 
	date,
	extract(week from date) week_of_year,
	pc_id,
	count(distinct session_id) nm_of_visit
from
	tb_log_purchase_cycle_s1 
group by 1,2,3
order by 1;

select * from tb_log_purchase_cycle_s2 limit 100;

drop table if exists tb_log_purchase_cycle;
create table tb_log_purchase_cycle as
select 
	(sum(nm_of_visit)::real/count(pc_id)) avg_visit_per_customer
from
	tb_log_purchase_cycle_s2;

--#################################
--#Average Customer Visit Per Week#
--#################################

drop table if exists tb_avg_customer_value_per_week;
create table tb_avg_customer_value_per_week as
select
	round(avg_revenue_per_visit * avg_visit_per_customer, 2) as avg_cv_per_week
from	
	(select*from tb_log_purchase_cycle)a,
	(select*from tb_log_expenditures_per_visit)b;
	

--###########################
--#Average Customer Lifespan#
--###########################

drop table if exists tb_avg_customer_lifespan_s1;
create table tb_avg_customer_lifespan_s1 as
select 
	pc_id,
	max(date)::date - min(date)::date as  customer_lifespan
from tb_log_view_daily_report
group by 1;

drop table if exists tb_avg_customer_lifespan;
create table tb_avg_customer_lifespan as
select 
	sum(customer_lifespan) / count(pc_id)
from
	tb_avg_customer_lifespan_s1;

--#########################
--#Customer Retention Rate#
--#########################
drop table if exists tb_log_total_customer_first_day_of_every_month;
create table tb_log_total_customer_first_day_of_every_month as
select 
	extract(year from date) as year,
	extract(month from date) as month,
	min(extract(day from date)) as day,
	count(pc_id) customers_start
from 	
	tb_log_view_daily_report
group by 1,2
order by 1,2,3;

drop table if exists tb_log_total_customer_last_day_of_every_month;
create table tb_log_total_customer_last_day_of_every_month as
select 
	extract(year from date) as year,
	extract(month from date) as month,
	max(extract(day from date)) as day,
	count(pc_id) customers_end
from 	
	tb_log_view_daily_report
group by 1,2
order by 1,2,3;

drop table if exists tb_log_total_new_customer_s1;
create table tb_log_total_new_customer_s1 as
select
	distinct pc_id,
	extract(year from min(date)) as year,
	extract(month from min(date)) as month,
	extract(day from min(date)) as day
from 
	tb_log_view_daily_report
group by 1
order by 2,3,4;

drop table if exists tb_log_total_new_customer;
create table tb_log_total_new_customer as
select
	year,
	month,
	count(pc_id) as new_customer
from
	tb_log_total_new_customer_s1	
group by 1,2
order by 1,2;

select * from tb_log_total_new_customer; 
select * from tb_log_total_customer_last_day_of_every_month;
select * from tb_log_total_customer_first_day_of_every_month;


drop table if exists tb_log_customer_retention_rate;
create table tb_log_customer_retention_rate as
select
	a.year,a.month,
	((a.customers_end::real - b.new_customer::real)/c.customers_start::real)*100 as CRR
	, a.customers_end::real
	, b.new_customer::real
	, c.customers_start
from
	tb_log_total_customer_last_day_of_every_month a
	inner join
	tb_log_total_new_customer b on a.year=b.year and a.month=b.month
	inner join
	tb_log_total_customer_first_day_of_every_month c on a.year=c.year and a.month=c.month
order by 1,2;
	

--############################
--#Profit Margin Per Customer#
--############################


--##################
--#Rate of Discount#
--##################


--#########################################
--#Avg. Gross Margin per Customer Lifespan#
--#########################################



