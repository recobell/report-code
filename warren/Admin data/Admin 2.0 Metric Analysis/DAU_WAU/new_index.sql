--#############################
--##TOTAL RECO & NO_RECO PCID##
--#############################
drop table if exists tb_log_visitor_total;
create table tb_log_visitor_total as 
select
    min(convert_timezone('KST', server_time)::date) date,
    case when rc_code is not null then pc_id end as reco_pcid,
    case when rc_code is null then pc_id end as no_reco_pcid
from
    tb_log_view_daily_report
group by 2,3
order by 1;

--#####################
--##DAILY ACTIVE USER##
--#####################
drop table if exists tb_log_visitor_total_DAU;
create table tb_log_visitor_total_DAU as 
select
    date,
    count(distinct reco_pcid) as reco_DAU,
    count(distinct no_reco_pcid) as no_reco_DAU
from
    tb_log_visitor_total
group by 1
order by 1;

--######################
--##WEEKLY ACTIVE USER##
--######################
drop table if exists tb_log_visitor_total_WAU;
create table tb_log_visitor_total_WAU as 
select  
    a.date, 
    count(distinct b.reco_pcid) as reco_WAU, 
    count(distinct b.no_reco_pcid) as no_reco_WAU
from (select distinct(date) from tb_log_visitor_total) a
inner join tb_log_visitor_total b 
on a.date >= b.date-7 and a.date < b.date
group by 1
order by 1;




--#############################
--##TOTAL PCID##
--#############################
drop table if exists tb_log_visitor_total_s1;
create table tb_log_visitor_total_s1 as 
select
    min(convert_timezone('KST', server_time)::date) date,
    pc_id
from
    tb_log_view_daily_report
group by 2
order by 1;

--###########################
--##TOTAL DAILY ACTIVE USER##
--###########################
drop table if exists tb_log_visitor_total_DAU_s1;
create table tb_log_visitor_total_DAU_s1 as 
select
    date,
    count(distinct pc_id) as total_DAU
from
    tb_log_visitor_total_s1
group by 1
order by 1;

--############################
--##TOTAL WEEKLY ACTIVE USER##
--############################
drop table if exists tb_log_visitor_total_WAU_s1;
create table tb_log_visitor_total_WAU_s1 as 
select  
    a.date, 
    count(distinct b.pc_id) as total_WAU
from (select distinct(date) from tb_log_visitor_total_s1) a
inner join tb_log_visitor_total_s1 b 
on a.date >= b.date-7 and a.date < b.date
group by 1
order by 1;

drop table if exists tb_log_no_reco_stickiness_s1;
create table tb_log_no_reco_stickiness_s1 as 
select
    a.date,
    round((a.total_DAU::real/b.total_WAU::real)*100,2) as total_stickiness
from
    tb_log_visitor_total_DAU_s1 a
inner join
    tb_log_visitor_total_WAU_s1 b on a.date = b.date
order by 1;



--##########
--##OUTPUT##
--##########

select
    a.date,
    round((a.reco_DAU::real/b.reco_WAU::real)*100,2) as reco_stickiness,
    round((a.no_reco_DAU::real/b.no_reco_WAU::real)*100,2) as no_reco_stickiness,
    round((c.total_DAU::real/d.total_WAU::real)*100,2) as total_stickiness
from
    tb_log_visitor_total_DAU a
inner join
    tb_log_visitor_total_WAU b on a.date = b.date
inner join
    tb_log_visitor_total_DAU_s1 c on a.date = c.date
inner join
    tb_log_visitor_total_WAU_s1 d on a.date = d.date
where a.date > '2015-10-06'
order by 1;
