drop table if exists tb_log_visitor_total;
create table tb_log_visitor_total as 
select
    min(convert_timezone('KST', server_time)::date) date,
    case when rc_code is not null then pc_id end as reco_pcid,
    case when rc_code is null then pc_id end as no_reco_pcid
from
    tb_log_view_daily_report
group by 2,3
order by 1;

--#####################
--##DAILY ACTIVE USER##
--#####################
drop table if exists tb_log_visitor_total_DAU;
create table tb_log_visitor_total_DAU as 
select
    date,
    count(distinct reco_pcid) as reco_DAU,
    count(distinct no_reco_pcid) as no_reco_DAU
from
    tb_log_visitor_total
group by 1
order by 1

--#####################
--##WEEKLY ACTIVE USER##
--#####################
drop table if exists tb_log_visitor_total_WAU;
create table tb_log_visitor_total_WAU as 
select  
    a.date, 
    count(distinct b.reco_pcid) as reco_WAU, 
    count(distinct b.no_reco_pcid) as no_reco_WAU
from (select distinct(date) from tb_log_visitor_total) a
inner join tb_log_visitor_total b 
on a.date >= b.date-7 and a.date < b.date
group by 1
order by 1


select
    a.date,
    (a.reco_DAU::real/b.reco_WAU::real)*100 as reco_stickiness,
    (a.no_reco_DAU::real/b.no_reco_WAU::real)*100 as no_reco_stickiness
from
    tb_log_visitor_total_DAU a
inner join
    tb_log_visitor_total_WAU b on a.date = b.date
order by 1
