
--#########################
--#Customer Retention Rate#
--#########################

drop table if exists tb_total_customer_weekly_s1;
create table tb_total_customer_weekly_s1 as
select 
	extract(year from date) as year,
	extract(month from date) as month,
	extract(day from date) as day,
	pc_id,
	rank() over (order by date asc) as rank
	
from 	
	tb_log_view_daily_report
group by 1,2,3,4
order by 1

drop table if exists tb_total_customer_weekly_s2;
create table tb_total_customer_weekly_s2 as
select
	date,
	rank,
	sum(total_customer) OVER (ORDER BY RANK ASC ROWS BETWEEN CURRENT ROW and 7 following) as weekly_total_customer
from
	tb_total_customer_weekly_s1
group by 1,2,total_customer
order by 1;

drop table if exists tb_total_customer_weekly;
create table tb_total_customer_weekly as
select
	x1.*
from 
	(select * from tb_total_customer_weekly_s2) x1
where x1.rank % 7 = 1
order by x1.date;


--아직  


drop table if exists tb_log_total_new_customer;
create table tb_log_total_new_customer as
select
	extract(year from date) as year,
	extract(month from date) as month,
	count(pc_id) as new_customer
from
	(select
		distinct pc_id,
		min(date) as date
	from 
		tb_log_view_daily_report
	group by 1
	order by 2)
group by 1,2
order by 1,2;

select * from tb_log_view_daily_report where pc_id = '1443085818897657140';

drop table if exists tb_log_customer_retention_rate_weekly;
create table tb_log_customer_retention_rate_weekly as
select 
	*,
	((c.period_end::real - a.new_customer::real)/b.period_start::real)*100 as CRR
from
	tb_log_total_new_customer a
	inner join
	(select year,month,rank,total_customer as period_start from tb_total_customer) b on a.year = b.year and a.month = b.month
	inner join
	(select year,month,rank,total_customer as period_end from tb_total_customer) c on b.rank = c.rank - 1
order by c.year,c.month;




drop table if exists tb_log_customer_retention_rate;
create table tb_log_customer_retention_rate as
select
	a.year,a.month,
	((a.customer_end::real - b.new_customer::real)/c.customer_start::real)*100 as CRR
	, a.customer_end
	, b.new_customer
	, c.customer_start
from
	last_day a
	inner join
	tb_log_total_new_customer b on a.year=b.year and a.month=b.month
	inner join
	first_day c on a.year=c.year and a.month=c.month
order by 1,2;
	

--############################
--#Profit Margin Per Customer#
--############################


--##################
--#Rate of Discount#
--##################


--#########################################
--#Avg. Gross Margin per Customer Lifespan#
--#########################################



