--referrer url for customer that bought the item
select * from tb_log_visit limit 10;


select 
	substring(A.referrer from 0 for 35) as referrer,
	count(*)
from 
	tb_log_visit A
	inner join
	tb_log_order B on A.pc_id = B.pc_id and A.session_id = B.session_id 
group by 1
order by 2 DESC;

--referrer url for customer that click on the the item

select 
	substring(A.referrer from 0 for 35) as referrer,
	count(*)
from 
	tb_log_visit A
	inner join
	tb_log_view B on A.pc_id = B.pc_id and A.session_id = B.session_id 
group by 1
order by 2 DESC;
