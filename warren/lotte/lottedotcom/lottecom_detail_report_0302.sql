
drop table if exists tb_log_view_report;
CREATE TABLE tb_log_view_report as
select
	server_time,
	extract(year from convert_timezone('KST', server_time)) as year, 
	extract(month from convert_timezone('KST', server_time)) as month, 
	extract(day from convert_timezone('KST', server_time)) as day, 
	extract(hour from convert_timezone('KST', server_time)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_Agent,
	pc_Id, 
	session_id, 
	gc_Id, 
	rc_Code,
	item_Id, 
	search_term
from 
tb_log_view 
where device <> 'PW' and convert_timezone('KST', server_time) between '2016-01-25' and '2016-02-24';

drop table if exists tb_log_order_report;
CREATE TABLE tb_log_order_report as
select
	server_time,
	extract(year from convert_timezone('KST', server_time)) as year, 
	extract(month from convert_timezone('KST', server_time)) as month, 
	
	extract(day from convert_timezone('KST', server_time)) as day, 
	extract(hour from convert_timezone('KST', server_time)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_Agent,
	pc_Id, 
	session_id, 
	gc_Id, 
	rc_Code,
	item_Id, 
	search_term, 
	order_Id, 
	order_price,
	price, 
	quantity
from 
tb_log_order 
where device <> 'PW' and convert_timezone('KST', server_time) between '2016-01-25' and '2016-02-24';

drop table if exists tb_product_info;
create table tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0
 group by item_id;

drop table if exists tmp_recommend_result_view_session;
CREATE TABLE tmp_recommend_result_view_session as
select
	year
	,month
	,day
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_Code like '%M_main%' then 1 else 0 end) as spec_reco_view
	,sum(case when rc_Code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_Code is null then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by year,month,day,  session_id, device;

drop table if exists tmp_recommend_result_order_session;
CREATE TABLE tmp_recommend_result_order_session as
select
	A.year
	,A.month
	,A.day
	,A.session_id 
	,A.device
	,count(A.order_Id) order_count

	,sum(A.price*A.quantity) revenue
	,sum(case when B.spec_reco_view > 0 then A.price*quantity else 0 end) spec_reco_revenue
	,sum(case when B.reco_view > 0 then A.price*quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 then A.price*quantity else 0 end) no_reco_revenue

from tb_log_order_report a
left join tmp_recommend_result_view_session b on A.year = B.year and a.month = b.month and a.day = b.day and A.session_id = B.session_id 
group by A.year,A.month,A.day,A.session_id,A.device;

--revenue/reco_revenue
drop table if exists tb_recommend_result_revenue_daily_s1;
CREATE TABLE tb_recommend_result_revenue_daily_s1 AS
select
	year
	,month
	,day
	,sum(revenue) revenue
	,sum(spec_reco_revenue) spec_reco_revenue
	,sum(reco_revenue) reco_revenue    
    
from tmp_recommend_result_order_session
group by 1,2,3;

--session/reco_session
drop table if exists tb_recommend_result_visit_daily_s1;
CREATE TABLE tb_recommend_result_visit_daily_s1 AS
select
	year
	,month
	,day
	,count(distinct session_id) visit
	,count(distinct case when spec_reco_view > 0 then session_id end) spec_reco_visit
	,count(distinct case when reco_view > 0 then session_id end) reco_visit 
	 
    
from tmp_recommend_result_view_session
group by 1,2,3;

--pv
drop table if exists tb_recommend_result_pv_daily_s1;
CREATE TABLE tb_recommend_result_pv_daily_s1 AS
select
	year
	,month
	,day
	,sum(view) pv
	,sum(spec_reco_view) spec_reco_pv
	,sum(reco_view) reco_pv
	
from tmp_recommend_result_view_session
group by 1,2,3;

--direct revenue/session
-- 
-- drop table if exists tb_log_view_report_rank_s1;
-- CREATE TABLE tb_log_view_report_rank_s1 as
-- select
-- 	* 
-- 	,rank() over (partition by session_id order by server_time)
-- from tb_log_view_report;
-- 
-- drop table if exists tb_log_view_report_rank_s2;
-- CREATE TABLE tb_log_view_report_rank_s2 as
-- select
-- 	session_id
-- 	,min(rank) first_reco_view
-- 	,max(rank) last_reco_view
-- from tb_log_view_report_rank_s1
-- where rc_code is not null
-- group by session_id;
-- 
-- drop table if exists tb_log_view_report_rank_s3;
-- CREATE TABLE tb_log_view_report_rank_s3 as
-- select * from tb_log_view_report_rank_s1 where rc_code is not null;
-- 
-- drop table if exists tb_log_view_report_rank_s4;
-- CREATE TABLE tb_log_view_report_rank_s4 as
-- select 
-- A.session_id
-- ,A.first_reco_view
-- ,B.rc_code first_rc_code
-- ,A.last_reco_view
-- from
-- tb_log_view_report_rank_s2 A
-- inner join tb_log_view_report_rank_s3 B 
-- on A.session_id = B.session_id and A.first_reco_view = B.rank;
-- 
-- drop table if exists tb_log_view_report_rank;
-- CREATE TABLE tb_log_view_report_rank as
-- select 
-- A.session_id
-- ,A.first_reco_view
-- ,A.first_rc_code
-- ,A.last_reco_view
-- ,B.rc_code last_rc_code
-- from tb_log_view_report_rank_s4 A
-- inner join tb_log_view_report_rank_s3 B
-- on A.session_id = B.session_id and A.last_reco_view = B.rank;
-- 
drop table if exists tb_session_id_view_s1;
create table tb_session_id_view_s1 as
select
	A.session_id
	,count(*) as view
	,sum(case when A.rc_code like '%M_main%' then 1 else 0 end) as spec_reco_view
	,sum(case when A.rc_code is not null then 1 else 0 end) as reco_view
	,count(distinct A.item_id) as itemvar
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) first_server_time

from tb_log_view_report A
group by A.session_id;

-- drop table if exists tb_session_id_view;
-- create table tb_session_id_view as
-- select
-- 	A.*
-- 	,B.first_reco_view
-- 	,B.first_rc_code
-- 	,B.last_reco_view
-- 	,B.last_rc_code
-- from tb_session_id_view_s1 A 
-- left join tb_log_view_report_rank B on A.session_id = B.session_id;
-- 
-- drop table if exists tb_log_view_first_four;
-- CREATE TABLE tb_log_view_first_four as 
-- select 
-- 	session_id
-- 	,max(case when rank = 1 and a.item_id is not null and rc_code like '%M_main' then concat('reco_',a.rc_code) when rank = 1 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) first_view
-- 	,max(case when rank = 2 and a.item_id is not null and rc_code like '%M_main' then concat('reco_',a.rc_code) when rank = 2 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) second_view
-- 	,max(case when rank = 3 and a.item_id is not null and rc_code like '%M_main' then concat('reco_',a.rc_code) when rank = 3 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) third_view
-- 	,max(case when rank = 4 and a.item_id is not null and rc_code like '%M_main' then concat('reco_',a.rc_code) when rank = 4 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) fourth_view
-- 	,max(case when rank = 1 and a.item_id is not null then b.category1 end) first_category
-- 	,max(case when rank = 2 and a.item_id is not null then b.category1 end) second_category
-- 	,max(case when rank = 3 and a.item_id is not null then b.category1 end) third_category
-- 	,max(case when rank = 4 and a.item_id is not null then b.category1 end) fourth_category
-- from tb_log_view_report_rank_s1 a
-- left join tb_product_info b on a.item_id = b.item_id
-- group by session_id;
-- 




drop table if exists tb_order_stat;
create table tb_order_stat as
select
	order_id
	,sum(quantity) order_item_count
	,sum(price * quantity) order_price
from tb_log_order_report
group by 1;

drop table if exists tb_order_item_within_24h;
create table tb_order_item_within_24h as
select 
	a.pc_id
	,a.session_id
	,a.rc_code
	,a.item_id
	,a.server_time view_time
	,b.server_time order_time
	,b.order_id
	,b.price
	,b.quantity
from tb_log_view_report a
join tb_log_order_report b on a.pc_id = b.pc_id and a.item_id = b.item_id
where datediff (minute, a.server_time, b.server_time ) < 1440 and datediff(minute, a.server_time, b.server_time ) >= 0;

drop table if exists tb_view_order_24h;
create table tb_view_order_24h as
select a.pc_id, a.session_id, a.rc_code, a.item_id, a.server_time as view_time, b.order_time, b.order_id 
from tb_log_view_report a
left join tb_order_item_within_24h b on a.pc_id = b.pc_id and a.item_id = b.item_id and a.session_id = b.session_id and a.server_time = b.view_time;


drop table if exists tb_session_direct_revenue;
create table tb_session_direct_revenue as
select
	session_id
	,sum(price * quantity) spec_reco_direct_revenue
from (select session_id, item_id, avg(price) price, max(quantity) quantity, max(order_time) order_time, count(*) count from tb_order_item_within_24h where rc_code like '%M_main%' group by 1,2)
group by 1;


--##########################################################################################
--view and order
--order (frequency, quantity, revenue)
--##########################################################################################
drop table if exists tb_session_id_order;
create table tb_session_id_order as
	SELECT 
		session_id
		,min(server_time) server_time
		--sum(quantity) quantity,
        ,sum(price * quantity) revenue
		--count(distinct item_id) buyitemnum,
		--count(distinct order_id) ordercount
    FROM
        tb_log_order_report
    GROUP BY session_id;



--##########################################################################################
--final funnel metric table
--##########################################################################################

drop table if exists tb_session_funnel;
create table tb_session_funnel as
select
--	a.first_server_time first_view_time
	
--	,a.session_id
--	,a.first_reco_view
--	,a.first_rc_code
--	,a.last_reco_view
--	,a.last_rc_code
--	,a.view view_count
	a.reco_view reco_view_count
	,a.spec_reco_view spec_reco_view_count
--	,a.itemvar item_var
--	,a.period view_period
--	,b.first_view
--	,b.second_view
--	,b.third_view
--	,b.fourth_view
--	,b.first_category
--	,b.second_category
--	,b.third_category
--	,b.fourth_category
	,c.spec_reco_direct_revenue 
--	,d.revenue order_revenue
	,d.server_time order_time
from tb_session_id_view_s1 A	
-- from tb_session_id_view A
--left join tb_log_view_first_four B on A.session_id = B.session_id
left join tb_session_direct_revenue C on A.session_id = C.session_id
left join tb_session_id_order D on A.session_id = D.session_id;

drop table if exists tb_reco_session;
create table tb_reco_session as
select 
	extract(year from convert_timezone('KST', order_time)) as year
	,extract(month from convert_timezone('KST', order_time)) as month
	,extract(day from convert_timezone('KST', order_time)) as day
	,sum(spec_reco_direct_revenue) spec_reco_direct_revenue
	,sum(case when reco_view_count > 0 then 1 else 0 end) reco_order_session
	,sum(case when spec_reco_view_count > 0 then 1 else 0 end) spec_reco_order_session
	,sum(case when reco_view_count = 0 then 1 else 0 end) no_reco_order_session
from tb_session_funnel
where order_time is not null
group by 1,2,3;



select
	a.year, 
	a.month,
	a.day,
	a.revenue,
	a.spec_reco_revenue,
	a.spec_reco_revenue::float/a.revenue::float, 
	d.spec_reco_direct_revenue,
	d.spec_reco_direct_revenue::float/a.revenue::float,
	b.visit,
	b.spec_reco_visit,
	b.spec_reco_visit::float/b.visit::float
	,c.spec_reco_pv
	,d.spec_reco_order_session
	,d.spec_reco_order_session::float/b.spec_reco_visit::float
	,d.no_reco_order_session
	,d.no_reco_order_session::float/(b.visit-b.reco_visit)::float

from tb_recommend_result_revenue_daily_s1 a 
left join tb_recommend_result_visit_daily_s1 b on a.year = b.year and a.month = b.month and a.day = b.day
left join tb_recommend_result_pv_daily_s1 c on a.year = c.year and a.month = c.month and a.day = c.day
left join tb_reco_session d on a.year = d.year and a.month = d.month and a.day = d.day
order by year,month,day;
