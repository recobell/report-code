--@drop-tb_log_view
drop table if exists tb_log_view;
--@end

--@create-tb_log_view
create table tb_log_view (	
cuid varchar(200),
type varchar(200),
serverTime timestamp,
ipAddress varchar(200),
userAgent varchar(2000),
apiVersion varchar(200),
domain varchar(200),
device varchar(200),
pcId varchar(200),
sessionId varchar(200),
adId varchar(200),
pushId varchar(200),
gcId varchar(200),
userId varchar(1000),
abGroup varchar(200),
rcCode varchar(200),
adCode varchar(200),
locale varchar(200),
itemId varchar(200),
searchTerm varchar(200)
);
--@end

--@drop-tb_log_search
drop table if exists tb_log_search;
--@end

--@create-tb_log_search
create table tb_log_search (
cuid varchar(200),
type varchar(200),
serverTime timestamp,
ipAddress varchar(200),
userAgent varchar(2000),
apiVersion varchar(200),
domain varchar(200),
device varchar(200),
pcId varchar(200),
sessionId varchar(200),
adId varchar(200),
pushId varchar(200),
gcId varchar(200),
userId varchar(200),
abGroup varchar(200),
rcCode varchar(200),
adCode varchar(200),
locale varchar(200),
searchTerm varchar(200),
searchResult varchar(200)
);
--@end

--@drop-tb_log_order
drop table if exists tb_log_order;
--@end

--@create-tb_log_order
create table tb_log_order (
cuid varchar(200),
type varchar(200),
serverTime timestamp,
ipAddress varchar(200),
userAgent varchar(2000),
apiVersion varchar(200),
domain varchar(200),
device varchar(200),
pcId varchar(200),
sessionId varchar(200),
adId varchar(200),
pushId varchar(200),
gcId varchar(200),
userId varchar(200),
abGroup varchar(200),
rcCode varchar(200),
adCode varchar(200),
locale varchar(200),
itemId varchar(200),
searchTerm varchar(200),
orderId varchar(200),
orderPrice bigint,
price bigint,
quantity bigint
);
--@end

--@drop-tmp_product_info
drop table if exists tmp_product_info;
--@end

--@create-tmp_product_info
create table tmp_product_info (
cuid varchar(200),
type varchar(200),
item_id varchar(200),
item_name varchar(2000),
item_image varchar(200),
item_url varchar(2000),
original_price int,
sale_price int,
category1 varchar(200),
category2 varchar(200),
category3 varchar(200),
category4 varchar(200),
category5 varchar(200),
reg_date varchar(200),
update_date timestamp,
expire_date timestamp,
stock int,
state varchar(200),
description varchar(200),
extra_image varchar(200),
locale varchar(200)
);
--@end

--@drop-tb_log_visit
drop table if exists tb_log_visit;
--@end

--@create-tb_log_visit
create table tb_log_visit (
cuid varchar(200),
type varchar(200),
pcId varchar(200),
gcId varchar(200),
sessionId varchar(200),
device varchar(200),
url varchar(2000),
referrer varchar(2000),
ipAddress varchar(200),
userAgent varchar(2000),
abGroup varchar(200),
rcCode varchar(200),
adCode varchar(200),
locale varchar(200),
itemId varchar(200),
searchTerm varchar(200),
orderId varchar(200),
orderPrice bigint,
price bigint,
quantity bigint
);
--@end


--# @DATE_STR should be replaced by a date pattern 

--@copy-tb_log_view
copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/12/21'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';


copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/12/22'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';


copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/12/23'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/12/24'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/12/25'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/12/26'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/12/27'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/12/28'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/12/29'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/12/30'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/12/31'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2016/01'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

--@end

--@copy-tb_log_search
copy tb_log_search
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/search/2015/12/21'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_keyETuQOe2UKt8dlvRWvhctHTDgHltOrtn3azkEyf1B'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_search
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/search/2015/12/22'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_search
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/search/2015/12/23'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_search
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/search/2015/12/24'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_search
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/search/2015/12/25'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_search
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/search/2015/12/26'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_search
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/search/2015/12/27'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_search
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/search/2015/12/28'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_search
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/search/2015/12/29'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_search
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/search/2015/12/30'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_search
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/search/2016/01'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';
--@end

--@copy-tb_log_order
copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/12/21'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/12/22'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/12/23'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/12/24'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/12/25'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/12/26'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/12/27'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/12/28'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/12/29'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/12/30'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/12/31'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2016/01'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';
--@end

--@copy-tmp_product_info
copy tmp_product_info
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/product/2015/12/21'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/product/2015/12/22'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';


copy tmp_product_info
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/product/2015/12/23'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/product/2015/12/24'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';


copy tmp_product_info
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/product/2015/12/25'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/product/2015/12/26'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/product/2015/12/27'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/product/2015/12/28'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/product/2015/12/29'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/product/2015/12/30'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/product/2015/12/31'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/product/2016/01'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
maxerror 100000
region 'ap-northeast-1';
--@end

--@end


--####################################################################################
--narrowing data we use

drop table if exists tb_log_view_report;
CREATE TABLE tb_log_view_report as
select
	servertime,
	extract(year from convert_timezone('KST', servertime)) as year, 
	extract(month from convert_timezone('KST', servertime)) as month, 
	extract(day from convert_timezone('KST', servertime)) as day, 
	extract(hour from convert_timezone('KST', servertime)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	useragent,
	pcid, 
	sessionid, 
	gcid, 
	rccode,
	itemid, 
	searchterm
from 
tb_log_view;

drop table if exists tb_log_order_report;
CREATE TABLE tb_log_order_report as
select
	servertime,
	extract(year from convert_timezone('KST', servertime)) as year, 
	extract(month from convert_timezone('KST', servertime)) as month, 
	
	extract(day from convert_timezone('KST', servertime)) as day, 
	extract(hour from convert_timezone('KST', servertime)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	useragent,
	pcid, 
	sessionid, 
	gcid, 
	rccode,
	itemid, 
	searchterm, 
	orderid, 
	orderprice,
	price, 
	quantity
from 
tb_log_order;

select * from tb_log_order_report limit 100;
drop table if exists tb_product_info;
--@end

--@create-tb_product_info
create table tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0
 group by item_id;
 --@end
 
drop table if exists tb_product_info_report;
CREATE TABLE tb_product_info_report as
select
	item_id, 
	item_name, 
	original_price, 
	sale_price, 
	category1, 
	category2, 
	category3, 
	category4, 
	category5, 
	reg_date
	stock
from 
tb_product_info;

--recommendation resource table
drop table if exists tmp_recommend_result_view;
CREATE TABLE tmp_recommend_result_view as
select 
	year
	,month
	,day
	,pcid 
	,device
	,count(*) as view
	,sum(case when rccode is not null then 1 else 0 end) as reco_view
	,sum(case when rccode is null then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by year,month,day , pcid, device;

drop table if exists tmp_recommend_result_order;
CREATE TABLE tmp_recommend_result_order as
select
	year
	,month
	,day
	,pcid 
	,device
	
	,sum(price*quantity) revenue
	,sum(case when rccode is not null then price*quantity else 0 end) reco_revenue
	,sum(case when rccode is null then price*quantity else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(case when rccode is not null then 1 else 0 end) reco_frequency
	,count(case when rccode is null then 1 else 0 end) no_reco_frequency
	
from tb_log_order_report
group by year,month,day, pcid, device;

drop table if exists tmp_recommend_result_view_session;
CREATE TABLE tmp_recommend_result_view_session as
select
	year
	,month
	,day
	,sessionid 
	,device
	,count(*) as view
	,sum(case when rccode is not null then 1 else 0 end) as reco_view
	,sum(case when rccode is null then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by year,month,day,  sessionid, device;

drop table if exists tmp_recommend_result_order_session;
CREATE TABLE tmp_recommend_result_order_session as
select
	A.year
	,A.month
	,A.day
	,A.sessionid 
	,A.orderid
	,A.device
	
	,sum(A.price*A.quantity) revenue
	,sum(case when B.reco_view > 0 then A.price*quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 then A.price*quantity else 0 end) no_reco_revenue
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.itemid) itemvar
	,count(distinct case when B.reco_view > 0 then A.itemid end) reco_itemvar
	,count(distinct case when B.reco_view = 0 then A.itemid end) no_reco_itemvar
	
	,count(distinct A.orderid) frequency
	,count(distinct case when B.reco_view > 0 then A.orderid end) reco_frequency
	,count(distinct case when B.reco_view = 0 then A.orderid end) no_reco_frequency
	
from tb_log_order_report A
left join tmp_recommend_result_view_session B on A.sessionid = B.sessionid 
group by A.year,A.month,A.day,A.sessionid,A.orderid,A.device;



--####################################################################################################
--중간 테이블 확인
--####################################################################################################
select * from tb_log_view limit 10;
select * from tb_log_order where orderprice > 3000 limit 100;
select * from tb_log_search limit 10;
select * from tb_product_info limit 100;
--####################################################################################################
--group
--####################################################################################################

--loyal group : comparison
drop table if exists tb_recommend_loyal_group;
CREATE TABLE tb_recommend_loyal_group AS SELECT * FROM
    tmp_recommend_result_view_session
WHERE
	view > 1;

drop table if exists tb_recommend_reco_group;
CREATE TABLE tb_recommend_reco_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
where reco_view > 0;
	



--####################################################################################################
--현 상황
--####################################################################################################

--********
--daily
--********
--UV check
drop table if exists tb_recommend_result_uv_daily_s1;
CREATE TABLE tb_recommend_result_uv_daily_s1 AS
select
	year
	,month
	,day
	,count(distinct pcid) uv
    ,count(distinct case when reco_view > 0 then pcid end) reco_uv
from tmp_recommend_result_view
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_uv_daily;
CREATE TABLE tb_recommend_result_uv_daily AS
select
	*
	,round( reco_uv :: real / uv :: real , 2) ratio
from tb_recommend_result_uv_daily_s1;


--visit
drop table if exists tb_recommend_result_visit_daily_s1;
CREATE TABLE tb_recommend_result_visit_daily_s1 AS
select
	year
	,month
	,day
	,count(distinct sessionid) visit
    ,count(distinct case when reco_view > 0 then sessionid end) reco_visit    
    
from tmp_recommend_result_view_session
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_visit_daily;
CREATE TABLE tb_recommend_result_visit_daily AS
select
	*
	,round( reco_visit :: real / visit :: real , 2) ratio
from tb_recommend_result_visit_daily_s1;

--detail view click
drop table if exists tb_recommend_result_pv_daily_s1;
CREATE TABLE tb_recommend_result_pv_daily_s1 AS
select
	year
	,month
	,day
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv
    
    
from tmp_recommend_result_view_session
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_pv_daily;
CREATE TABLE tb_recommend_result_pv_daily AS
select
	*
	,round( reco_pv :: real / pv :: real , 2) ratio
from tb_recommend_result_pv_daily_s1;

-- frequency
drop table if exists tb_recommend_result_frequency_daily_s1;
CREATE TABLE tb_recommend_result_frequency_daily_s1 AS
select
	year
	,month
	,day
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency    
    
from tmp_recommend_result_order_session
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_frequency_daily;
CREATE TABLE tb_recommend_result_frequency_daily AS
select
	*
	,round( reco_frequency :: real / frequency :: real , 2) ratio
from tb_recommend_result_frequency_daily_s1;


-- revenue
drop table if exists tb_recommend_result_revenue_daily_s1;
CREATE TABLE tb_recommend_result_revenue_daily_s1 AS
select
	year
	,month
	,day
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
    
from tmp_recommend_result_order_session
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_revenue_daily;
CREATE TABLE tb_recommend_result_revenue_daily AS
select
	*
	,round( reco_revenue :: real / revenue :: real , 2) ratio
from tb_recommend_result_revenue_daily_s1;

--####################################################################################################
--추천 지표
--####################################################################################################

--********
--daily
--********

drop table if exists tb_recommend_result_prevenue_daily_s1;
CREATE TABLE tb_recommend_result_prevenue_daily_s1 AS
select
	A.year
	,A.month
	,A.day
	,sum(A.revenue)/sum(A.frequency) prevenue
    ,sum(case when C.sessionid is not null then A.revenue end)/sum(case when C.sessionid is not null then A.frequency end) reco_prevenue
    ,sum(case when C.sessionid is null then A.revenue end)/sum(case when C.sessionid is null then A.frequency end) no_reco_prevenue
    --,sum(case when B.sessionid is not null then A.revenue end)/sum(case when B.sessionid is not null  then A.frequency end) loyal_prevenue
	
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.year = B.year and A.month = B.month and A.sessionid = B.sessionid
left join tb_recommend_reco_group C on A.year = C.year and A.month = C.month and A.sessionid = C.sessionid
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_prevenue_daily;
CREATE TABLE tb_recommend_result_prevenue_daily AS SELECT *,
    round(reco_prevenue :: real / no_reco_prevenue :: real, 4) as reco_up_ratio
FROM tb_recommend_result_prevenue_daily_s1;

--미완성
--quantity (구매당 아이템)
drop table if exists tb_recommend_result_quantity_daily_s1;
CREATE TABLE tb_recommend_result_quantity_daily_s1 AS
select
	A.year, A.month,A.day
	,round(avg(A.quantity :: real),3) quantity
    ,round(avg(case when C.sessionid is not null then A.quantity :: real end),3) reco_quantity
    ,round(avg(case when C.sessionid is null then A.quantity :: real end),3) no_reco_quantity
    --,round(avg(case when B.sessionid is not null then A.quantity :: real end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.year = B.year and A.month = B.month and A.sessionid = B.sessionid
left join tb_recommend_reco_group C on A.year = C.year and A.month = C.month and A.sessionid = C.sessionid
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_quantity_daily;
CREATE TABLE tb_recommend_result_quantity_daily AS SELECT *,
    round(reco_quantity :: real / no_reco_quantity :: real, 4) as reco_up_ratio
FROM tb_recommend_result_quantity_daily_s1;



--pclick data 
drop table if exists tb_recommend_result_click_daily_s1;
CREATE TABLE tb_recommend_result_click_daily_s1 AS 
SELECT 
	year, month,day
	,round(AVG(view :: real),3) click
    ,round(AVG(case when reco_view>0 then view :: real end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: real end),3) no_reco_click
    --,round(AVG(case when view > 1 then view :: real  end),3) loyal_click
    
FROM
    tmp_recommend_result_view_session
group by 1,2,3
order by 1,2,3;



drop table if exists tb_recommend_result_click_daily;
CREATE TABLE tb_recommend_result_click_daily AS SELECT *,
    round(reco_click / no_reco_click, 4) as reco_up_ratio
FROM tb_recommend_result_click_daily_s1;   

-- CVR data
drop table if exists tb_recommend_result_cvr_daily_s1;
CREATE TABLE tb_recommend_result_cvr_daily_s1 AS 
select 
	A.year, A.month,A.day
	,round(sum(B.frequency :: real)/count(distinct A.sessionid),3) cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: real end)/count(distinct case when A.reco_view >0 then A.sessionid end),3) reco_cr
	,round(sum(case when A.reco_view =0 then B.frequency :: real end)/count(distinct case when A.reco_view =0 then A.sessionid end),3) no_reco_cr
	--,round(sum(case when A.view >1 then B.frequency :: real end)/count(distinct case when A.view >1 then A.sessionid end),3) loyal_cr
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.year = B.year and A.month = B.month  and A.sessionid = B.sessionid
group by 1,2,3
order by 1,2,3;


drop table if exists tb_recommend_result_cvr_daily;
CREATE TABLE tb_recommend_result_cvr_daily AS SELECT *,
    round(reco_cr / no_reco_cr, 4) as reco_up_ratio
FROM tb_recommend_result_cvr_daily_s1;   

-- sa data
drop table if exists tb_recommend_result_sa_daily_s1;
CREATE TABLE tb_recommend_result_sa_daily_s1 AS 
select 
	A.year, A.month,A.day
	,round(sum(B.revenue :: real)/count(distinct A.sessionid),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: real end)/count(distinct case when A.reco_view >0 then A.sessionid end),3) reco_sa
	,round(sum(case when A.reco_view =0 then B.revenue :: real end)/count(distinct case when A.reco_view =0 then A.sessionid end),3) no_reco_sa
	--,round(sum(case when A.view >1 then B.revenue :: real end)/count(distinct case when A.view >1 then A.sessionid end),3) loyal_sa
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.year = B.year and A.month = B.month  and A.sessionid = B.sessionid
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_sa_daily;
CREATE TABLE tb_recommend_result_sa_daily AS SELECT *,
    round(reco_sa / no_reco_sa, 4) as reco_up_ratio
FROM tb_recommend_result_sa_daily_s1; 


--###############################################
--Funnel
--###############################################


--************************************************************************
--pc_id, session_id matching
--************************************************************************

drop table if exists tb_log_session_pc_id;
CREATE TABLE tb_log_session_pc_id as
select
	pcid
	,sessionid
from tb_log_view_report
group by pcid, sessionid;


--************************************************************************
--first reco view
--************************************************************************
drop table if exists tb_log_view_report_rank_s1;
CREATE TABLE tb_log_view_report_rank_s1 as
select
	* 
	,rank() over (partition by sessionid order by servertime)
from tb_log_view_report;

drop table if exists tb_log_view_report_rank_s2;
CREATE TABLE tb_log_view_report_rank_s2 as
select
	sessionid
	,min(rank) first_reco_view
	,max(rank) last_reco_view
from tb_log_view_report_rank_s1
where rccode is not null
group by sessionid;

drop table if exists tb_log_view_report_rank_s3;
CREATE TABLE tb_log_view_report_rank_s3 as
select * from tb_log_view_report_rank_s1 where rccode is not null;

drop table if exists tb_log_view_report_rank_s4;
CREATE TABLE tb_log_view_report_rank_s4 as
select 
A.sessionid
,A.first_reco_view
,B.rccode first_rc_code
,A.last_reco_view
from
tb_log_view_report_rank_s2 A
inner join tb_log_view_report_rank_s3 B 
on A.sessionid = B.sessionid and A.first_reco_view = B.rank;

drop table if exists tb_log_view_report_rank;
CREATE TABLE tb_log_view_report_rank as
select 
A.sessionid
,A.first_reco_view
,A.first_rc_code
,A.last_reco_view
,B.rccode last_rc_code

from tb_log_view_report_rank_s4 A
inner join tb_log_view_report_rank_s3 B
on A.sessionid = B.sessionid and A.last_reco_view = B.rank;

--************************************************************************
--first four view
--************************************************************************
drop table if exists tb_log_view_first_four;
CREATE TABLE tb_log_view_first_four as 
select 
	sessionid
	,max(case when rank = 1 and a.itemid is not null and rccode is not null then concat('reco_',a.rccode) when rank = 1 and rccode is null then a.itemid when a.itemid is null then 'exit' end) first_view
	,max(case when rank = 2 and a.itemid is not null and rccode is not null then concat('reco_',a.rccode) when rank = 2 and rccode is null then a.itemid when a.itemid is null then 'exit' end) second_view
	,max(case when rank = 3 and a.itemid is not null and rccode is not null then concat('reco_',a.rccode) when rank = 3 and rccode is null then a.itemid when a.itemid is null then 'exit' end) third_view
	,max(case when rank = 4 and a.itemid is not null and rccode is not null then concat('reco_',a.rccode) when rank = 4 and rccode is null then a.itemid when a.itemid is null then 'exit' end) fourth_view
	,max(case when rank = 1 and a.itemid is not null then b.category1 end) first_category
	,max(case when rank = 2 and a.itemid is not null then b.category1 end) second_category
	,max(case when rank = 3 and a.itemid is not null then b.category1 end) third_category
	,max(case when rank = 4 and a.itemid is not null then b.category1 end) fourth_category
from tb_log_view_report_rank_s1 a
left join tb_product_info b on a.itemid = b.item_id
group by sessionid;

--************************************************************************
--view
--************************************************************************

drop table if exists tb_crm_kpi_view_s1;
create table tb_crm_kpi_view_s1 as
select
	A.pcid
	,A.device
	,A.sessionid
	,count(*) as view
	,sum(case when A.rccode is not null then 1 else 0 end) as reco_view
	,count(distinct A.itemid) as itemvar
	,datediff(second,min(servertime),max(servertime)) as period
	,min(servertime) first_server_time

from tb_log_view_report A
group by A.pcid, A.device, A.sessionid;



drop table if exists tb_crm_kpi_view_s2;
create table tb_crm_kpi_view_s2 as
select
	A.*
	,B.first_reco_view
	,B.first_rc_code
	,B.last_reco_view
	,B.last_rc_code
from tb_crm_kpi_view_s1 A 
left join tb_log_view_report_rank B on A.sessionid = B.sessionid;

drop table if exists tb_reco_revenue_s1;
create table tb_reco_revenue_s1 as
select 
	A.sessionid
	,A.itemid
	,B.price
	,B.quantity
from (select min(case when rccode is not null then servertime end) server_time, sessionid, itemid, max(case when rccode is not null then 1 else 0 end) rc_num from tb_log_view_report group by sessionid, itemid) A
inner join tb_log_order_report B on A.sessionid = B.sessionid  and A.itemid = B.itemid
where A.rc_num = 1 and a.server_time < b.servertime;

drop table if exists tb_reco_revenue;
create table tb_reco_revenue as
select 
	sessionid
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s1
group by sessionid;

drop table if exists tb_crm_kpi_order_s1;
create table tb_crm_kpi_order_s1 as
	SELECT 
		
        pcid,
		sessionid,
		min(servertime) server_time,
		sum(quantity) quantity,
        sum(price * quantity) revenue,
		count(distinct itemid) buyitemnum,
		count(distinct orderid) ordercount
    FROM
        tb_log_order_report
    GROUP BY pcid, sessionid;



--##########################################################################################
--final funnel metric table
--##########################################################################################

drop table if exists tb_session_funnel;
create table tb_session_funnel as
select
	A.first_server_time first_view_time
	,B.server_time order_time
	,A.sessionid
	,E.pcid
	,A.device
	,A.first_reco_view
	,A.first_rc_code
	,A.last_reco_view
	,A.last_rc_code
	,A.view view_count
	,A.reco_view reco_view_count
	,A.itemvar item_var
	,A.period view_period
	,D.first_view
	,D.second_view
	,D.third_view
	,D.fourth_view
	,D.first_category
	,D.second_category
	,D.third_category
	,D.fourth_category
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count
	,C.reco_revenue 
	,C.reco_quantity
	
from tb_crm_kpi_view_s2 A
left join tb_crm_kpi_order_s1 B on A.sessionid = B.sessionid
left join tb_reco_revenue C on A.sessionid = C.sessionid
left join tb_log_view_first_four D on A.sessionid = D.sessionid
left join tb_log_session_pc_id E on A.sessionid = E.sessionid;

drop table if exists tb_direct_session;
create table tb_direct_session as
select 
	a.year,a.month,a.day,
	a.direct_reco_session,
	b.direct_no_reco_session
from
	(select extract(year from convert_timezone('KST', order_time)) as year, 
		extract(month from convert_timezone('KST', order_time)) as month, 
		extract(day from convert_timezone('KST', order_time)) as day,
		count(sessionid) as direct_reco_session
	from tb_session_funnel 
	where first_reco_view is not null and order_time is not null 
	group by 1,2,3
	order by 1,2,3) a
inner join
	(select extract(year from convert_timezone('KST', order_time)) as year, 
		extract(month from convert_timezone('KST', order_time)) as month, 
		extract(day from convert_timezone('KST', order_time)) as day, 
		count(sessionid) direct_no_reco_session
	from tb_session_funnel 
	where first_reco_view is null and order_time is not null 
	group by 1,2,3 order by 1,2,3) b on a.year = b.year and a.month = b.month and a.day=b.day
order by 1,2,3;

drop table if exists tb_reco_revenue_s3;
create table tb_reco_revenue_s3 as
select 
	B.year,B.month,B.day,
	A.sessionid
	,A.itemid
	,B.price
	,B.quantity
from (select min(case when rccode is not null then servertime end) servertime, sessionid, itemid, max(case when rccode is not null then 1 else 0 end) rc_num from tb_log_view_report group by sessionid, itemid) A
inner join tb_log_order_report B on A.sessionid = B.sessionid  and A.itemid = B.itemid
where A.rc_num = 1 and a.servertime < b.servertime
group by 1,2,3,4,5,6,7
order by 1,2,3;

drop table if exists tb_reco_revenue_s4;
create table tb_reco_revenue_s4 as
select 
	year,month,day,
	sessionid
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s3
group by year,month,day,sessionid
order by 1,2,3

-- revenue
drop table if exists tb_reco_revenue_funnel;
create table tb_reco_revenue_funnel as
select
	year
	,month
	,day
	,sum(reco_revenue) reco_revenue    
    
from tb_reco_revenue_s4
group by 1,2,3
order by 1,2,3;


--####################################################################################################
--한달 지표 확인
--####################################################################################################

select 
	a.year, 
	a.month, 
	a.day,   
	a.revenue,
	a.reco_revenue,
	e.reco_revenue as direct_reco_revenue,
	b.visit,
	b.reco_visit,
	c.pv,
	d.direct_reco_session,
	d.direct_no_reco_session
from 
	tb_recommend_result_revenue_daily a
		INNER JOIN
	tb_recommend_result_visit_daily b on a.day = b.day and a.year = b.year and a.month = b.month and a.day = b.day 
		INNER JOIN
	tb_recommend_result_pv_daily c on a.day = c.day and a.year = c.year and a.month = c.month and a.day = c.day
		INNER JOIN
	tb_direct_session d on a.day = d.day and a.year = d.year and a.month = d.month and a.day = d.day
		INNER JOIN
	tb_reco_revenue_funnel e on a.day = e.day and a.year = e.year and a.month = e.month and a.day = e.day
order by year,month,day;


--revenue: 전체 매출
--reco_revenue: 경유 매출
--direct_reco_revenue: 추천매출
--visit: 열람 세션수
--reco_visit: 클릭 세션수
--pv: 클릭수
--direct_reco_session: 추천 경유 구매 세션수
--direct_no_reco_session: 비추천 경유 구매 세션수

