
drop table if exists tb_log_view;
create table tb_log_view (	
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(1000),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
item_id varchar(200),
search_term varchar(200)
);

drop table if exists tb_log_search;
create table tb_log_search (
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(200),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
search_term varchar(200),
search_result varchar(200)
);



drop table if exists tb_log_order;
create table tb_log_order (
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(200),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
item_id varchar(200),
search_term varchar(200),
order_id varchar(200),
order_price real,
price real,
quantity bigint
);

drop table if exists tmp_product_info;
create table tmp_product_info (
cuid varchar(200),
type varchar(200),
item_id varchar(200),
item_name varchar(2000),
item_image varchar(200),
item_url varchar(2000),
original_price real,
sale_price real,
category1 varchar(200),
category2 varchar(200),
category3 varchar(200),
category4 varchar(200),
category5 varchar(200),
reg_date varchar(200),
update_date timestamp,
expire_date timestamp,
stock int,
state varchar(200),
description varchar(200),
extra_image varchar(200),
locale varchar(200)
);



--@copy-tb_log_view
copy tb_log_view
from 's3://rb-logs-apne1/5dfabb12-dae3-4a56-a123-c097dadbca58/view/2015/10/31'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/5dfabb12-dae3-4a56-a123-c097dadbca58/view/2015/11'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';
--@end

--@copy-tb_log_order
copy tb_log_order
from 's3://rb-logs-apne1/5dfabb12-dae3-4a56-a123-c097dadbca58/order/2015/10/31'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/5dfabb12-dae3-4a56-a123-c097dadbca58/order/2015/11'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
maxerror 100000
timeformat 'auto'
region 'ap-northeast-1';
--@end

--@copy-tmp_product_info
copy tmp_product_info
from 's3://rb-logs-apne1/5dfabb12-dae3-4a56-a123-c097dadbca58/product/2015/10/31'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
maxerror 100000
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';


copy tmp_product_info
from 's3://rb-logs-apne1/5dfabb12-dae3-4a56-a123-c097dadbca58/product/2015/11'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
maxerror 100000
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';
--@end


drop table if exists tb_product_info;
CREATE TABLE tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0
 group by item_id;



drop table if exists tb_log_view_report;
CREATE TABLE tb_log_view_report as
select
	server_time,
	extract(year from convert_timezone('KST', server_time)) as year, 
	extract(month from convert_timezone('KST', server_time)) as month, 
	
	extract(day from convert_timezone('KST', server_time)) as day, 
	extract(hour from convert_timezone('KST', server_time)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term
from 
tb_log_view;



drop table if exists tb_log_order_report;
CREATE TABLE tb_log_order_report as
select
	server_time,
	extract(year from convert_timezone('KST', server_time)) as year, 
	extract(month from convert_timezone('KST', server_time)) as month, 
	
	extract(day from convert_timezone('KST', server_time)) as day, 
	extract(hour from convert_timezone('KST', server_time)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term, 
	order_id, 
	order_price,
	price, 
	quantity
from 
tb_log_order;



drop table if exists tmp_recommend_result_view;
CREATE TABLE tmp_recommend_result_view as
select 
	year
	,month
	,day
	,pc_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null  then 1 else 0 end) as reco_view
	,sum(case when rc_code is null  then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by year,month,day , pc_id, device;

drop table if exists tmp_recommend_result_order;
CREATE TABLE tmp_recommend_result_order as
select
	year
	,month
	,day
	,pc_id 
	,device
	
	,sum(price*quantity) revenue
	,sum(case when rc_code is not null then price*quantity else 0 end) reco_revenue
	,sum(case when rc_code is null then price*quantity else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(case when rc_code is not null then 1 else 0 end) reco_frequency
	,count(case when rc_code is null then 1 else 0 end) no_reco_frequency
	
from tb_log_order_report
group by year,month,day, pc_id, device;

drop table if exists tmp_recommend_result_view_session;
CREATE TABLE tmp_recommend_result_view_session as
select
	year
	,month
	,day
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
	
	,datediff(second,min(server_time),max(server_time)) as period
from tb_log_view_report
group by year,month,day,  session_id, device;

drop table if exists tmp_recommend_result_order_session;
CREATE TABLE tmp_recommend_result_order_session as
select
	A.year
	,A.month
	,A.day
	,A.session_id 
	,A.order_id
	,A.device
	
	,sum(A.price * A.quantity) revenue
	,sum(case when B.reco_view > 0 then A.price * A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 then A.price * A.quantity else 0 end) no_reco_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 then A.order_price else 0 end) no_reco_actual_payment

	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 then A.order_id end) no_reco_frequency

	
	
from tb_log_order_report A
left join tmp_recommend_result_view_session B on A.session_id = B.session_id 
group by A.year,A.month,A.day,A.session_id,A.order_id,A.device;



--loyal group : comparison
drop table if exists tb_recommend_loyal_group;
CREATE TABLE tb_recommend_loyal_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
WHERE
	view > 1;

drop table if exists tb_recommend_reco_group;
CREATE TABLE tb_recommend_reco_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
where reco_view > 0;
	



--####################################################################################################
--현 상황
--####################################################################################################

--********
--daily
--********
--UV check
drop table if exists tb_recommend_result_uv_daily;
CREATE TABLE tb_recommend_result_uv_daily AS
select
	year
	,month
	,day
	,device
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
	,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
	,count(distinct case when view > 1 then pc_id end) loyal_uv
	,round( count(distinct case when reco_view > 0 then pc_id end) :: real / count(distinct pc_id) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_view
group by 1,2,3,4
order by 1,2,3,4;

--visit
drop table if exists tb_recommend_result_visit_daily;
CREATE TABLE tb_recommend_result_visit_daily AS
select
	year
	,month
	,day
	,device
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,count(distinct case when view > 1 then session_id end) loyal_visit  
    ,round(count(distinct case when reco_view > 0 then session_id end) :: real / count(distinct session_id) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_view_session
group by 1,2,3,4
order by 1,2,3,4;

--detail view click
drop table if exists tb_recommend_result_pv_daily;
CREATE TABLE tb_recommend_result_pv_daily AS
select
	year
	,month
	,day
	,device
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv
	,sum(case when reco_view = 0 then view else 0 end) no_reco_pv
	,sum(case when view > 0 then view else 0 end) loyal_pv
    ,round( sum(case when reco_view > 0 then view else 0 end) :: real / sum(view) :: real , 4)*100 reco_ratio
    
from tmp_recommend_result_view_session
group by 1,2,3,4
order by 1,2,3,4;


-- frequency
drop table if exists tb_recommend_result_frequency_daily;
CREATE TABLE tb_recommend_result_frequency_daily AS
select
	A.year
	,A.month
	,A.day
	,A.device
	,sum(A.frequency) frequency
	,sum(A.reco_frequency) reco_frequency 
	,sum(A.no_reco_frequency) no_reco_frequency 
	,sum(case when B.session_id is not null then A.frequency end) loyal_frequency 
	,round( sum(A.reco_frequency) :: real / sum(A.frequency) :: real , 4)*100 reco_ratio
    
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.session_id = B.session_id
group by 1,2,3,4
order by 1,2,3,4;

-- revenue
drop table if exists tb_recommend_result_revenue_daily_s1;
CREATE TABLE tb_recommend_result_revenue_daily_s1 AS
select
	A.year
	,A.month
	,A.day
	,A.device
	,sum(A.revenue) revenue
	,sum(A.reco_revenue) reco_revenue    
	,sum(A.no_reco_revenue) no_reco_revenue 
	,sum(case when B.session_id is not null then A.revenue end) loyal_revenue    
    ,round( sum(A.reco_revenue) :: real / sum(A.revenue) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.session_id = B.session_id
group by 1,2,3,4
order by 1,2,3,4;

--discount
drop table if exists tb_recommend_result_discount_daily;
CREATE TABLE tb_recommend_result_discount_daily AS
select
	A.year
	,A.month
	,A.day
	,A.device
	,sum(A.revenue) - sum(A.actual_payment) discount
	,sum(A.reco_revenue) - sum(A.reco_actual_payment) reco_discount
	,sum(A.no_reco_revenue) - sum(A.no_reco_actual_payment) no_reco_discount
	,sum(case when B.session_id is not null then A.no_reco_revenue end) - sum(case when B.session_id is not null then A.no_reco_actual_payment end) loyal_discount
    ,round( sum(A.reco_revenue) :: real - sum(A.reco_actual_payment) :: real / sum(A.revenue) :: real - sum(A.actual_payment) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.session_id = B.session_id
group by 1,2,3,4
order by 1,2,3,4;

--********
--monthly
--********
--UV check
drop table if exists tb_recommend_result_uv_monthly;
CREATE TABLE tb_recommend_result_uv_monthly AS
select
	year
	,month
	,device
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
	,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
	,count(distinct case when view > 1 then pc_id end) loyal_uv
	,round( count(distinct case when reco_view > 0 then pc_id end) :: real / count(distinct pc_id) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_view
group by 1,2,3
order by 1,2,3;

--visit
drop table if exists tb_recommend_result_visit_monthly;
CREATE TABLE tb_recommend_result_visit_monthly AS
select
	year
	,month
	,device
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,count(distinct case when view > 1 then session_id end) loyal_visit  
    ,round(count(distinct case when reco_view > 0 then session_id end) :: real / count(distinct session_id) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_view_session
group by 1,2,3
order by 1,2,3;

--detail view click
drop table if exists tb_recommend_result_pv_monthly;
CREATE TABLE tb_recommend_result_pv_monthly AS
select
	year
	,month
	,device
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv
	,sum(case when reco_view = 0 then view else 0 end) no_reco_pv
	,sum(case when view > 0 then view else 0 end) loyal_pv
    ,round( sum(case when reco_view > 0 then view else 0 end) :: real / sum(view) :: real , 4)*100 reco_ratio
    
from tmp_recommend_result_view_session
group by 1,2,3
order by 1,2,3;

-- frequency
drop table if exists tb_recommend_result_frequency_monthly;
CREATE TABLE tb_recommend_result_frequency_monthly AS
select
	A.year
	,A.month
	,A.device
	,sum(A.frequency) frequency
	,sum(A.reco_frequency) reco_frequency 
	,sum(A.no_reco_frequency) no_reco_frequency 
	,sum(case when B.session_id is not null then A.frequency end) loyal_frequency 
	,round( sum(A.reco_frequency) :: real / sum(A.frequency) :: real , 4)*100 reco_ratio
    
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.session_id = B.session_id
group by 1,2,3
order by 1,2,3;

-- revenue
drop table if exists tb_recommend_result_revenue_monthly;
CREATE TABLE tb_recommend_result_revenue_monthly AS
select
	A.year
	,A.month
	,A.device
	,sum(A.revenue) revenue
	,sum(A.reco_revenue) reco_revenue    
	,sum(A.no_reco_revenue) no_reco_revenue 
	,sum(case when B.session_id is not null then A.revenue end) loyal_revenue    
    ,round( sum(A.reco_revenue) :: real / sum(A.revenue) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.session_id = B.session_id
group by 1,2,3
order by 1,2,3;

--discount
drop table if exists tb_recommend_result_discount_monthly;
CREATE TABLE tb_recommend_result_discount_monthly AS
select
	A.year
	,A.month
	,A.device
	,sum(A.revenue) - sum(A.actual_payment) discount
	,sum(A.reco_revenue) - sum(A.reco_actual_payment) reco_discount
	,sum(A.no_reco_revenue) - sum(A.no_reco_actual_payment) no_reco_discount
	,sum(case when B.session_id is not null then A.no_reco_revenue end) - sum(case when B.session_id is not null then A.no_reco_actual_payment end) loyal_discount
    ,round( sum(A.reco_revenue) :: real - sum(A.reco_actual_payment) :: real / sum(A.revenue) :: real - sum(A.actual_payment) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.session_id = B.session_id
group by 1,2,3
order by 1,2,3;


--####################################################################################################
--추천 지표
--####################################################################################################

--********
--daily
--********

drop table if exists tb_recommend_result_prevenue_daily_s1;
CREATE TABLE tb_recommend_result_prevenue_daily_s1 AS
select
	A.year
	,A.month
	,A.day
	,A.device
	,sum(A.revenue)/sum(A.frequency) prevenue
    ,sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end) reco_prevenue
    ,sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end) no_reco_prevenue
    ,sum(case when B.session_id is not null then A.revenue end)/sum(case when B.session_id is not null  then A.frequency end) loyal_prevenue
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.year = B.year and A.month = B.month and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.year = C.year and A.month = C.month and A.session_id = C.session_id
group by 1,2,3,4
order by 1,2,3,4;

drop table if exists tb_recommend_result_prevenue_daily;
CREATE TABLE tb_recommend_result_prevenue_daily AS SELECT *,
    round(reco_prevenue :: real / no_reco_prevenue :: real, 4)*100 as reco_up_ratio
FROM tb_recommend_result_prevenue_daily_s1;


drop table if exists tb_recommend_result_pdiscount_daily_s1;
CREATE TABLE tb_recommend_result_pdiscount_daily_s1 AS
select
	A.year
	,A.month
	,A.day
	,A.device
	,(sum(A.revenue) - sum(A.actual_payment))/sum(A.frequency) pdiscount
    ,(sum(case when C.session_id is not null then A.revenue end) - sum(case when C.session_id is not null then A.actual_payment end))/sum(case when C.session_id is not null then A.frequency end) reco_pdiscount
    ,(sum(case when C.session_id is null then A.revenue end) - sum(case when C.session_id is null then A.actual_payment end))/sum(case when C.session_id is null then A.frequency end) no_reco_pdiscount
    ,(sum(case when B.session_id is not null then A.revenue end) - sum(case when B.session_id is not null then A.actual_payment end))/sum(case when B.session_id is not null  then A.frequency end) loyal_pdiscount
	
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.year = B.year and A.month = B.month and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.year = C.year and A.month = C.month and A.session_id = C.session_id
group by 1,2,3,4
order by 1,2,3,4;

drop table if exists tb_recommend_result_pdiscount_daily;
CREATE TABLE tb_recommend_result_pdiscount_daily AS SELECT *,
    round(reco_pdiscount :: real / no_reco_pdiscount :: real, 4)*100 as reco_up_ratio
FROM tb_recommend_result_pdiscount_daily_s1;


--미완성
--quantity (구매당 아이템)
drop table if exists tb_recommend_result_quantity_daily_s1;
CREATE TABLE tb_recommend_result_quantity_daily_s1 AS
select
	A.year, A.month,A.day, A.device
	,round(avg(A.quantity :: real),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: real end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: real end),3) no_reco_quantity
    ,round(avg(case when B.session_id is not null then A.quantity :: real end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.year = B.year and A.month = B.month and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.year = C.year and A.month = C.month and A.session_id = C.session_id
group by 1,2,3,4
order by 1,2,3,4;

drop table if exists tb_recommend_result_quantity_daily;
CREATE TABLE tb_recommend_result_quantity_daily AS SELECT *,
    round(reco_quantity :: real / no_reco_quantity :: real, 4)*100 as reco_up_ratio
FROM tb_recommend_result_quantity_daily_s1;



--pclick data 
drop table if exists tb_recommend_result_click_daily_s1;
CREATE TABLE tb_recommend_result_click_daily_s1 AS 
SELECT 
	year, month,day, device
	,round(AVG(view :: real),3) click
    ,round(AVG(case when reco_view>0 then view :: real end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: real end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: real  end),3) loyal_click
    
FROM
    tmp_recommend_result_view_session
group by 1,2,3,4
order by 1,2,3,4;



drop table if exists tb_recommend_result_click_daily;
CREATE TABLE tb_recommend_result_click_daily AS SELECT *,
    round(reco_click / no_reco_click, 4)*100 as reco_up_ratio
FROM tb_recommend_result_click_daily_s1;   

--period
drop table if exists tb_recommend_result_period_daily_s1;
CREATE TABLE tb_recommend_result_period_daily_s1 AS 
SELECT 
	year, month,day,  device
	,round(AVG(period :: real),3) period
    ,round(AVG(case when reco_view>0 then period :: real end),3) reco_period
    
FROM
    tmp_recommend_result_view_session
group by 1,2,3,4
order by 1,2,3,4;


drop table if exists tb_recommend_result_period_daily;
CREATE TABLE tb_recommend_result_period_daily AS SELECT *,
    round(reco_period / period, 4) as reco_up_ratio
FROM tb_recommend_result_period_daily_s1;   

-- CVR data
drop table if exists tb_recommend_result_cvr_daily_s1;
CREATE TABLE tb_recommend_result_cvr_daily_s1 AS 
select 
	A.year, A.month,A.day, A.device
	,round(sum(B.frequency :: real)/count(distinct A.session_id),3) cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_cvr
	,round(sum(case when A.reco_view =0 then B.frequency :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_cvr
	,round(sum(case when A.view >1 then B.frequency :: real end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_cvr
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.year = B.year and A.month = B.month  and A.session_id = B.session_id
group by 1,2,3,4
order by 1,2,3,4;


drop table if exists tb_recommend_result_cvr_daily;
CREATE TABLE tb_recommend_result_cvr_daily AS SELECT *,
    round(reco_cvr / no_reco_cvr, 4) as reco_up_ratio
FROM tb_recommend_result_cvr_daily_s1;   

-- sa data
drop table if exists tb_recommend_result_sa_daily_s1;
CREATE TABLE tb_recommend_result_sa_daily_s1 AS 
select 
	A.year, A.month,A.day, A.device
	,round(sum(B.revenue :: real)/count(distinct A.session_id),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
	,round(sum(case when A.reco_view =0 then B.revenue :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
	,round(sum(case when A.view >1 then B.revenue :: real end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_sa
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.year = B.year and A.month = B.month  and A.session_id = B.session_id
group by 1,2,3,4
order by 1,2,3,4;

drop table if exists tb_recommend_result_sa_daily;
CREATE TABLE tb_recommend_result_sa_daily AS SELECT *,
    round(reco_sa / no_reco_sa, 4) as reco_up_ratio
FROM tb_recommend_result_sa_daily_s1; 








--********
--monthly
--********
--prevenue
drop table if exists tb_recommend_result_prevenue_monthly_s1;
CREATE TABLE tb_recommend_result_prevenue_monthly_s1 AS
select
	A.year
	,A.month
	,A.device
	,sum(A.revenue)/sum(A.frequency) prevenue
    ,sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end) reco_prevenue
	,sum(case when B.session_id is not null then A.revenue end)/sum(case when B.session_id is not null  then A.frequency end) loyal_prevenue
	
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.year = B.year and A.month = B.month and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.year = C.year and A.month = C.month and A.session_id = C.session_id
group by 1,2,3
order by 1,2,3;



drop table if exists tb_recommend_result_prevenue_monthly;
CREATE TABLE tb_recommend_result_prevenue_monthly AS SELECT *,
    round(reco_prevenue :: real / prevenue :: real, 4) as reco_up_ratio
FROM tb_recommend_result_prevenue_monthly_s1;



drop table if exists tb_recommend_result_pdiscount_monthly_s1;
CREATE TABLE tb_recommend_result_pdiscount_monthly_s1 AS
select
	A.year
	,A.month
	,A.device
	,(sum(A.revenue) - sum(A.actual_payment))/sum(A.frequency) pdiscount
    ,(sum(case when C.session_id is not null then A.revenue end) - sum(case when C.session_id is not null then A.actual_payment end))/sum(case when C.session_id is not null then A.frequency end) reco_pdiscount
    ,(sum(case when C.session_id is null then A.revenue end) - sum(case when C.session_id is null then A.actual_payment end))/sum(case when C.session_id is null then A.frequency end) no_reco_pdiscount
    ,(sum(case when B.session_id is not null then A.revenue end) - sum(case when B.session_id is not null then A.actual_payment end))/sum(case when B.session_id is not null  then A.frequency end) loyal_pdiscount
	
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.year = B.year and A.month = B.month and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.year = C.year and A.month = C.month and A.session_id = C.session_id
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_pdiscount_monthly;
CREATE TABLE tb_recommend_result_pdiscount_monthly AS SELECT *,
    round(reco_pdiscount :: real / no_reco_pdiscount :: real, 4) as reco_up_ratio
FROM tb_recommend_result_pdiscount_monthly_s1;


--미완성
--quantity (구매당 아이템)
drop table if exists tb_recommend_result_quantity_monthly_s1;
CREATE TABLE tb_recommend_result_quantity_monthly_s1 AS
select
	A.year, A.month, A.device
	,round(avg(A.quantity :: real),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: real end),3) reco_quantity
	,round(avg(case when C.session_id is null then A.quantity :: real end),3) no_reco_quantity
	,round(avg(case when B.session_id is not null then A.quantity :: real end),3) loyal_quantity

from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.year = B.year and A.month = B.month and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.year = C.year and A.month = C.month and A.session_id = C.session_id
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_quantity_monthly;
CREATE TABLE tb_recommend_result_quantity_monthly AS SELECT *,
    round(reco_quantity :: real / quantity :: real, 4) as reco_up_ratio
FROM tb_recommend_result_quantity_monthly_s1;


--pclick data 
drop table if exists tb_recommend_result_click_monthly_s1;
CREATE TABLE tb_recommend_result_click_monthly_s1 AS 
SELECT 
	year, month, device
	,round(AVG(view :: real),3) click
    ,round(AVG(case when reco_view>0 then view :: real end),3) reco_click
	,round(AVG(case when view>1 then view :: real end),3) loyal_click
    
FROM
    tmp_recommend_result_view_session
group by 1,2,3
order by 1,2,3;


drop table if exists tb_recommend_result_click_monthly;
CREATE TABLE tb_recommend_result_click_monthly AS SELECT *,
    round(reco_click / click, 4) as reco_up_ratio
FROM tb_recommend_result_click_monthly_s1;   


--period
drop table if exists tb_recommend_result_period_monthly_s1;
CREATE TABLE tb_recommend_result_period_monthly_s1 AS 
SELECT 
	year, month, device
	,round(AVG(period :: real),3) period
    ,round(AVG(case when reco_view>0 then period :: real end),3) reco_period
	,round(AVG(case when view>1 then period :: real end),3) loyal_period
    
FROM
    tmp_recommend_result_view_session
group by 1,2,3
order by 1,2,3;


drop table if exists tb_recommend_result_period_monthly;
CREATE TABLE tb_recommend_result_period_monthly AS SELECT *,
    round(reco_period / period, 4) as reco_up_ratio
FROM tb_recommend_result_period_monthly_s1;   

-- CVR data
drop table if exists tb_recommend_result_cvr_monthly_s1;
CREATE TABLE tb_recommend_result_cvr_monthly_s1 AS 
select 
	A.year, A.month, A.device
	,round(sum(B.frequency :: real)/count(distinct A.session_id),3) cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_cvr
	,round(sum(case when A.reco_view =0 then B.frequency :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_cvr
	,round(sum(case when A.view >1 then B.frequency :: real end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_cvr
	
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.year = B.year and A.month = B.month  and A.session_id = B.session_id
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_cvr_monthly;
CREATE TABLE tb_recommend_result_cvr_monthly AS SELECT *,
    round(reco_cvr / cvr, 4) as reco_up_ratio
FROM tb_recommend_result_cvr_monthly_s1;   

-- sa data
drop table if exists tb_recommend_result_sa_monthly_s1;
CREATE TABLE tb_recommend_result_sa_monthly_s1 AS 
select 
	A.year, A.month, A.device
	,round(sum(B.revenue :: real)/count(distinct A.session_id),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
	,round(sum(case when A.reco_view =0 then B.revenue :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
	,round(sum(case when A.view >1 then B.revenue :: real end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_sa

from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.year = B.year and A.month = B.month  and A.session_id = B.session_id
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_sa_monthly;
CREATE TABLE tb_recommend_result_sa_monthly AS SELECT *,
    round(reco_sa / sa, 4) as reco_up_ratio
FROM tb_recommend_result_sa_monthly_s1;   






select year,month,day,device,uv,reco_uv,(reco_ratio/100) from tb_recommend_result_uv_daily order by device,month,day; --유니크방문
select year,month,day,device,visit,reco_visit,(reco_ratio/100) from tb_recommend_result_visit_daily order by device,month,day; --세션방문
select year,month,day,device,pv,reco_pv,(reco_ratio/100) from tb_recommend_result_pv_daily order by device,month,day; --페이지뷰수
--order status
select year,month,day,device,frequency,reco_frequency, (reco_ratio/100) reco_ratio from tb_recommend_result_frequency_daily order by device,month,day; -- 구매건수
select year,month,day,device,revenue,reco_revenue, (reco_ratio/100) reco_ratio from tb_recommend_result_revenue_daily_s1 order by device,month,day; -- 매출
select year,month,day,device,prevenue,reco_prevenue,no_reco_prevenue, (reco_prevenue/prevenue) ratio from  tb_recommend_result_prevenue_daily_s1 order by device,month,day; -- 할인
--index
select * from tb_recommend_result_prevenue_daily order by device,month,day; --객단가 : 구매당 평균 구매금액
select * from tb_recommend_result_pdiscount_daily order by device,month,day; --평균 할인가 : 구매당 평균 할인금액 (실제 금액 (상품 가격 * 주문개수) - 주문 금액)
select year,month,day,device,quantity,reco_quantity,no_reco_quantity, (reco_up_ratio/100) ratio from tb_recommend_result_quantity_daily order by device,month,day; --구매당 평균 아이템 수
select year,month,day,device,click,reco_click,no_reco_click,(reco_up_ratio/100) ratio from tb_recommend_result_click_daily order by device,month,day; -- 방문당 평균 상품클릭수
select * from tb_recommend_result_period_daily order by device,month,day; -- 방문당 평균 상품클릭수
select year,month,day,device,cvr,reco_cvr,no_reco_cvr,(reco_up_ratio/100) ratio from tb_recommend_result_cvr_daily order by device,month,day; -- 구매전환율
select year,month,day,device,sa,reco_sa,no_reco_sa,(reco_up_ratio/100) ratio from tb_recommend_result_sa_daily order by device,month,day; -- 세션당 구매금액

--********
--monthly
--********
--view status

select year,month,device,uv,reco_uv,(reco_ratio/100) ratio from tb_recommend_result_uv_monthly where month='11' order by device='PW',month; --유니크방문
select year,month,device,visit,reco_visit,(reco_ratio/100) ratio from tb_recommend_result_visit_monthly where month='11' order by device='PW',month; --세션방문
select year,month,device,pv,reco_pv,(reco_ratio/100) ratio from tb_recommend_result_pv_monthly where month='11' order by device='PW',month; --페이지뷰수
--order status
select year,month,device,frequency,reco_frequency,(reco_ratio/100) ratio from tb_recommend_result_frequency_monthly where month='11' order by device='PW',month; -- 구매건수
select year,month,device,revenue,reco_revenue,(reco_ratio/100) ratio from tb_recommend_result_revenue_monthly where month='11' order by device='PW',month; -- 매출
select * from tb_recommend_result_discount_monthly where month='11' order by device='PW',month; -- 할인
--index
select year,month,device,prevenue,reco_prevenue,(reco_prevenue/prevenue) ratio from tb_recommend_result_prevenue_monthly where month='11' order by device='PW',month; --객단가 : 구매당 평균 구매금액
select * from tb_recommend_result_pdiscount_monthly where month='11' order by device='PW',month; --평균 할인가 : 구매당 평균 할인금액 (실제 금액 (상품 가격 * 주문개수) - 주문 금액)
select year,month,device,quantity,reco_quantity,(reco_up_ratio/100) ratio from tb_recommend_result_quantity_monthly where month='11' order by device='PW',month; --구매당 평균 아이템 수
select year,month,device,click,reco_click,(reco_up_ratio/100) ratio from tb_recommend_result_click_monthly where month='11' order by device='PW',month; -- 방문당 평균 상품클릭수
select * from tb_recommend_result_period_monthly where month='11' order by device='PW',month; -- 방문당 평균 상품클릭수
select year,month,device,cvr,reco_cvr,(reco_up_ratio/100) ratio from tb_recommend_result_cvr_monthly where month='11' order by device='PW',month; -- 구매전환율
select year,month,device,sa,reco_sa,(reco_up_ratio/100) ratio from tb_recommend_result_sa_monthly where month='11' order by device='PW',month; -- 세션당 구매금액



select 
	a.year, 
	a.month, 
	a.day, 
	a.device,  
	a.uv, 
	b.visit,
	c.pv,
	d.frequency, 
	e.revenue, 
	f.prevenue, 
	g.quantity, 
	h.click, 
	i.cvr, 
	j.sa
from 
	tb_recommend_result_uv_daily a
		INNER JOIN
	tb_recommend_result_visit_daily b on a.day = b.day and a.year = b.year and a.month = b.month and a.device = b.device 
		INNER JOIN
	tb_recommend_result_pv_daily c on a.day = c.day and a.year = c.year and a.month = c.month and a.device = c.device 
		INNER JOIN
	tb_recommend_result_frequency_daily d on a.day = d.day and a.year = d.year and a.month = d.month and a.device = d.device 
		INNER JOIN		
	tb_recommend_result_revenue_daily e on a.day = e.day and a.year = e.year and a.month = e.month and a.device = e.device 
		INNER JOIN	
	tb_recommend_result_prevenue_daily f on a.day = f.day and a.year = f.year and a.month = f.month and a.device = f.device 
		INNER JOIN			
	tb_recommend_result_quantity_daily g on a.day = g.day and a.year = g.year and a.month = g.month and a.device = g.device 
		INNER JOIN			
	tb_recommend_result_click_daily h on a.day = h.day and a.year = h.year and a.month = h.month and a.device = h.device 
		INNER JOIN	
	tb_recommend_result_cvr_daily i on a.day = i.day and a.year = i.year and a.month = i.month and a.device = i.device 
		INNER JOIN	
	tb_recommend_result_sa_daily j on a.day = j.day and a.year = j.year and a.month = j.month and a.device = j.device 
order by month,day,device;


select 
	a.year, 
	a.month, 
	a.day, 
	a.device,  
	a.reco_uv, 
	b.reco_visit,
	c.reco_pv,
	d.reco_frequency, 
	e.reco_revenue, 
	f.reco_prevenue, 
	g.reco_quantity, 
	h.reco_click, 
	i.reco_cr, 
	j.reco_sa
from 
	tb_recommend_result_uv_daily a
		INNER JOIN
	tb_recommend_result_visit_daily b on a.day = b.day and a.year = b.year and a.month = b.month and a.device = b.device 
		INNER JOIN
	tb_recommend_result_pv_daily c on a.day = c.day and a.year = c.year and a.month = c.month and a.device = c.device 
		INNER JOIN
	tb_recommend_result_frequency_daily d on a.day = d.day and a.year = d.year and a.month = d.month and a.device = d.device 
		INNER JOIN		
	tb_recommend_result_revenue_daily e on a.day = e.day and a.year = e.year and a.month = e.month and a.device = e.device 
		INNER JOIN	
	tb_recommend_result_prevenue_daily f on a.day = f.day and a.year = f.year and a.month = f.month and a.device = f.device 
		INNER JOIN			
	tb_recommend_result_quantity_daily g on a.day = g.day and a.year = g.year and a.month = g.month and a.device = g.device 
		INNER JOIN			
	tb_recommend_result_click_daily h on a.day = h.day and a.year = h.year and a.month = h.month and a.device = h.device 
		INNER JOIN	
	tb_recommend_result_cvr_daily i on a.day = i.day and a.year = i.year and a.month = i.month and a.device = i.device 
		INNER JOIN	
	tb_recommend_result_sa_daily j on a.day = j.day and a.year = j.year and a.month = j.month and a.device = j.device 
order by month,day,device;










--####################################################################################################
--추천 영역 상품 관련
--####################################################################################################

drop table if exists tb_recommend_result_top30_s1;
CREATE TABLE tb_recommend_result_top30_s1 as
select
	A.item_id
	,C.item_name
	,sum(A.price * A.quantity) product_revenue
	
from tb_log_order_report A
inner join (select session_id from tb_log_view_report where rc_code is not null group by session_id) B on A.session_id = B.session_id
inner join tb_product_info C on A.item_id = C.item_id
group by 1,2;

drop table if exists tb_recommend_result_top30;
CREATE TABLE tb_recommend_result_top30 as
select * , rank() over (order by product_revenue desc)
from tb_recommend_result_top30_s1
order by product_revenue desc;


--####################################################################################################
--CRM KPI 
--#################################################################################################

drop table if exists tb_group_reco;
create table tb_group_reco as
select session_id from tb_log_view_report where rc_code is not null group by session_id;

--platform 

drop table if exists tb_crm_kpi_member_s1;
create table tb_crm_kpi_member_s1 as
	SELECT 
        A.pc_id
		,max(case when B.session_id is not null then 1 else 0 end) reco
        ,max(CASE
                WHEN A.user_agent like '%iPhone%' THEN 'iPhone'
                ELSE 'Android'
            END) AS platform
    FROM
        tb_log_view_report A
	left join tb_group_reco B on A.session_id = B.session_id 
	where A.device <> 'PW'
	GROUP BY A.pc_id; 

drop table if exists tb_crm_kpi_member;
create table tb_crm_kpi_member as
select
	platform,
	count(distinct pc_id) user_num, 
	count(distinct case when reco>0 then pc_id end) reco_user_num
from tb_crm_kpi_member_s1
group by platform;
--##############################################################################################################################
--view

drop table if exists tb_crm_kpi_view_s1;
create table tb_crm_kpi_view_s1 as
select
	A.pc_id
	,A.session_id
	,count(*) as view
	,sum(case when A.rc_code is not null then 1 else 0 end) as reco_view
	,count(distinct A.item_id) as itemvar
	,count(A.item_id) as itemnum
	,count(distinct B.category1) as cat1var
	,count(distinct B.category2) as cat2var
	,count(distinct B.category3) as cat3var
	,datediff(second,min(server_time),max(server_time)) as period

from tb_log_view_report A
inner join tb_product_info B on A.item_id = B.item_id
group by A.pc_id, A.session_id;

drop table if exists tb_crm_kpi_view_s2;
create table tb_crm_kpi_view_s2 as
select
	pc_id
	,count(*) session_count
	,sum(case when reco_view <> 0 then 1 else 0 end) reco_session_count
	,round(sum(case when reco_view <> 0 then 1 else 0 end)::real/count(*)::real,2) reco_session_ratio
	
	,sum(view) view_count
	,sum(reco_view) reco_view_count
	,round(sum(reco_view)::real/sum(view)::real,2) reco_view_ratio
	
	,round(avg(itemnum::real),1) itemnum
	,round(avg(itemvar::real),1) itemvar
	,round(avg(cat1var::real),1) cat1var
	,round(avg(cat2var::real),1) cat2var
	,round(avg(cat3var::real),1) cat3var
from tb_crm_kpi_view_s1
group by pc_id;


drop table if exists tb_crm_kpi_session_itemvar;
create table tb_crm_kpi_session_itemvar as
select
	reco_session_ratio
	,avg(itemvar) avg_itemvar
	,count(*) count
from tb_crm_kpi_view_s2
where view_count>20 and session_count>5
group by reco_session_ratio;

drop table if exists tb_crm_kpi_view_itemvar;
create table tb_crm_kpi_view_itemvar as
select
	reco_view_ratio
	,avg(itemvar) avg_itemvar
	,count(*) count
from tb_crm_kpi_view_s2
where view_count>20 and session_count>5
group by reco_view_ratio;

drop table if exists tb_crm_kpi_session_cat1var;
create table tb_crm_kpi_session_cat1var as
select
	reco_session_ratio
	,avg(cat1var) avg_cat1var
	,count(*) count
from tb_crm_kpi_view_s2
where view_count>20 and session_count>5
group by reco_session_ratio;

drop table if exists tb_crm_kpi_session_itemnum;
create table tb_crm_kpi_session_itemnum as
select
	reco_session_ratio
	,avg(itemnum) avg_itemnum
	,count(*) count
from tb_crm_kpi_view_s2
where view_count>20 and session_count>5
group by reco_session_ratio;

drop table if exists tb_crm_kpi_session_itemnum;
create table tb_crm_kpi_session_itemnum as
select
	reco_session_ratio
	,avg(itemnum) avg_itemnum
	,count(*) count
from tb_crm_kpi_view_s1
where view_count>20 and session_count>5
group by reco_session_ratio;

select * from tb_crm_kpi_session_itemvar where count > 100 order by reco_session_ratio;
select * from tb_crm_kpi_view_itemvar where count > 100 order by reco_view_ratio;
select * from tb_crm_kpi_session_cat1var where count > 100 order by reco_session_ratio;
select * from tb_crm_kpi_view_cat1var where count > 100 order by reco_view_ratio;
select * from tb_crm_kpi_session_itemnum where count > 100 order by reco_session_ratio;
select * from tb_crm_kpi_view_itemnum where count > 100 order by reco_view_ratio;

-- vip/ gold/ silver/ blonde uv cal

drop table if exists tb_crm_kpi_view_s3;
create table tb_crm_kpi_view_s3 as
select
	round(sum(reco_session_count::real)/sum(session_count::real),2) avg_reco_session_ratio
	,stddev_pop(reco_session_ratio) stddev_reco_session_ratio
from tb_crm_kpi_view_s2;

drop table if exists tb_crm_kpi_view_pop;
create table tb_crm_kpi_view_pop as
select
	count(*) total_uv
	,count(case when reco_session_ratio > (select avg_reco_session_ratio + 2 * stddev_reco_session_ratio from tb_crm_kpi_view_s3) then pc_id end) vip_reco_customer_uv
	,count(case when reco_session_ratio > (select avg_reco_session_ratio + stddev_reco_session_ratio from tb_crm_kpi_view_s3) then pc_id end) as gold_reco_customer_uv
	,count(case when reco_session_ratio > (select avg_reco_session_ratio from tb_crm_kpi_view_s3) then pc_id end) as silver_reco_customer_uv
	,count(case when reco_session_ratio <= (select avg_reco_session_ratio from tb_crm_kpi_view_s3) then pc_id end) as blonde_reco_customer_uv
from tb_crm_kpi_view_s2;

--first click rc or not -> item click
drop table if exists tb_crm_kpi_first_view_group_s1;
create table tb_crm_kpi_first_view_group_s1 as
select
	*
	,rank() over (partition by session_id order by server_time)
from tb_log_view_report;

drop table if exists tb_crm_kpi_first_view_group;
create table tb_crm_kpi_first_view_group as
select 
	session_id
	,(case when rc_code is null then 0 else 1 end) rc_code
from tb_crm_kpi_first_view_group_s1
where rank = 1;

select * from tb_crm_kpi_first_view_group limit 100;

drop table if exists tb_crm_kpi_first_view_item_num_s1;
create table tb_crm_kpi_first_view_item_num_s1 as
select 
	A.session_id
	,B.rc_code
	,count(item_id) count
	,count(distinct item_id) dist_count
from tb_log_view_report A
left join tb_crm_kpi_first_view_group B on A.session_id = B.session_id
group by A.session_id, B.rc_code;

drop table if exists tb_crm_kpi_first_view_item_num;
create table tb_crm_kpi_first_view_item_num as
select rc_code, avg(count:: real), avg(dist_count:: real) from tb_crm_kpi_first_view_item_num group by rc_code;

--################################################################################중요한 지표
--rc ratio on one session influence to item click?

drop table if exists tb_crm_kpi_rcratio_itemnum_s1;
create table tb_crm_kpi_rcratio_itemnum_s1 as
select
	round(reco_view::real/view::real,2) as rcratio
	,reco_view
	,view
	,itemnum
	,itemvar
	,cat1var
	,period
	
from tb_crm_kpi_view_s1;

drop table if exists tb_crm_kpi_rcratio_itemnum;
create table tb_crm_kpi_rcratio_itemnum as
select
	rcratio
	,round(avg(itemnum::real),2) avg_itemnum
	,round(avg(itemvar::real),2) avg_itemvar
	,round(avg(cat1var::real),2) avg_cat1var
	,round(avg(period::real),2) avg_period
	,count(*) count
from tb_crm_kpi_rcratio_itemnum_s1
where itemnum > 2
group by rcratio
having count(*) > 500;

-- view period 
select * from tb_crm_kpi_rcratio_itemnum order by rcratio;


--##########################################################################################
--view and order
--order (frequency, quantity, revenue)
drop table if exists tb_crm_kpi_order_s1;
create table tb_crm_kpi_order_s1 as
	SELECT 
        pc_id,
		session_id,
		
		sum(quantity) quantity,
        sum(price * quantity) revenue,
		count(distinct item_id) buyitemnum,
		count(distinct order_id) ordercount
    FROM
        tb_log_order_report
    GROUP BY pc_id, session_id;

drop table if exists tb_crm_kpi_conversion_s1;
create table tb_crm_kpi_conversion_s1 as
select 
	A.session_id
	,A.view
	,A.reco_view reco_view
	,A.itemvar view_var
	,A.itemnum view_itemnum
	,A.cat1var view_catvar
	,A.period view_period
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count

from tb_crm_kpi_view_s1 A
left join tb_crm_kpi_order_s1 B on A.session_id = B.session_id;

select * from tb_crm_kpi_conversion_s1 limit 100;

drop table if exists tb_crm_kpi_conversion_s2;
create table tb_crm_kpi_conversion_s2 as
select 
	round(reco_view::real/view::real,2) rcratio
	,case when order_revenue is not null then 1 else 0 end as conversion
	,order_quantity
	,order_revenue
	,order_itemnum

from tb_crm_kpi_conversion_s1
where view>1;
	
select * from tb_crm_kpi_conversion_s2 limit 100;

drop table if exists tb_crm_kpi_conversion_s3;
create table tb_crm_kpi_conversion_s3 as
select 
	rcratio	
	,round(count(case when conversion = 1 then conversion end)::real/ count(*)::real, 4) as cvr
	,round(avg(order_quantity::real),2) avg_quantity
	,round(avg(order_revenue::real),2) order_revenue
	,round(avg(order_itemnum::real),2) order_itemnum
	,count(*) count
from tb_crm_kpi_conversion_s2
group by rcratio
having count(*)>500;

select * from tb_crm_kpi_conversion_s3 order by rcratio limit 100;



--이정도?

drop table if exists tb_crm_kpi_order_s2;
create table tb_crm_kpi_order_s2 as
	select
		pc_id,
		sum(quantity) sum_quantity,
		round(avg(quantity::real),2) avg_quantity,
		sum(revenue) total_revenue,
		round(avg(revenue::real),2) per_revenue, 
		count(*) frequency, 
		
		sum(reco_quantity) sum_reco_quantity,
		round(avg(reco_quantity::real),2) avg_reco_quantity,
		sum(reco_revenue) total_reco_revenue,
		round(avg(reco_revenue::real),2) per_reco_revenue, 
		count(case when reco_quantity <> 0 then 1 end) reco_frequency
		
		
	from tb_crm_kpi_order_s1
	group by pc_id;
		


--favorite category (order)
drop table if exists tb_crm_kpi_o_fav_cat_s1;
create table tb_crm_kpi_o_fav_cat_s1 as
	select
		A.pc_id
		, B.category1 category
		, sum(A.price * A.quantity) cat_revenue
	from tb_log_order_report A
	inner join tb_product_info B on A.item_id = B.item_id
	group by A.pc_id , B.category1;

drop table if exists tb_crm_kpi_o_fav_cat_s2;
create table tb_crm_kpi_o_fav_cat_s2 as
select
	*
	,rank() over (partition by pc_id order by cat_revenue desc)
from tb_crm_kpi_o_fav_cat_s1;

drop table if exists tb_crm_kpi_o_fav_cat;
create table tb_crm_kpi_o_fav_cat as
select
	pc_id, category, cat_revenue
from tb_crm_kpi_o_fav_cat_s2
where rank = 1;


--favorite category (view)
drop table if exists tb_crm_kpi_v_fav_cat_s1;
create table tb_crm_kpi_v_fav_cat_s1 as
	select
		A.pc_id
		, B.category1 category
		, count(*) view_count
	from tb_log_view_report A
	inner join tb_product_info B on A.item_id = B.item_id
	group by A.pc_id , B.category1;

drop table if exists tb_crm_kpi_v_fav_cat_s2;
create table tb_crm_kpi_v_fav_cat_s2 as
select
	*
	,rank() over (partition by pc_id order by view_count desc)
from tb_crm_kpi_v_fav_cat_s1;

drop table if exists tb_crm_kpi_v_fav_cat;
create table tb_crm_kpi_v_fav_cat as
select
	pc_id, category, view_count
from tb_crm_kpi_v_fav_cat_s2
where rank = 1;	




--time slot?
drop table if exists tb_crm_kpi_time_slot_s1;
create table tb_crm_kpi_time_slot_s1 as
select
    pc_id,
	order_id,
    (CASE
        WHEN hour IN (02 , 03, 04, 05, 06, 07) THEN 'T_02_07'
        WHEN hour IN (08 , 09) THEN 'T_08_09'
        WHEN hour IN (10 , 11, 12) THEN 'T_10_12'
        WHEN hour IN (13 , 14, 15, 16, 17) THEN 'T_13_17'
        WHEN hour IN (18 , 19, 20, 21, 22) THEN 'T_18_22'
        WHEN hour IN (23 , 00, 01) THEN 'T_23_01'
    END) time_slot
FROM
	tb_log_order_report
group by 1,2,3;

drop table if exists tb_crm_kpi_time_slot_s2;
create table tb_crm_kpi_time_slot_s2 as
select
    pc_id,
    time_slot, 
	count(*) buy_count
FROM
	tb_crm_kpi_time_slot_s1
group by 1,2;

drop table if exists tb_crm_kpi_time_slot_s3;
create table tb_crm_kpi_time_slot_s3 as
select
	* 
	,rank() over (partition by pc_id order by buy_count desc)
from tb_crm_kpi_time_slot_s2;

drop table if exists tb_crm_kpi_time_slot;
create table tb_crm_kpi_time_slot as
select
	pc_id, time_slot, buy_count
from tb_crm_kpi_time_slot_s3
where rank = 1; 




