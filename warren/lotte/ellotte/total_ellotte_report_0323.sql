drop table if exists tb_log_view_report;
CREATE TABLE tb_log_view_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date,
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term
from 
tb_log_view;

drop table if exists tb_log_order_report;
CREATE TABLE tb_log_order_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term, 
	order_id, 
	order_price,
	price, 
	quantity
from 
tb_log_order;

drop table if exists tb_product_info;
--@end

--@create-tb_product_info
create table tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0
 group by item_id;
 --@end
 
drop table if exists tb_product_info_report;
CREATE TABLE tb_product_info_report as
select
	item_id, 
	item_name, 
	original_price, 
	sale_price, 
	category1, 
	category2, 
	category3, 
	category4, 
	category5, 
	reg_date
	stock
from 
tb_product_info;

--recommendation resource table
drop table if exists tmp_recommend_result_view;
CREATE TABLE tmp_recommend_result_view as
select 
	date
	,pc_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by date , pc_id, device;

drop table if exists tmp_recommend_result_order;
CREATE TABLE tmp_recommend_result_order as
select
	date
	,pc_id 
	,device
	
	,sum(price*quantity) revenue
	,sum(case when rc_code is not null then price*quantity else 0 end) reco_revenue
	,sum(case when rc_code is null then price*quantity else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(distinct case when rc_code is not null then order_id else null end) reco_frequency
	,count(distinct case when rc_code is null then order_id else null end) no_reco_frequency
	
from tb_log_order_report
group by date, pc_id, device;

drop table if exists tmp_recommend_result_view_session;
CREATE TABLE tmp_recommend_result_view_session as
select
	date
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by date,session_id, device;

drop table if exists tmp_recommend_result_order_session;
CREATE TABLE tmp_recommend_result_order_session as
select
	A.date
	,A.session_id 
	,A.order_id
	,A.device
	
	,sum(A.price*A.quantity) revenue
	,sum(case when B.reco_view > 0 then A.price*A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.price*A.quantity else 0 end) no_reco_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_price else 0 end) no_reco_actual_payment
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_id end) no_reco_frequency
	
from tb_log_order_report A
left join tmp_recommend_result_view_session B on A.session_id = B.session_id and A.date = B.date
group by A.date,A.session_id,A.order_id,A.device;

--####################################################################################################
--중간 테이블 확인
--####################################################################################################
select * from tb_log_view limit 10;
select * from tb_log_order where order_price > 3000 limit 100;
select * from tb_log_search limit 10;
select * from tb_product_info limit 100;
--####################################################################################################
--group
--####################################################################################################

--loyal group : comparison
drop table if exists tb_recommend_loyal_group;
CREATE TABLE tb_recommend_loyal_group AS SELECT * FROM
    tmp_recommend_result_view_session
WHERE
	view > 1;

drop table if exists tb_recommend_reco_group;
CREATE TABLE tb_recommend_reco_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
where reco_view > 0;
	



--####################################################################################################
--현 상황
--####################################################################################################

--********
--daily
--********
--UV check
drop table if exists tb_recommend_result_uv_daily_s1;
CREATE TABLE tb_recommend_result_uv_daily_s1 AS
select
	date
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
from tmp_recommend_result_view
group by 1
order by 1;

drop table if exists tb_recommend_result_uv_daily;
CREATE TABLE tb_recommend_result_uv_daily AS
select
	*
	,round( reco_uv :: real / uv :: real , 2) ratio
from tb_recommend_result_uv_daily_s1;


--visit
drop table if exists tb_recommend_result_visit_daily_s1;
CREATE TABLE tb_recommend_result_visit_daily_s1 AS
select
	date
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit    
    
from tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_visit_daily;
CREATE TABLE tb_recommend_result_visit_daily AS
select
	*
	,round( reco_visit :: real / visit :: real , 2) ratio
from tb_recommend_result_visit_daily_s1;

--detail view click
drop table if exists tb_recommend_result_pv_daily_s1;
CREATE TABLE tb_recommend_result_pv_daily_s1 AS
select
	date
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv
    
    
from tmp_recommend_result_view_session
group by 1
order by 1;

drop table if exists tb_recommend_result_pv_daily;
CREATE TABLE tb_recommend_result_pv_daily AS
select
	*
	,round( reco_pv :: real / pv :: real , 2) ratio
from tb_recommend_result_pv_daily_s1;

-- frequency
drop table if exists tb_recommend_result_frequency_daily_s1;
CREATE TABLE tb_recommend_result_frequency_daily_s1 AS
select
	date
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency    
    
from tmp_recommend_result_order_session
group by 1
order by 1;

drop table if exists tb_recommend_result_frequency_daily;
CREATE TABLE tb_recommend_result_frequency_daily AS
select
	*
	,round( reco_frequency :: real / frequency :: real , 2) ratio
from tb_recommend_result_frequency_daily_s1;


-- revenue
drop table if exists tb_recommend_result_revenue_daily_s1;
CREATE TABLE tb_recommend_result_revenue_daily_s1 AS
select
	date
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
    
from tmp_recommend_result_order_session
group by 1
order by 1;

drop table if exists tb_recommend_result_revenue_daily;
CREATE TABLE tb_recommend_result_revenue_daily AS
select
	*
	,round( reco_revenue :: real / revenue :: real , 2) ratio
from tb_recommend_result_revenue_daily_s1;

--####################################################################################################
--추천 지표
--####################################################################################################

--********
--daily
--********

drop table if exists tb_recommend_result_prevenue_daily_s1;
CREATE TABLE tb_recommend_result_prevenue_daily_s1 AS
select
	A.date
	,sum(A.revenue)/sum(A.frequency) prevenue
    ,sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end) reco_prevenue
    ,sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end) no_reco_prevenue
    --,sum(case when B.session_id is not null then A.revenue end)/sum(case when B.session_id is not null  then A.frequency end) loyal_prevenue
	
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_prevenue_daily;
CREATE TABLE tb_recommend_result_prevenue_daily AS SELECT *,
    round(reco_prevenue :: real / no_reco_prevenue :: real, 4) as reco_up_ratio
FROM tb_recommend_result_prevenue_daily_s1;

--quantity (구매당 아이템)
drop table if exists tb_recommend_result_quantity_daily_s1;
CREATE TABLE tb_recommend_result_quantity_daily_s1 AS
select
	A.date
	,round(avg(A.quantity :: real),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: real end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: real end),3) no_reco_quantity
    --,round(avg(case when B.session_id is not null then A.quantity :: real end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id group by 1
order by 1;

drop table if exists tb_recommend_result_quantity_daily;
CREATE TABLE tb_recommend_result_quantity_daily AS SELECT *,
    round(reco_quantity :: real / no_reco_quantity :: real, 4) as reco_up_ratio
FROM tb_recommend_result_quantity_daily_s1;



--pclick data 
drop table if exists tb_recommend_result_click_daily_s1;
CREATE TABLE tb_recommend_result_click_daily_s1 AS 
SELECT 
	date
	,round(AVG(view :: real),3) click
    ,round(AVG(case when reco_view>0 then view :: real end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: real end),3) no_reco_click
    --,round(AVG(case when view > 1 then view :: real  end),3) loyal_click
    
FROM
    tmp_recommend_result_view_session
group by 1
order by 1;



drop table if exists tb_recommend_result_click_daily;
CREATE TABLE tb_recommend_result_click_daily AS SELECT *,
    round(reco_click / no_reco_click, 4) as reco_up_ratio
FROM tb_recommend_result_click_daily_s1;   

-- CVR data
drop table if exists tb_recommend_result_cvr_daily_s1;
CREATE TABLE tb_recommend_result_cvr_daily_s1 AS 
select 
	A.date
	,round(sum(B.frequency :: real)/count(distinct A.session_id),3) cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_cr
	,round(sum(case when A.reco_view =0 then B.frequency :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_cr
	--,round(sum(case when A.view >1 then B.frequency :: real end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_cr
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.session_id = B.session_id
group by 1
order by 1;


drop table if exists tb_recommend_result_cvr_daily;
CREATE TABLE tb_recommend_result_cvr_daily AS SELECT *,
    round(reco_cr / no_reco_cr, 4) as reco_up_ratio
FROM tb_recommend_result_cvr_daily_s1;   

-- sa data
drop table if exists tb_recommend_result_sa_daily_s1;
CREATE TABLE tb_recommend_result_sa_daily_s1 AS 
select 
	A.date
	,round(sum(B.revenue :: real)/count(distinct A.session_id),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
	,round(sum(case when A.reco_view =0 then B.revenue :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
	--,round(sum(case when A.view >1 then B.revenue :: real end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_sa
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date  and A.session_id = B.session_id
group by 1
order by 1;

drop table if exists tb_recommend_result_sa_daily;
CREATE TABLE tb_recommend_result_sa_daily AS SELECT *,
    round(reco_sa / no_reco_sa, 4) as reco_up_ratio
FROM tb_recommend_result_sa_daily_s1; 


--###############################################
--Funnel
--###############################################


--************************************************************************
--pc_id, session_id matching
--************************************************************************

drop table if exists tb_log_session_pc_id;
CREATE TABLE tb_log_session_pc_id as
select
	pc_id
	,session_id
from tb_log_view_report
group by pc_id, session_id;


--************************************************************************
--first reco view
--************************************************************************
drop table if exists tb_log_view_report_rank_s1;
CREATE TABLE tb_log_view_report_rank_s1 as
select
	* 
	,rank() over (partition by session_id order by server_time)
from tb_log_view_report;

drop table if exists tb_log_view_report_rank_s2;
CREATE TABLE tb_log_view_report_rank_s2 as
select
	session_id
	,min(rank) first_reco_view
	,max(rank) last_reco_view
from tb_log_view_report_rank_s1
where rc_code is not null
group by session_id;

drop table if exists tb_log_view_report_rank_s3;
CREATE TABLE tb_log_view_report_rank_s3 as
select * from tb_log_view_report_rank_s1 where rc_code is not null;

drop table if exists tb_log_view_report_rank_s4;
CREATE TABLE tb_log_view_report_rank_s4 as
select 
A.session_id
,A.first_reco_view
,B.rc_code first_rc_code
,A.last_reco_view
from
tb_log_view_report_rank_s2 A
inner join tb_log_view_report_rank_s3 B 
on A.session_id = B.session_id and A.first_reco_view = B.rank;

drop table if exists tb_log_view_report_rank;
CREATE TABLE tb_log_view_report_rank as
select 
A.session_id
,A.first_reco_view
,A.first_rc_code
,A.last_reco_view
,B.rc_code last_rc_code

from tb_log_view_report_rank_s4 A
inner join tb_log_view_report_rank_s3 B
on A.session_id = B.session_id and A.last_reco_view = B.rank;

--************************************************************************
--first four view
--************************************************************************
drop table if exists tb_log_view_first_four;
CREATE TABLE tb_log_view_first_four as 
select 
	session_id
	,max(case when rank = 1 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 1 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) first_view
	,max(case when rank = 2 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 2 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) second_view
	,max(case when rank = 3 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 3 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) third_view
	,max(case when rank = 4 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 4 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) fourth_view
	,max(case when rank = 1 and a.item_id is not null then b.category1 end) first_category
	,max(case when rank = 2 and a.item_id is not null then b.category1 end) second_category
	,max(case when rank = 3 and a.item_id is not null then b.category1 end) third_category
	,max(case when rank = 4 and a.item_id is not null then b.category1 end) fourth_category
from tb_log_view_report_rank_s1 a
left join tb_product_info b on a.item_id = b.item_id
group by session_id;

--************************************************************************
--view
--************************************************************************

drop table if exists tb_crm_kpi_view_s1;
create table tb_crm_kpi_view_s1 as
select
	A.pc_id
	,A.device
	,A.session_id
	,count(*) as view
	,sum(case when A.rc_code is not null then 1 else 0 end) as reco_view
	,count(distinct A.item_id) as itemvar
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) first_server_time

from tb_log_view_report A
group by A.pc_id, A.device, A.session_id;



drop table if exists tb_crm_kpi_view_s2;
create table tb_crm_kpi_view_s2 as
select
	A.*
	,B.first_reco_view
	,B.first_rc_code
	,B.last_reco_view
	,B.last_rc_code
from tb_crm_kpi_view_s1 A 
left join tb_log_view_report_rank B on A.session_id = B.session_id;

drop table if exists tb_reco_revenue_s1;
create table tb_reco_revenue_s1 as
select 
	A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code is not null then server_time end) server_time, session_id, item_id, max(case when rc_code is not null then 1 else 0 end) rc_num from tb_log_view_report group by session_id, item_id) A
inner join tb_log_order_report B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time;

drop table if exists tb_reco_revenue;
create table tb_reco_revenue as
select 
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s1
group by session_id;

drop table if exists tb_crm_kpi_order_s1;
create table tb_crm_kpi_order_s1 as
	SELECT 
		
		pc_id,
		session_id,
		min(server_time) server_time,
		sum(quantity) quantity,
		sum(price * quantity) revenue,
		count(distinct item_id) buyitemnum,
		count(distinct order_id) ordercount
	FROM
		tb_log_order_report
	GROUP BY pc_id, session_id;



--##########################################################################################
--final funnel metric table
--##########################################################################################

drop table if exists tb_session_funnel;
create table tb_session_funnel as
select
	A.first_server_time first_view_time
	,B.server_time order_time
	,A.session_id
	,E.pc_id
	,A.device
	,A.first_reco_view
	,A.first_rc_code
	,A.last_reco_view
	,A.last_rc_code
	,A.view view_count
	,A.reco_view reco_view_count
	,A.itemvar item_var
	,A.period view_period
	,D.first_view
	,D.second_view
	,D.third_view
	,D.fourth_view
	,D.first_category
	,D.second_category
	,D.third_category
	,D.fourth_category
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count
	,C.reco_revenue 
	,C.reco_quantity
	
from tb_crm_kpi_view_s2 A
left join tb_crm_kpi_order_s1 B on A.session_id = B.session_id
left join tb_reco_revenue C on A.session_id = C.session_id
left join tb_log_view_first_four D on A.session_id = D.session_id
left join tb_log_session_pc_id E on A.session_id = E.session_id;

drop table if exists tb_direct_session;
create table tb_direct_session as
select 
	a.date,
	a.direct_reco_session,
	b.direct_no_reco_session
from
	(select convert_timezone('KST', order_time)::date as date,
		count(session_id) as direct_reco_session
	from tb_session_funnel 
	where first_reco_view is not null and order_time is not null 
	group by 1
	order by 1) a
inner join
	(select convert_timezone('KST', order_time)::date as date, 
		count(session_id) direct_no_reco_session
	from tb_session_funnel 
	where first_reco_view is null and order_time is not null 
	group by 1 order by 1) b on a.date = b.date
order by 1;

drop table if exists tb_reco_revenue_s3;
create table tb_reco_revenue_s3 as
select 
	B.date
	,A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code is not null then server_time end) server_time, session_id, item_id, max(case when rc_code is not null then 1 else 0 end) rc_num from tb_log_view_report group by session_id, item_id) A
inner join tb_log_order_report B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time
group by 1,2,3,4,5
order by 1;

drop table if exists tb_reco_revenue_s4;
create table tb_reco_revenue_s4 as
select 
	date,
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s3
group by date,session_id
order by 1;

-- revenue
drop table if exists tb_reco_revenue_funnel;
create table tb_reco_revenue_funnel as
select
	date
	,sum(reco_revenue) reco_revenue    
    
from tb_reco_revenue_s4
group by 1
order by 1;


--####################################################################################################
--한달 지표 확인
--####################################################################################################

select 
	a.date,   
	a.revenue,
	a.reco_revenue,
	e.reco_revenue as direct_reco_revenue,
	b.visit,
	b.reco_visit,
	c.pv,
	d.direct_reco_session,
	d.direct_no_reco_session
from 
	tb_recommend_result_revenue_daily a
		INNER JOIN
	tb_recommend_result_visit_daily b on a.date= b.date
		INNER JOIN
	tb_recommend_result_pv_daily c on a.date = c.date
		INNER JOIN
	tb_direct_session d on a.date = d.date
		INNER JOIN
	tb_reco_revenue_funnel e on a.date = e.date
order by 1;


--revenue: 전체 매출
--reco_revenue: 경유 매출
--direct_reco_revenue: 추천매출
--visit: 열람 세션수
--reco_visit: 클릭 세션수
--pv: 클릭수
--direct_reco_session: 추천 경유 구매 세션수
--direct_no_reco_session: 비추천 경유 구매 세션수
