select rc_code, count(*) from tb_log_view_report group by rc_code;
select * from tb_log_view_report where rc_code like '%RECENT%';
select * from tb_log_view_report where rc_code like '%TODAY%' limit 100;
select device, count(*) from tb_log_view group by device;

select device from tb_log_view group by device;


select 
(case when rc_code like '%RECO%' then 'm_main'
when rc_code like '%REO%' then 'main'
when rc_code like '%TODAY%' then 'today'
when rc_code like '%Detail_product%' then 'detail'
when rc_code like '%Detail_m_product%' then 'mdetail' 
when rc_code like '%BasketView_product%' then 'cart' 
when rc_code like '%BasketView_m_product%' then 'mcart' 
when rc_code like '%Search_product%' then 'search' 
when rc_code like '%Search_m_product%' then 'msearch'
when rc_code like '%RECENT%' then 'recent'
end) as rc,
sum(price * quantity) 
from 
(select
	a.session_id, a.item_id, a.server_time, a.price, a.quantity, c.rc_code
from
(select 
a.session_id,a.item_id,b.price, b.quantity, max(a.server_time) server_time
from tb_log_view a
inner join tb_log_order b on a.session_id = b.session_id and a.item_id = b.item_id
group by a.session_id,a.item_id, b.price, b.quantity
)a 
left join tb_log_view c on a.session_id = c.session_id and a.item_id= c.item_id and a.server_time = c.server_time)
group by 
(case when rc_code like '%RECO%' then 'm_main'
when rc_code like '%REO%' then 'main'
when rc_code like '%TODAY%' then 'today'
when rc_code like '%Detail_product%' then 'detail'
when rc_code like '%Detail_m_product%' then 'mdetail' 
when rc_code like '%BasketView_product%' then 'cart' 
when rc_code like '%BasketView_m_product%' then 'mcart' 
when rc_code like '%Search_product%' then 'search' 
when rc_code like '%Search_m_product%' then 'msearch'
when rc_code like '%RECENT%' then 'recent'
end);


select 
(case when rc_code like '%RECO%' then 'm_main'
when rc_code like '%REO%' then 'main'
when rc_code like '%TODAY%' then 'today'
when rc_code like '%Detail_product%' then 'detail'
when rc_code like '%Detail_m_product%' then 'mdetail' 
when rc_code like '%BasketView_product%' then 'cart' 
when rc_code like '%BasketView_m_product%' then 'mcart' 
when rc_code like '%Search_product%' then 'search' 
when rc_code like '%Search_m_product%' then 'msearch'
when rc_code like '%RECENT%' then 'recent'
end) as rc, 
count(*) count
from tb_log_view_report 
group by 
(case when rc_code like '%RECO%' then 'm_main'
when rc_code like '%REO%' then 'main'
when rc_code like '%TODAY%' then 'today'
when rc_code like '%Detail_product%' then 'detail'
when rc_code like '%Detail_m_product%' then 'mdetail' 
when rc_code like '%BasketView_product%' then 'cart' 
when rc_code like '%BasketView_m_product%' then 'mcart' 
when rc_code like '%Search_product%' then 'search' 
when rc_code like '%Search_m_product%' then 'msearch'
when rc_code like '%RECENT%' then 'recent'
end)
order by 2 desc;

select 
(case when rc_code like '%RECO%' then 'm_main'
when rc_code like '%REO%' then 'main'
when rc_code like '%TODAY%' then 'today'
when rc_code like '%Detail_product%' then 'detail'
when rc_code like '%Detail_m_product%' then 'mdetail' 
when rc_code like '%BasketView_product%' then 'cart' 
when rc_code like '%BasketView_m_product%' then 'mcart' 
when rc_code like '%Search_product%' then 'search' 
when rc_code like '%Search_m_product%' then 'msearch'
when rc_code like '%Recent%' then 'recent'
end) as rc, 
count(distinct pc_id) uv
from tb_log_view_report 
group by 
(case when rc_code like '%RECO%' then 'm_main'
when rc_code like '%REO%' then 'main'
when rc_code like '%TODAY%' then 'today'
when rc_code like '%Detail_product%' then 'detail'
when rc_code like '%Detail_m_product%' then 'mdetail' 
when rc_code like '%BasketView_product%' then 'cart' 
when rc_code like '%BasketView_m_product%' then 'mcart' 
when rc_code like '%Search_product%' then 'search' 
when rc_code like '%Search_m_product%' then 'msearch'
when rc_code like '%Recent%' then 'recent'
end)
order by 2 desc;

