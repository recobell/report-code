
select
	(case when convert_timezone('KST', server_time)::date >= '2015-11-23' and convert_timezone('KST', server_time)::date <= '2015-11-29' then 1 
	when convert_timezone('KST', server_time)::date >= '2015-11-30' and convert_timezone('KST', server_time)::date <= '2015-12-06' then 2 
	when convert_timezone('KST', server_time)::date >= '2015-12-07' and convert_timezone('KST', server_time)::date <= '2015-12-13' then 3 
	when convert_timezone('KST', server_time)::date >= '2015-12-14' and convert_timezone('KST', server_time)::date <= '2015-12-20' then 4 end) timeslot
	,count(distinct pc_id) pcid_number
	,count(distinct session_id) session_number
	
	,count(*) pv
	,count(case when rc_code like '%Search%' then 1 end) search_pv
	,round(count(case when rc_code like '%Search%' then 1 end)::real/count(*)::real,4) search_share 
	,round(count(case when rc_code like '%Detail%' then 1 end)::real/count(*)::real,4) Detail_share 
	,round(count(distinct session_id)::real/count(distinct pc_id)::real,2) retention
	,round(count(*)::real / count(distinct session_id)::real,3) click
from tb_log_view
where device = 'PW'
group by 1
order by 1;



select (case when convert_timezone('KST', server_time)::date >= '2015-11-23' and convert_timezone('KST', server_time)::date <= '2015-11-29' then 1 
	when convert_timezone('KST', server_time)::date >= '2015-11-30' and convert_timezone('KST', server_time)::date <= '2015-12-06' then 2 
	when convert_timezone('KST', server_time)::date >= '2015-12-07' and convert_timezone('KST', server_time)::date <= '2015-12-13' then 3 
	when convert_timezone('KST', server_time)::date >= '2015-12-14' and convert_timezone('KST', server_time)::date <= '2015-12-20' then 4 end) timeslot
	,count(distinct pc_id) pcid_number
	,count(distinct session_id) session_number
	,count(*) search_num
	,round(count(*)::real / count(distinct session_id)::real,3) search_number
from tb_log_search
where device = 'PW'
group by 1
order by 1;




drop table if exists tb_reco_revenue_s1;
create table tb_reco_revenue_s1 as
select 
	A.pc_id
	,A.device
	,A.rc_code
	,A.item_id
	,A.server_time
	,B.price
	,B.quantity
from (select pc_id, device, item_id,rc_code, server_time from tb_log_view where rc_code is not null group by 1,2,3,4,5) A
inner join tb_log_order B on A.pc_id = B.pc_id  and A.item_id = B.item_id
where datediff(minute,a.server_time, b.server_time)>0 and datediff(minute,a.server_time, b.server_time)<2440
group by 1,2,3,4,5,6,7;

drop table if exists tb_reco_revenue;
create table tb_reco_revenue as
select 
	rc_code
	,sum(price*quantity) reco_revenue
from tb_reco_revenue_s1
where device = 'PW' and server_time::date > '2015-12-15'
group by 1;

select sum(reco_revenue) from tb_reco_revenue;