
--************************************************************************
--pc_id, session_id matching
--************************************************************************

drop table if exists tb_log_session_pc_id;
CREATE TABLE tb_log_session_pc_id as
select
	pc_id
	,session_id
from tb_log_view
group by pc_id, session_id;


--************************************************************************
--first reco view
--************************************************************************
drop table if exists tb_log_view_report_rank_s1;
CREATE TABLE tb_log_view_report_rank_s1 as
select
	* 
	,rank() over (partition by session_id order by server_time)
from tb_log_view;

drop table if exists tb_log_view_report_rank_s2;
CREATE TABLE tb_log_view_report_rank_s2 as
select
	session_id
	,min(rank) first_reco_view
from tb_log_view_report_rank_s1
where rc_code is not null
group by session_id, pc_id;

drop table if exists tb_log_view_report_rank;
CREATE TABLE tb_log_view_report_rank as
select 
A.session_id
,B.device
,A.first_reco_view
,B.rc_code
from
tb_log_view_report_rank_s2 A
inner join tb_log_view_report_rank_s1 B 
on A.session_id = B.session_id and A.first_reco_view = B.rank;

--************************************************************************
--first four view
--************************************************************************
drop table if exists tb_log_view_first_four;
CREATE TABLE tb_log_view_first_four as 
select 
	session_id
	,max(case when rank = 1 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 1 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) first_view
	,max(case when rank = 2 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 2 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) second_view
	,max(case when rank = 3 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 3 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) third_view
	,max(case when rank = 4 and a.item_id is not null and rc_code is not null then concat('reco_',a.rc_code) when rank = 4 and rc_code is null then a.item_id when a.item_id is null then 'exit' end) fourth_view
	,max(case when rank = 1 and a.item_id is not null then b.category1 end) first_category
	,max(case when rank = 2 and a.item_id is not null then b.category1 end) second_category
	,max(case when rank = 3 and a.item_id is not null then b.category1 end) third_category
	,max(case when rank = 4 and a.item_id is not null then b.category1 end) fourth_category
from tb_log_view_report_rank_s1 a
left join tb_product_info b on a.item_id = b.item_id
group by session_id;

--************************************************************************
--view
--************************************************************************

drop table if exists tb_crm_kpi_view_s1;
create table tb_crm_kpi_view_s1 as
select
	A.pc_id
	,A.device
	,A.session_id
	,count(*) as view
	,sum(case when A.rc_code is not null then 1 else 0 end) as reco_view
	,count(distinct A.item_id) as itemvar
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) first_server_time

from tb_log_view_report A
group by A.pc_id, A.device, A.session_id;



drop table if exists tb_crm_kpi_view_s2;
create table tb_crm_kpi_view_s2 as
select
	A.*
	,B.first_reco_view
	,B.rc_code frist_reco
from tb_crm_kpi_view_s1 A 
left join tb_log_view_report_rank B on A.session_id = B.session_id;





--************************************************************************
--reco order
--************************************************************************
drop table if exists tb_reco_revenue_s1;
create table tb_reco_revenue_s1 as
select 
	A.session_id
	,A.item_id
	,B.price
	,B.quantity
from (select min(case when rc_code is not null then server_time end) server_time, session_id, item_id, max(case when rc_code is not null then 1 else 0 end) rc_num from tb_log_view_report group by session_id, item_id) A
inner join tb_log_order_report B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_num = 1 and a.server_time < b.server_time;

drop table if exists tb_reco_revenue;
create table tb_reco_revenue as
select 
	session_id
	,sum(price*quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s1
group by session_id;

--##########################################################################################
--view and order
--order (frequency, quantity, revenue)
--##########################################################################################
drop table if exists tb_crm_kpi_order_s1;
create table tb_crm_kpi_order_s1 as
	SELECT 
		
        pc_id,
		session_id,
		min(server_time) server_time,
		sum(quantity) quantity,
        sum(price * quantity) revenue,
		count(distinct item_id) buyitemnum,
		count(distinct order_id) ordercount
    FROM
        tb_log_order_report
    GROUP BY pc_id, session_id;



--##########################################################################################
--final funnel metric table
--##########################################################################################

drop table if exists tb_session_funnel;
create table tb_session_funnel as
select
	A.first_server_time first_view_time
	,B.server_time order_time
	,A.session_id
	,E.pc_id
	,A.device
	,A.first_reco_view
	,A.frist_reco
	,A.view view_count
	,A.reco_view reco_view_count
	,A.itemvar item_var
	,A.period view_period
	,D.first_view
	,D.second_view
	,D.third_view
	,D.fourth_view
	,D.first_category
	,D.second_category
	,D.third_category
	,D.fourth_category
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count
	,C.reco_revenue 
	,C.reco_quantity
	
from tb_crm_kpi_view_s2 A
left join tb_crm_kpi_order_s1 B on A.session_id = B.session_id
left join tb_reco_revenue C on A.session_id = C.session_id
left join tb_log_view_first_four D on A.session_id = D.session_id
left join tb_log_session_pc_id E on A.session_id = E.session_id;


--##########################################################################################
--analytics
--##########################################################################################

select 
sum(order_revenue) total_revenue
, sum(reco_revenue) reco_revenue
, sum(case when reco_view_count > 0 then order_revenue end) old_reco_revenue
from tb_session_funnel;

--1,2,3,4 번째 클릭이 reco 클릭일 때 구매전환율 비교
select 
	count(case when order_revenue is not null then 1 end)::real/count(*)::real total_cvr
	,count(case when order_revenue is not null and reco_view_count>0 then 1 end)::real/count(case when reco_view_count>0 then 1 end)::real reco_cvr

	,count(case when order_revenue is not null and first_reco_view = 1 then 1 end)::real/count(case when first_reco_view = 1 then 1 end)::real first_reco_one_cvr
	,count(case when order_revenue is not null and view_count>=1 then 1 end)::real/count(case when view_count>=1  then 1 end)::real one_cvr


	,count(case when order_revenue is not null and first_reco_view = 2  then 1 end)::real/count(case when first_reco_view = 2 then 1 end)::real first_reco_two_cvr
	,count(case when order_revenue is not null and view_count>=2 then 1 end)::real/count(case when view_count>=2  then 1 end)::real two_cvr

	,count(case when order_revenue is not null and first_reco_view = 3  then 1 end)::real/count(case when first_reco_view = 3 then 1 end)::real first_reco_three_cvr
	,count(case when order_revenue is not null and view_count>=3 then 1 end)::real/count(case when view_count>=3  then 1 end)::real three_cvr

	,count(case when order_revenue is not null and first_reco_view = 4  then 1 end)::real/count(case when first_reco_view = 4 then 1 end)::real first_reco_four_cvr
	,count(case when order_revenue is not null and view_count>=4 then 1 end)::real/count(case when view_count>=4  then 1 end)::real four_cvr
from tb_session_funnel;
