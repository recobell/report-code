drop table if exists tbs_consult.tb_log_cv_rec_s1;
create table tbs_consult.tb_log_cv_rec_s1 as
select
	pcid
	,item_id
	,user_id
	,device
	,server_time
	,reco
from tbs_rblog.tb_log_view
where date_format(convert_tz(server_time, '+00:00','+09:00'), '%y-%m') = '15-12';


drop table if exists tbs_consult.tb_log_cv_rec_s2;
create table tbs_consult.tb_log_cv_rec_s2 as
select
	a.server_time
	,a.pcid
	,a.device
	,a.user_id
	,a.order_id
	,a.item_num
	,a.revenue order_price
	,b.item_id
	,b.price/b.quantity price
	,b.quantity
	,b.item_name
	,b.item_category
from tbs_rblog.tb_log_order a
inner join tbs_rblog.tb_log_order_item b on a.order_id = b.order_id
where date_format(convert_tz(a.server_time, '+00:00','+09:00'), '%y-%m') = '15-12';

create index pcid on tbs_consult.tb_log_cv_rec_s1 (pcid);
create index item_id on tbs_consult.tb_log_cv_rec_s1 (item_id);
create index pcid on tbs_consult.tb_log_cv_rec_s2 (pcid);
create index item_id on tbs_consult.tb_log_cv_rec_s2 (item_id);


drop table if exists tbs_consult.tb_log_cv_rec_s3;
create table tbs_consult.tb_log_cv_rec_s3 as
select 
	a.pcid
	,a.reco
	,a.item_id
	,a.server_time view_time
	,b.server_time order_time
	,b.order_id
	,b.order_price
	,b.price
	,b.quantity
from tbs_consult.tb_log_cv_rec_s1 a
left join tbs_consult.tb_log_cv_rec_s2 b on a.pcid = b.pcid and a.item_id = b.item_id
where timestampdiff (minute, a.server_time, b.server_time )<1440 and timestampdiff (minute, a.server_time, b.server_time ) > 0
order by a.pcid, a.item_id, a.server_time;

drop table if exists tbs_consult.tb_log_cv_rec_s4;
create table tbs_consult.tb_log_cv_rec_s4 as
select 
	pcid
	,item_id
	,order_id
	,max(case when reco is not null then 1 else 0 end) reco
	,max(order_price) order_price
	,max(price) price
	,max(quantity) quantity
from tbs_consult.tb_log_cv_rec_s3
group by 1,2,3;


select reco, count(distinct order_id) from tbs_consult.tb_log_cv_rec_s4 group by reco;
select reco, count(order_id) from tbs_consult.tb_log_cv_rec_s4 group by reco;




select * from tbs_consult.tb_log_cv_rec_s3 where order_id = 'G2015122330055';
select * from tbs_consult.tb_log_cv_rec_s4 where reco = 1; 

select pcid,item_id,count(*) from tbs_consult.tb_log_cv_rec_s3 group by pcid,item_id having count(*)>1;


select reco, round(sum(price * quantity)) from tbs_consult.tb_log_cv_rec_s3 group by reco;

select count(*) from tbs_consult.tb_log_cv_rec_s1 limit 100;
select count(*) from tbs_consult.tb_log_cv_rec_s2 limit 100;