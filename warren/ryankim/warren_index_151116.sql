drop table if exists tb_log_view_report;
CREATE TABLE tb_log_view_report as
select
	server_time,
	extract(year from convert_timezone('KST', server_time)) as year, 
	extract(month from convert_timezone('KST', server_time)) as month, 
	
	extract(day from convert_timezone('KST', server_time)) as day, 
	extract(hour from convert_timezone('KST', server_time)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term
from 
tb_log_view;

select * from tb_log_view limit 100;

drop table if exists tb_log_order_report;
CREATE TABLE tb_log_order_report as
select
	server_time,
	extract(year from convert_timezone('KST', server_time)) as year, 
	extract(month from convert_timezone('KST', server_time)) as month, 
	
	extract(day from convert_timezone('KST', server_time)) as day, 
	extract(hour from convert_timezone('KST', server_time)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term, 
	order_id, 
	order_price,
	price, 
	quantity
from 
tb_log_order;
--************************************************************************
--first reco click
--************************************************************************
drop table if exists tb_log_session_pc_id;
CREATE TABLE tb_log_session_pc_id as
select
	pc_id
	,session_id
from tb_log_view 
group by pc_id, session_id;


drop table if exists tb_log_view_report_rank_s1;
CREATE TABLE tb_log_view_report_rank_s1 as
select
	* 
	,rank() over (partition by session_id order by servertime)
from tb_log_view_report;

drop table if exists tb_log_view_report_rank_s2;
CREATE TABLE tb_log_view_report_rank_s2 as
select
	session_id
	,min(rank) first_reco_view
from tb_log_view_report_rank_s1
where rc_code is not null
group by session_id, pc_id;

drop table if exists tb_log_view_report_rank_s3;
CREATE TABLE tb_log_view_report_rank_s3 as
select 
A.session_id
,B.device
,A.first_reco_view
,B.rc_code
from
tb_log_view_report_rank_s2 A
inner join tb_log_view_report_rank_s1 B on A.session_id = B.session_id and A.first_reco_view = B.rank;

drop table if exists tb_log_view_report_rank;
CREATE TABLE tb_log_view_report_rank as 
select
first_reco_view,
device,
count(*)
from tb_log_view_report_rank_s3 
group by 1,2
order by 2,1 ;

--************************************************************************
--first four view
--************************************************************************
drop table if exists tb_log_view_first_four;
CREATE TABLE tb_log_view_first_four as 
select 
	session_id
	,max(case when rank = 1 then rc_code end) first_view
	,max(case when rank = 2 then rc_code end) second_view
	,max(case when rank = 3 then rc_code end) third_view
	,max(case when rank = 4 then rc_code end) fourth_view
from tb_log_view_report_rank_s1
group by session_id;

select * from tb_log_view_first_four limit 100;
--************************************************************************
--view
--************************************************************************

drop table if exists tb_crm_kpi_view_s1;
create table tb_crm_kpi_view_s1 as
select
	A.pc_id
	,A.device
	,A.session_id
	,count(*) as view
	,sum(case when A.rc_code is not null then 1 else 0 end) as reco_view
	,count(distinct A.item_id) as itemvar
	,count(A.item_id) as itemnum
	,datediff(second,min(servertime),max(servertime)) as period

from tb_log_view_report A
group by A.pc_id, A.device, A.session_id;


drop table if exists tb_crm_kpi_view_s2;
create table tb_crm_kpi_view_s2 as
select
	A.*
	,B.first_reco_view
	,B.rc_code frist_reco
from tb_crm_kpi_view_s1 A 
left join tb_log_view_report_rank_s3 B on A.session_id = B.session_id;





--************************************************************************
--reco order
--************************************************************************
drop table if exists tb_reco_revenue_s1;
create table tb_reco_revenue_s1 as
select 
	A.session_id
	,A.item_id
	,A.rc_code
	,B.price
	,B.quantity
from tb_log_view_report A
inner join tb_log_order_report B on A.session_id = B.session_id  and A.item_id = B.item_id
where A.rc_code is not null;

drop table if exists tb_reco_revenue;
create table tb_reco_revenue as
select 
	session_id
	,sum(price) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s1
group by session_id;


--##########################################################################################
--view and order
--order (frequency, quantity, revenue)
--##########################################################################################
drop table if exists tb_crm_kpi_order_s1;
create table tb_crm_kpi_order_s1 as
	SELECT 
        pc_id,
		session_id,
		sum(quantity) quantity,
        sum(price * quantity) revenue,
		count(distinct item_id) buyitemnum,
		count(distinct orderid) ordercount
    FROM
        tb_log_order_report
    GROUP BY pc_id, session_id;


drop table if exists tb_session_funnel;
create table tb_session_funnel as
select 
	A.session_id
	,E.pc_id
	,A.device
	,A.first_reco_view
	,A.frist_reco
	,A.view view_count
	,A.reco_view reco_view_count
	,A.itemvar view_var
	,A.itemnum view_itemnum
	,A.period view_period
	,D.first_view
	,D.second_view
	,D.third_view
	,D.fourth_view
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count
	,C.reco_revenue 
	,C.reco_quantity
	
from tb_crm_kpi_view_s2 A
left join tb_crm_kpi_order_s1 B on A.session_id = B.session_id
left join tb_reco_revenue C on A.session_id = C.session_id
left join tb_log_view_first_four D on A.session_id = D.session_id
left join tb_log_session_pc_id E on A.session_id = E.session_id;



--reco revenue / old reco revenue/ total revenue
select 
	sum(reco_revenue) reco_revenue
	,sum(case when reco_view_count <> 0 then order_revenue end) old_reco_revenue
	,sum(order_revenue) total_revenue
from tb_session_funnel;

--1,2,3,4 번째 클릭이 reco 클릭일 때 구매전환율 비교
select 
	count(case when order_revenue is not null then 1 end)::real/count(*)::real total_cvr
	,count(case when order_revenue is not null and reco_view_count>0 then 1 end)::real/count(case when reco_view_count>0 then 1 end)::real reco_cvr

	,count(case when order_revenue is not null and first_view is not null then 1 end)::real/count(case when first_reco_view = 1 then 1 end)::real first_reco_one_cvr
	,count(case when order_revenue is not null and view_count>=1 then 1 end)::real/count(case when view_count>=1  then 1 end)::real one_cvr


	,count(case when order_revenue is not null and first_reco_view = 2  then 1 end)::real/count(case when first_reco_view = 2 then 1 end)::real first_reco_two_cvr
	,count(case when order_revenue is not null and view_count>=2 then 1 end)::real/count(case when view_count>=2  then 1 end)::real two_cvr

	,count(case when order_revenue is not null and first_reco_view = 3  then 1 end)::real/count(case when first_reco_view = 3 then 1 end)::real first_reco_three_cvr
	,count(case when order_revenue is not null and view_count>=3 then 1 end)::real/count(case when view_count>=3  then 1 end)::real three_cvr

	,count(case when order_revenue is not null and first_reco_view = 4  then 1 end)::real/count(case when first_reco_view = 4 then 1 end)::real first_reco_four_cvr
	,count(case when order_revenue is not null and view_count>=4 then 1 end)::real/count(case when view_count>=4  then 1 end)::real four_cvr
from tb_session_funnel;

-- pc_id, item_id별로 구매전환율 체크 추천클릭한 상품이 구매전환이 될 확률이 높은지, 추천 클릭하지 않은 상품이 구매전환이 될 확률이 높은지 확인
drop table if exists tb_rc_code_cvr_s1;
create table tb_rc_code_cvr_s1 as
select 
A.pc_id
,A.session_id
,A.item_id view_item
,A.rc_code rc_code
,B.item_id order_item
,B.price
from tb_log_view_report A
left join tb_log_order_report B on A.pc_id=B.pc_id and A.session_id = B.session_id and A.item_id = B.item_id;

select * from tb_log_order_report limit 10;

drop table if exists tb_rc_code_cvr_s2;
create table tb_rc_code_cvr_s2 as
select 
pc_id
, view_item
--, price
, max(case when order_item is not null then 1 else 0 end) as cv
, sum(case when rc_code is not null then 1 else 0 end) rec_count
, count(*) all_count 
from tb_rc_code_cvr_s1
group by pc_id, view_item;--, price;

drop table if exists tb_rc_code_cvr;
create table tb_rc_code_cvr as
select
	count(case when a.rec_count = 0 then a.pc_id end) no_reco_view_count
	,count(case when a.rec_count = 0 and a.cv = 1 then a.pc_id end) no_reco_order_count
	,count(case when a.rec_count = 0 and a.cv = 1 then a.pc_id end) :: real  / count(case when a.rec_count = 0 then a.pc_id end) :: real no_reco_cvr
	,avg(case when a.rec_count = 0 and a.cv = 1 then b.sale_price end) no_reco_avg_sale_price
	,avg(case when a.rec_count = 0 and a.cv = 1 then b.original_price end) no_reco_avg_original_price
	,sqrt(variance(case when a.rec_count = 0 and a.cv = 1 then b.sale_price end)) no_reco_var_sale_price

	,count(case when a.rec_count > 0 then pc_id end) reco_view_count
	,count(case when a.rec_count > 0 and a.cv = 1 then a.pc_id end) reco_order_count
	,count(case when a.rec_count > 0 and a.cv = 1 then a.pc_id end) :: real  / count(case when a.rec_count > 0 then a.pc_id end) :: real reco_cvr
	,avg(case when a.rec_count > 0 and a.cv = 1 then b.sale_price end) reco_avg_sale_price
	,avg(case when a.rec_count > 0 and a.cv = 1 then b.original_price end) reco_avg_original_price
	,sqrt(variance(case when a.rec_count > 0 and a.cv = 1 then b.sale_price end)) reco_var_sale_price
from tb_rc_code_cvr_s2 a
inner join tb_product_info b on a.view_item = b.item_id ;

select * from tb_rc_code_cvr_s1 limit 100;
select * from tb_rc_code_cvr_s2 where cv = 1 and rec_count<>0 limit 100;
select * from tb_rc_code_cvr;


--구매한 상품을 보았을 때 추천으로 볼수록, 상품 클릭을 더 적게하고 구매한다.
select 
avg(case when rec_count <> 0 then all_count::real end) as reco_count
,avg(case when rec_count = 0 then all_count::real end) as no_reco_count
from tb_rc_code_cvr_s2 where cv = 1 limit 100;


--##########################################################################################
--rcratio vs index
--##########################################################################################


drop table if exists tb_crm_kpi_rcratio_itemnum_s1;
create table tb_crm_kpi_rcratio_itemnum_s1 as
select
	round(reco_view::real/view::real,1) as rcratio
	,reco_view
	,view
	,itemnum
	,itemvar
	,period
from tb_session_funnel;

drop table if exists tb_crm_kpi_rcratio_itemnum;
create table tb_crm_kpi_rcratio_itemnum as
select
	rcratio
	,round(avg(itemnum::real),2) avg_itemnum
	,round(avg(itemvar::real),2) avg_itemvar
	,round(avg(period::real),2) avg_period
	,count(*) count
	,round(avg(view::real),2) avg_view
from tb_crm_kpi_rcratio_itemnum_s1
group by rcratio;

--rc_code - index about order
drop table if exists tb_crm_kpi_conversion_s2;
create table tb_crm_kpi_conversion_s2 as
select 
	round(reco_view_count::real/view_count::real,1) rcratio
	,case when order_revenue is not null then 1 else 0 end as conversion
	,order_quantity
	,order_revenue
	,order_itemnum
	,view_count
	,view_period
from tb_session_funnel;

drop table if exists tb_crm_kpi_conversion_s3;
create table tb_crm_kpi_conversion_s3 as
select 
	rcratio	
	,round(count(case when conversion = 1 then conversion end)::real/ count(*)::real, 4) as cvr
	,round(avg(order_quantity::real),2) avg_quantity
	,round(avg(order_revenue::real),2) order_revenue
	,round(avg(order_itemnum::real),2) order_itemnum
	,count(*) count
	
	,round(avg(view_count::real),2) avg_view_count
	,round(avg(view_period::real),2) avg_view_period
	
from tb_crm_kpi_conversion_s2
group by rcratio;

--##########################################################################################
--view vs index
--##########################################################################################

--view_count - index about order
drop table if exists tb_crm_kpi_view_count;
create table tb_crm_kpi_view_count as
select
view_count
,session_count
,order_count
,order_revenue
,round(order_count::real/session_count::real, 2) cvr
,round(order_revenue::real/order_count::real,2) prevenue
from
(
select 
view_count 
,count(*) session_count
,sum(case when order_count is not null then order_count end) order_count
,sum(case when order_revenue is not null then order_revenue end) order_revenue
from tb_session_funnel 
where reco_view_count =0
group by view_count
)
order by session_count desc;

--##########################################################################################
--reco view vs index
--##########################################################################################

--reco_view_count - index about order
drop table if exists tb_crm_kpi_recoview_count;
create table tb_crm_kpi_recoview_count as
select
reco_view_count
,session_count
,order_count
,order_revenue
,round(order_count::real/session_count::real, 2) cvr
,round(order_revenue::real/order_count::real,2) prevenue

from
(
select 
reco_view_count
,count(*) session_count
,sum(case when order_count is not null then order_count end) order_count
,sum(case when order_revenue is not null then order_revenue end) order_revenue
from tb_session_funnel 

group by reco_view_count
)
order by session_count desc;

--##########################################################################################
--period vs index
--##########################################################################################

--period - index about order
drop table if exists tb_index_period_order_s1;
create table tb_index_period_order_s1 as
select 
	round(view_period/60, 0) as minute
	,case when order_revenue is not null then 1 else 0 end as conversion
	,order_quantity
	,order_revenue
	,order_itemnum
	,view_count
	
from tb_session_funnel;

drop table if exists tb_index_period_order;
create table tb_index_period_order as
select 
	minute
	,round(count(case when conversion = 1 then conversion end)::real/ count(*)::real, 4) as cvr
	,round(avg(order_quantity::real),2) avg_quantity
	,round(avg(order_revenue::real),2) order_revenue
	,round(avg(order_itemnum::real),2) order_itemnum
	,count(*) count
	
	,round(avg(view_count::real),2) avg_view_count
	,round(avg(minute::real),2) avg_view_period
	
from tb_index_period_order_s1
group by minute;


select * from tb_crm_kpi_rcratio_itemnum order by rcratio;
select * from tb_crm_kpi_conversion_s3 order by rcratio;
select * from tb_crm_kpi_view_count order by view_count;
select * from tb_crm_kpi_recoview_count order by reco_view_count;
select * from tb_index_period_order order by minute;

