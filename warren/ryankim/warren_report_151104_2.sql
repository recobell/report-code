--@drop-tb_log_view
drop table if exists tb_log_view;
--@end

--@create-tb_log_view
create table tb_log_view (	
cuid varchar(200),
type varchar(200),
serverTime timestamp,
ipAddress varchar(200),
userAgent varchar(2000),
apiVersion varchar(200),
domain varchar(200),
device varchar(200),
pcId varchar(200),
sessionId varchar(200),
adId varchar(200),
pushId varchar(200),
gcId varchar(200),
userId varchar(1000),
abGroup varchar(200),
rcCode varchar(200),
adCode varchar(200),
locale varchar(200),
itemId varchar(200),
searchTerm varchar(200)
);
--@end

--@drop-tb_log_search
drop table if exists tb_log_search;
--@end

--@create-tb_log_search
create table tb_log_search (
cuid varchar(200),
type varchar(200),
serverTime timestamp,
ipAddress varchar(200),
userAgent varchar(2000),
apiVersion varchar(200),
domain varchar(200),
device varchar(200),
pcId varchar(200),
sessionId varchar(200),
adId varchar(200),
pushId varchar(200),
gcId varchar(200),
userId varchar(200),
abGroup varchar(200),
rcCode varchar(200),
adCode varchar(200),
locale varchar(200),
searchTerm varchar(200),
searchResult varchar(200)
);
--@end

--@drop-tb_log_order
drop table if exists tb_log_order;
--@end

--@create-tb_log_order
create table tb_log_order (
cuid varchar(200),
type varchar(200),
serverTime timestamp,
ipAddress varchar(200),
userAgent varchar(2000),
apiVersion varchar(200),
domain varchar(200),
device varchar(200),
pcId varchar(200),
sessionId varchar(200),
adId varchar(200),
pushId varchar(200),
gcId varchar(200),
userId varchar(200),
abGroup varchar(200),
rcCode varchar(200),
adCode varchar(200),
locale varchar(200),
itemId varchar(200),
searchTerm varchar(200),
orderId varchar(200),
orderPrice bigint,
price bigint,
quantity bigint
);
--@end

--@drop-tmp_product_info
drop table if exists tmp_product_info;
--@end

--@create-tmp_product_info
create table tmp_product_info (
cuid varchar(200),
type varchar(200),
item_id varchar(200),
item_name varchar(2000),
item_image varchar(200),
item_url varchar(2000),
original_price int,
sale_price int,
category1 varchar(200),
category2 varchar(200),
category3 varchar(200),
category4 varchar(200),
category5 varchar(200),
reg_date varchar(200),
update_date timestamp,
expire_date timestamp,
stock int,
state varchar(200),
description varchar(200),
extra_image varchar(200),
locale varchar(200)
);
--@end


--# @DATE_STR should be replaced by a date pattern 

--@copy-tb_log_view
copy tb_log_view
from 's3://@LOG_BUCKET/@CUID/view/@DATE_STR'
CREDENTIALS 'aws_access_key_id=AKIAINS5BEHVPHMF3KKQ;aws_secret_access_key=/hLKtZWjQ7gWMvHg2hOfISuTgqeWVEv511cDdTFJ'
GZIP
FORMAT AS JSON 's3://rb-rec-report-conf/jsonpaths/tb_log_view.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';
--@end

--@copy-tb_log_search
copy tb_log_search
from 's3://@LOG_BUCKET/@CUID/search/@DATE_STR'
CREDENTIALS 'aws_access_key_id=AKIAINS5BEHVPHMF3KKQ;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://rb-rec-report-conf/jsonpaths/tb_log_search.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';
--@end

--@copy-tb_log_order
copy tb_log_order
from 's3://@LOG_BUCKET/@CUID/order/@DATE_STR'
CREDENTIALS 'aws_access_key_id=AKIAINS5BEHVPHMF3KKQ;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://rb-rec-report-conf/jsonpaths/tb_log_order.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';
--@end

--@copy-tmp_product_info
copy tmp_product_info
from 's3://@LOG_BUCKET/@CUID/product/@DATE_STR'
CREDENTIALS 'aws_access_key_id=AKIAINS5BEHVPHMF3KKQ;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://rb-rec-report-conf/jsonpaths/tmp_product_info.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';
--@end



-- refine tables
-- 'KST' should replaced


--@drop-tb_log_view_daily_report
drop table if exists tb_log_view_daily_report;
--@end

--@create-tb_log_view_daily_report
create table tb_log_view_daily_report as
select
	servertime,
	--extract(year from convert_timezone('KST', servertime)) as year, 
	--extract(month from convert_timezone('KST', servertime)) as month, 
	--extract(day from convert_timezone('KST', servertime)) as day, 
	--extract(hour from convert_timezone('KST', servertime)) as hour,
	convert_timezone('KST', servertime)::date as date, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	useragent,
	pcid, 
	sessionid, 
	gcid, 
	rccode,
	itemid, 
	searchterm
from 
tb_log_view
where 
convert_timezone('KST', servertime)::date = convert_timezone('KST', sysdate)::date - 1
--extract(year from convert_timezone('KST', servertime)) = extract(year from convert_timezone('KST', current_date) - interval '1 day')
--and extract(month from convert_timezone('KST', servertime)) = extract(month from convert_timezone('KST', current_date) - interval '1 day')
--and extract(day from convert_timezone('KST', servertime)) = extract(day from convert_timezone('KST', current_date) - interval '1 day')
;
--@end


--@drop-tb_log_search_daily_report
drop table if exists tb_log_search_daily_report;
--@end

--@create-tb_log_search_daily_report
create table tb_log_search_daily_report as
select
	servertime,
	--extract(year from convert_timezone('KST', servertime)) as year, 
	--extract(month from convert_timezone('KST', servertime)) as month, 
	--extract(day from convert_timezone('KST', servertime)) as day, 
	--extract(hour from convert_timezone('KST', servertime)) as hour,
	convert_timezone('KST', servertime)::date as date, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	useragent,
	pcid, 
	sessionid, 
	gcid, 
	rccode,
	searchterm
from 
tb_log_search
where 
convert_timezone('KST', servertime)::date = convert_timezone('KST', sysdate)::date - 1
--extract(year from convert_timezone('KST', servertime)) = extract(year from convert_timezone('KST', current_date) - interval '1 day')
--and extract(month from convert_timezone('KST', servertime)) = extract(month from convert_timezone('KST', current_date) - interval '1 day')
--and extract(day from convert_timezone('KST', servertime)) = extract(day from convert_timezone('KST', current_date) - interval '1 day')
;
--@end


--@drop-tb_log_order_daily_report
drop table if exists tb_log_order_daily_report;
--@end

--@create-tb_log_order_daily_report
create table tb_log_order_daily_report as
select
	servertime,
	--extract(year from convert_timezone('KST', servertime)) as year, 
	--extract(month from convert_timezone('KST', servertime)) as month, 
	--extract(day from convert_timezone('KST', servertime)) as day, 
	--extract(hour from convert_timezone('KST', servertime)) as hour,
	convert_timezone('KST', servertime)::date as date, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	useragent,
	pcid, 
	sessionid, 
	gcid, 
	rccode,
	itemid, 
	searchterm, 
	orderid, 
	orderprice,
	price, 
	quantity
from 
tb_log_order
where 
convert_timezone('KST', servertime)::date = convert_timezone('KST', sysdate)::date - 1
--extract(year from convert_timezone('KST', servertime)) = extract(year from convert_timezone('KST', current_date) - interval '1 day')
--and extract(month from convert_timezone('KST', servertime)) = extract(month from convert_timezone('KST', current_date) - interval '1 day')
--and extract(day from convert_timezone('KST', servertime)) = extract(day from convert_timezone('KST', current_date) - interval '1 day')
;
--@end




--@drop-tb_log_view_monthly_report
drop table if exists tb_log_view_monthly_report;
--@end

--@create-tb_log_view_monthly_report
create table tb_log_view_monthly_report as
select
	servertime,
	--extract(year from convert_timezone('KST', servertime)) as year, 
	--extract(month from convert_timezone('KST', servertime)) as month, 
	--extract(day from convert_timezone('KST', servertime)) as day, 
	--extract(hour from convert_timezone('KST', servertime)) as hour,
	convert_timezone('KST', servertime)::date - extract(day from convert_timezone('KST', servertime))+1 as date, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	useragent,
	pcid, 
	sessionid, 
	gcid, 
	rccode,
	itemid, 
	searchterm
from 
tb_log_view
where 
extract(year from convert_timezone('KST', servertime)) = extract(year from convert_timezone('KST', current_date) - interval '1 month')
and extract(month from convert_timezone('KST', servertime)) = extract(month from convert_timezone('KST', current_date) - interval '1 month')
;
--@end


--@drop-tb_log_search_monthly_report
drop table if exists tb_log_search_monthly_report;
--@end

--@create-tb_log_search_monthly_report
create table tb_log_search_monthly_report as
select
	servertime,
	--extract(year from convert_timezone('KST', servertime)) as year, 
	--extract(month from convert_timezone('KST', servertime)) as month, 
	--extract(day from convert_timezone('KST', servertime)) as day, 
	--extract(hour from convert_timezone('KST', servertime)) as hour,
	convert_timezone('KST', servertime)::date - extract(day from convert_timezone('KST', servertime))+1 as date, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	useragent,
	pcid, 
	sessionid, 
	gcid, 
	rccode,
	searchterm
from 
tb_log_search
where 
extract(year from convert_timezone('KST', servertime)) = extract(year from convert_timezone('KST', current_date) - interval '1 month')
and extract(month from convert_timezone('KST', servertime)) = extract(month from convert_timezone('KST', current_date) - interval '1 month')
;
--@end

--@drop-tb_log_order_monthly_report
drop table if exists tb_log_order_monthly_report;
--@end

--@create-tb_log_order_monthly_report
create table tb_log_order_monthly_report as
select
	servertime,
	--extract(year from convert_timezone('KST', servertime)) as year, 
	--extract(month from convert_timezone('KST', servertime)) as month, 
	--extract(day from convert_timezone('KST', servertime)) as day, 
	--extract(hour from convert_timezone('KST', servertime)) as hour,
	convert_timezone('KST', servertime)::date - extract(day from convert_timezone('KST', servertime))+1 as date, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	useragent,
	pcid, 
	sessionid, 
	gcid, 
	rccode,
	itemid, 
	searchterm, 
	orderid, 
	orderprice,
	price, 
	quantity
from 
tb_log_order
where 
extract(year from convert_timezone('KST', servertime)) = extract(year from convert_timezone('KST', current_date) - interval '1 month')
and extract(month from convert_timezone('KST', servertime)) = extract(month from convert_timezone('KST', current_date) - interval '1 month')
;
--@end



--@drop-tb_product_info
drop table if exists tb_product_info;
--@end

--@create-tb_product_info
create table tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0
 group by item_id;
--@end

--@drop-tb_product_info_report
drop table if exists tb_product_info_report;
--@end

--@create-tb_product_info_report
CREATE TABLE tb_product_info_report as
select
	item_id, 
	item_name, 
	original_price, 
	sale_price, 
	category1, 
	category2, 
	category3, 
	category4, 
	category5, 
	reg_date
	stock
from 
tb_product_info;
--@end

--@drop-tmp_recommend_result_view
drop table if exists tmp_recommend_result_view;
--@end

--@create-tmp_recommend_result_view
CREATE TABLE tmp_recommend_result_view as
select 
	date
	,pcid 
	,device
	,count(*) as view
	,sum(case when rccode is not null then 1 else 0 end) as reco_view
	,sum(case when rccode is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date , pcid, device;
--@end

--@drop-tmp_recommend_result_order
drop table if exists tmp_recommend_result_order;
--@end

--@create-tmp_recommend_result_order
CREATE TABLE tmp_recommend_result_order as
select
	date
	,pcid 
	,device
	
	,sum(price) revenue
	,sum(case when rccode is not null then price else 0 end) reco_revenue
	,sum(case when rccode is null then price else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(case when rccode is not null then 1 else 0 end) reco_frequency
	,count(case when rccode is null then 1 else 0 end) no_reco_frequency
	
from tb_log_order_daily_report
group by date, pcid, device;
--@end

--@drop-tmp_recommend_result_view_session
drop table if exists tmp_recommend_result_view_session;
--@end

--@create-tmp_recommend_result_view_session
CREATE TABLE tmp_recommend_result_view_session as
select
	date
	,sessionid 
	,device
	,count(*) as view
	,sum(case when rccode is not null then 1 else 0 end) as reco_view
	,sum(case when rccode is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date,sessionid, device;
--@end

--@drop-tmp_recommend_result_order_session
drop table if exists tmp_recommend_result_order_session;
--@end
--@create-tmp_recommend_result_order_session
CREATE TABLE tmp_recommend_result_order_session as
select
	A.date
	,A.sessionid 
	,A.orderid
	,A.device
	
	,sum(A.price) revenue
	,sum(case when B.reco_view > 0 then A.price else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 then A.price else 0 end) no_reco_revenue
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.itemid) itemvar
	,count(distinct case when B.reco_view > 0 then A.itemid end) reco_itemvar
	,count(distinct case when B.reco_view = 0 then A.itemid end) no_reco_itemvar
	
	,count(distinct A.orderid) frequency
	,count(distinct case when B.reco_view > 0 then A.orderid end) reco_frequency
	,count(distinct case when B.reco_view = 0 then A.orderid end) no_reco_frequency
	
from tb_log_order_daily_report A
left join tmp_recommend_result_view_session B on A.sessionid = B.sessionid 
group by A.date,A.sessionid,A.orderid,A.device;
--@end

--@drop-tb_recommend_loyal_group
drop table if exists tb_recommend_loyal_group;
--@end

--@create-tb_recommend_loyal_group
CREATE TABLE tb_recommend_loyal_group AS SELECT * FROM
    tmp_recommend_result_view_session
WHERE
	view > 1;
--@end

--@drop-tb_recommend_reco_group
drop table if exists tb_recommend_reco_group;
--@end

--@create-tb_recommend_reco_group
CREATE TABLE tb_recommend_reco_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
where reco_view > 0;
--@end



--@drop-tb_recommend_result_uv_daily_s1
drop table if exists tb_recommend_result_uv_daily_s1;
--@end
--@create-tb_recommend_result_uv_daily_s1
CREATE TABLE tb_recommend_result_uv_daily_s1 AS
select
	date
	,device
	,count(distinct pcid) uv
    ,count(distinct case when reco_view > 0 then pcid end) reco_uv
from tmp_recommend_result_view
group by 1,2
order by 1,2;
--@end

--@drop-tb_recommend_result_uv_daily
drop table if exists tb_recommend_result_uv_daily;
--@end
--@create-tb_recommend_result_uv_daily
CREATE TABLE tb_recommend_result_uv_daily AS
select
	*
	,round( reco_uv :: real / uv :: real , 2) ratio
from tb_recommend_result_uv_daily_s1;
--@end
--@drop-tb_recommend_result_visit_daily_s1
drop table if exists tb_recommend_result_visit_daily_s1;
--@end
--@create-tb_recommend_result_visit_daily_s1
CREATE TABLE tb_recommend_result_visit_daily_s1 AS
select
	date
	,device
	,count(distinct sessionid) visit
    ,count(distinct case when reco_view > 0 then sessionid end) reco_visit    
    
from tmp_recommend_result_view_session
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_visit_daily
drop table if exists tb_recommend_result_visit_daily;
--@end
--@create-tb_recommend_result_visit_daily
CREATE TABLE tb_recommend_result_visit_daily AS
select
	*
	,round( reco_visit :: real / visit :: real , 2) ratio
from tb_recommend_result_visit_daily_s1;
--@end
--@drop-tb_recommend_result_pv_daily_s1
drop table if exists tb_recommend_result_pv_daily_s1;
--@end
--@create-tb_recommend_result_pv_daily_s1
CREATE TABLE tb_recommend_result_pv_daily_s1 AS
select
	date
	,device
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv
    
    
from tmp_recommend_result_view_session
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_pv_daily
drop table if exists tb_recommend_result_pv_daily;
--@end
--@create-tb_recommend_result_pv_daily
CREATE TABLE tb_recommend_result_pv_daily AS
select
	*
	,round( reco_pv :: real / pv :: real , 2) ratio
from tb_recommend_result_pv_daily_s1;
--@end
--@drop-tb_recommend_result_frequency_daily_s1
drop table if exists tb_recommend_result_frequency_daily_s1;
--@end
--@create-tb_recommend_result_frequency_daily_s1
CREATE TABLE tb_recommend_result_frequency_daily_s1 AS
select
	date
	,device
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency    
    
from tmp_recommend_result_order_session
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_frequency_daily
drop table if exists tb_recommend_result_frequency_daily;
--@end
--@create-tb_recommend_result_frequency_daily
CREATE TABLE tb_recommend_result_frequency_daily AS
select
	*
	,round( reco_frequency :: real / frequency :: real , 2) ratio
from tb_recommend_result_frequency_daily_s1;
--@end
--@drop-tb_recommend_result_revenue_daily_s1
drop table if exists tb_recommend_result_revenue_daily_s1;
--@end
--@create-tb_recommend_result_revenue_daily_s1
CREATE TABLE tb_recommend_result_revenue_daily_s1 AS
select
	date
	,device
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
    
from tmp_recommend_result_order_session
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_revenue_daily
drop table if exists tb_recommend_result_revenue_daily;
--@end
--@create-tb_recommend_result_revenue_daily
CREATE TABLE tb_recommend_result_revenue_daily AS
select
	*
	,round( reco_revenue :: real / revenue :: real , 2) ratio
from tb_recommend_result_revenue_daily_s1;
--@end


--@drop-tb_recommend_result_prevenue_daily_s1
drop table if exists tb_recommend_result_prevenue_daily_s1;
--@end
--@create-tb_recommend_result_prevenue_daily_s1
CREATE TABLE tb_recommend_result_prevenue_daily_s1 AS
select
	A.date
	,A.device
	,sum(A.revenue)/sum(A.frequency) prevenue
    ,sum(case when C.sessionid is not null then A.revenue end)/sum(case when C.sessionid is not null then A.frequency end) reco_prevenue
    ,sum(case when C.sessionid is null then A.revenue end)/sum(case when C.sessionid is null then A.frequency end) no_reco_prevenue
	
from tmp_recommend_result_order_session A
left join tb_recommend_reco_group C on A.date = C.date and A.sessionid = C.sessionid
group by 1,2
order by 1,2;
--@end

--@drop-tb_recommend_result_prevenue_daily
drop table if exists tb_recommend_result_prevenue_daily;
--@end
--@create-tb_recommend_result_prevenue_daily
CREATE TABLE tb_recommend_result_prevenue_daily AS SELECT *,
    round(reco_prevenue :: real / prevenue :: real, 4) as reco_up_ratio
FROM tb_recommend_result_prevenue_daily_s1;
--@end
--@drop-tb_recommend_result_quantity_daily_s1
drop table if exists tb_recommend_result_quantity_daily_s1;
--@end
--@create-tb_recommend_result_quantity_daily_s1
CREATE TABLE tb_recommend_result_quantity_daily_s1 AS
select
	A.date, A.device
	,round(avg(A.quantity :: real),3) quantity
    ,round(avg(case when C.sessionid is not null then A.quantity :: real end),3) reco_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_reco_group C on A.date = C.date and A.sessionid = C.sessionid
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_quantity_daily
drop table if exists tb_recommend_result_quantity_daily;
--@end
--@create-tb_recommend_result_quantity_daily
CREATE TABLE tb_recommend_result_quantity_daily AS SELECT *,
    round(reco_quantity :: real / quantity :: real, 4) as reco_up_ratio
FROM tb_recommend_result_quantity_daily_s1;
--@end
--@drop-tb_recommend_result_click_daily_s1
drop table if exists tb_recommend_result_click_daily_s1;
--@end
--@create-tb_recommend_result_click_daily_s1
CREATE TABLE tb_recommend_result_click_daily_s1 AS 
SELECT 
	date, device
	,round(AVG(view :: real),3) click
    ,round(AVG(case when reco_view>0 then view :: real end),3) reco_click
    
FROM
    tmp_recommend_result_view_session
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_click_daily
drop table if exists tb_recommend_result_click_daily;
--@end
--@create-tb_recommend_result_click_daily
CREATE TABLE tb_recommend_result_click_daily AS SELECT *,
    round(reco_click / click, 4) as reco_up_ratio
FROM tb_recommend_result_click_daily_s1;   
--@end
--@drop-tb_recommend_result_cvr_daily_s1
drop table if exists tb_recommend_result_cvr_daily_s1;
--@end
--@create-tb_recommend_result_cvr_daily_s1
CREATE TABLE tb_recommend_result_cvr_daily_s1 AS 
select 
	A.date, A.device
	,round(sum(B.frequency :: real)/count(distinct A.sessionid),3) cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: real end)/count(distinct case when A.reco_view >0 then A.sessionid end),3) reco_cvr
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date  and A.sessionid = B.sessionid
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_cvr_daily
drop table if exists tb_recommend_result_cvr_daily;
--@end
--@create-tb_recommend_result_cvr_daily
CREATE TABLE tb_recommend_result_cvr_daily AS SELECT *,
    round(reco_cvr / cvr, 4) as reco_up_ratio
FROM tb_recommend_result_cvr_daily_s1;   
--end
--@drop-tb_recommend_result_sa_daily_s1
drop table if exists tb_recommend_result_sa_daily_s1;
--@end
--@create-tb_recommend_result_sa_daily_s1
CREATE TABLE tb_recommend_result_sa_daily_s1 AS 
select 
	A.date, A.device
	,round(sum(B.revenue :: real)/count(distinct A.sessionid),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: real end)/count(distinct case when A.reco_view >0 then A.sessionid end),3) reco_sa
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.sessionid = B.sessionid
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_sa_daily
drop table if exists tb_recommend_result_sa_daily;
--@end
--@create-tb_recommend_result_sa_daily
CREATE TABLE tb_recommend_result_sa_daily AS SELECT *,
    round(reco_sa / sa, 4) as reco_up_ratio
FROM tb_recommend_result_sa_daily_s1; 
--@end
--@drop-tb_recommend_result_daily
drop table if exists tb_recommend_result_daily;
--@end
--@create-tb_recommend_result_daily
CREATE TABLE tb_recommend_result_daily AS 
select
a.date,
a.device,
a.visit session_visit_total,
a.reco_visit session_visit_reco, 
a.ratio session_visit_rate,

b.uv unique_visit_total,
b.reco_uv unique_visit_reco,
b.ratio unique_visit_rate,

c.pv page_view_total,
c.reco_pv page_view_reco,
c.ratio page_view_rate,

d.frequency purchase_count_total,
d.reco_frequency purchase_count_reco,
d.ratio purchase_count_rate,

e.revenue revenue_total,
e.reco_revenue revenue_reco,
e.ratio revenue_rate,

f.prevenue customer_transaction_avg_total,
f.reco_prevenue customer_transaction_avg_reco,
f.reco_up_ratio customer_transaction_avg_rate,

g.quantity purchase_item_count_avg_total,
g.reco_quantity purchase_item_count_avg_reco,
g.reco_up_ratio purchase_item_count_avg_rate,

h.click visit_item_click_avg_total,
h.reco_click visit_item_click_avg_reco,
h.reco_up_ratio visit_item_click_avg_rate,

i.cvr purchase_conversion_rate_total,
i.reco_cvr purchase_conversion_rate_reco,
i.reco_up_ratio purchase_conversion_rate_rate,

j.sa session_purchase_revenue_avg_total,
j.reco_sa session_purchase_revenue_avg_reco,
j.reco_up_ratio session_purchase_revenue_avg_rate

from tb_recommend_result_visit_daily a
left join tb_recommend_result_uv_daily b on a.date = b.date and a.device = b.device
left join tb_recommend_result_pv_daily c on a.date = c.date and a.device = c.device
left join tb_recommend_result_frequency_daily d on a.date = d.date and a.device = d.device
left join tb_recommend_result_revenue_daily e on a.date = e.date and a.device = e.device
left join tb_recommend_result_prevenue_daily f on a.date = f.date and a.device = f.device
left join tb_recommend_result_quantity_daily g on a.date = g.date and a.device = g.device
left join tb_recommend_result_click_daily h on a.date = h.date and a.device = h.device
left join tb_recommend_result_cvr_daily i on a.date = i.date and a.device = i.device
left join tb_recommend_result_sa_daily j on a.date = j.date and a.device = j.device;
--@end
--@drop-tb_product_info
drop table if exists tb_product_info;
--@end

--@create-tb_product_info
create table tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0
 group by item_id;
--@end

--@drop-tb_product_info_report
drop table if exists tb_product_info_report;
--@end

--@create-tb_product_info_report
CREATE TABLE tb_product_info_report as
select
	item_id, 
	item_name, 
	original_price, 
	sale_price, 
	category1, 
	category2, 
	category3, 
	category4, 
	category5, 
	reg_date
	stock
from 
tb_product_info;
--@end

--@drop-tmp_recommend_result_view
drop table if exists tmp_recommend_result_view;
--@end

--@create-tmp_recommend_result_view
CREATE TABLE tmp_recommend_result_view as
select 
	date
	,pcid 
	,device
	,count(*) as view
	,sum(case when rccode is not null then 1 else 0 end) as reco_view
	,sum(case when rccode is null then 1 else 0 end) as no_reco_view
from tb_log_view_monthly_report
group by date , pcid, device;
--@end

--@drop-tmp_recommend_result_order
drop table if exists tmp_recommend_result_order;
--@end

--@create-tmp_recommend_result_order
CREATE TABLE tmp_recommend_result_order as
select
	date
	,pcid 
	,device
	
	,sum(price) revenue
	,sum(case when rccode is not null then price else 0 end) reco_revenue
	,sum(case when rccode is null then price else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(case when rccode is not null then 1 else 0 end) reco_frequency
	,count(case when rccode is null then 1 else 0 end) no_reco_frequency
	
from tb_log_order_monthly_report
group by date, pcid, device;
--@end

--@drop-tmp_recommend_result_view_session
drop table if exists tmp_recommend_result_view_session;
--@end

--@create-tmp_recommend_result_view_session
CREATE TABLE tmp_recommend_result_view_session as
select
	date
	,sessionid 
	,device
	,count(*) as view
	,sum(case when rccode is not null then 1 else 0 end) as reco_view
	,sum(case when rccode is null then 1 else 0 end) as no_reco_view
from tb_log_view_monthly_report
group by date,  sessionid, device;
--@end

--@drop-tmp_recommend_result_order_session
drop table if exists tmp_recommend_result_order_session;
--@end
--@create-tmp_recommend_result_order_session
CREATE TABLE tmp_recommend_result_order_session as
select
	A.date
	,A.sessionid 
	,A.orderid
	,A.device
	
	,sum(A.price) revenue
	,sum(case when B.reco_view > 0 then A.price else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 then A.price else 0 end) no_reco_revenue
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.itemid) itemvar
	,count(distinct case when B.reco_view > 0 then A.itemid end) reco_itemvar
	,count(distinct case when B.reco_view = 0 then A.itemid end) no_reco_itemvar
	
	,count(distinct A.orderid) frequency
	,count(distinct case when B.reco_view > 0 then A.orderid end) reco_frequency
	,count(distinct case when B.reco_view = 0 then A.orderid end) no_reco_frequency
	
from tb_log_order_monthly_report A
left join tmp_recommend_result_view_session B on A.sessionid = B.sessionid 
group by A.date,A.sessionid,A.orderid,A.device;
--@end

--@drop-tb_recommend_loyal_group
drop table if exists tb_recommend_loyal_group;
--@end

--@create-tb_recommend_loyal_group
CREATE TABLE tb_recommend_loyal_group AS SELECT * FROM
    tmp_recommend_result_view_session
WHERE
	view > 1;
--@end

--@drop-tb_recommend_reco_group
drop table if exists tb_recommend_reco_group;
--@end

--@create-tb_recommend_reco_group
CREATE TABLE tb_recommend_reco_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
where reco_view > 0;
--@end



--@drop-tb_recommend_result_uv_monthly_s1
drop table if exists tb_recommend_result_uv_monthly_s1;
--@end
--@create-tb_recommend_result_uv_monthly_s1
CREATE TABLE tb_recommend_result_uv_monthly_s1 AS
select
	date
	,device
	,count(distinct pcid) uv
    ,count(distinct case when reco_view > 0 then pcid end) reco_uv
    
from tmp_recommend_result_view
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_uv_monthly
drop table if exists tb_recommend_result_uv_monthly;
--@end
--@create-tb_recommend_result_uv_monthly
CREATE TABLE tb_recommend_result_uv_monthly AS
select
	*
	,round( reco_uv :: real / uv :: real , 2) ratio
from tb_recommend_result_uv_monthly_s1;
--@end
--@drop-tb_recommend_result_visit_monthly_s1
drop table if exists tb_recommend_result_visit_monthly_s1;
--@end
--@create-tb_recommend_result_visit_monthly_s1
CREATE TABLE tb_recommend_result_visit_monthly_s1 AS
select
	date
	,device
	,count(distinct sessionid) visit
    ,count(distinct case when reco_view > 0 then sessionid end) reco_visit    
    
from tmp_recommend_result_view_session
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_visit_monthly
drop table if exists tb_recommend_result_visit_monthly;
--@end
--@create-tb_recommend_result_visit_monthly
CREATE TABLE tb_recommend_result_visit_monthly AS
select
	*
	,round( reco_visit :: real / visit :: real , 2) ratio
from tb_recommend_result_visit_monthly_s1;
--@end
--@drop-tb_recommend_result_pv_monthly_s1
drop table if exists tb_recommend_result_pv_monthly_s1;
--@end
--@create-tb_recommend_result_pv_monthly_s1
CREATE TABLE tb_recommend_result_pv_monthly_s1 AS
select
	date
	,device
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv
    
from tmp_recommend_result_view_session
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_pv_monthly
drop table if exists tb_recommend_result_pv_monthly;
--@end
--@create-tb_recommend_result_pv_monthly
CREATE TABLE tb_recommend_result_pv_monthly AS
select
	*
	,round( reco_pv :: real / pv :: real , 2) ratio
from tb_recommend_result_pv_monthly_s1;
--@end
--@drop-tb_recommend_result_frequency_monthly_s1
drop table if exists tb_recommend_result_frequency_monthly_s1;
--@end
--@create-tb_recommend_result_frequency_monthly_s1
CREATE TABLE tb_recommend_result_frequency_monthly_s1 AS
select
	date
	,device
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency
    
from tmp_recommend_result_order_session
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_frequency_monthly
drop table if exists tb_recommend_result_frequency_monthly;
--@end
--@create-tb_recommend_result_frequency_monthly
CREATE TABLE tb_recommend_result_frequency_monthly AS
select
	*
	,round( reco_frequency :: real / frequency :: real , 2) ratio
from tb_recommend_result_frequency_monthly_s1;
--@end
--@drop-tb_recommend_result_revenue_monthly_s1
drop table if exists tb_recommend_result_revenue_monthly_s1;
--@end
--@create-tb_recommend_result_revenue_monthly_s1
CREATE TABLE tb_recommend_result_revenue_monthly_s1 AS
select
	date
	,device
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
    
from tmp_recommend_result_order_session
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_revenue_monthly
drop table if exists tb_recommend_result_revenue_monthly;
--@end
--@create-tb_recommend_result_revenue_monthly
CREATE TABLE tb_recommend_result_revenue_monthly AS
select
	*
	,round( reco_revenue :: real / revenue :: real , 2) ratio
from tb_recommend_result_revenue_monthly_s1;
--@end
--@drop-tb_recommend_result_prevenue_monthly_s1
drop table if exists tb_recommend_result_prevenue_monthly_s1;
--@end
--@create-tb_recommend_result_prevenue_monthly_s1
CREATE TABLE tb_recommend_result_prevenue_monthly_s1 AS
select
	A.date
	,A.device
	,sum(A.revenue)/sum(A.frequency) prevenue
    ,sum(case when C.sessionid is not null then A.revenue end)/sum(case when C.sessionid is not null then A.frequency end) reco_prevenue
    
from tmp_recommend_result_order_session A
left join tb_recommend_reco_group C on A.date = C.date and A.sessionid = C.sessionid
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_prevenue_monthly
drop table if exists tb_recommend_result_prevenue_monthly;
--@end
--@create-tb_recommend_result_prevenue_monthly
CREATE TABLE tb_recommend_result_prevenue_monthly AS SELECT *,
    round(reco_prevenue :: real / prevenue :: real, 4) as reco_up_ratio
FROM tb_recommend_result_prevenue_monthly_s1;
--@end
--@drop-tb_recommend_result_quantity_monthly_s1
drop table if exists tb_recommend_result_quantity_monthly_s1;
--@end
--@create-tb_recommend_result_quantity_monthly_s1
CREATE TABLE tb_recommend_result_quantity_monthly_s1 AS
select
	A.date, A.device
	,round(avg(A.quantity :: real),3) quantity
    ,round(avg(case when C.sessionid is not null then A.quantity :: real end),3) reco_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_reco_group C on A.date = C.date and A.sessionid = C.sessionid
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_quantity_monthly
drop table if exists tb_recommend_result_quantity_monthly;
--@end
--@create-tb_recommend_result_quantity_monthly
CREATE TABLE tb_recommend_result_quantity_monthly AS SELECT *,
    round(reco_quantity :: real / quantity :: real, 4) as reco_up_ratio
FROM tb_recommend_result_quantity_monthly_s1;
--@end
--@drop-tb_recommend_result_click_monthly_s1
drop table if exists tb_recommend_result_click_monthly_s1;
--@end
--@create-tb_recommend_result_click_monthly_s1
CREATE TABLE tb_recommend_result_click_monthly_s1 AS 
SELECT 
	date, device
	,round(AVG(view :: real),3) click
    ,round(AVG(case when reco_view>0 then view :: real end),3) reco_click
FROM
    tmp_recommend_result_view_session
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_click_monthly
drop table if exists tb_recommend_result_click_monthly;
--@end
--@create-tb_recommend_result_click_monthly
CREATE TABLE tb_recommend_result_click_monthly AS SELECT *,
    round(reco_click / click, 4) as reco_up_ratio
FROM tb_recommend_result_click_monthly_s1;   
--@end
--@drop-tb_recommend_result_cvr_monthly_s1
drop table if exists tb_recommend_result_cvr_monthly_s1;
--@end
--@create-tb_recommend_result_cvr_monthly_s1
CREATE TABLE tb_recommend_result_cvr_monthly_s1 AS 
select 
	A.date, A.device
	,round(sum(B.frequency :: real)/count(distinct A.sessionid),3) cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: real end)/count(distinct case when A.reco_view >0 then A.sessionid end),3) reco_cvr
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.sessionid = B.sessionid
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_cvr_monthly
drop table if exists tb_recommend_result_cvr_monthly;
--@end
--@create-tb_recommend_result_cvr_monthly
CREATE TABLE tb_recommend_result_cvr_monthly AS SELECT *,
    round(reco_cvr / cvr, 4) as reco_up_ratio
FROM tb_recommend_result_cvr_monthly_s1;   
--@end
--@drop-tb_recommend_result_sa_monthly_s1
drop table if exists tb_recommend_result_sa_monthly_s1;
--@end
--@create-tb_recommend_result_sa_monthly_s1
CREATE TABLE tb_recommend_result_sa_monthly_s1 AS 
select 
	A.date, A.device
	,round(sum(B.revenue :: real)/count(distinct A.sessionid),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: real end)/count(distinct case when A.reco_view >0 then A.sessionid end),3) reco_sa
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.sessionid = B.sessionid
group by 1,2
order by 1,2;
--@end
--@drop-tb_recommend_result_sa_monthly
drop table if exists tb_recommend_result_sa_monthly;
--@end
--@create-tb_recommend_result_sa_monthly
CREATE TABLE tb_recommend_result_sa_monthly AS SELECT *,
    round(reco_sa / sa, 4) as reco_up_ratio
FROM tb_recommend_result_sa_monthly_s1;   
--@end


--@drop-tb_recommend_result_monthly
drop table if exists tb_recommend_result_monthly;
--@end
--@create-tb_recommend_result_monthly
CREATE TABLE tb_recommend_result_monthly AS 
select
a.date,
a.device,
a.visit session_visit_total,
a.reco_visit session_visit_reco, 
a.ratio session_visit_rate,

b.uv unique_visit_total,
b.reco_uv unique_visit_reco,
b.ratio unique_visit_rate,

c.pv page_view_total,
c.reco_pv page_view_reco,
c.ratio page_view_rate,

d.frequency purchase_count_total,
d.reco_frequency purchase_count_reco,
d.ratio purchase_count_rate,

e.revenue revenue_total,
e.reco_revenue revenue_reco,
e.ratio revenue_rate,

f.prevenue customer_transaction_avg_total,
f.reco_prevenue customer_transaction_avg_reco,
f.reco_up_ratio customer_transaction_avg_rate,

g.quantity purchase_item_count_avg_total,
g.reco_quantity purchase_item_count_avg_reco,
g.reco_up_ratio purchase_item_count_avg_rate,

h.click visit_item_click_avg_total,
h.reco_click visit_item_click_avg_reco,
h.reco_up_ratio visit_item_click_avg_rate,

i.cvr purchase_conversion_rate_total,
i.reco_cvr purchase_conversion_rate_reco,
i.reco_up_ratio purchase_conversion_rate_rate,

j.sa session_purchase_revenue_avg_total,
j.reco_sa session_purchase_revenue_avg_reco,
j.reco_up_ratio session_purchase_revenue_avg_rate

from tb_recommend_result_visit_monthly a
left join tb_recommend_result_uv_monthly b on a.date = b.date and a.device = b.device
left join tb_recommend_result_pv_monthly c on a.date = c.date and a.device = c.device
left join tb_recommend_result_frequency_monthly d on a.date = d.date and a.device = d.device
left join tb_recommend_result_revenue_monthly e on a.date = e.date and a.device = e.device
left join tb_recommend_result_prevenue_monthly f on a.date = f.date and a.device = f.device
left join tb_recommend_result_quantity_monthly g on a.date = g.date and a.device = g.device
left join tb_recommend_result_click_monthly h on a.date = h.date and a.device = h.device
left join tb_recommend_result_cvr_monthly i on a.date = i.date and a.device = i.device
left join tb_recommend_result_sa_monthly j on a.date = j.date and a.device = j.device;
--@end

