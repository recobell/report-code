drop table if exists tb_log_view_201601;
create table tb_log_view_201601 as
select
	pc_id
	,session_id
	,item_id
	,user_id
	,device
	,convert_timezone('KST',server_time) server_time
	,rc_code
from tb_log_view
where extract(month from convert_timezone('KST',server_time)) = 12;

select case when rc_code is not null then 1 else 0 end, count(*) from tb_log_view_201601 group by 1;

select rc_code, count(*) from (select session_id, max(case when rc_code is not null then 1 else 0 end) rc_code from tb_log_view_201601 group by 1) group by 1;

drop table if exists tb_log_order_201601;
create table tb_log_order_201601 as
select
	convert_timezone('KST',server_time) server_time
	,pc_id
	,device
	,user_id
	,order_id
	,order_price
	,item_id
	,price
	,quantity
from tb_log_order
where extract(month from convert_timezone('KST',server_time)) = 12;

drop table tb_order_stat;
create table tb_order_stat as
select
	order_id
	,sum(quantity) order_item_count
	,sum(price * quantity) order_price
from tb_log_order
where extract(month from convert_timezone('KST',server_time)) = 12
group by 1;

drop table if exists tb_order_item_within_24h;
create table tb_order_item_within_24h as
select 
	a.pc_id
	,a.session_id
	,a.rc_code
	,a.item_id
	,a.server_time view_time
	,b.server_time order_time
	,b.order_id
	,b.price
	,b.quantity
from tb_log_view_201601 a
join tb_log_order_201601 b on a.pc_id = b.pc_id and a.item_id = b.item_id
where datediff (minute, a.server_time, b.server_time ) < 1440 and datediff(minute, a.server_time, b.server_time ) >= 0;

drop table if exists tb_view_order_24h;
create table tb_view_order_24h as
select a.pc_id, a.session_id, a.rc_code, a.item_id, a.server_time as view_time, b.order_time, b.order_id 
from tb_log_view_201601 a
left join tb_order_item_within_24h b on a.pc_id = b.pc_id and a.item_id = b.item_id and a.session_id = b.session_id and a.server_time = b.view_time;

create table tb_rc_code_map as
select
	rc_code,
	case when rc_code like '%ProductDetail_product%' then 'pw_detail'
	when rc_code like '%BasketView_product%' then 'pw_basket'
	when rc_code like '%Search_product%' then 'pw_search' 
	when rc_code like '%RECENT%' then 'mw_recent'
	when rc_code like '%REO%' then 'mw_main'
	when rc_code like '%RECO%' then 'reco'
	when rc_code like '%ProductDetail_m_product%' then 'mw_detail'
	when rc_code like '%BasketView_m_product%' then 'mw_basket'
	when rc_code like '%Search_m_product%' then 'mw_search' 
	when rc_code like '%TODAY%' then 'mw_main' 
	else rc_code
	end as rc_code_map
from
(select distinct rc_code from tb_view_order_24h);


--order ct
-- 1) 
select t1.is_rec, count(*) order_count, avg(t2.order_price) as avg_order_price, avg(t2.order_item_count::float) as avg_item_count  from
(select order_id, max(case when rc_code is not null then 1 else 0 end) as is_rec from tb_view_order_24h
where order_id is not null
group by order_id) t1
join tb_order_stat t2 on t2.order_id = t1.order_id
group by t1.is_rec;

drop table tb_order_rc_code;
create table tb_order_rc_code as
select order_id, rc_code_map
from 
	(select order_id, rc_code_map, count(*) over (partition by order_id)
	from
		(select order_id, rc_code_map
		from tb_view_order_24h t1
		join tb_rc_code_map t2 on t2.rc_code = t1.rc_code
		where order_id is not null and t1.rc_code is not null
		group by order_id, rc_code_map) 
	)
where count = 1;

-- 2)
select rc_code_map, count(*) order_count, avg(t2.order_price) as avg_order_price, avg(t2.order_item_count::float) as avg_item_count
from tb_order_rc_code t1
join tb_order_stat t2 on t1.order_id = t2.order_id
group by t1.rc_code_map;

--product ct
select is_rec, count(*) freq , avg(price)
from 
(select item_id, pc_id, max(case when is_rec  = 1 then 1 else 0 end) is_rec, avg(price) as price
from
(select case when rc_code is not null then 1 else 0 end as is_rec, item_id, pc_id, price from tb_order_item_within_24h)
group by 1,2)
group by is_rec;






--order cv
-- 3)
select is_rec, sum(view_count), sum(order_count) from
(select is_rec, item_id, count(distinct session_id) as view_count, count(distinct case when order_id is not null then session_id else null end) as order_count
from
(select pc_id, session_id, is_rec, item_id, order_id
	from
	(select pc_id, session_id, case when rc_code is null then 0 else 1 end as is_rec, item_id, order_id
	from tb_view_order_24h)
group by pc_id, session_id, is_rec, item_id, order_id)
group by is_rec, item_id)
group by is_rec;

-- 4)
select rc_code_map, sum(view_count), sum(order_count) from
(select rc_code_map, item_id, count(distinct session_id) as view_count, count(distinct case when order_id is not null then session_id else null end) as order_count
from
(select pc_id, session_id, rc_code_map, item_id, order_id
	from
	(select pc_id, session_id, rc_code_map, item_id, order_id
	from tb_view_order_24h t1
	join tb_rc_code_map t2 on t2.rc_code = t1.rc_code)
group by pc_id, session_id, rc_code_map, item_id, order_id)
group by rc_code_map, item_id)
group by rc_code_map;
