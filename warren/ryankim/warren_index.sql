
--view
drop table if exists tb_log_view;
Create table tb_log_view (	
cuid varchar(200),
type varchar(200),
serverTime timestamp,
ipAddress varchar(200),
userAgent varchar(2000),
apiVersion varchar(200),
domain varchar(200),
device varchar(200),
pcId varchar(200),
sessionId varchar(200),
adId varchar(200),
pushId varchar(200),
gcId varchar(200),
userId varchar(1000),
abGroup varchar(200),
rcCode varchar(200),
adCode varchar(200),
locale varchar(200),
itemId varchar(200),
searchTerm varchar(200)
 );
truncate tb_log_view;


copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/09/30'

CREDENTIALS 'aws_access_key_id=AKIAJ45BXFSKLTAEQE7A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
MAXERROR 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/view/2015/10'

CREDENTIALS 'aws_access_key_id=AKIAJ45BXFSKLTAEQE7A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
MAXERROR 100000
timeformat 'auto'
region 'ap-northeast-1';


--order resource
drop table if exists tb_log_order;
Create table tb_log_order (
cuid varchar(200),
type varchar(200),
serverTime timestamp,
ipAddress varchar(200),
userAgent varchar(2000),
apiVersion varchar(200),
domain varchar(200),
device varchar(200),
pcId varchar(200),
sessionId varchar(200),
adId varchar(200),
pushId varchar(200),
gcId varchar(200),
userId varchar(200),
abGroup varchar(200),
rcCode varchar(200),
adCode varchar(200),
locale varchar(200),
itemId varchar(200),
searchTerm varchar(200),
orderId varchar(200),
orderPrice bigint,
price bigint,
quantity bigint
 );
truncate tb_log_order;

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/09/30'
CREDENTIALS 'aws_access_key_id=AKIAJ45BXFSKLTAEQE7A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
MAXERROR 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/fdd29847-94cd-480d-a0d9-16144485d58b/order/2015/10'
CREDENTIALS 'aws_access_key_id=AKIAJ45BXFSKLTAEQE7A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
MAXERROR 100000
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';


-- product info 
drop table if exists tmp_product_info;
Create table tmp_product_info (
cuid varchar(200),
type varchar(200),
item_id varchar(200),
item_name varchar(2000),
item_image varchar(200),
item_url varchar(2000),
original_price int,
sale_price int,
category1 varchar(200),
category2 varchar(200),
category3 varchar(200),
category4 varchar(200),
category5 varchar(200),
reg_date varchar(200),
update_date timestamp,
expire_date timestamp,
stock int,
state varchar(200),
description varchar(200),
extra_image varchar(200),
locale varchar(200)
 );
truncate tmp_product_info;


copy tmp_product_info
from 's3://rb-logs-apne1/75d4d21b-82c4-83ba-6727-14a0aad6d560/product/2015/09/30'
CREDENTIALS 'aws_access_key_id=AKIAJ45BXFSKLTAEQE7A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
MAXERROR 100000
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';


copy tmp_product_info
from 's3://rb-logs-apne1/75d4d21b-82c4-83ba-6727-14a0aad6d560/product/2015/10/'
CREDENTIALS 'aws_access_key_id=AKIAJ45BXFSKLTAEQE7A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
MAXERROR 100000
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

drop table if exists tb_product_info;
CREATE TABLE tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0
 group by item_id;

drop table if exists tb_log_view_report;
CREATE TABLE tb_log_view_report as
select
	servertime,
	extract(year from convert_timezone('KST', servertime)) as year, 
	extract(month from convert_timezone('KST', servertime)) as month, 
	
	extract(day from convert_timezone('KST', servertime)) as day, 
	extract(hour from convert_timezone('KST', servertime)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	useragent,
	pcid, 
	sessionid, 
	gcid, 
	rccode,
	itemid, 
	searchterm
from 
tb_log_view;

drop table if exists tb_log_order_report;
CREATE TABLE tb_log_order_report as
select
	servertime,
	extract(year from convert_timezone('KST', servertime)) as year, 
	extract(month from convert_timezone('KST', servertime)) as month, 
	
	extract(day from convert_timezone('KST', servertime)) as day, 
	extract(hour from convert_timezone('KST', servertime)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	useragent,
	pcid, 
	sessionid, 
	gcid, 
	rccode,
	itemid, 
	searchterm, 
	orderid, 
	orderprice,
	price, 
	quantity
from 
tb_log_order;

drop table if exists tb_product_info_report;
CREATE TABLE tb_product_info_report as
select
	item_id, 
	item_name, 
	original_price, 
	sale_price, 
	category1, 
	category2, 
	category3, 
	category4, 
	category5, 
	reg_date
	stock
from 
tb_product_info;


drop table if exists tb_crm_kpi_view_s1;
create table tb_crm_kpi_view_s1 as
select
	A.pcid
	,A.sessionid
	,count(*) as view
	,sum(case when A.rccode is not null then 1 else 0 end) as reco_view
	,count(distinct A.itemid) as itemvar
	,count(A.itemid) as itemnum
--	,count(distinct B.category1) as cat1var
--	,count(distinct B.category2) as cat2var
	,datediff(second,min(servertime),max(servertime)) as period

from tb_log_view_report A
--inner join tb_product_info B on A.itemid = B.item_id
group by A.pcid, A.sessionid;




drop table if exists tb_crm_kpi_rcratio_itemnum_s1;
create table tb_crm_kpi_rcratio_itemnum_s1 as
select
	round(reco_view::real/view::real,1) as rcratio
	,reco_view
	,view
	,itemnum
	,itemvar
	--,cat1var
	,period
	
from tb_crm_kpi_view_s1;

drop table if exists tb_crm_kpi_rcratio_itemnum;
create table tb_crm_kpi_rcratio_itemnum as
select
	rcratio


	,round(avg(itemnum::real),2) avg_itemnum
	,round(avg(itemvar::real),2) avg_itemvar
	--,round(avg(cat1var::real),2) avg_cat1var
	,round(avg(period::real),2) avg_period
	,count(*) count
	
	,round(avg(view::real),2) avg_view
from tb_crm_kpi_rcratio_itemnum_s1
where view=10
group by rcratio
having count(*) > 100;

-- view period 
select * from tb_crm_kpi_rcratio_itemnum order by rcratio;


--##########################################################################################
--view and order
--order (frequency, quantity, revenue)
drop table if exists tb_crm_kpi_order_s1;
create table tb_crm_kpi_order_s1 as
	SELECT 
        pcid,
		sessionid,
		
		sum(quantity) quantity,
        sum(price * quantity) revenue,
		count(distinct itemid) buyitemnum,
		count(distinct orderid) ordercount
    FROM
        tb_log_order_report
    GROUP BY pcid, sessionid;


drop table if exists tb_crm_kpi_conversion_s1;
create table tb_crm_kpi_conversion_s1 as
select 
	A.sessionid
	,A.view view_count
	,A.reco_view reco_view_count
	,A.itemvar view_var
	,A.itemnum view_itemnum
	--,A.cat1var view_catvar
	,A.period view_period
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count

from tb_crm_kpi_view_s1 A
left join tb_crm_kpi_order_s1 B on A.sessionid = B.sessionid;

select
view_count
,session_count
,order_count
,order_revenue
,round(order_count::real/session_count::real, 2) cvr
,round(order_revenue::real/order_count::real,2) prevenue
from
(
select 
view_count 
,count(*) session_count
,sum(case when order_count is not null then order_count end) order_count
,sum(case when order_revenue is not null then order_revenue end) order_revenue
from tb_crm_kpi_conversion_s1 
where reco_view_count =0
group by view_count
)
order by session_count desc;


select
reco_view_count
,session_count
,order_count
,order_revenue
,round(order_count::real/session_count::real, 2) cvr
,round(order_revenue::real/order_count::real,2) prevenue
from
(
select 
reco_view_count
,count(*) session_count
,sum(case when order_count is not null then order_count end) order_count
,sum(case when order_revenue is not null then order_revenue end) order_revenue
from tb_crm_kpi_conversion_s1 

group by reco_view_count
)
order by session_count desc;

select * from tb_crm_kpi_conversion_s1 limit 100;

drop table if exists tb_crm_kpi_conversion_s2;
create table tb_crm_kpi_conversion_s2 as
select 
	round(reco_view_count::real/view_count::real,1) rcratio
	,case when order_revenue is not null then 1 else 0 end as conversion
	
	,order_quantity
	,order_revenue
	,order_itemnum
	
	,view_count
	,view_period

from tb_crm_kpi_conversion_s1
where view_count=10;
	
select * from tb_crm_kpi_conversion_s2 limit 100;

drop table if exists tb_crm_kpi_conversion_s3;
create table tb_crm_kpi_conversion_s3 as
select 
	rcratio	
	,round(count(case when conversion = 1 then conversion end)::real/ count(*)::real, 4) as cvr
	,round(avg(order_quantity::real),2) avg_quantity
	,round(avg(order_revenue::real),2) order_revenue
	,round(avg(order_itemnum::real),2) order_itemnum
	,count(*) count
	
	,round(avg(view_count::real),2) avg_view_count
	,round(avg(view_period::real),2) avg_view_period
	
from tb_crm_kpi_conversion_s2
group by rcratio
having count(*)>100;

select * from tb_crm_kpi_conversion_s3 order by rcratio limit 100;







