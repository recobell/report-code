--########################refine
-- view data filtering
drop table if exists tb_log_direct_rec_s1;
create table tb_log_direct_rec_s1 as
select
	pc_id
	,item_id
	,user_id
	,device
	,convert_timezone('KST',server_time) server_time
	,rc_code
from tb_log_view
where extract(month from convert_timezone('KST',server_time)) = 01;

-- order data filtering
drop table if exists tb_log_direct_rec_s2;
create table tb_log_direct_rec_s2 as
select
	convert_timezone('KST',server_time) server_time
	,pc_id
	,device
	,user_id
	,order_id
	,order_price
	,item_id
	,price
	,quantity
from tb_log_order
where extract(month from convert_timezone('KST',server_time)) = 01;

drop table if exists tb_order_id;
create table tb_order_id as
select
	order_id
	,sum(price * quantity) revenue
from tb_log_order
where extract(month from convert_timezone('KST',server_time)) = 01
group by 1;



--########################direct order table by product level
--click item -> order item  set
drop table if exists tb_log_direct_rec_s3;
create table tb_log_direct_rec_s3 as
select 
	a.pc_id
	,a.rc_code
	,a.item_id
	,a.server_time view_time
	,b.server_time order_time
	,b.order_id
	,b.order_price
	,b.price
	,b.quantity
from tb_log_direct_rec_s1 a
left join tb_log_direct_rec_s2 b on a.pc_id = b.pc_id and a.item_id = b.item_id
where datediff (minute, a.server_time, b.server_time )<1440 and datediff(minute, a.server_time, b.server_time ) > 0
order by a.pc_id, a.item_id, a.server_time;


--########################customer transaction
--verify reco order id
drop table if exists tb_log_direct_rec_ct_s1;
create table tb_log_direct_rec_ct_s1 as
select 
	pc_id
	,order_id
	,rc_code
	,rank() over (partition by pc_id, order_id order by view_time desc) as rank
from tb_log_direct_rec_s3
where rc_code is not null;

drop table if exists tb_log_direct_rec_ct_s2;
create table tb_log_direct_rec_ct_s2 as
select 
	rc_code
	,pc_id
	,order_id
from tb_log_direct_rec_ct_s1
where rank = 1;


drop table if exists tb_log_direct_rec_ct_s3;
create table tb_log_direct_rec_ct_s3 as
select 
	a.pc_id
	,a.order_id
	,max(b.rc_code) rc_code
	
from tb_log_direct_rec_s3 a
left join tb_log_direct_rec_ct_s2 b on a.pc_id = b.pc_id and a.order_id = b.order_id
group by 1,2;


--########################customer transaction output
--추천 직접 매출 -> 객단가/구매건수
drop table if exists tb_log_ct_rec_category;
create table tb_log_ct_rec_category as
select 
	a.rc_code
	, count(*) freq 
	, avg(b.revenue) ct
from tb_log_direct_rec_ct_s3 a
inner join tb_order_id b on a.order_id = b.order_id
group by 1;

drop table if exists tb_log_ct_rec_all;
create table tb_log_ct_rec_all as
select 
	(case when a.rc_code is not null then 1 else 0 end) as rc_code
	, count(*) freq 
	, avg(b.revenue) ct
from tb_log_direct_rec_ct_s3 a
inner join tb_order_id b on a.order_id = b.order_id
group by 1;


--롯데면세점 추천 직접 매출 -> 객단가/구매건수
drop table if exists tb_log_ct_rec_lotte;
create table tb_log_ct_rec_lotte as
select 
	(case when rc_code like '%ProductDetail_product%' then 'pw_detail'
	when rc_code like '%BasketView_product%' then 'pw_basket'
	when rc_code like '%Search_product%' then 'pw_search' 
	when rc_code like '%RECENT%' then 'mw_recent'
	when rc_code like '%REO%' then 'mw_main'
	when rc_code like '%RECO%' then 'reco'
	when rc_code like '%ProductDetail_m_product%' then 'mw_detail'
	when rc_code like '%BasketView_m_product%' then 'mw_basket'
	when rc_code like '%Search_m_product%' then 'mw_search' 
	when rc_code like '%TODAY%' then 'mw_main' 
	else rc_code
	end) rc_code
	, count(*) freq 
	, avg(b.revenue) ct
from tb_log_direct_rec_ct_s3 a
inner join tb_order_id b on a.order_id = b.order_id
group by 1;



--########################conversion ratio all
drop table if exists tb_log_direct_rec_cv_s1;
create table tb_log_direct_rec_cv_s1 as
select 
	pc_id
	,item_id
	,avg(price) price
	,max(case when rc_code is not null then 1 else 0 end) as rc_code
from tb_log_direct_rec_s3
group by 1,2;

drop table if exists tb_log_direct_rec_cv_s2;
create table tb_log_direct_rec_cv_s2 as
select 
	
	pc_id
	,item_id
	,case when rc_code is not null then 1 else 0 end as rc_code
from tb_log_direct_rec_s1
group by 1,2,3;

drop table if exists tb_log_direct_rec_cv_s3;
create table tb_log_direct_rec_cv_s3 as
select 
	a.rc_code
	,a.item_id
	,sum(case when b.pc_id is not null then 1 else 0 end)::float/sum(case when a.pc_id is not null then 1 else 0 end)::float cv
	,sum(case when b.pc_id is not null then 1 else 0 end) order_item
	,sum(case when a.pc_id is not null then 1 else 0 end) view_item
	,avg(case when b.price is not null then b.price else null end) avg_price
from tb_log_direct_rec_cv_s2 a
left join tb_log_direct_rec_cv_s1 b on a.rc_code = b.rc_code and a.item_id= b.item_id and a.pc_id = b.pc_id
group by 1,2;

drop table if exists tb_log_direct_rec_cv;
create table tb_log_direct_rec_cv as
select 
	rc_code
	, sum(order_item)::real/sum(view_item)::real cv
	, sum(avg_price * order_item)/sum(order_item) avg_price  
	, count(*) count
from tb_log_direct_rec_cv_s3
where order_item >0
group by rc_code;




--########################item price
drop table if exists tb_item_price;
create table tb_item_price as
select
	item_id
	,avg(price) price
from tb_log_order
group by 1;


--########################conversion ratio category
drop table if exists tb_log_direct_rec_cv_category_s1;
create table tb_log_direct_rec_cv_category_s1 as
select 
	pc_id
	,item_id
	,quantity
	,case when rc_code is null then '' else rc_code end as rc_code
	,rank() over (partition by pc_id, item_id order by view_time desc) rank
from tb_log_direct_rec_s3
where rc_code is not null;

drop table if exists tb_log_direct_rec_cv_category_s2;
create table tb_log_direct_rec_cv_category_s2 as
select 
	pc_id
	,item_id
	,quantity
	,rc_code
from tb_log_direct_rec_cv_category_s1
where rank = 1;

drop table if exists tb_log_direct_rec_cv_category_s3;
create table tb_log_direct_rec_cv_category_s3 as
select 
	pc_id
	,item_id
	,quantity
	,case when rc_code is null then '' else rc_code end as rc_code
	,rank() over (partition by pc_id, item_id order by view_time desc) rank
from tb_log_direct_rec_s3
where rc_code is null;

drop table if exists tb_log_direct_rec_cv_category_s4;
create table tb_log_direct_rec_cv_category_s4 as
select 
	pc_id
	,item_id
	,quantity
	,rc_code
from tb_log_direct_rec_cv_category_s3
where rank = 1;

drop table if exists tb_log_direct_rec_cv_category_s5;
create table tb_log_direct_rec_cv_category_s5 as
select
	rc_code
	,item_id 
	,sum(quantity) quantity
	,count(distinct pc_id) order_count
from tb_log_direct_rec_cv_category_s2
group by 1,2;

drop table if exists tb_log_direct_rec_cv_category_s6;
create table tb_log_direct_rec_cv_category_s6 as
select
	rc_code
	,item_id 
	,sum(quantity) quantity
	,count(distinct pc_id) order_count
from tb_log_direct_rec_cv_category_s4
group by 1,2;

--view
drop table if exists tb_log_direct_rec_cv_category_s7;
create table tb_log_direct_rec_cv_category_s7 as
select 	
	pc_id
	,item_id
	,case when rc_code is null then '' else rc_code end as rc_code
from tb_log_cv_rec_s1
group by 1,2,3;

drop table if exists tb_log_direct_rec_cv_category_s8;
create table tb_log_direct_rec_cv_category_s8 as
select
	rc_code
	,item_id
	,count(distinct pc_id) view_count
from tb_log_direct_rec_cv_category_s7
group by 1,2;

--calculate cv 
drop table if exists tb_log_direct_rec_cv_category_s9;
create table tb_log_direct_rec_cv_category_s9 as
select 
	a.rc_code
	,a.item_id
	,b.order_count reco_order
	,c.order_count no_reco_order
	,a.view_count view_count
	,case when a.rc_code <> '' then b.order_count::real/a.view_count::real
		else c.order_count::real/a.view_count::real end as cv
	,case when a.rc_code <> '' then b.quantity
		else c.quantity end as quantity
	
from tb_log_direct_rec_cv_category_s8 a
left join tb_log_direct_rec_cv_category_s5 b on a.rc_code = b.rc_code and a.item_id = b.item_id 
left join tb_log_direct_rec_cv_category_s6 c on a.rc_code = c.rc_code and a.item_id = c.item_id;



drop table if exists tb_log_direct_rec_cv_category_s10;
create table tb_log_direct_rec_cv_category_s10 as
select 
	a.*
	,b.price price
from tb_log_direct_rec_cv_category_s9 a
left join tb_item_price b on a.item_id = b.item_id; 

drop table if exists tb_log_direct_rec_cv_category;
create table tb_log_direct_rec_cv_category as
select 
	rc_code
	, sum(case when rc_code <> '' then reco_order::real else no_reco_order end)/sum(view_count::real) cv
	, count(*) count
	, sum(price*quantity)/sum(quantity) price
from tb_log_direct_rec_cv_category_s10
where no_reco_order is not null or reco_order is not null
group by 1;


drop table if exists tb_log_direct_rec_cv_category_lotte;
create table tb_log_direct_rec_cv_category_lotte as
select 
	(case when rc_code like '%ProductDetail_product%' then 'pw_detail'
	when rc_code like '%BasketView_product%' then 'pw_basket'
	when rc_code like '%Search_product%' then 'pw_search' 
	when rc_code like '%RECENT%' then 'mw_recent'
	when rc_code like '%REO%' then 'mw_main'
	when rc_code like '%RECO%' then 'reco'
	when rc_code like '%ProductDetail_m_product%' then 'mw_detail'
	when rc_code like '%BasketView_m_product%' then 'mw_basket'
	when rc_code like '%Search_m_product%' then 'mw_search' 
	when rc_code like '%TODAY%' then 'mw_main' 
	else rc_code
	end) rc_code
	, sum(case when rc_code <> '' then reco_order::real else no_reco_order end)/sum(view_count::real) cv
	, sum(price*quantity)/sum(quantity) price
	, count(*) count
	
from tb_log_direct_rec_cv_category_s10
where no_reco_order is not null or reco_order is not null
group by 1;

--test
--customer tranction
select * from tb_log_ct_rec_category;
select * from tb_log_ct_rec_all;
select * from tb_log_ct_rec_lotte;

--conversion rate
select * from tb_log_direct_rec_cv;
select * from tb_log_direct_rec_cv_category order by cv desc;
select * from tb_log_direct_rec_cv_category_lotte;