select 
(case when last_rc_code like '%ProductDetail_product%' then 'pw_detail'
	when last_rc_code like '%BasketView_product%' then 'pw_basket'
	when last_rc_code like '%Search_product%' then 'pw_search' 
	when last_rc_code like '%REO%' or last_rc_code like '%RECENT%' or (last_rc_code like '%RECO%' and device = 'PW') then 'pw_main'
	when last_rc_code like '%ProductDetail_m_product%' then 'mw_detail'
	when last_rc_code like '%BasketView_m_product%' then 'mw_basket'
	when last_rc_code like '%Search_m_product%' then 'mw_search' 
	when last_rc_code like '%TODAY%' or (last_rc_code like '%RECO%' and device <> 'PW') then 'mw_main' 
	else last_rc_code
	end) rc_code,
sum(order_revenue)  
from tb_session_funnel 
where reco_view_count > 0
group by 1
order by 1;

select sum(reco_revenue) from tb_recommend_result_revenue_monthly;
select sum(reco_revenue) from tmp_recommend_result_order_session_monthly;

select sum(reco_revenue) from tb_recommend_result_revenue_daily;
select sum(reco_revenue) from tmp_recommend_result_order_session_daily;



select (case when rc_code like '%ProductDetail_product%' then 'pw_detail'
	when rc_code like '%BasketView_product%' then 'pw_basket'
	when rc_code like '%Search_product%' then 'pw_search' 
	when rc_code like '%REO%' or rc_code like '%RECENT%' or (rc_code like '%RECO%' and device = 'PW')  then 'pw_main'
	when rc_code like '%ProductDetail_m_product%' then 'mw_detail'
	when rc_code like '%BasketView_m_product%' then 'mw_basket'
	when rc_code like '%Search_m_product%' then 'mw_search' 
	when rc_code like '%TODAY%' or (rc_code like '%RECO%' and device <> 'PW' ) then 'mw_main' 
	else rc_code
	end) rc_code, device, COUNT(*)

from tb_log_view 
group by 1,2
order by 1,2;

select rc_code, device, count(*)
from tb_log_view
where rc_code like '%RECO%'
group by 1,2
order by 1,2;


select * from tb_session_funnel where reco_view_count

select
	sum(A.revenue) revenue
	,sum(A.reco_revenue) reco_revenue    
	,sum(A.no_reco_revenue) no_reco_revenue 
    ,round( sum(A.reco_revenue) :: real / sum(A.revenue) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_order_session A;




select session_id from tb_crm_kpi_view_s2 group by 1 having count(*)>1;
select session_id from tb_crm_kpi_order_s1 group by 1 having count(*)>1;
select session_id from tb_reco_revenue group by 1 having count(*)>1;
select session_id from tb_log_view_first_four group by 1 having count(*)>1;
select session_id from tb_log_session_pc_id group by 1 having count(*)>1;
