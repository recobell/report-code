
drop table if exists tb_log_view;
create table tb_log_view (	
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(1000),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
item_id varchar(200),
search_term varchar(200)
);

drop table if exists tb_log_search;
create table tb_log_search (
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(200),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
search_term varchar(200),
search_result varchar(200)
);



drop table if exists tb_log_order;
create table tb_log_order (
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(200),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
item_id varchar(200),
search_term varchar(200),
order_id varchar(200),
order_price float,
price float,
quantity bigint
);

drop table if exists tmp_product_info;
create table tmp_product_info (
cuid varchar(200),
type varchar(200),
item_id varchar(200),
item_name varchar(2000),
item_image varchar(200),
item_url varchar(2000),
original_price float,
sale_price float,
category1 varchar(200),
category2 varchar(200),
category3 varchar(200),
category4 varchar(200),
category5 varchar(200),
reg_date varchar(200),
update_date timestamp,
expire_date timestamp,
stock int,
state varchar(200),
description varchar(200),
extra_image varchar(200),
locale varchar(200)
);


copy tb_log_view
from 's3://rb-logs-apne1/3a825a40-8183-43e6-afd5-3303a18c6d63/view/2015/12/'
CREDENTIALS 'aws_access_key_id=AKIAINS5BEHVPHMF3KKQ;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://rb-rec-report-conf/jsonpaths/tb_log_view.jsonpaths'
compupdate ON
STATUPDATE OFF
MAXERROR 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_search
from 's3://rb-logs-apne1/3a825a40-8183-43e6-afd5-3303a18c6d63/search/2015/12/'
CREDENTIALS 'aws_access_key_id=AKIAINS5BEHVPHMF3KKQ;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
MAXERROR 100000
FORMAT AS JSON  's3://rb-rec-report-conf/jsonpaths/tb_log_search.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/3a825a40-8183-43e6-afd5-3303a18c6d63/order/2015/12/'
CREDENTIALS 'aws_access_key_id=AKIAINS5BEHVPHMF3KKQ;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://rb-rec-report-conf/jsonpaths/tb_log_order.jsonpaths'
compupdate ON
STATUPDATE OFF
MAXERROR 100000
timeformat 'auto'
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/3a825a40-8183-43e6-afd5-3303a18c6d63/product/2015/12/'
CREDENTIALS 'aws_access_key_id=AKIAINS5BEHVPHMF3KKQ;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
MAXERROR 100000
FORMAT AS JSON  's3://rb-rec-report-conf/jsonpaths/tmp_product_info.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';



drop table if exists tb_product_info;
CREATE TABLE tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0
 group by item_id;

drop table if exists tb_log_view_report;
CREATE TABLE tb_log_view_report as
select
	server_time,
	extract(year from convert_timezone('KST', server_time)) as year, 
	extract(month from convert_timezone('KST', server_time)) as month, 
	
	extract(day from convert_timezone('KST', server_time)) as day, 
	extract(hour from convert_timezone('KST', server_time)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	ad_code,
	item_id, 
	search_term
from 
tb_log_view
where extract(month from convert_timezone('KST', server_time))=12;



drop table if exists tb_log_order_report;
CREATE TABLE tb_log_order_report as
select
	server_time,
	extract(year from convert_timezone('KST', server_time)) as year, 
	extract(month from convert_timezone('KST', server_time)) as month, 
	
	extract(day from convert_timezone('KST', server_time)) as day, 
	extract(hour from convert_timezone('KST', server_time)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term, 
	order_id, 
	order_price,
	price, 
	quantity
from 
tb_log_order
where extract(month from convert_timezone('KST', server_time))=12;

--####################################################################################################
--session/pcid dimension daily/monthly
--####################################################################################################

drop table if exists tmp_recommend_result_view_daily;
CREATE TABLE tmp_recommend_result_view_daily as
select 
	year
	,month
	,day
	,pc_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null  then 1 else 0 end) as reco_view
	,sum(case when rc_code is null  then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by year,month,day , pc_id, device;

drop table if exists tmp_recommend_result_view_session_daily;
CREATE TABLE tmp_recommend_result_view_session_daily as
select
	year
	,month
	,day
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
	,sum(case when ad_code is not null then 1 else 0 end) as ad_view
	
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) as first_click
	,max(ad_code) as ad_code
from tb_log_view_report
group by year,month,day,  session_id, device;

drop table if exists tmp_recommend_result_order_session_daily;
CREATE TABLE tmp_recommend_result_order_session_daily as
select
	A.year
	,A.month
	,A.day
	,A.session_id 
	,A.device
	
	,B.first_click
	,max(B.ad_code) ad_code
	
	,sum(A.price * A.quantity) revenue

	,sum(case when B.reco_view > 0 then A.price * A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 then A.price * A.quantity else 0 end) no_reco_revenue

	,sum(case when B.ad_view > 0 then A.price * A.quantity else 0 end) ad_revenue
	,sum(distinct case when B.ad_view > 0 then A.order_price else 0 end) ad_revenue2
	
	,sum(case when B.session_id is not null then A.price * A.quantity else 0 end) click_revenue
	,sum(case when B.session_id is null then A.price * A.quantity else 0 end) no_click_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 then A.order_price else 0 end) no_reco_actual_payment

	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 then A.order_id end) no_reco_frequency

	,count(distinct case when B.session_id is not null then A.order_id end) session_frequency
	,count(distinct case when B.session_id is null then A.order_id end) no_session_frequency
	
from tb_log_order_report A
left join tmp_recommend_result_view_session B on A.session_id = B.session_id and a.year = b.year and a.month = b.month and a.day = b.day
group by A.year,A.month,A.day,A.session_id,A.device, B.first_click;


drop table if exists tmp_recommend_result_view_monthly;
CREATE TABLE tmp_recommend_result_view_monthly as
select 
	year
	,month
	,pc_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null  then 1 else 0 end) as reco_view
	,sum(case when rc_code is null  then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by year,month, pc_id, device;

drop table if exists tmp_recommend_result_view_session_monthly;
CREATE TABLE tmp_recommend_result_view_session_monthly as
select
	year
	,month
	,session_id 
	,device
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
	,sum(case when ad_code is not null then 1 else 0 end) as ad_view
	
	,datediff(second,min(server_time),max(server_time)) as period
	,min(server_time) as first_click
	,max(ad_code) as ad_code
from tb_log_view_report
group by year,month,  session_id, device;

drop table if exists tmp_recommend_result_order_session_monthly;
CREATE TABLE tmp_recommend_result_order_session_monthly as
select
	A.year
	,A.month
	,A.session_id 
	,A.device
	
	,B.first_click
	,max(B.ad_code) ad_code
	
	,sum(A.price * A.quantity) revenue

	,sum(case when B.reco_view > 0 then A.price * A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 then A.price * A.quantity else 0 end) no_reco_revenue

	,sum(case when B.ad_view > 0 then A.price * A.quantity else 0 end) ad_revenue
	,sum(distinct case when B.ad_view > 0 then A.order_price else 0 end) ad_revenue2
	
	,sum(case when B.session_id is not null then A.price * A.quantity else 0 end) click_revenue
	,sum(case when B.session_id is null then A.price * A.quantity else 0 end) no_click_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 then A.order_price else 0 end) no_reco_actual_payment

	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 then A.order_id end) no_reco_frequency

	,count(distinct case when B.session_id is not null then A.order_id end) session_frequency
	,count(distinct case when B.session_id is null then A.order_id end) no_session_frequency
	
from tb_log_order_report A
left join tmp_recommend_result_view_session B on A.session_id = B.session_id and a.year = b.year and a.month = b.month
group by A.year,A.month,A.session_id,A.device, B.first_click;

--####################################################################################################
--segment
--####################################################################################################


drop table if exists tb_recommend_reco_group_daily;
CREATE TABLE tb_recommend_reco_group_daily AS 
SELECT * FROM
    tmp_recommend_result_view_session_daily
where reco_view > 0;
	
drop table if exists tb_recommend_reco_group_monthly;
CREATE TABLE tb_recommend_reco_group_monthly AS 
SELECT * FROM
    tmp_recommend_result_view_session_monthly
where reco_view > 0;

--####################################################################################################
--metric : uv, visit, pv, frequency, revenue, prevenue, pdiscount, quantity, ...
--####################################################################################################

--********
--daily
--********
--UV check
drop table if exists tb_recommend_result_uv_daily;
CREATE TABLE tb_recommend_result_uv_daily AS
select
	year
	,month
	,day
	,device
	,count(distinct pc_id) uv
	,count(distinct case when reco_view > 0 then pc_id end) reco_uv
	,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
	,round( count(distinct case when reco_view > 0 then pc_id end) :: real / count(distinct pc_id) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_view_daily
group by 1,2,3,4
order by 1,2,3,4;

--visit
drop table if exists tb_recommend_result_visit_daily;
CREATE TABLE tb_recommend_result_visit_daily AS
select
	year
	,month
	,day
	,device
	,count(distinct session_id) visit
	,count(distinct case when reco_view > 0 then session_id end) reco_visit  
	,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,round(count(distinct case when reco_view > 0 then session_id end) :: real / count(distinct session_id) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_view_session_daily
group by 1,2,3,4
order by 1,2,3,4;

--detail view click
drop table if exists tb_recommend_result_pv_daily;
CREATE TABLE tb_recommend_result_pv_daily AS
select
	year
	,month
	,day
	,device
	,sum(view) pv
	,sum(case when reco_view > 0 then view else 0 end) reco_pv
	,sum(case when reco_view = 0 then view else 0 end) no_reco_pv
	,round( sum(case when reco_view > 0 then view else 0 end) :: real / sum(view) :: real , 4)*100 reco_ratio
	
from tmp_recommend_result_view_session_daily
group by 1,2,3,4
order by 1,2,3,4;


-- frequency
drop table if exists tb_recommend_result_frequency_daily;
CREATE TABLE tb_recommend_result_frequency_daily AS
select
	A.year
	,A.month
	,A.day
	,A.device
	,sum(A.frequency) frequency
	,sum(A.reco_frequency) reco_frequency 
	,sum(A.no_reco_frequency) no_reco_frequency 
	,round( sum(A.reco_frequency) :: real / sum(A.frequency) :: real , 4)*100 reco_ratio
    
from tmp_recommend_result_order_session_daily A
group by 1,2,3,4
order by 1,2,3,4;

-- revenue
drop table if exists tb_recommend_result_revenue_daily;
CREATE TABLE tb_recommend_result_revenue_daily AS
select
	A.year
	,A.month
	,A.day
	,A.device
	,sum(A.revenue) revenue
	,sum(A.reco_revenue) reco_revenue    
	,sum(A.no_reco_revenue) no_reco_revenue 
    ,round( sum(A.reco_revenue) :: real / sum(A.revenue) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_order_session_daily A
group by 1,2,3,4
order by 1,2,3,4;


drop table if exists tb_recommend_result_prevenue_daily_s1;
CREATE TABLE tb_recommend_result_prevenue_daily_s1 AS
select
	A.year
	,A.month
	,A.day
	,A.device
	,sum(A.revenue)/sum(A.frequency) prevenue
    ,sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end) reco_prevenue
    ,sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end) no_reco_prevenue
from tmp_recommend_result_order_session_daily A
left join tb_recommend_reco_group_daily C on A.year = C.year and A.month = C.month and A.day = C.day and A.session_id = C.session_id
group by 1,2,3,4
order by 1,2,3,4;

drop table if exists tb_recommend_result_prevenue_daily;
CREATE TABLE tb_recommend_result_prevenue_daily AS SELECT *,
    round(reco_prevenue :: real / no_reco_prevenue :: real, 4)*100 as reco_up_ratio
FROM tb_recommend_result_prevenue_daily_s1;

--quantity (구매당 아이템)
drop table if exists tb_recommend_result_quantity_daily_s1;
CREATE TABLE tb_recommend_result_quantity_daily_s1 AS
select
	A.year, A.month,A.day, A.device
	,round(avg(A.quantity :: real),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: real end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: real end),3) no_reco_quantity
from tmp_recommend_result_order_session_daily A
left join tb_recommend_reco_group_daily C on A.year = C.year and A.month = C.month and A.day = C.day and A.session_id = C.session_id
group by 1,2,3,4
order by 1,2,3,4;

drop table if exists tb_recommend_result_quantity_daily;
CREATE TABLE tb_recommend_result_quantity_daily AS SELECT *,
    round(reco_quantity :: real / no_reco_quantity :: real, 4)*100 as reco_up_ratio
FROM tb_recommend_result_quantity_daily_s1;



--pclick data 
drop table if exists tb_recommend_result_click_daily_s1;
CREATE TABLE tb_recommend_result_click_daily_s1 AS 
SELECT 
	year, month,day, device
	,round(AVG(view :: real),3) click
    ,round(AVG(case when reco_view>0 then view :: real end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: real end),3) no_reco_click
    
FROM
    tmp_recommend_result_view_session_daily
group by 1,2,3,4
order by 1,2,3,4;



drop table if exists tb_recommend_result_click_daily;
CREATE TABLE tb_recommend_result_click_daily AS SELECT *,
    round(reco_click / no_reco_click, 4)*100 as reco_up_ratio
FROM tb_recommend_result_click_daily_s1;   

--period
drop table if exists tb_recommend_result_period_daily_s1;
CREATE TABLE tb_recommend_result_period_daily_s1 AS 
SELECT 
	year, month,day,  device
	,round(AVG(period :: real),3) period
    ,round(AVG(case when reco_view>0 then period :: real end),3) reco_period
    
FROM
    tmp_recommend_result_view_session_daily
group by 1,2,3,4
order by 1,2,3,4;


drop table if exists tb_recommend_result_period_daily;
CREATE TABLE tb_recommend_result_period_daily AS SELECT *,
    round(reco_period / period, 4) as reco_up_ratio
FROM tb_recommend_result_period_daily_s1;   

-- CVR data
drop table if exists tb_recommend_result_cvr_daily_s1;
CREATE TABLE tb_recommend_result_cvr_daily_s1 AS 
select 
	A.year, A.month,A.day, A.device
	,round(sum(B.frequency :: real)/count(distinct A.session_id),3) cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_cvr
	,round(sum(case when A.reco_view =0 then B.frequency :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_cvr
	
from tmp_recommend_result_view_session_daily A
left join tmp_recommend_result_order_session_daily B on A.year = B.year and A.month = B.month and A.day = B.day  and A.session_id = B.session_id
group by 1,2,3,4
order by 1,2,3,4;


drop table if exists tb_recommend_result_cvr_daily;
CREATE TABLE tb_recommend_result_cvr_daily AS SELECT *,
    round(reco_cvr / no_reco_cvr, 4) as reco_up_ratio
FROM tb_recommend_result_cvr_daily_s1;   

-- sa data
drop table if exists tb_recommend_result_sa_daily_s1;
CREATE TABLE tb_recommend_result_sa_daily_s1 AS 
select 
	A.year, A.month,A.day, A.device
	,round(sum(B.revenue :: real)/count(distinct A.session_id),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
	,round(sum(case when A.reco_view =0 then B.revenue :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
	
from tmp_recommend_result_view_session_daily A
left join tmp_recommend_result_order_session_daily B on A.year = B.year and A.month = B.month and A.day = B.day  and A.session_id = B.session_id
group by 1,2,3,4
order by 1,2,3,4;

drop table if exists tb_recommend_result_sa_daily;
CREATE TABLE tb_recommend_result_sa_daily AS SELECT *,
    round(reco_sa / no_reco_sa, 4) as reco_up_ratio
FROM tb_recommend_result_sa_daily_s1; 




--********
--monthly
--********
--UV check
drop table if exists tb_recommend_result_uv_monthly;
CREATE TABLE tb_recommend_result_uv_monthly AS
select
	year
	,month
	,device
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
	,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
	,round( count(distinct case when reco_view > 0 then pc_id end) :: real / count(distinct pc_id) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_view_monthly
group by 1,2,3
order by 1,2,3;

--visit
drop table if exists tb_recommend_result_visit_monthly;
CREATE TABLE tb_recommend_result_visit_monthly AS
select
	year
	,month
	,device
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
    ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
    ,round(count(distinct case when reco_view > 0 then session_id end) :: real / count(distinct session_id) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_view_session_monthly
group by 1,2,3
order by 1,2,3;

--detail view click
drop table if exists tb_recommend_result_pv_monthly;
CREATE TABLE tb_recommend_result_pv_monthly AS
select
	year
	,month
	,device
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv
	,sum(case when reco_view = 0 then view else 0 end) no_reco_pv
    ,round( sum(case when reco_view > 0 then view else 0 end) :: real / sum(view) :: real , 4)*100 reco_ratio
    
from tmp_recommend_result_view_session_monthly
group by 1,2,3
order by 1,2,3;

-- frequency
drop table if exists tb_recommend_result_frequency_monthly;
CREATE TABLE tb_recommend_result_frequency_monthly AS
select
	A.year
	,A.month
	,A.device
	,sum(A.frequency) frequency
	,sum(A.reco_frequency) reco_frequency 
	,sum(A.no_reco_frequency) no_reco_frequency 
	,round( sum(A.reco_frequency) :: real / sum(A.frequency) :: real , 4)*100 reco_ratio
    
from tmp_recommend_result_order_session_monthly A
group by 1,2,3
order by 1,2,3;

-- revenue
drop table if exists tb_recommend_result_revenue_monthly;
CREATE TABLE tb_recommend_result_revenue_monthly AS
select
	A.year
	,A.month
	,A.device
	,sum(A.revenue) revenue
	,sum(A.reco_revenue) reco_revenue    
	,sum(A.no_reco_revenue) no_reco_revenue 
    ,round( sum(A.reco_revenue) :: real / sum(A.revenue) :: real , 4) * 100 reco_ratio
from tmp_recommend_result_order_session_monthly A
group by 1,2,3
order by 1,2,3;




--prevenue
drop table if exists tb_recommend_result_prevenue_monthly_s1;
CREATE TABLE tb_recommend_result_prevenue_monthly_s1 AS
select
	A.year
	,A.month
	,A.device
	,sum(A.revenue)/sum(A.frequency) prevenue
    ,sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end) reco_prevenue
	
from tmp_recommend_result_order_session_monthly A
left join tb_recommend_reco_group_monthly C on A.year = C.year and A.month = C.month and A.session_id = C.session_id
group by 1,2,3
order by 1,2,3;



drop table if exists tb_recommend_result_prevenue_monthly;
CREATE TABLE tb_recommend_result_prevenue_monthly AS SELECT *,
    round(reco_prevenue :: real / prevenue :: real, 4) as reco_up_ratio
FROM tb_recommend_result_prevenue_monthly_s1;

--quantity (구매당 아이템)
drop table if exists tb_recommend_result_quantity_monthly_s1;
CREATE TABLE tb_recommend_result_quantity_monthly_s1 AS
select
	A.year, A.month, A.device
	,round(avg(A.quantity :: real),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: real end),3) reco_quantity
	,round(avg(case when C.session_id is null then A.quantity :: real end),3) no_reco_quantity

from tmp_recommend_result_order_session_monthly A
left join tb_recommend_reco_group_monthly C on A.year = C.year and A.month = C.month and A.session_id = C.session_id
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_quantity_monthly;
CREATE TABLE tb_recommend_result_quantity_monthly AS SELECT *,
    round(reco_quantity :: real / quantity :: real, 4) as reco_up_ratio
FROM tb_recommend_result_quantity_monthly_s1;


--pclick data 
drop table if exists tb_recommend_result_click_monthly_s1;
CREATE TABLE tb_recommend_result_click_monthly_s1 AS 
SELECT 
	year, month, device
	,round(AVG(view :: real),3) click
    ,round(AVG(case when reco_view>0 then view :: real end),3) reco_click
    
FROM
    tmp_recommend_result_view_session_monthly
group by 1,2,3
order by 1,2,3;


drop table if exists tb_recommend_result_click_monthly;
CREATE TABLE tb_recommend_result_click_monthly AS SELECT *,
    round(reco_click / click, 4) as reco_up_ratio
FROM tb_recommend_result_click_monthly_s1;   


--period
drop table if exists tb_recommend_result_period_monthly_s1;
CREATE TABLE tb_recommend_result_period_monthly_s1 AS 
SELECT 
	year, month, device
	,round(AVG(period :: real),3) period
    ,round(AVG(case when reco_view>0 then period :: real end),3) reco_period
    
FROM
    tmp_recommend_result_view_session_monthly
group by 1,2,3
order by 1,2,3;


drop table if exists tb_recommend_result_period_monthly;
CREATE TABLE tb_recommend_result_period_monthly AS SELECT *,
    round(reco_period / period, 4) as reco_up_ratio
FROM tb_recommend_result_period_monthly_s1;   

-- CVR data
drop table if exists tb_recommend_result_cvr_monthly_s1;
CREATE TABLE tb_recommend_result_cvr_monthly_s1 AS 
select 
	A.year, A.month, A.device
	,round(sum(B.frequency :: real)/count(distinct A.session_id),3) cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_cvr
	,round(sum(case when A.reco_view =0 then B.frequency :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_cvr
	
from tmp_recommend_result_view_session_monthly A
left join tmp_recommend_result_order_session_monthly B on A.year = B.year and A.month = B.month  and A.session_id = B.session_id
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_cvr_monthly;
CREATE TABLE tb_recommend_result_cvr_monthly AS SELECT *,
    round(reco_cvr / cvr, 4) as reco_up_ratio
FROM tb_recommend_result_cvr_monthly_s1;   

-- sa data
drop table if exists tb_recommend_result_sa_monthly_s1;
CREATE TABLE tb_recommend_result_sa_monthly_s1 AS 
select 
	A.year, A.month, A.device
	,round(sum(B.revenue :: real)/count(distinct A.session_id),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
	,round(sum(case when A.reco_view =0 then B.revenue :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa

from tmp_recommend_result_view_session_monthly A
left join tmp_recommend_result_order_session_monthly B on A.year = B.year and A.month = B.month  and A.session_id = B.session_id
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_sa_monthly;
CREATE TABLE tb_recommend_result_sa_monthly AS SELECT *,
    round(reco_sa / sa, 4) as reco_up_ratio
FROM tb_recommend_result_sa_monthly_s1;   


--********
--daily
--********
--view status
select * from tb_recommend_result_uv_daily order by device,month,day; --유니크방문
select * from tb_recommend_result_visit_daily order by device,month,day; --세션방문
select * from tb_recommend_result_pv_daily order by device,month,day; --페이지뷰수
--order status
select * from tb_recommend_result_frequency_daily order by device,month,day; -- 구매건수
select * from tb_recommend_result_revenue_daily order by device,month,day; -- 매출
--index
select * from tb_recommend_result_prevenue_daily order by device,month,day; --객단가 : 구매당 평균 구매금액
select * from tb_recommend_result_quantity_daily order by device,month,day; --구매당 평균 아이템 수
select * from tb_recommend_result_click_daily order by device,month,day; -- 방문당 평균 상품클릭수
select * from tb_recommend_result_period_daily order by device,month,day; -- 방문당 평균 상품클릭수
select * from tb_recommend_result_cvr_daily order by device,month,day; -- 구매전환율
select * from tb_recommend_result_sa_daily order by device,month,day; -- 세션당 구매금액

--********
--monthly
--********
--view status

select * from tb_recommend_result_uv_monthly order by device,month; --유니크방문
select * from tb_recommend_result_visit_monthly order by device,month; --세션방문
select * from tb_recommend_result_pv_monthly order by device,month; --페이지뷰수
--order status
select * from tb_recommend_result_frequency_monthly order by device,month; -- 구매건수
select * from tb_recommend_result_revenue_monthly order by device,month; -- 매출
--index
select * from tb_recommend_result_prevenue_monthly order by device,month; --객단가 : 구매당 평균 구매금액
select * from tb_recommend_result_quantity_monthly order by device,month; --구매당 평균 아이템 수
select * from tb_recommend_result_click_monthly order by device,month; -- 방문당 평균 상품클릭수
select * from tb_recommend_result_period_monthly order by device,month; -- 방문당 평균 상품클릭수
select * from tb_recommend_result_cvr_monthly order by device,month; -- 구매전환율
select * from tb_recommend_result_sa_monthly order by device,month; -- 세션당 구매금액