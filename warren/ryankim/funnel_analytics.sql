
--reco revenue / old reco revenue/ total revenue
select 
	sum(reco_revenue) reco_revenue
	,sum(case when reco_view_count <> 0 then order_revenue end) old_reco_revenue
	,sum(order_revenue) total_revenue
from tb_session_funnel;

--1,2,3,4 번째 클릭이 reco 클릭일 때 구매전환율 비교


select 
	count(case when order_revenue is not null then 1 end)::real/count(*)::real total_cvr
	,count(case when order_revenue is not null and reco_view_count>0 then 1 end)::real/count(case when reco_view_count>0 then 1 end)::real reco_cvr

	,count(case when order_revenue is not null and first_view is not null then 1 end)::real/count(case when first_reco_view = 1 then 1 end)::real first_reco_one_cvr
	,count(case when order_revenue is not null and view_count>=1 then 1 end)::real/count(case when view_count>=1  then 1 end)::real one_cvr


	,count(case when order_revenue is not null and first_reco_view = 2  then 1 end)::real/count(case when first_reco_view = 2 then 1 end)::real first_reco_two_cvr
	,count(case when order_revenue is not null and view_count>=2 then 1 end)::real/count(case when view_count>=2  then 1 end)::real two_cvr

	,count(case when order_revenue is not null and first_reco_view = 3  then 1 end)::real/count(case when first_reco_view = 3 then 1 end)::real first_reco_three_cvr
	,count(case when order_revenue is not null and view_count>=3 then 1 end)::real/count(case when view_count>=3  then 1 end)::real three_cvr

	,count(case when order_revenue is not null and first_reco_view = 4  then 1 end)::real/count(case when first_reco_view = 4 then 1 end)::real first_reco_four_cvr
	,count(case when order_revenue is not null and view_count>=4 then 1 end)::real/count(case when view_count>=4  then 1 end)::real four_cvr
from tb_session_funnel;

-- pc_id, item_id별로 구매전환율 체크 추천클릭한 상품이 구매전환이 될 확률이 높은지, 추천 클릭하지 않은 상품이 구매전환이 될 확률이 높은지 확인
drop table if exists tb_rc_code_cvr_s1;
create table tb_rc_code_cvr_s1 as
select 
A.pc_id
,A.session_id
,A.item_id view_item
,A.rc_code rc_code
,B.item_id order_item
,B.price
from tb_log_view_report A
left join tb_log_order_report B on A.pc_id=B.pc_id and A.session_id = B.session_id and A.item_id = B.item_id;

select * from tb_log_order_report limit 10;

drop table if exists tb_rc_code_cvr_s2;
create table tb_rc_code_cvr_s2 as
select 
pc_id
, view_item
--, price
, max(case when order_item is not null then 1 else 0 end) as cv
, sum(case when rc_code is not null then 1 else 0 end) rec_count
, count(*) all_count 
from tb_rc_code_cvr_s1
group by pc_id, view_item;--, price;

drop table if exists tb_rc_code_cvr;
create table tb_rc_code_cvr as
select
	count(case when a.rec_count = 0 then a.pc_id end) no_reco_view_count
	,count(case when a.rec_count = 0 and a.cv = 1 then a.pc_id end) no_reco_order_count
	,count(case when a.rec_count = 0 and a.cv = 1 then a.pc_id end) :: real  / count(case when a.rec_count = 0 then a.pc_id end) :: real no_reco_cvr
	,avg(case when a.rec_count = 0 and a.cv = 1 then b.sale_price end) no_reco_avg_sale_price
	,avg(case when a.rec_count = 0 and a.cv = 1 then b.original_price end) no_reco_avg_original_price
	,sqrt(variance(case when a.rec_count = 0 and a.cv = 1 then b.sale_price end)) no_reco_var_sale_price

	,count(case when a.rec_count > 0 then pc_id end) reco_view_count
	,count(case when a.rec_count > 0 and a.cv = 1 then a.pc_id end) reco_order_count
	,count(case when a.rec_count > 0 and a.cv = 1 then a.pc_id end) :: real  / count(case when a.rec_count > 0 then a.pc_id end) :: real reco_cvr
	,avg(case when a.rec_count > 0 and a.cv = 1 then b.sale_price end) reco_avg_sale_price
	,avg(case when a.rec_count > 0 and a.cv = 1 then b.original_price end) reco_avg_original_price
	,sqrt(variance(case when a.rec_count > 0 and a.cv = 1 then b.sale_price end)) reco_var_sale_price
from tb_rc_code_cvr_s2 a
inner join tb_product_info b on a.view_item = b.item_id ;

select * from tb_rc_code_cvr_s1 limit 100;
select * from tb_rc_code_cvr_s2 where cv = 1 and rec_count<>0 limit 100;
select * from tb_rc_code_cvr;


--구매한 상품을 보았을 때 추천으로 볼수록, 상품 클릭을 더 적게하고 구매한다.
drop table if exists tb_rc_order_click;
create table tb_rc_order_click as
select 
avg(case when rec_count <> 0 then all_count::real end) as reco_count
,avg(case when rec_count = 0 then all_count::real end) as no_reco_count
from tb_rc_code_cvr_s2 where cv = 1;


--##########################################################################################
--rcratio vs index
--##########################################################################################


drop table if exists tb_crm_kpi_rcratio_itemnum_s1;
create table tb_crm_kpi_rcratio_itemnum_s1 as
select
	round(reco_view::real/view::real,1) as rcratio
	,reco_view_count
	,view
	,itemnum
	,itemvar
	,period
from tb_session_funnel;

drop table if exists tb_crm_kpi_rcratio_itemnum;
create table tb_crm_kpi_rcratio_itemnum as
select
	rcratio
	,round(avg(itemnum::real),2) avg_itemnum
	,round(avg(itemvar::real),2) avg_itemvar
	,round(avg(period::real),2) avg_period
	,count(*) count
	,round(avg(view::real),2) avg_view
from tb_crm_kpi_rcratio_itemnum_s1
group by rcratio;

--rc_code - index about order
drop table if exists tb_crm_kpi_conversion_s2;
create table tb_crm_kpi_conversion_s2 as
select 
	round(reco_view_count::real/view_count::real,1) rcratio
	,case when order_revenue is not null then 1 else 0 end as conversion
	,order_quantity
	,order_revenue
	,order_itemnum
	,view_count
	,view_period
from tb_session_funnel;

drop table if exists tb_crm_kpi_conversion_s3;
create table tb_crm_kpi_conversion_s3 as
select 
	rcratio	
	,round(count(case when conversion = 1 then conversion end)::real/ count(*)::real, 4) as cvr
	,round(avg(order_quantity::real),2) avg_quantity
	,round(avg(order_revenue::real),2) order_revenue
	,round(avg(order_itemnum::real),2) order_itemnum
	,count(*) count
	
	,round(avg(view_count::real),2) avg_view_count
	,round(avg(view_period::real),2) avg_view_period
	
from tb_crm_kpi_conversion_s2
group by rcratio;

--##########################################################################################
--view vs index
--##########################################################################################

--view_count - index about order
drop table if exists tb_crm_kpi_view_count;
create table tb_crm_kpi_view_count as
select
view_count
,session_count
,order_count
,order_revenue
,round(order_count::real/session_count::real,3) cvr
,round(order_revenue::real/order_count::real,3) prevenue
from
(
select 
view_count 
,count(*) session_count
,sum(case when order_count is not null then order_count end) order_count
,sum(case when order_revenue is not null then order_revenue end) order_revenue
from tb_session_funnel 
group by view_count
)
order by session_count desc;

--##########################################################################################
--reco view vs index
--##########################################################################################

--reco_view_count - index about order
drop table if exists tb_crm_kpi_recoview_count;
create table tb_crm_kpi_recoview_count as
select
reco_view_count
,session_count
,order_count
,order_revenue
,round(order_count::real/session_count::real,3) cvr
,round(order_revenue::real/order_count::real,3) prevenue

from
(
select 
reco_view_count
,count(*) session_count
,sum(case when order_count is not null then order_count end) order_count
,sum(case when order_revenue is not null then order_revenue end) order_revenue
from tb_session_funnel 

group by reco_view_count
)
order by session_count desc;

--##########################################################################################
--period vs index
--##########################################################################################

--period - index about order
drop table if exists tb_index_period_order_s1;
create table tb_index_period_order_s1 as
select 
	round(view_period/60, 0) as minute
	,case when order_revenue is not null then 1 else 0 end as conversion
	,order_quantity
	,order_revenue
	,order_itemnum
	,view_count
	
from tb_session_funnel;

drop table if exists tb_index_period_order;
create table tb_index_period_order as
select 
	minute
	,round(count(case when conversion = 1 then conversion end)::real/ count(*)::real, 4) as cvr
	,round(avg(order_quantity::real),2) avg_quantity
	,round(avg(order_revenue::real),2) order_revenue
	,round(avg(order_itemnum::real),2) order_itemnum
	,count(*) count
	
	,round(avg(view_count::real),2) avg_view_count
	,round(avg(minute::real),2) avg_view_period
	
from tb_index_period_order_s1
group by minute;


select * from tb_crm_kpi_rcratio_itemnum order by rcratio;
select * from tb_crm_kpi_conversion_s3 order by rcratio;
select * from tb_crm_kpi_view_count order by view_count;
select * from tb_crm_kpi_recoview_count order by reco_view_count;
select * from tb_index_period_order order by minute;

drop table if exists tb_crm_kpi_view_count_A;
create table tb_crm_kpi_view_count_A as
select * from tb_crm_kpi_view_count order by view_count;

drop table if exists tb_crm_kpi_recoview_count_A;
create table tb_crm_kpi_recoview_count_A as
select * from tb_crm_kpi_recoview_count order by reco_view_count;

drop table if exists tb_index_period_order_A;
create table tb_index_period_order_A as
select * from tb_index_period_order order by minute;


drop table if exists tb_rc_code_cvr_A;
create table tb_rc_code_cvr_A as
select * from tb_rc_code_cvr;

drop table if exists tb_rc_order_click_A;
create table tb_rc_order_click_A as
select * from tb_rc_order_click;



drop table if exists tb_crm_kpi_view_count_B;
create table tb_crm_kpi_view_count_B as
select * from tb_crm_kpi_view_count order by view_count;

drop table if exists tb_crm_kpi_recoview_count_B;
create table tb_crm_kpi_recoview_count_B as
select * from tb_crm_kpi_recoview_count order by reco_view_count;

drop table if exists tb_index_period_order_B;
create table tb_index_period_order_B as
select * from tb_index_period_order order by minute;

drop table if exists tb_rc_code_cvr_B;
create table tb_rc_code_cvr_B as
select * from tb_rc_code_cvr;

drop table if exists tb_rc_order_click_B;
create table tb_rc_order_click_B as
select * from tb_rc_order_click;







select 
A.view_count
, A.cvr a_cvr
, A.prevenue a_prevenue
, A.session_count a_count
, B.cvr b_cvr
, B.prevenue b_prevenue
, B.session_count b_count
from tb_crm_kpi_view_count_A A 
left join tb_crm_kpi_view_count_B B on A.view_count = B.view_count
order by A.view_count;

select 
A.minute
, A.cvr a_cvr
, A.order_revenue a_prevenue
, A.count A_count
, B.cvr b_cvr
, B.order_revenue b_prevenue
, B.count B_count
from tb_index_period_order_A A 
left join tb_index_period_order_B B on A.minute = B.minute
order by A.minute;