drop table if exists tb_log_view_daily_report;
create table tb_log_view_daily_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
	(case when device = 'PW' then 'PW' else 'MW' end) as device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term,
	substring(pc_id from 14 for 7)::bigint %2 as AB
from 
tb_log_view
where extract(day from convert_timezone('KST', server_time)) in (11,12,13,14,15,16,17);

drop table if exists tb_log_order_daily_report;
create table tb_log_order_daily_report as
select
	server_time,
	convert_timezone('KST', server_time)::date as date, 
	(case when device = 'PW' then 'PW' else 'MW' end) as device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term, 
	order_id, 
	order_price,
	price, 
	quantity,
	substring(pc_id from 14 for 7)::bigint %2 as AB
from 
tb_log_order
where extract(day from convert_timezone('KST', server_time)) in (11,12,13,14,15,16,17);


drop table if exists tb_product_info;
create table tb_product_info as
select
  item_id,
  max(item_name) item_name,
  max(item_image) item_image,
  max(item_url) item_url,
  max(original_price) original_price,
  max(sale_price) sale_price,
  max(COALESCE(category3, category2, category1, '')) category,
  coalesce(max(category1), '') category1,
  coalesce(max(category2), '') category2,
  coalesce(max(category3), '') category3,
  coalesce(max(category4), '') category4,
  coalesce(max(category5), '') category5,
  min(reg_date) reg_date,
  min(update_date) update_date,
  min(expire_date) expire_date,
  min(stock) stock,
  min(state) state,
  max(description) description, 
  max(extra_image) extra_image,
  max(locale) locale
 from
  tmp_product_info
 where
  original_price is not null and original_price > 0 and sale_price > 0 group by item_id;


drop table if exists tb_product_info_report;
CREATE TABLE tb_product_info_report as
select
	item_id, 
	item_name, 
	original_price, 
	sale_price, 
	category1, 
	category2, 
	category3, 
	category4, 
	category5, 
	reg_date
	stock
from 
tb_product_info;

drop table if exists tmp_recommend_result_view;
CREATE TABLE tmp_recommend_result_view as
select 
	date
	,pc_id 
	,device
	,AB
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date , pc_id, device,AB;

drop table if exists tmp_recommend_result_order;
CREATE TABLE tmp_recommend_result_order as
select
	date
	,pc_id 
	,device
	,AB
	,sum(price*quantity) revenue
	,sum(case when rc_code is not null then price*quantity else 0 end) reco_revenue
	,sum(case when rc_code is null then price*quantity else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(distinct case when rc_code is not null then order_id else null end) reco_frequency
	,count(distinct case when rc_code is null then order_id else null end) no_reco_frequency
	
from tb_log_order_daily_report
group by date, pc_id, device,AB;

drop table if exists tmp_recommend_result_view_session;
CREATE TABLE tmp_recommend_result_view_session as
select
	date
	,session_id 
	,device
	,AB
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_daily_report
group by date,session_id, device,AB;

drop table if exists tmp_recommend_result_order_session;
CREATE TABLE tmp_recommend_result_order_session as
select
	A.date
	,A.session_id 
	,A.order_id
	,A.device
	,A.AB
	,sum(A.price*A.quantity) revenue
	,sum(case when B.reco_view > 0 then A.price*A.quantity else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.price*A.quantity else 0 end) no_reco_revenue
	
	,sum(distinct A.order_price) actual_payment
	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
	,sum(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_price else 0 end) no_reco_actual_payment
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 or B.reco_view is null then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_id end) no_reco_frequency
	
from tb_log_order_daily_report A
left join tmp_recommend_result_view_session B on A.session_id = B.session_id and A.date=B.date
group by A.date,A.session_id,A.order_id,A.device,A.AB;

drop table if exists tb_recommend_loyal_group;
CREATE TABLE tb_recommend_loyal_group AS SELECT * FROM
    tmp_recommend_result_view_session
WHERE
	view > 1;

drop table if exists tb_recommend_reco_group;
CREATE TABLE tb_recommend_reco_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
where reco_view > 0;







--###########
--####추천####
--###########

--uv
drop table if exists tb_recommend_result_uv_daily_device;
CREATE TABLE tb_recommend_result_uv_daily_device AS
select
	date
	,device
	,AB
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv   
from tmp_recommend_result_view
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_uv_daily_s1;
CREATE TABLE tb_recommend_result_uv_daily_s1 AS
select * from tb_recommend_result_uv_daily_device
union all
select * from tb_recommend_result_uv_daily_total;

drop table if exists tb_recommend_result_uv_daily;
CREATE TABLE tb_recommend_result_uv_daily AS
	select
	*
	,(case when uv <> 0 then round( reco_uv :: float / uv :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_uv_daily_s1;

--visit

drop table if exists tb_recommend_result_visit_daily_device;
CREATE TABLE tb_recommend_result_visit_daily_device AS
select
	date
	,device
	,AB
	,count(distinct session_id) visit
        ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
        ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,count(distinct case when view > 1 then session_id end) loyal_visit  
    
    
from tmp_recommend_result_view_session
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_visit_daily_total;
CREATE TABLE tb_recommend_result_visit_daily_total AS
select
	date
	,'AL' device
	,AB
	,count(distinct session_id) visit
        ,count(distinct case when reco_view > 0 then session_id end) reco_visit  
        ,count(distinct case when reco_view = 0 then session_id end) no_reco_visit  
	,count(distinct case when view > 1 then session_id end) loyal_visit  
    
    
from tmp_recommend_result_view_session
group by 1,3
order by 1,3;

drop table if exists tb_recommend_result_visit_daily_s1;
CREATE TABLE tb_recommend_result_visit_daily_s1 AS
select * from tb_recommend_result_visit_daily_device
union all
select * from tb_recommend_result_visit_daily_total;

drop table if exists tb_recommend_result_visit_daily;
CREATE TABLE tb_recommend_result_visit_daily AS
select
	*
	,(case when visit <> 0 then round( reco_visit :: float / visit :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_visit_daily_s1;

--pv
drop table if exists tb_recommend_result_pv_daily_device;
CREATE TABLE tb_recommend_result_pv_daily_device AS
select
	date
	,device
	,AB
	,sum(view) pv
        ,sum(reco_view) reco_pv
	,sum(no_reco_view) no_reco_pv
    
from tmp_recommend_result_view_session
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_pv_daily_total;
CREATE TABLE tb_recommend_result_pv_daily_total AS
select
	date
	,'AL' device
	,AB
	,sum(view) pv
        ,sum(reco_view) reco_pv
	,sum(no_reco_view) no_reco_pv
    
from tmp_recommend_result_view_session
group by 1,3
order by 1,3;

drop table if exists tb_recommend_result_pv_daily_s1;
CREATE TABLE tb_recommend_result_pv_daily_s1 AS
select * from tb_recommend_result_pv_daily_device
union all
select * from tb_recommend_result_pv_daily_total;

drop table if exists tb_recommend_result_pv_daily;
CREATE TABLE tb_recommend_result_pv_daily AS
select
	*
	,(case when pv <> 0 then round( reco_pv :: float / pv :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_pv_daily_s1;

--frequency
drop table if exists tb_recommend_result_frequency_daily_device;
CREATE TABLE tb_recommend_result_frequency_daily_device AS
select
	date
	,device
	,AB
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency 
	,sum(no_reco_frequency) no_reco_frequency 
	    
    
from tmp_recommend_result_order_session
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_frequency_daily_total;
CREATE TABLE tb_recommend_result_frequency_daily_total AS
select
	date
	,'AL' device
	,AB
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency 
	,sum(no_reco_frequency) no_reco_frequency 
	
    
from tmp_recommend_result_order_session
group by 1,3
order by 1,3;

drop table if exists tb_recommend_result_frequency_daily_s1;
CREATE TABLE tb_recommend_result_frequency_daily_s1 AS
select * from tb_recommend_result_frequency_daily_device
union all
select * from tb_recommend_result_frequency_daily_total;

drop table if exists tb_recommend_result_frequency_daily;
CREATE TABLE tb_recommend_result_frequency_daily AS
select
	*
	,(case when frequency <> 0 then round( reco_frequency :: float / frequency :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_frequency_daily_s1;



--revenue
drop table if exists tb_recommend_result_revenue_daily_device;
CREATE TABLE tb_recommend_result_revenue_daily_device AS
select
	date
	,device
	,AB
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
	,sum(no_reco_revenue) no_reco_revenue 
	,sum(case when session_id is not null then revenue end) loyal_revenue    
    
from tmp_recommend_result_order_session
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_revenue_daily_total;
CREATE TABLE tb_recommend_result_revenue_daily_total AS
select
	date
	,'AL' device
	,AB
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
	,sum(no_reco_revenue) no_reco_revenue 
	,sum(case when session_id is not null then revenue end) loyal_revenue    
    
from tmp_recommend_result_order_session
group by 1,3
order by 1,3;

drop table if exists tb_recommend_result_revenue_daily_s1;
CREATE TABLE tb_recommend_result_revenue_daily_s1 AS
select * from tb_recommend_result_revenue_daily_device
union all
select * from tb_recommend_result_revenue_daily_total;

drop table if exists tb_recommend_result_revenue_daily;
CREATE TABLE tb_recommend_result_revenue_daily AS
select
	*
	,(case when revenue <> 0 then round( reco_revenue :: float / revenue :: float , 4) * 100 else null end) reco_ratio
from tb_recommend_result_revenue_daily_s1;



--prevenue
drop table if exists tb_recommend_result_prevenue_daily_s1_device;
CREATE TABLE tb_recommend_result_prevenue_daily_s1_device AS
select
	A.date
	,A.device
	,A.AB
	,round(sum(A.revenue)/sum(A.frequency),2) prevenue
    ,round(sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end),2) reco_prevenue
    ,round(sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end),2) no_reco_prevenue
    ,round(sum(case when B.session_id is not null then A.revenue end)/sum(case when B.session_id is not null  then A.frequency end),2) loyal_prevenue
    
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_prevenue_daily_s1_total;
CREATE TABLE tb_recommend_result_prevenue_daily_s1_total AS
select
	A.date
	,'AL' device
	,A.AB	
	,round(sum(A.revenue)/sum(A.frequency),2) prevenue
    ,round(sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end),2) reco_prevenue
    ,round(sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end),2) no_reco_prevenue
    ,round(sum(case when B.session_id is not null then A.revenue end)/sum(case when B.session_id is not null  then A.frequency end),2) loyal_prevenue
    
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1,3
order by 1,3;

drop table if exists tb_recommend_result_prevenue_daily_s1;
CREATE TABLE tb_recommend_result_prevenue_daily_s1 AS
select * from tb_recommend_result_prevenue_daily_s1_device
union all
select * from tb_recommend_result_prevenue_daily_s1_total;


drop table if exists tb_recommend_result_prevenue_daily;
CREATE TABLE tb_recommend_result_prevenue_daily AS SELECT *,
    (case when prevenue <> 0  then round(reco_prevenue :: float / (prevenue :: float), 4) * 100 else null end) as reco_up_ratio
FROM tb_recommend_result_prevenue_daily_s1;




--quantity
drop table if exists tb_recommend_result_quantity_daily_s1_device;
CREATE TABLE tb_recommend_result_quantity_daily_s1_device AS
select
	A.date, A.device, A.AB
	,round(avg(A.quantity :: float),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: float end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: float end),3) no_reco_quantity
    ,round(avg(case when B.session_id is not null then A.quantity :: float end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_quantity_daily_s1_total;
CREATE TABLE tb_recommend_result_quantity_daily_s1_total AS
select
	A.date
	,'AL' device
	,A.AB
	,round(avg(A.quantity :: float),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: float end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: float end),3) no_reco_quantity
    ,round(avg(case when B.session_id is not null then A.quantity :: float end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.date = B.date and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.date = C.date and A.session_id = C.session_id
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_quantity_daily_s1;
CREATE TABLE tb_recommend_result_quantity_daily_s1 AS
select * from tb_recommend_result_quantity_daily_s1_device
union all
select * from tb_recommend_result_quantity_daily_s1_total;


drop table if exists tb_recommend_result_quantity_daily;
CREATE TABLE tb_recommend_result_quantity_daily AS SELECT *,
    (case when quantity <> 0  then round(reco_quantity :: float / (quantity :: float), 4) * 100 else null end) as reco_up_ratio
FROM tb_recommend_result_quantity_daily_s1;


--click
drop table if exists tb_recommend_result_click_daily_s1_device;
CREATE TABLE tb_recommend_result_click_daily_s1_device AS 
SELECT 
	date, device,ab
	,round(AVG(view :: float),3) click
    ,round(AVG(case when reco_view>0 then view :: float end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: float end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: float  end),3) loyal_click
FROM
    tmp_recommend_result_view_session
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_click_daily_s1_total;
CREATE TABLE tb_recommend_result_click_daily_s1_total AS 
SELECT 
	date,'AL' device, ab
	,round(AVG(view :: float),3) click
    ,round(AVG(case when reco_view>0 then view :: float end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: float end),3) no_reco_click
    ,round(AVG(case when view > 1 then view :: float  end),3) loyal_click
FROM
    tmp_recommend_result_view_session
group by 1,3
order by 1,3;

drop table if exists tb_recommend_result_click_daily_s1;
CREATE TABLE tb_recommend_result_click_daily_s1 AS 
select * from tb_recommend_result_click_daily_s1_device
union all
select * from tb_recommend_result_click_daily_s1_total;

drop table if exists tb_recommend_result_click_daily;
CREATE TABLE tb_recommend_result_click_daily AS SELECT *,
    (case when click <> 0 then round(reco_click / (click), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_click_daily_s1;   


--cvr
drop table if exists tb_recommend_result_cvr_daily_s1_device;
CREATE TABLE tb_recommend_result_cvr_daily_s1_device AS 
select 
	A.date, A.device, A.AB
	
	,round(sum(B.frequency :: float)/count(distinct A.session_id),4)*100 cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),4)*100 reco_cvr
	,round(sum(case when A.reco_view =0 then B.frequency :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),4)*100 no_reco_cvr
	,round(sum(case when A.view >1 then B.frequency :: float end)/count(distinct case when A.view >1 then A.session_id end),4)*100 loyal_cr
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date  and A.session_id = B.session_id
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_cvr_daily_s1_total;
CREATE TABLE tb_recommend_result_cvr_daily_s1_total AS 
select 
	A.date, 'AL' device
	,A.AB
	,round(sum(B.frequency :: float)/count(distinct A.session_id),4)*100 cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),4)*100 reco_cvr
	,round(sum(case when A.reco_view =0 then B.frequency :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),4)*100 no_reco_cvr
	,round(sum(case when A.view >1 then B.frequency :: float end)/count(distinct case when A.view >1 then A.session_id end),4)*100 loyal_cr
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date  and A.session_id = B.session_id
group by 1,3
order by 1,3;

drop table if exists tb_recommend_result_cvr_daily_s1;
CREATE TABLE tb_recommend_result_cvr_daily_s1 AS 
select * from tb_recommend_result_cvr_daily_s1_device
union all
select * from tb_recommend_result_cvr_daily_s1_total;

drop table if exists tb_recommend_result_cvr_daily;
CREATE TABLE tb_recommend_result_cvr_daily AS SELECT *,
    (case when cvr <> 0 then round(reco_cvr / (cvr), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_cvr_daily_s1;   


--sa
drop table if exists tb_recommend_result_sa_daily_s1_device;
CREATE TABLE tb_recommend_result_sa_daily_s1_device AS 
select 
	A.date, A.device
	,A.AB
	,round(sum(B.revenue :: float)/count(distinct A.session_id),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
	,round(sum(case when A.reco_view =0 then B.revenue :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
	,round(sum(case when A.view >1 then B.revenue :: float end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_sa
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.session_id = B.session_id
group by 1,2,3
order by 1,2,3;

drop table if exists tb_recommend_result_sa_daily_s1_total;
CREATE TABLE tb_recommend_result_sa_daily_s1_total AS 
select 
	A.date, 'AL' device,A.AB
	
	,round(sum(B.revenue :: float)/count(distinct A.session_id),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: float end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
	,round(sum(case when A.reco_view =0 then B.revenue :: float end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
	,round(sum(case when A.view >1 then B.revenue :: float end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_sa
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.date = B.date and A.session_id = B.session_id
group by 1,3
order by 1,3;

drop table if exists tb_recommend_result_sa_daily_s1;
CREATE TABLE tb_recommend_result_sa_daily_s1 AS 
select * from tb_recommend_result_sa_daily_s1_device
union all
select * from tb_recommend_result_sa_daily_s1_total;

drop table if exists tb_recommend_result_sa_daily;
CREATE TABLE tb_recommend_result_sa_daily AS SELECT *,
    (case when sa <> 0  then round(reco_sa / (sa), 4)*100 else null end) as reco_up_ratio
FROM tb_recommend_result_sa_daily_s1;



select
a.date,
a.device,
a.AB,
a.visit session_visit_total,
a.reco_visit session_visit_reco, 
a.reco_ratio session_visit_rate,

b.uv unique_visit_total,
b.reco_uv unique_visit_reco,
b.reco_ratio unique_visit_rate,

c.pv page_view_total,
c.reco_pv page_view_reco,
c.reco_ratio page_view_rate,

d.frequency purchase_count_total,
d.reco_frequency purchase_count_reco,
d.reco_ratio purchase_count_rate,

e.revenue revenue_total,
e.reco_revenue revenue_reco,
e.reco_ratio revenue_rate,

f.prevenue customer_transaction_avg_total,
f.reco_prevenue customer_transaction_avg_reco,
f.reco_up_ratio customer_transaction_avg_result,

g.quantity purchase_item_count_avg_total,
g.reco_quantity purchase_item_count_avg_reco,
g.reco_up_ratio purchase_item_count_avg_result,

h.click visit_item_click_avg_total,
h.reco_click visit_item_click_avg_reco,
h.reco_up_ratio visit_item_click_avg_result,

i.cvr purchase_conversion_rate_total,
i.reco_cvr purchase_conversion_rate_reco,
i.reco_up_ratio purchase_conversion_rate_result,

j.sa session_purchase_revenue_avg_total,
j.reco_sa session_purchase_revenue_avg_reco,
j.reco_up_ratio session_purchase_revenue_avg_result

from tb_recommend_result_visit_daily a
left join tb_recommend_result_uv_daily b on a.date = b.date and a.device = b.device and a.ab = b.ab
left join tb_recommend_result_pv_daily c on a.date = c.date and a.device = c.device and a.ab = c.ab
left join tb_recommend_result_frequency_daily d on a.date = d.date and a.device = d.device and a.ab = d.ab
left join tb_recommend_result_revenue_daily e on a.date = e.date and a.device = e.device and a.ab = e.ab
left join tb_recommend_result_prevenue_daily f on a.date = f.date and a.device = f.device and a.ab = f.ab
left join tb_recommend_result_quantity_daily g on a.date = g.date and a.device = g.device and a.ab = g.ab
left join tb_recommend_result_click_daily h on a.date = h.date and a.device = h.device and a.ab = h.ab
left join tb_recommend_result_cvr_daily i on a.date = i.date and a.device = i.device and a.ab = i.ab
left join tb_recommend_result_sa_daily j on a.date = j.date and a.device = j.device and a.ab = j.ab
where A.AB is not null
order by 2,1,3;









