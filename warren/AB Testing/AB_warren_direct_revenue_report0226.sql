drop table if exists tb_log_view_201602;
create table tb_log_view_201602 as
select
	pc_id
	,substring(pc_id from 14 for 7)::bigint %2 as AB 
	,session_id
	,item_id
	,user_id
	,device
	,convert_timezone('KST',server_time) server_time
	,rc_code
from tb_log_view
where convert_timezone('KST',server_time) between '2016-02-23 10:00:00' and '2016-02-25 10:00:00';

drop table if exists tb_log_order_201602;
create table tb_log_order_201602 as
select
	convert_timezone('KST',server_time) server_time
	,pc_id
	,substring(pc_id from 14 for 7)::bigint %2 as AB 
	,device
	,user_id
	,order_id
	,order_price
	,item_id
	,price
	,quantity
from tb_log_order
where convert_timezone('KST',server_time) between '2016-02-23 10:00:00' and '2016-02-25 10:00:00';

drop table if exists tb_order_stat;
create table tb_order_stat as
select
	order_id
	,sum(quantity) order_item_count
	,sum(price * quantity) order_price
from tb_log_order
where convert_timezone('KST',server_time) between '2016-02-23 10:00:00' and '2016-02-25 10:00:00'
group by 1;


drop table if exists tb_order_item_within_24h;
create table tb_order_item_within_24h as
select 
	a.pc_id
	,a.ab
	,a.session_id
	,a.rc_code
	,a.item_id
	,a.server_time view_time
	,b.server_time order_time
	,b.order_id
from tb_log_view_201602 a
join tb_log_order_201602 b on a.pc_id = b.pc_id and a.item_id = b.item_id
where datediff (minute, a.server_time, b.server_time ) < 1440 and datediff(minute, a.server_time, b.server_time ) >= 0;

drop table if exists tb_view_order_24h;
create table tb_view_order_24h as
select a.pc_id, a.ab, a.session_id, a.rc_code, a.item_id, a.server_time as view_time, b.order_time, b.order_id, a.device 
from tb_log_view_201602 a
left join tb_order_item_within_24h b on a.pc_id = b.pc_id and a.item_id = b.item_id and a.session_id = b.session_id and a.server_time = b.view_time;

drop table if exists tb_rc_code_map;
create table tb_rc_code_map as
select
	rc_code,
	case when rc_code like '%ProductDetail_product%' then 'pw_detail'
	when rc_code like '%BasketView_product%' then 'pw_basket'
	when rc_code like '%Search_product%' then 'pw_search' 
	when rc_code like '%RECENT%' then 'mw_recent'
	when rc_code like '%REO%' then 'mw_main'
	when rc_code like '%RECO%' then 'reco'
	when rc_code like '%ProductDetail_m_product%' then 'mw_detail'
	when rc_code like '%BasketView_m_product%' then 'mw_basket'
	when rc_code like '%Search_m_product%' then 'mw_search' 
	when rc_code like '%TODAY%' then 'mw_main' 
	else rc_code
	end as rc_code_map
from
(select distinct rc_code from tb_view_order_24h);

-- 1) 
select t1.order_date, t1.is_rec, t1.ab,count(*) order_count, avg(t2.order_price) as avg_order_price, avg(t2.order_item_count::float) as avg_item_count  from
(select extract(day from order_time) as order_date, ab, order_id, max(case when rc_code is not null then 1 else 0 end) as is_rec from tb_view_order_24h
where order_id is not null
group by order_time, order_id,ab) t1
join tb_order_stat t2 on t2.order_id = t1.order_id
group by t1.order_date,t1.is_rec, t1.ab
order by 1,2,3;

drop table tb_order_rc_code;
create table tb_order_rc_code as
select order_id, rc_code_map
from 
	(select order_id, rc_code_map, count(*) over (partition by order_id)
	from
		(select order_id, rc_code_map
		from tb_view_order_24h t1
		join tb_rc_code_map t2 on t2.rc_code = t1.rc_code
		where order_id is not null and t1.rc_code is not null
		group by order_id, rc_code_map)
	)
where count = 1;

-- 2)
select rc_code_map, count(*) order_count, avg(t2.order_price) as avg_order_price, avg(t2.order_item_count::float) as avg_item_count
from tb_order_rc_code t1
join tb_order_stat t2 on t1.order_id = t2.order_id
group by t1.rc_code_map;

-- 3)

select * from tb_view_order_24h limit 100;



select day, ab, is_rec, sum(view_count), sum(order_count) from
(select day, ab, is_rec, item_id, count(distinct session_id) as view_count, count(distinct case when order_id is not null then session_id else null end) as order_count
from
(select day, pc_id, ab, session_id, is_rec, item_id, order_id
	from
	(select extract(day from view_time) as day, pc_id, ab, session_id, case when rc_code is null then 0 else 1 end as is_rec, item_id, order_id
	from tb_view_order_24h)
group by day, pc_id, ab, session_id, is_rec, item_id, order_id)
group by day,ab,is_rec,item_id)
where is_rec =1
group by day,ab,is_rec
order by 1,2;

-- 4)
select rc_code_map, sum(view_count), sum(order_count) from
(select rc_code_map, item_id, count(distinct session_id) as view_count, count(distinct case when order_id is not null then session_id else null end) as order_count
from
(select pc_id, session_id, rc_code_map, item_id, order_id
	from
	(select pc_id, session_id, rc_code_map, item_id, order_id
	from tb_view_order_24h t1
	join tb_rc_code_map t2 on t2.rc_code = t1.rc_code)
group by pc_id, session_id, rc_code_map, item_id, order_id)
group by rc_code_map, item_id)
group by rc_code_map;

--direct revenue

select 
	a.day,
	a.ab,
	a.device,
	sum(b.order_price)
from
(select day, pc_id, ab, device, session_id, is_rec, item_id, order_id
	from
	(select extract(day from order_time) as day, pc_id, ab, session_id, device, case when rc_code is null then 0 else 1 end as is_rec, item_id, order_id
	from tb_view_order_24h)
group by day, pc_id, ab, device, session_id, is_rec, item_id, order_id) a
inner join 
tb_order_stat b on a.order_id = b.order_id
where a.is_rec = 1
group by 1,2,3
order by 1,3,2;