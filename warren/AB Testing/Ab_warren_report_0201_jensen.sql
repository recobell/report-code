drop table if exists tb_log_view;
create table tb_log_view (	
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(1000),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
item_id varchar(200),
search_term varchar(200)
);

drop table if exists tb_log_search;
create table tb_log_search (
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(200),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
search_term varchar(200),
search_result varchar(200)
);



drop table if exists tb_log_order;
create table tb_log_order (
cuid varchar(200),
type varchar(200),
server_time timestamp,
ip_address varchar(200),
user_agent varchar(2000),
api_version varchar(200),
domain varchar(200),
device varchar(200),
pc_id varchar(200),
session_id varchar(200),
ad_id varchar(200),
push_id varchar(200),
gc_id varchar(200),
user_id varchar(200),
ab_group varchar(200),
rc_code varchar(200),
ad_code varchar(200),
locale varchar(200),
item_id varchar(200),
search_term varchar(200),
order_id varchar(200),
order_price bigint,
price bigint,
quantity bigint
);

drop table if exists tmp_product_info;
create table tmp_product_info (
cuid varchar(200),
type varchar(200),
item_id varchar(200),
item_name varchar(2000),
item_image varchar(200),
item_url varchar(2000),
original_price int,
sale_price int,
category1 varchar(200),
category2 varchar(200),
category3 varchar(200),
category4 varchar(200),
category5 varchar(200),
reg_date varchar(200),
update_date timestamp,
expire_date timestamp,
stock int,
state varchar(200),
description varchar(200),
extra_image varchar(200),
locale varchar(200)
);

--# @DATE_STR should be replaced by a date pattern 

--@copy-tb_log_view
copy tb_log_view
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/view/2015/11/14'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/view/2015/11/15'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/view/2015/11/16'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/view/2015/11/17'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_view
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/view/2015/11/18'

CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/view_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';
--@end

--@copy-tb_log_search
copy tb_log_search
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/search/2015/09/30'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_keyETuQOe2UKt8dlvRWvhctHTDgHltOrtn3azkEyf1B'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_search
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/search/2015/10'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate ON
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';
--@end

--@copy-tb_log_order
copy tb_log_order
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/order/2015/11/14'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/order/2015/11/15'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/order/2015/11/16'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/order/2015/11/17'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';

copy tb_log_order
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/order/2015/11/18'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/order_data.jsonpaths'
compupdate ON
STATUPDATE OFF
timeformat 'auto'
region 'ap-northeast-1';

--@end

--@copy-tmp_product_info
copy tmp_product_info
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/product/2015/11/14'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';


copy tmp_product_info
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/product/2015/11/15'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/product/2015/11/16'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/product/2015/11/17'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

copy tmp_product_info
from 's3://rb-logs-apne1/0f8265c6-6457-4b4a-b557-905d58f9f216/product/2015/11/18'
CREDENTIALS 'aws_access_key_id=AKIAIMNCJVDNVJMCX26A;aws_secret_access_key=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'
GZIP
compupdate on
STATUPDATE OFF
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/product_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

--@end


--##################################################################



drop table if exists tb_log_view_report;
CREATE TABLE tb_log_view_report as
select
	server_time,
	extract(year from convert_timezone('KST', server_time)) as year, 
	extract(month from convert_timezone('KST', server_time)) as month, 
	
	extract(day from convert_timezone('KST', server_time)) as day, 
	extract(hour from convert_timezone('KST', server_time)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term,
	substring(pc_id from 14 for 7)::bigint %2 as AB
from 
tb_log_view;
where device <>'PW'
and extract(day from convert_timezone('KST', server_time)) in (24,25,26,27,28,29);

drop table if exists tb_log_order_report;
CREATE TABLE tb_log_order_report as
select
	server_time,
	extract(year from convert_timezone('KST', server_time)) as year, 
	extract(month from convert_timezone('KST', server_time)) as month, 
	
	extract(day from convert_timezone('KST', server_time)) as day, 
	extract(hour from convert_timezone('KST', server_time)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	user_agent,
	pc_id, 
	session_id, 
	gc_id, 
	rc_code,
	item_id, 
	search_term, 
	order_id, 
	order_price,
	price, 
	quantity,
	substring(pc_id from 14 for 7)::bigint %2 as AB
from 
tb_log_order
where device <> 'PW'
and extract(day from convert_timezone('KST', server_time)) in (24,25,26,27,28,29);

select * from tb_log_order_report limit 10;

--recommendation resource table
drop table if exists tmp_recommend_result_view;
CREATE TABLE tmp_recommend_result_view as
select 
	year
	,month
	,day
	,pc_id 
	,device
	,AB
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by year,month,day , pc_id, device,ab;

drop table if exists tmp_recommend_result_order;
CREATE TABLE tmp_recommend_result_order as
select
	year
	,month
	,day
	,pc_id 
	,device
	,AB
	,sum(price) revenue
	,sum(case when rc_code is not null then price else 0 end) reco_revenue
	,sum(case when rc_code is null then price else 0 end) no_reco_revenue
	
	,count(*) frequency
	,count(case when rc_code is not null then 1 else 0 end) reco_frequency
	,count(case when rc_code is null then 1 else 0 end) no_reco_frequency
	
from tb_log_order_report
group by year,month,day, pc_id, device, ab;

drop table if exists tmp_recommend_result_view_session;
CREATE TABLE tmp_recommend_result_view_session as
select
	year
	,month
	,day
	,session_id 
	,device
	,AB
	,count(*) as view
	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
from tb_log_view_report
group by year,month,day,  session_id, device,ab;

drop table if exists tmp_recommend_result_order_session;
CREATE TABLE tmp_recommend_result_order_session as
select
	A.year
	,A.month
	,A.day
	,A.session_id 
	,A.order_id
	,A.device
	,A.AB
	,sum(A.price) revenue
	,sum(case when B.reco_view > 0 then A.price else 0 end) reco_revenue
	,sum(case when B.reco_view = 0 then A.price else 0 end) no_reco_revenue
	
	,sum(A.quantity) quantity
	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
	,sum(case when B.reco_view = 0 then A.quantity else 0 end) no_reco_quantity
	
	,count(distinct A.item_id) itemvar
	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
	,count(distinct case when B.reco_view = 0 then A.item_id end) no_reco_itemvar
	
	,count(distinct A.order_id) frequency
	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
	,count(distinct case when B.reco_view = 0 then A.order_id end) no_reco_frequency
	
from tb_log_order_report A
left join tmp_recommend_result_view_session B on A.session_id = B.session_id 
group by A.year,A.month,A.day,A.session_id,A.order_id,A.device,a.ab;



--####################################################################################################
--group
--####################################################################################################

--loyal group : comparison
drop table if exists tb_recommend_loyal_group;
CREATE TABLE tb_recommend_loyal_group AS SELECT * FROM
    tmp_recommend_result_view_session
WHERE
	view > 1;

drop table if exists tb_recommend_reco_group;
CREATE TABLE tb_recommend_reco_group AS 
SELECT * FROM
    tmp_recommend_result_view_session
where reco_view > 0;
	



--####################################################################################################
--현 상황
--####################################################################################################

--********
--daily
--********
--UV check
drop table if exists tb_recommend_result_uv_daily_s1;
CREATE TABLE tb_recommend_result_uv_daily_s1 AS
select
	year
	,month
	,day
	,device
	,AB
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
from tmp_recommend_result_view
group by 1,2,3,4,5
order by 1,2,3,4,5;

drop table if exists tb_recommend_result_uv_daily;
CREATE TABLE tb_recommend_result_uv_daily AS
select
	*
	,round( reco_uv :: real / uv :: real , 2) ratio
from tb_recommend_result_uv_daily_s1;


--visit
drop table if exists tb_recommend_result_visit_daily_s1;
CREATE TABLE tb_recommend_result_visit_daily_s1 AS
select
	year
	,month
	,day
	,device
	,AB
	,count(distinct session_id) visit
    ,count(distinct case when reco_view > 0 then session_id end) reco_visit    
    
from tmp_recommend_result_view_session
group by 1,2,3,4,5
order by 1,2,3,4,5;

drop table if exists tb_recommend_result_visit_daily;
CREATE TABLE tb_recommend_result_visit_daily AS
select
	*
	,round( reco_visit :: real / visit :: real , 2) ratio
from tb_recommend_result_visit_daily_s1;

--detail view click
drop table if exists tb_recommend_result_pv_daily_s1;
CREATE TABLE tb_recommend_result_pv_daily_s1 AS
select
	year
	,month
	,day
	,device
	,AB
	,sum(view) pv
    ,sum(case when reco_view > 0 then view else 0 end) reco_pv
    
    
from tmp_recommend_result_view_session
group by 1,2,3,4,5
order by 1,2,3,4,5;

drop table if exists tb_recommend_result_pv_daily;
CREATE TABLE tb_recommend_result_pv_daily AS
select
	*
	,round( reco_pv :: real / pv :: real , 2) ratio
from tb_recommend_result_pv_daily_s1;

-- frequency
drop table if exists tb_recommend_result_frequency_daily_s1;
CREATE TABLE tb_recommend_result_frequency_daily_s1 AS
select
	year
	,month
	,day
	,device
	,AB
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency    
    
from tmp_recommend_result_order_session
group by 1,2,3,4,5
order by 1,2,3,4,5;

drop table if exists tb_recommend_result_frequency_daily;
CREATE TABLE tb_recommend_result_frequency_daily AS
select
	*
	,round( reco_frequency :: real / frequency :: real , 2) ratio
from tb_recommend_result_frequency_daily_s1;


-- revenue
drop table if exists tb_recommend_result_revenue_daily_s1;
CREATE TABLE tb_recommend_result_revenue_daily_s1 AS
select
	year
	,month
	,day
	,device
	,AB
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
    
from tmp_recommend_result_order_session
group by 1,2,3,4,5
order by 1,2,3,4,5;

drop table if exists tb_recommend_result_revenue_daily;
CREATE TABLE tb_recommend_result_revenue_daily AS
select
	*
	,round( reco_revenue :: real / revenue :: real , 2) ratio
from tb_recommend_result_revenue_daily_s1;



--####################################################################################################
--추천 지표
--####################################################################################################

--********
--daily
--********

drop table if exists tb_recommend_result_prevenue_daily_s1;
CREATE TABLE tb_recommend_result_prevenue_daily_s1 AS
select
	A.year
	,A.month
	,A.day
	,A.device
	,A.AB
	,sum(A.revenue)/sum(A.frequency) prevenue
    ,sum(case when C.session_id is not null then A.revenue end)/sum(case when C.session_id is not null then A.frequency end) reco_prevenue
    ,sum(case when C.session_id is null then A.revenue end)/sum(case when C.session_id is null then A.frequency end) no_reco_prevenue
    --,sum(case when B.session_id is not null then A.revenue end)/sum(case when B.session_id is not null  then A.frequency end) loyal_prevenue
	
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.year = B.year and A.month = B.month and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.year = C.year and A.month = C.month and A.session_id = C.session_id
group by 1,2,3,4,5
order by 1,2,3,4,5;

drop table if exists tb_recommend_result_prevenue_daily;
CREATE TABLE tb_recommend_result_prevenue_daily AS SELECT *,
    round(reco_prevenue :: real / no_reco_prevenue :: real, 4) as reco_up_ratio
FROM tb_recommend_result_prevenue_daily_s1;

--미완성
--quantity (구매당 아이템)
drop table if exists tb_recommend_result_quantity_daily_s1;
CREATE TABLE tb_recommend_result_quantity_daily_s1 AS
select
	A.year, A.month,A.day, A.device, A.AB
	,round(avg(A.quantity :: real),3) quantity
    ,round(avg(case when C.session_id is not null then A.quantity :: real end),3) reco_quantity
    ,round(avg(case when C.session_id is null then A.quantity :: real end),3) no_reco_quantity
    --,round(avg(case when B.session_id is not null then A.quantity :: real end),3) loyal_quantity
from tmp_recommend_result_order_session A
left join tb_recommend_loyal_group B on A.year = B.year and A.month = B.month and A.session_id = B.session_id
left join tb_recommend_reco_group C on A.year = C.year and A.month = C.month and A.session_id = C.session_id
group by 1,2,3,4,5
order by 1,2,3,4,5;

drop table if exists tb_recommend_result_quantity_daily;
CREATE TABLE tb_recommend_result_quantity_daily AS SELECT *,
    round(reco_quantity :: real / no_reco_quantity :: real, 4) as reco_up_ratio
FROM tb_recommend_result_quantity_daily_s1;



--pclick data 
drop table if exists tb_recommend_result_click_daily_s1;
CREATE TABLE tb_recommend_result_click_daily_s1 AS 
SELECT 
	year, month,day, device, AB
	,round(AVG(view :: real),3) click
    ,round(AVG(case when reco_view>0 then view :: real end),3) reco_click
    ,round(AVG(case when reco_view=0 then view :: real end),3) no_reco_click
    --,round(AVG(case when view > 1 then view :: real  end),3) loyal_click
    
FROM
    tmp_recommend_result_view_session
group by 1,2,3,4,5
order by 1,2,3,4,5;



drop table if exists tb_recommend_result_click_daily;
CREATE TABLE tb_recommend_result_click_daily AS SELECT *,
    round(reco_click / no_reco_click, 4) as reco_up_ratio
FROM tb_recommend_result_click_daily_s1;   

-- CVR data
drop table if exists tb_recommend_result_cvr_daily_s1;
CREATE TABLE tb_recommend_result_cvr_daily_s1 AS 
select 
	A.year, A.month,A.day, A.device, A.AB
	,round(sum(B.frequency :: real)/count(distinct A.session_id),3) cvr
	,round(sum(case when A.reco_view >0 then B.frequency :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_cr
	,round(sum(case when A.reco_view =0 then B.frequency :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_cr
	--,round(sum(case when A.view >1 then B.frequency :: real end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_cr
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.year = B.year and A.month = B.month  and A.session_id = B.session_id
group by 1,2,3,4,5
order by 1,2,3,4,5;


drop table if exists tb_recommend_result_cvr_daily;
CREATE TABLE tb_recommend_result_cvr_daily AS SELECT *,
    round(reco_cr / no_reco_cr, 4) as reco_up_ratio
FROM tb_recommend_result_cvr_daily_s1;   

-- sa data
drop table if exists tb_recommend_result_sa_daily_s1;
CREATE TABLE tb_recommend_result_sa_daily_s1 AS 
select 
	A.year, A.month,A.day, A.device, A.AB
	,round(sum(B.revenue :: real)/count(distinct A.session_id),3) sa
	,round(sum(case when A.reco_view >0 then B.revenue :: real end)/count(distinct case when A.reco_view >0 then A.session_id end),3) reco_sa
	,round(sum(case when A.reco_view =0 then B.revenue :: real end)/count(distinct case when A.reco_view =0 then A.session_id end),3) no_reco_sa
	--,round(sum(case when A.view >1 then B.revenue :: real end)/count(distinct case when A.view >1 then A.session_id end),3) loyal_sa
	
from tmp_recommend_result_view_session A
left join tmp_recommend_result_order_session B on A.year = B.year and A.month = B.month  and A.session_id = B.session_id
group by 1,2,3,4,5 
order by 1,2,3,4,5;

drop table if exists tb_recommend_result_sa_daily;
CREATE TABLE tb_recommend_result_sa_daily AS SELECT *,
    round(reco_sa / no_reco_sa, 4) as reco_up_ratio
FROM tb_recommend_result_sa_daily_s1; 



select * from tb_recommend_result_uv_daily order by month,day,device; --유니크방문
select * from tb_recommend_result_visit_daily order by month,day,device; --세션방문
select * from tb_recommend_result_pv_daily order by month,day,device; --페이지뷰수

--order status
select * from tb_recommend_result_frequency_daily order by month,day,device; -- 구매건수
select * from tb_recommend_result_revenue_daily order by month,day,device; -- 매출
--index
select * from tb_recommend_result_prevenue_daily order by month,day,device; --객단가 : 구매당 평균 구매금액
select * from tb_recommend_result_quantity_daily order by month,day,device; --구매당 평균 아이템 수
select * from tb_recommend_result_click_daily order by month,day,device; -- 방문당 평균 상품클릭수
select * from tb_recommend_result_cvr_daily order by month,day,device; -- 구매전환율
select * from tb_recommend_result_sa_daily order by month,day,device; -- 세션당 구매금액


select 
	a.year, 
	a.month, 
	a.day, 
	a.device, 
	a.ab, 
	a.uv, 
	b.visit,
	c.pv,
	d.frequency, 
	e.revenue, 
	f.prevenue, 
	g.quantity, 
	h.click, 
	i.cvr, 
	j.sa
from 
	tb_recommend_result_uv_daily a
		INNER JOIN
	tb_recommend_result_visit_daily b on a.day = b.day and a.year = b.year and a.month = b.month and a.device = b.device and a.ab=b.ab 
		INNER JOIN
	tb_recommend_result_pv_daily c on a.day = c.day and a.year = c.year and a.month = c.month and a.device = c.device and a.ab=c.ab 
		INNER JOIN
	tb_recommend_result_frequency_daily d on a.day = d.day and a.year = d.year and a.month = d.month and a.device = d.device and a.ab=d.ab
		INNER JOIN		
	tb_recommend_result_revenue_daily e on a.day = e.day and a.year = e.year and a.month = e.month and a.device = e.device and a.ab=e.ab
		INNER JOIN	
	tb_recommend_result_prevenue_daily f on a.day = f.day and a.year = f.year and a.month = f.month and a.device = f.device and a.ab=f.ab
		INNER JOIN			
	tb_recommend_result_quantity_daily g on a.day = g.day and a.year = g.year and a.month = g.month and a.device = g.device and a.ab=g.ab
		INNER JOIN			
	tb_recommend_result_click_daily h on a.day = h.day and a.year = h.year and a.month = h.month and a.device = h.device and a.ab=h.ab
		INNER JOIN	
	tb_recommend_result_cvr_daily i on a.day = i.day and a.year = i.year and a.month = i.month and a.device = i.device and a.ab=i.ab
		INNER JOIN	
	tb_recommend_result_sa_daily j on a.day = j.day and a.year = j.year and a.month = j.month and a.device = j.device and a.ab=j.ab
order by month,day,ab,device;