/*
drop table if exists tb_log_search;
Create table tb_log_search (
cuid varchar(200),
type varchar(200),
serverTime timestamp,
ipAddress varchar(200),
userAgent varchar(2000),
apiVersion varchar(200),
domain varchar(200),
device varchar(200),
pcId varchar(200),
sessionId varchar(200),
adId varchar(200),
pushId varchar(200),
gcId varchar(200),
userId varchar(200),
abGroup varchar(200),
rcCode varchar(200),
adCode varchar(200),
locale varchar(200),
searchTerm varchar(200),
searchResult varchar(200)
 );
*/

-- 기존 네이버 키워드 엑셀에 저장되어 있는 자료를 받아 DB 로 저장하는 테이블
-- 아래와 같이 테이블 생성하고, import 구문으로 받아오도록 함
CREATE TABLE "public"."tb_keyword_list" (
"keyword" varchar(200),
"date" timestamp
);
-- 이후 import 사용해서 수동으로 가져오도록 일단 조치함


-- 현재 라인 이후부터 실행하면 OK

-- 워렌에서 수집된 사용자 검색이력을 새롭게 monthly 정리하는 부분: 일단 다 지우고 최근 3개월만 가져오도록 할 예정. Monthly Update?
truncate tb_log_search;

-- 2015/09 데이터 삽입 (만약 특정 날짜를 구분해서 넣고 싶다면 2015/09/09 와 같은 식으로 세부 지정 가능)
copy tb_log_search
from 's3://rb-logs-apne1/212a41cd-3c2e-4150-811b-0b2f71bac62e/search/2015/09'
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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'
GZIP
compupdate on
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';

-- select * from tb_log_search limit 1000;

-- 2015/10 데이터 삽입
copy tb_log_search
from 's3://rb-logs-apne1/212a41cd-3c2e-4150-811b-0b2f71bac62e/search/2015/10'
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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'
GZIP
compupdate on
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';


-- 2015/11 데이터 삽입
copy tb_log_search
from 's3://rb-logs-apne1/212a41cd-3c2e-4150-811b-0b2f71bac62e/search/2015/11'
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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'
GZIP
compupdate on
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';



-- 2015/12 데이터 삽입
copy tb_log_search
from 's3://rb-logs-apne1/212a41cd-3c2e-4150-811b-0b2f71bac62e/search/2015/12'
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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'
GZIP
compupdate on
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';


-- 토너짱 롱테일 키워드 추출 쿼리
drop table if exists tb_longtail_keyword;
create table tb_longtail_keyword as
select searchterm, count(distinct pcid) UV, count(*) cnt, current_date date
from tb_log_search
where length(searchterm) >= 3
	and left(searchterm,1) <= 'z' -- delete Korean characters
	and right(searchterm,1) <= 'z'
	and substring(searchterm,2,1) <= 'z'
	and substring(searchterm,-2,1) <= 'z'
group by searchterm
order by searchterm;

-- select * from tb_longtail_keyword limit 1000;


-- 네이버 키워드 template 채우는 부분
drop table if exists tb_longtail_keyword_result;
create table tb_longtail_keyword_result as
select (case when left(A.searchterm,1) = '' or left(A.searchterm,1) = ' ' or left(A.searchterm,1) is null then substring(A.searchterm,2,200) 			 
						 when right(A.searchterm,1) = '' or right(A.searchterm,1) = ' ' or right(A.searchterm,1) is null then substring(A.searchterm,-2,200) 		
						 when left(A.searchterm,2) = '' or left(A.searchterm,2) = '  ' or left(A.searchterm,2) is null then substring(A.searchterm,3,200) 			 
				else A.searchterm end) searchterm
			, concat(A.searchterm, ' 토너짱')  title
			, concat(A.searchterm, ' 공장직영, 도매가격, 우체국당일발송, 잉크토너 사무용품전문몰!') description
			, concat ('http://www.tonerzzang.com/search/search1.php?sort=low_price&top_search_field=search_total&total_search_keyword=', A.searchterm)URL
			, 70 price
from tb_longtail_keyword A left outer join tb_keyword_list B on A.searchterm = B.keyword
where B.keyword is null -- 기존에 롱테일키워드 아니고 이미 키워드 마케팅 하고 있었다면 중복되지 않도록 삭제해주는 부분
  and length(A.searchterm) >= 3
  and A.searchterm not like '%~%'
  and A.searchterm not like '%!%'
  and A.searchterm not like '%@%'
  and A.searchterm not like '%#%'
  and A.searchterm not like '%$%'
  
  and A.searchterm not like '%^%'
  and A.searchterm not like '%&%'
  and A.searchterm not like '%,%'
  and A.searchterm not like '%:%'
  and A.searchterm not like '%;%'

  and A.searchterm not like '%(%'
  and A.searchterm not like '%)%'
  and A.searchterm not like '%{%'
  and A.searchterm not like '%}%'
  and A.searchterm not like '%[%'
  and A.searchterm not like '%]%'

  and A.searchterm not like '%+%'
  and A.searchterm not like '%=%'
  and A.searchterm not like '%"%'
  and A.searchterm not like '%|%'

order by A.searchterm;

 
select * from tb_longtail_keyword_result 
order by (case when length(searchterm) = 4 and searchterm <= '9999' and searchterm >= '0000' then 0 
							 when length(searchterm) = 3 and searchterm <= '999' and searchterm >= '000' then 0 
							 when length(searchterm) = 5 and searchterm <= '99999' and searchterm >= '00000' then 0 
							 when length(searchterm) = 6 and searchterm <= '999999' and searchterm >= '000000' then 0 else 1
          end) desc, searchterm -- 숫자로만 구성된 키워드를 후순위로 빼버림;