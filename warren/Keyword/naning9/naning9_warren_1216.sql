
drop table if exists tb_log_search;
Create table tb_log_search (
cuid varchar(200),
type varchar(200),
server_Time timestamp,
ip_Address varchar(200),
user_Agent varchar(2000),
api_Version varchar(200),
domain varchar(200),
device varchar(200),
pc_Id varchar(200),
session_Id varchar(200),
ad_Id varchar(200),
push_Id varchar(200),
gc_Id varchar(200),
user_Id varchar(200),
ab_Group varchar(200),
rc_Code varchar(200),
ad_Code varchar(200),
locale varchar(200),
search_Term varchar(200),
search_Result varchar(200)
 );


-- 기존 네이버 키워드 엑셀에 저장되어 있는 자료를 받아 DB 로 저장하는 테이블
-- 아래와 같이 테이블 생성하고, import 구문으로 받아오도록 함
CREATE TABLE "public"."tb_keyword_list" (
"keyword" varchar(200),
"date" timestamp
);
-- 이후 import 사용해서 수동으로 가져오도록 일단 조치함


-- 현재 라인 이후부터 실행하면 OK

-- 워렌에서 수집된 사용자 검색이력을 새롭게 monthly 정리하는 부분: 일단 다 지우고 최근 3개월만 가져오도록 할 예정. Monthly Update?
truncate tb_log_search;

-- 2015/09 데이터 삽입 (만약 특정 날짜를 구분해서 넣고 싶다면 2015/09/09 와 같은 식으로 세부 지정 가능)
/*copy tb_log_search
from 's3://rb-logs-apne1/212a41cd-3c2e-4150-811b-0b2f71bac62e/search/2015/09'
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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'
GZIP
compupdate on
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';*/

-- select * from tb_log_search limit 1000;

-- 2015/10 데이터 삽입

copy tb_log_search
from 's3://rb-logs-apne1/4b1defbf-e4bc-4a42-8124-a7202813c4b0/search/2015/10/31'
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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'
GZIP
compupdate on
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
region 'ap-northeast-1';


-- 2015/11 데이터 삽입

copy tb_log_search
from 's3://rb-logs-apne1/4b1defbf-e4bc-4a42-8124-a7202813c4b0/search/2015/11'
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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'
GZIP
compupdate on
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
MAXERROR 100000
region 'ap-northeast-1';



-- 2015/12 데이터 삽입
copy tb_log_search
from 's3://rb-logs-apne1/4b1defbf-e4bc-4a42-8124-a7202813c4b0/search/2015/12'
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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'
GZIP
compupdate on
FORMAT AS JSON  's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths'
timeformat 'auto'
MAXERROR 100000
region 'ap-northeast-1';


-- 토너짱 롱테일 키워드 추출 쿼리
drop table if exists tb_longtail_keyword;
create table tb_longtail_keyword as
select search_term, count(distinct pc_id) UV, count(*) cnt, current_date date
from tb_log_search
where length(search_term) >= 3
group by search_term
having count(distinct pc_id)>2 and count(*) > 2
order by search_term;

select count(*) from tb_longtail_keyword;
select * from tb_longtail_keyword limit 1000;
select * from tb_log_search limit 100;

-- 네이버 키워드 template 채우는 부분
drop table if exists tb_longtail_keyword_result;
create table tb_longtail_keyword_result as
select (case when left(A.search_term,1) = '' or left(A.search_term,1) = ' ' or left(A.search_term,1) is null then substring(A.search_term,2,200)       
             when right(A.search_term,1) = '' or right(A.search_term,1) = ' ' or right(A.search_term,1) is null then substring(A.search_term,-2,200)    
             when left(A.search_term,2) = '' or left(A.searchterm,2) = '  ' or left(A.search_term,2) is null then substring(A.search_term,3,200)        
        else A.search_term end) search_term
      , concat(A.search_term, '난닝구')  title
      , concat(A.search_term, '핸드메이드 제품으로 더 돋보이게, 롯데백화점 입점') description
      , 'http://www.gsshop.com/search/main.gs?lseq=392814&tq='||A.searchterm||'&svcid=gc&initSrchYn=Y#1&so=7&vt=B&pg=30&po=0' URL
      , 70 price
from tb_longtail_keyword A left outer join tb_keyword_list B on A.search_term = B.keyword
where B.keyword is null -- 기존에 롱테일키워드 아니고 이미 키워드 마케팅 하고 있었다면 중복되지 않도록 삭제해주는 부분
  and length(A.search_term) >= 3
  and A.search_term not like '%~%'
  and A.search_term not like '%!%'
  and A.search_term not like '%@%'
  and A.search_term not like '%#%'
  and A.search_term not like '%$%'
  
  and A.search_term not like '%^%'
  and A.search_term not like '%&%'
  and A.search_term not like '%,%'
  and A.search_term not like '%:%'
  and A.search_term not like '%;%'

  and A.search_term not like '%(%'
  and A.search_term not like '%)%'
  and A.search_term not like '%{%'
  and A.search_term not like '%}%'
  and A.search_term not like '%[%'
  and A.search_term not like '%]%'

  and A.search_term not like '%+%'
  and A.search_term not like '%=%'
  and A.search_term not like '%"%'
  and A.search_term not like '%|%'
  and A.search_term not like '%??%'
  and A.search_term not like '%�%'

order by A.search_term;



select count(*) from tb_longtail_keyword_result; 
select * from tb_longtail_keyword_result
order by (case when length(search_term) = 4 and search_term <= '9999' and search_term >= '0000' then 0 
               when length(search_term) = 3 and search_term <= '999' and search_term >= '000' then 0 
               when length(search_term) = 5 and search_term <= '99999' and search_term >= '00000' then 0 
               when length(search_term) = 6 and search_term <= '999999' and search_term >= '000000' then 0 else 1
          end) desc, search_term; -- 숫자로만 구성된 키워드를 후순위로 빼버림;