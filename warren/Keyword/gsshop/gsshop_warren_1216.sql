drop table if exists tb_log_search;
Create table tb_log_search (
cuid varchar(200),
type varchar(200),
server_Time timestamp,
ip_Address varchar(200),
user_Agent varchar(2000),
api_Version varchar(200),
domain varchar(200),
device varchar(200),
pc_Id varchar(200),
session_Id varchar(200),
ad_Id varchar(200),
push_Id varchar(200),
gc_Id varchar(200),
user_Id varchar(200),
ab_Group varchar(200),
rc_Code varchar(200),
ad_Code varchar(200),
locale varchar(200),
search_Term varchar(200),
search_Result varchar(200)
);
  
CREATE TABLE "public"."tb_keyword_list" (
"keyword" varchar(200),
"date" timestamp
);

truncate tb_log_search;

copy tb_log_search from 's3://rb-logs-apne1/12b883d9-a6f6-4e24-8ac7-38d4285c08cb/search/2016/02' 
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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' 
GZIP 
compupdate on 
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths' 
timeformat 'auto' 
MAXERROR 100000 
region 'ap-northeast-1';
   
  
drop table if exists tb_longtail_keyword;
create table tb_longtail_keyword as 
select
  DISTINCT(LTRIM(RTRIM(search_term))) as search_term,
        count(distinct pc_id) UV, 
        count(*) cnt, 
        current_date date 
from tb_log_search 
where length(search_term) >= 5 and server_Time between '%2016-02-01%' and '%2016-02-11%'
group by 1,4
--having count(distinct pc_id)>=3
order by 1;
--where server_Time is between '%2016-01-08%' and '%2016-01-14%';
--where server_Time is between '%2016-01-15%' and '%2016-01-21%';
--where server_Time is between '%2016-01-22%' and '%2016-01-28%';
--where server_Time is between '%2016-01-29%' and '%2016-01-31%';

select count(*) from tb_longtail_keyword;
select * from tb_longtail_keyword order by uv asc, length(search_term) desc limit 5000 ;
select * from tb_log_search limit 100;


drop table if exists tb_longtail_keyword_result;
create table tb_longtail_keyword_result as
select (case when left(A.search_term,1) = '' or left(A.search_term,1) = ' ' or left(A.search_term,1) is null then substring(A.search_term,2,200)
    when right(A.search_term,1) = '' or right(A.search_term,1) = ' ' or right(A.search_term,1) is null then substring(A.search_term,-2,200)
    when left(A.search_term,1) = '' or left(A.search_term,1) = '  ' or left(A.search_term,2) is null then substring(A.search_term,3,200)
  else A.search_term end) search_term
  , 'http://www.gsshop.com/search/main.gs?lseq=392814&tq='||A.search_term||'&svcid=mc&initSrchYn=Y&ab=b#1&so=7&vt=B&pg=30&po=0' as URL
  , 70 price
from tb_longtail_keyword A 
     left outer join 
     tb_keyword_list B on A.search_term = B.keyword 
where B.keyword is null 
-- 기존에 롱테일키워드 아니고 이미 키워드 마케팅 하고 있었다면 중복되지 않도록 삭제해주는 부분 
and length(A.search_term) >= 5 
and length(A.search_term) <= 25 
and A.search_term not like '%false%'
and A.search_term not like '%~%' 
and A.search_term not like '%!%' 
and A.search_term not like '%@%' 
and A.search_term not like '%#%' 
and A.search_term not like '%$%'
and A.search_term not like '%^%' 
and A.search_term not like '%&%' 
and A.search_term not like '%,%' 
and A.search_term not like '%:%' 
and A.search_term not like '%;%' 
and A.search_term not like '%(%' 
and A.search_term not like '%)%'
and A.search_term not like '%{%' 
and A.search_term not like '%}%' 
and A.search_term not like '%[%' 
and A.search_term not like '%]%' 
and A.search_term not like '%+%' 
and A.search_term not like '%=%' 
and A.search_term not like '%"%' 
and A.search_term not like '%|%' 
and A.search_term not like '%??%' 
and A.search_term not like '%�%'
order by A.search_term;
  
  
    

select  A.search_term, 
    B.uv, 
    B.cnt, 
    A.URL 
from tb_longtail_keyword_result A inner join tb_longtail_keyword B on A.search_term = B.search_term
order by uv desc;
