drop table if exists tb_log_search;
Create table tb_log_search (
cuid varchar(200),
type varchar(200),
server_Time timestamp,
ip_Address varchar(200),
user_Agent varchar(2000),
api_Version varchar(200),
domain varchar(200),
device varchar(200),
pc_Id varchar(200),
session_Id varchar(200),
ad_Id varchar(200),
push_Id varchar(200),
gc_Id varchar(200),
user_Id varchar(200),
ab_Group varchar(200),
rc_Code varchar(200),
ad_Code varchar(200),
locale varchar(200),
search_Term varchar(200),
search_Result varchar(200)
);
  
CREATE TABLE "public"."tb_keyword_list" (
"keyword" varchar(200),
"date" timestamp
);

truncate tb_log_search;

copy tb_log_search from 's3://rb-logs-apne1/6c7b90f7-6cf1-427d-9cb5-fad9194ddc2a/search/2016/01/12' 
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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' 
GZIP 
compupdate on 
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths' 
timeformat 'auto' 
MAXERROR 100000 
region 'ap-northeast-1';

copy tb_log_search from 's3://rb-logs-apne1/6c7b90f7-6cf1-427d-9cb5-fad9194ddc2a/search/2016/01/13' 
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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' 
GZIP 
compupdate on 
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths' 
timeformat 'auto' 
MAXERROR 100000 
region 'ap-northeast-1';

copy tb_log_search from 's3://rb-logs-apne1/6c7b90f7-6cf1-427d-9cb5-fad9194ddc2a/search/2016/01/14' 
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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' 
GZIP 
compupdate on 
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths' 
timeformat 'auto' 
MAXERROR 100000 
region 'ap-northeast-1';

copy tb_log_search from 's3://rb-logs-apne1/6c7b90f7-6cf1-427d-9cb5-fad9194ddc2a/search/2016/01/15' 
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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' 
GZIP 
compupdate on 
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths' 
timeformat 'auto' 
MAXERROR 100000 
region 'ap-northeast-1';

copy tb_log_search from 's3://rb-logs-apne1/6c7b90f7-6cf1-427d-9cb5-fad9194ddc2a/search/2016/01/16' 
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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' 
GZIP 
compupdate on 
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths' 
timeformat 'auto' 
MAXERROR 100000 
region 'ap-northeast-1';

copy tb_log_search from 's3://rb-logs-apne1/6c7b90f7-6cf1-427d-9cb5-fad9194ddc2a/search/2016/01/17' 
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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' 
GZIP 
compupdate on 
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths' 
timeformat 'auto' 
MAXERROR 100000 
region 'ap-northeast-1';

copy tb_log_search from 's3://rb-logs-apne1/6c7b90f7-6cf1-427d-9cb5-fad9194ddc2a/search/2016/01/18' 
CREDENTIALS 'aws_access_key_id=AKIAJEFS7O7NI3SWRK2A;aws_secret_access_key=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' 
GZIP 
compupdate on 
FORMAT AS JSON 's3://jsonpaths/Warren_jsonpaths/search_data.jsonpaths' 
timeformat 'auto' 
MAXERROR 100000 
region 'ap-northeast-1';



drop table if exists tb_longtail_keyword;
create table tb_longtail_keyword as 
select
	DISTINCT(LTRIM(RTRIM(search_term))) as search_term,
        count(distinct pc_id) UV, 
        count(*) cnt, 
        current_date date 
from tb_log_search 
where length(search_term) >= 5 
group by 1,4
having count(distinct pc_id)>=3
order by 1;


select count(*) from tb_longtail_keyword;
select * from tb_longtail_keyword order by uv asc, length(search_term) desc limit 5000 ;
select * from tb_log_search limit 100;


drop table if exists tb_longtail_keyword_result;
create table tb_longtail_keyword_result as
select (case when left(A.search_term,1) = '' or left(A.search_term,1) = ' ' or left(A.search_term,1) is null then substring(A.search_term,2,200)
		when right(A.search_term,1) = '' or right(A.search_term,1) = ' ' or right(A.search_term,1) is null then substring(A.search_term,-2,200)
		when left(A.search_term,1) = '' or left(A.search_term,1) = '  ' or left(A.search_term,2) is null then substring(A.search_term,3,200)
	else A.search_term end) search_term
	, concat(A.search_term, ' 교보문고') title
	, concat(A.search_term, '현명한 책구매, 망설이지 말고 교보문고!') description
	, concat('http://www.kyobobook.co.kr/search/SearchCommonMain.jsp?vPstrCategory=TOT&vPoutSearch=1&vPstrKeyWord=', A.search_term) URL
	, 70 price

from tb_longtail_keyword A left outer join tb_keyword_list B on A.search_term = B.keyword
where B.keyword is null
	and length(A.search_term) >= 5
	and length(A.search_term) <= 25
	and A.search_term not like '%~%'
	and A.search_term not like '%!%'
	and A.search_term not like '%@%'
	and A.search_term not like '%#%'
	and A.search_term not like '%$%'
	and A.search_term not like '%^%'
	and A.search_term not like '%&%'
	and A.search_term not like '%,%'
	and A.search_term not like '%:%'
	and A.search_term not like '%;%'
	and A.search_term not like '%(%'
	and A.search_term not like '%)%'
	and A.search_term not like '%{%'
	and A.search_term not like '%}%'
	and A.search_term not like '%[%'
	and A.search_term not like '%]%'
	and A.search_term not like '%+%'
	and A.search_term not like '%=%'
	and A.search_term not like '%"%'
	and A.search_term not like '%|%'
	and A.search_term not like '%??%'
order by A.search_term;

		
select 	DISTINCT(A.search_term) as search_term,
		B.uv, 
		B.cnt, 
		A.URL 
from tb_longtail_keyword_result A inner join tb_longtail_keyword B on A.search_term = B.search_term
order by uv desc
;


select 	 search_term, 
		uv, 
		cnt, 
		URL 
from tb_longtail_keyword_result_s0
where search_term <> search_term_2
order by uv desc;
