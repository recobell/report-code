drop table if exists tb_log_view_report;
CREATE TABLE tb_log_view_report as
select
	servertime,
	extract(year from convert_timezone('KST', servertime)) as year, 
	extract(month from convert_timezone('KST', servertime)) as month, 
	
	extract(day from convert_timezone('KST', servertime)) as day, 
	extract(hour from convert_timezone('KST', servertime)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	useragent,
	pcid, 
	sessionid, 
	gcid, 
	rccode,
	itemid, 
	searchterm
from 
tb_log_view;


drop table if exists tb_log_order_report;
CREATE TABLE tb_log_order_report as
select
	servertime,
	extract(year from convert_timezone('KST', servertime)) as year, 
	extract(month from convert_timezone('KST', servertime)) as month, 
	
	extract(day from convert_timezone('KST', servertime)) as day, 
	extract(hour from convert_timezone('KST', servertime)) as hour, 
	(case when device = 'PW' then 'PW' else 'MO' end) device, 
	useragent,
	pcid, 
	sessionid, 
	gcid, 
	rccode,
	itemid, 
	searchterm, 
	orderid, 
	orderprice,
	price, 
	quantity
from 
tb_log_order;
--************************************************************************
--first reco click
--************************************************************************

drop table if exists tb_log_view_report_rank_s1;
CREATE TABLE tb_log_view_report_rank_s1 as
select
	* 
	,rank() over (partition by sessionid order by servertime)
from tb_log_view_report;

drop table if exists tb_log_view_report_rank_s2;
CREATE TABLE tb_log_view_report_rank_s2 as
select
	sessionid
	,min(rank) first_reco_view
from tb_log_view_report_rank_s1
where rccode is not null
group by sessionid;

drop table if exists tb_log_view_report_rank_s3;
CREATE TABLE tb_log_view_report_rank_s3 as
select 
A.sessionid
,B.device
,A.first_reco_view
,B.rccode
from
tb_log_view_report_rank_s2 A
inner join tb_log_view_report_rank_s1 B on A.sessionid = B.sessionid and A.first_reco_view = B.rank;

drop table if exists tb_log_view_report_rank;
CREATE TABLE tb_log_view_report_rank as 
select
first_reco_view,
device,
count(*)
from tb_log_view_report_rank_s3 
group by 1,2
order by 2,1 ;

--************************************************************************
--first four view
--************************************************************************
drop table if exists tb_log_view_first_four;
CREATE TABLE tb_log_view_first_four as 
select 
	sessionid
	,max(case when rank = 1 then rccode end) first_view
	,max(case when rank = 2 then rccode end) second_view
	,max(case when rank = 3 then rccode end) third_view
	,max(case when rank = 4 then rccode end) fourth_view
from tb_log_view_report_rank_s1
group by sessionid;

select * from tb_log_view_first_four limit 100;
--************************************************************************
--view
--************************************************************************

drop table if exists tb_crm_kpi_view_s1;
create table tb_crm_kpi_view_s1 as
select
	A.pcid
	,A.device
	,A.sessionid
	,count(*) as view
	,sum(case when A.rccode is not null then 1 else 0 end) as reco_view
	,count(distinct A.itemid) as itemvar
	,count(A.itemid) as itemnum
	,datediff(second,min(servertime),max(servertime)) as period

from tb_log_view_report A
group by A.pcid, A.device, A.sessionid;


drop table if exists tb_crm_kpi_view_s2;
create table tb_crm_kpi_view_s2 as
select
	A.*
	,B.first_reco_view
	,B.rccode frist_reco
from tb_crm_kpi_view_s1 A 
left join tb_log_view_report_rank_s3 B on A.sessionid = B.sessionid;





--************************************************************************
--reco order
--************************************************************************
drop table if exists tb_reco_revenue_s1;
create table tb_reco_revenue_s1 as
select 
	A.sessionid
	,A.itemid
	,A.rccode
	,B.price
	,B.quantity
from tb_log_view_report A
inner join tb_log_order_report B on A.sessionid = B.sessionid  and A.itemid = B.itemid
where A.rccode is not null
group by A.sessionid, A.itemid,A.rccode,B.price,B.quantity;

drop table if exists tb_reco_revenue;
create table tb_reco_revenue as
select 
	sessionid
	,sum(price * quantity) reco_revenue
	,sum(quantity) reco_quantity
from tb_reco_revenue_s1
group by sessionid;


--##########################################################################################
--view and order
--order (frequency, quantity, revenue)
--##########################################################################################
drop table if exists tb_crm_kpi_order_s1;
create table tb_crm_kpi_order_s1 as
	SELECT 
        pcid,
		sessionid,
		sum(quantity) quantity,
        sum(price * quantity) revenue,
		count(distinct itemid) buyitemnum,
		count(distinct orderid) ordercount
    FROM
        tb_log_order_report
    GROUP BY pcid, sessionid;


drop table if exists tb_crm_kpi_conversion_s1;
create table tb_crm_kpi_conversion_s1 as
select 
	A.sessionid
	,A.device
	,A.first_reco_view
	,A.frist_reco
	,A.view view_count
	,A.reco_view reco_view_count
	,A.itemvar view_var
	,A.itemnum view_itemnum
	,A.period view_period
	,D.first_view
	,D.second_view
	,D.third_view
	,D.fourth_view
	,B.quantity order_quantity
	,B.revenue order_revenue
	,B.buyitemnum order_itemnum
	,B.ordercount order_count
	,C.reco_revenue 
	,C.reco_quantity
	
from tb_crm_kpi_view_s2 A
left join tb_crm_kpi_order_s1 B on A.sessionid = B.sessionid
left join tb_reco_revenue C on A.sessionid = C.sessionid
left join tb_log_view_first_four D on A.sessionid = D.sessionid;

select * from tb_crm_kpi_conversion_s1 limit 100;

--reco revenue / old reco revenue/ total revenue
select 
	sum(reco_revenue) reco_revenue
	,sum(case when reco_view_count <> 0 then order_revenue end) old_reco_revenue
	,sum(order_revenue) total_revenue
from tb_crm_kpi_conversion_s1;

--1,2,3,4 번째 클릭이 reco 클릭일 때 구매전환율 비교
select 
	count(case when order_revenue is not null then 1 end)::real/count(*)::real total_cvr
	,count(case when order_revenue is not null and reco_view_count>0 then 1 end)::real/count(case when reco_view_count>0 then 1 end)::real reco_cvr

	,count(case when order_revenue is not null and first_view is not null then 1 end)::real/count(case when first_reco_view = 1 then 1 end)::real first_reco_one_cvr
	,count(case when order_revenue is not null and view_count>=1 then 1 end)::real/count(case when view_count>=1  then 1 end)::real one_cvr


	,count(case when order_revenue is not null and first_reco_view = 2  then 1 end)::real/count(case when first_reco_view = 2 then 1 end)::real first_reco_two_cvr
	,count(case when order_revenue is not null and view_count>=2 then 1 end)::real/count(case when view_count>=2  then 1 end)::real two_cvr

	,count(case when order_revenue is not null and first_reco_view = 3  then 1 end)::real/count(case when first_reco_view = 3 then 1 end)::real first_reco_three_cvr
	,count(case when order_revenue is not null and view_count>=3 then 1 end)::real/count(case when view_count>=3  then 1 end)::real three_cvr

	,count(case when order_revenue is not null and first_reco_view = 4  then 1 end)::real/count(case when first_reco_view = 4 then 1 end)::real first_reco_four_cvr
	,count(case when order_revenue is not null and view_count>=4 then 1 end)::real/count(case when view_count>=4  then 1 end)::real four_cvr
from tb_crm_kpi_conversion_s1;









--##########################################################################################
--rcratio vs index
--##########################################################################################


drop table if exists tb_crm_kpi_rcratio_itemnum_s1;
create table tb_crm_kpi_rcratio_itemnum_s1 as
select
	round(reco_view::real/view::real,1) as rcratio
	,reco_view
	,view
	,itemnum
	,itemvar
	,period
from tb_crm_kpi_view_s1;

drop table if exists tb_crm_kpi_rcratio_itemnum;
create table tb_crm_kpi_rcratio_itemnum as
select
	rcratio
	,round(avg(itemnum::real),2) avg_itemnum
	,round(avg(itemvar::real),2) avg_itemvar
	,round(avg(period::real),2) avg_period
	,count(*) count
	,round(avg(view::real),2) avg_view
from tb_crm_kpi_rcratio_itemnum_s1
group by rcratio;

--rccode - index about order
drop table if exists tb_crm_kpi_conversion_s2;
create table tb_crm_kpi_conversion_s2 as
select 
	round(reco_view_count::real/view_count::real,1) rcratio
	,case when order_revenue is not null then 1 else 0 end as conversion
	,order_quantity
	,order_revenue
	,order_itemnum
	,view_count
	,view_period
from tb_crm_kpi_conversion_s1;

drop table if exists tb_crm_kpi_conversion_s3;
create table tb_crm_kpi_conversion_s3 as
select 
	rcratio	
	,round(count(case when conversion = 1 then conversion end)::real/ count(*)::real, 4) as cvr
	,round(avg(order_quantity::real),2) avg_quantity
	,round(avg(order_revenue::real),2) order_revenue
	,round(avg(order_itemnum::real),2) order_itemnum
	,count(*) count
	
	,round(avg(view_count::real),2) avg_view_count
	,round(avg(view_period::real),2) avg_view_period
	
from tb_crm_kpi_conversion_s2
group by rcratio;

--##########################################################################################
--view vs index
--##########################################################################################

--view_count - index about order
drop table if exists tb_crm_kpi_view_count;
create table tb_crm_kpi_view_count as
select
view_count
,session_count
,order_count
,order_revenue
,round(order_count::real/session_count::real, 2) cvr
,round(order_revenue::real/order_count::real,2) prevenue
from
(
select 
view_count 
,count(*) session_count
,sum(case when order_count is not null then order_count end) order_count
,sum(case when order_revenue is not null then order_revenue end) order_revenue
from tb_crm_kpi_conversion_s1 
where reco_view_count =0
group by view_count
)
order by session_count desc;

--##########################################################################################
--reco view vs index
--##########################################################################################

--reco_view_count - index about order
drop table if exists tb_crm_kpi_recoview_count;
create table tb_crm_kpi_recoview_count as
select
reco_view_count
,session_count
,order_count
,order_revenue
,round(order_count::real/session_count::real, 2) cvr
,round(order_revenue::real/order_count::real,2) prevenue
,count(*) count
from
(
select 
reco_view_count
,count(*) session_count
,sum(case when order_count is not null then order_count end) order_count
,sum(case when order_revenue is not null then order_revenue end) order_revenue
from tb_crm_kpi_conversion_s1 

group by reco_view_count
)
order by session_count desc;

--##########################################################################################
--period vs index
--##########################################################################################

--period - index about order
drop table if exists tb_index_period_order_s1;
create table tb_index_period_order_s1 as
select 
	round(view_period/60, 0) as minute
	,case when order_revenue is not null then 1 else 0 end as conversion
	,order_quantity
	,order_revenue
	,order_itemnum
	,view_count
	
from tb_crm_kpi_conversion_s1;

drop table if exists tb_index_period_order;
create table tb_index_period_order as
select 
	minute
	,round(count(case when conversion = 1 then conversion end)::real/ count(*)::real, 4) as cvr
	,round(avg(order_quantity::real),2) avg_quantity
	,round(avg(order_revenue::real),2) order_revenue
	,round(avg(order_itemnum::real),2) order_itemnum
	,count(*) count
	
	,round(avg(view_count::real),2) avg_view_count
	,round(avg(view_period::real),2) avg_view_period
	
from tb_index_period_order_s1
group by minute;


select * from tb_crm_kpi_rcratio_itemnum order by rcratio;
select * from tb_crm_kpi_conversion_s3 order by rcratio;
select * from tb_crm_kpi_view_count order by view_count;
select * from tb_crm_kpi_recoview_count order by reco_view_count;
select * from tb_index_period_order order by minute;

