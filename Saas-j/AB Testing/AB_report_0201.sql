###################################################################################
#Change The Following Pointers to get your result
# '15-08-15' --> '16-01-01'
#@CUSTOMER_NAME_DB - Customer Name Query DB, example: 'liphop_consult'
#@CUSTOMER_NAME_RBLOG - Customer Name raw log, example: 'liphop_rblog'
###################################################################################


select 
date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') date
, substr(pcid, 18, 18)%2 ab
, sum(revenue) revenue
, sum(quantity)
from @CUSTOMER_NAME_rblog.tb_log_order 
where date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') > '15-08-18' and date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') < '15-09-22' and
device ='2'
group by 
date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d'),
substr(pcid, 18, 18)%2;

select
sum(revenue)
from @CUSTOMER_NAME_rblog.tb_log_order 
where date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') > '15-08-01' and date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') < '15-09-01';


select * from @CUSTOMER_NAME_rblog.tb_log_order limit 100;



select 
date_format(convert_tz(b.server_time,'+00:00','+09:00'), '%y-%m-%d') date
, substr(b.pcid, 18, 18)%2 ab
,sum(a.quantity) quantity
from @CUSTOMER_NAME_rblog.tb_log_order_item a 
inner join @CUSTOMER_NAME_rblog.tb_log_order b 
on a.order_id=b.order_id
where date_format(convert_tz(b.server_time,'+00:00','+09:00'), '%y-%m-%d') > '15-08-18' and date_format(convert_tz(b.server_time,'+00:00','+09:00'), '%y-%m-%d') < '15-09-22' and
b.device ='2'
group by 
date_format(convert_tz(b.server_time,'+00:00','+09:00'), '%y-%m-%d'),
substr(b.pcid, 18, 18)%2;


select 

substr(pcid, 18, 18)%2



drop table if exists @CUSTOMER_NAME_consult_jensen.tb_recommend_territory_result_cvr;
CREATE TABLE @CUSTOMER_NAME_consult_jensen.tb_recommend_territory_result_cvr AS
select 
date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') date
, substr(pcid, 18, 18)%2 ab
, count(revenue) freq
, sum(revenue) revenue
, (sum(revenue))/(count(revenue)) prevenue
from @CUSTOMER_NAME_rblog.tb_log_order 
where date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') > '15-08-18' and date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') < '15-09-22'
and device= '2'
group by 
date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d'),
substr(pcid, 18, 18)%2;

select 
A.*
, (sum(A.freq))/(count(distinct B.pcid)) cvr
from @CUSTOMER_NAME_consult_jensen.tb_recommend_territory_result_cvr A
    LEFT JOIN 
    @CUSTOMER_NAME_rblog.tb_log_order 
on group by ab,date order by date,ab;



drop table if exists @CUSTOMER_NAME_consult_jensen.tb_recommend_territory_result_view;
CREATE TABLE @CUSTOMER_NAME_consult_jensen.tb_recommend_territory_result_view AS 
select 
date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') date
, substr(pcid, 18, 18)%2 ab
, count(pcid) view
from @CUSTOMER_NAME_rblog.tb_log_view 
where date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') > '15-08-18' and date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') < '15-09-22'
group by 
date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d'),
substr(pcid, 18, 18)%2;


select 
*
, avg(view)
from @CUSTOMER_NAME_consult_jensen.tb_recommend_territory_result_view group by date,ab order by date,ab; 


where date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') > '15-08-18' and date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') < '15-09-22'
group by 
date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d'),
substr(pcid, 18, 18)%2;



drop table if exists @CUSTOMER_NAME_consult_jensen.tb_recommend_territory_result_click;
CREATE TABLE @CUSTOMER_NAME_consult_jensen.tb_recommend_territory_result_click AS
select 
*
, round(avg(view),3) click
, sum(view) pv
from @CUSTOMER_NAME_consult_jensen.tb_recommend_territory_result_view group by ab,date order by date,ab; 
where date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') > '15-08-18' and date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') < '15-09-22'
group by 
date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d'),
substr(pcid, 18, 18)%2;

select

select
sum(revenue)
from @CUSTOMER_NAME_rblog.tb_log_order 
where date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') > '15-08-01' and date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') < '15-09-01';






select 
date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') date
, substr(pcid, 18, 18)%2 ab
, COUNT(pcid)/count(distinct pcid) click
, count(pcid) pv
, count(distinct pcid) uv
from @CUSTOMER_NAME_rblog.tb_log_view 
where date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') > '15-08-18' and date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d') < '15-09-22'
and device ='2'
group by 
date_format(convert_tz(server_time,'+00:00','+09:00'), '%y-%m-%d'),
substr(pcid, 18, 18)%2;



select 
date_format(convert_tz(A.server_time,'+00:00','+09:00'), '%y-%m-%d') date
, substr(A.pcid, 18, 18)%2 ab
, (count(B.revenue))/(count(distinct A.pcid)) cvr
from @CUSTOMER_NAME_rblog.tb_log_view A
INNER JOIN
@CUSTOMER_NAME_rblog.tb_log_order B on A.pcid=B.pcid
where date_format(convert_tz(A.server_time,'+00:00','+09:00'), '%y-%m-%d') > '15-08-18' and date_format(convert_tz(A.server_time,'+00:00','+09:00'), '%y-%m-%d') < '15-09-22'
group by 
date_format(convert_tz(A.server_time,'+00:00','+09:00'), '%y-%m-%d'),
substr(pcid, 18, 18)%2;














select 

substr(pcid, 18, 18)%2