###################################################################################
#Change The Following Pointers to get your result
# '2015-12' --> Period
#@CUSTOMER_NAME_DB - Customer Name Query DB, example: 'mutnam_consult'
#@CUSTOMER_NAME_RBLOG - Customer Name raw log, example: 'mutnam_rblog'
###################################################################################

drop table if exists @CUSTOMER_NAME_consult.tb_log_view_daily_report;
create table @CUSTOMER_NAME_consult.tb_log_view_daily_report as
select
	server_time,
	DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'), '%Y-%m-%d') AS date,
	(case when device = '1' then 'PW' else 'MW' end) as device, 
	pcid pc_id,	
	reco,
	adcode,
	item_id,
	push_id
from 
@CUSTOMER_NAME_rblog.tb_log_view
WHERE
    date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-04'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-05'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-06'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-07'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-08'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-09'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-10'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-11'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-12'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2016-01';

create index pc_id on tb_log_view_daily_report (pc_id);
create index server_time on tb_log_view_daily_report (server_time);
create index date on tb_log_view_daily_report (date);
create index device on tb_log_view_daily_report (device);

drop table if exists @CUSTOMER_NAME_consult.tb_log_order_daily_report;
create table @CUSTOMER_NAME_consult.tb_log_order_daily_report as
select
	server_time,
	DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'), '%Y-%m-%d') AS date,
	(case when device = '1' then 'PW' else 'MW' end) as device, 
	pcid pc_id,
	reco,
	order_id,
	adcode,
	item_num,
	revenue
from 
@CUSTOMER_NAME_rblog.tb_log_order
WHERE
     date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-04'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-05'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-06'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-07'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-08'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-09'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-10'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-11'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-12'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2016-01';


create index pc_id on tb_log_order_daily_report (pc_id);
create index server_time on tb_log_order_daily_report (server_time);
create index date on tb_log_order_daily_report (date);
create index order_id on tb_log_order_daily_report (order_id);
create index device on tb_log_order_daily_report (device);


##################
###report-daily###
##################


drop table if exists tb_order_item_report;
create table tb_order_item_report as
select
  order_id,
  item_id,
  quantity,
  max(item_name) item_name,
  max(price) original_price,
  max(COALESCE(cvar3, cvar2, cvar1, '')) category,
  coalesce(max(cvar1), '') category1,
  coalesce(max(cvar2), '') category2,
  coalesce(max(cvar3), '') category3,
  coalesce(max(cvar4), '') category4,
  coalesce(max(cvar5), '') category5
from
  @CUSTOMER_NAME_rblog.tb_log_order_item
where
  price is not null and price > 0 and
  ( date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-04'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-05'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-06'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-07'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-08'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-09'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-10'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-11'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2015-12'
	or date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2016-01')
group by order_id,item_id,quantity;

create index order_id on tb_order_item_report (order_id);
create index item_id on tb_order_item_report (item_id);
create index quantity on tb_order_item_report (quantity);


drop table if exists tb_log_order_daily_report_tmp;
CREATE TABLE tb_log_order_daily_report_tmp as
select
	a.server_time,
	a.date,
	a.device, 
	a.pc_id,
	a.reco,
	a.order_id,
	a.adcode,
	a.item_num,
	a.revenue,
	b.item_id,
	b.quantity,
	b.item_name,
	b.original_price price,
	b.category, 
	b.category1,
	b.category2,
	b.category3,
	b.category4,
	b.category5

from tb_log_order_daily_report a
inner join tb_order_item_report b on a.order_id=b.order_id;

create index server_time on tb_log_order_daily_report_tmp (server_time);
create index date on tb_log_order_daily_report_tmp (date);
create index device on tb_log_order_daily_report_tmp (device);
create index pc_id on tb_log_order_daily_report_tmp (pc_id);




drop table if exists @CUSTOMER_NAME_consult.tmp_recommend_result_view;
CREATE TABLE @CUSTOMER_NAME_consult.tmp_recommend_result_view as
select 
	date
	,pc_id 
	,device
	,count(*) as view
	,sum(case when reco is not null then 1 else 0 end) as reco_view
	,sum(case when reco is null then 1 else 0 end) as no_reco_view
from @CUSTOMER_NAME_consult.tb_log_view_daily_report
group by date , pc_id, device;



create index date on tmp_recommend_result_view (date);
create index pc_id on tmp_recommend_result_view (pc_id);
create index device on tmp_recommend_result_view (device);



drop table if exists @CUSTOMER_NAME_consult.tmp_recommend_result_order_s0;
CREATE TABLE @CUSTOMER_NAME_consult.tmp_recommend_result_order_s0 as
select
	A.date
	,A.pc_id 
	,A.order_id
	,A.device
	
	,sum(A.revenue) revenue
	,sum(case when A.reco is not null then A.revenue else 0 end) reco_revenue
	,sum(case when A.reco is null then A.revenue else 0 end) no_reco_revenue
	
	,count(distinct A.order_id) frequency
	,count(distinct case when A.reco is not null then A.order_id end) reco_frequency
	,count(distinct case when A.reco is null then A.order_id end) no_reco_frequency
	
from @CUSTOMER_NAME_consult.tb_log_order_daily_report A
group by A.date,A.pc_id,A.order_id,A.device;


create index date on @CUSTOMER_NAME_consult.tmp_recommend_result_order_s0 (date);
create index pc_id on @CUSTOMER_NAME_consult.tmp_recommend_result_order_s0 (pc_id);
create index order_id on @CUSTOMER_NAME_consult.tmp_recommend_result_order_s0 (order_id);
create index device on @CUSTOMER_NAME_consult.tmp_recommend_result_order_s0 (device);


drop table if exists @CUSTOMER_NAME_consult.tmp_recommend_result_order_s1;
CREATE TABLE @CUSTOMER_NAME_consult.tmp_recommend_result_order_s1 as
select 
	date
	,pc_id
	,order_id
	,device
	
	,count(distinct item_id) itemvar
	,count(distinct case when reco is not null then item_id end) reco_itemvar
	,count(distinct case when reco is null then item_id end) no_reco_itemvar
	
	,sum(quantity) quantity
	,sum(case when reco is not null then quantity else 0 end) reco_quantity
	,sum(case when reco is null then quantity else 0 end) no_reco_quantity

from @CUSTOMER_NAME_consult.tb_log_order_daily_report_tmp
group by 1,2,3,4;

create index date on @CUSTOMER_NAME_consult.tmp_recommend_result_order_s1 (date);
create index pc_id on @CUSTOMER_NAME_consult.tmp_recommend_result_order_s1 (pc_id);
create index order_id on @CUSTOMER_NAME_consult.tmp_recommend_result_order_s1 (order_id);
create index device on @CUSTOMER_NAME_consult.tmp_recommend_result_order_s1 (device);

drop table if exists @CUSTOMER_NAME_consult.tmp_recommend_result_order;
CREATE TABLE @CUSTOMER_NAME_consult.tmp_recommend_result_order as
select 
	A.date
	,A.pc_id
	,A.order_id
	,A.device
	,A.revenue
	,A.reco_revenue
	,A.no_reco_revenue
	,A.frequency
	,A.reco_frequency
	,A.no_reco_frequency
	,B.itemvar
	,B.reco_itemvar
	,B.no_reco_itemvar
	,B.quantity
	,B.reco_quantity
	,B.no_reco_quantity

from @CUSTOMER_NAME_consult.tmp_recommend_result_order_s0 A 
left join @CUSTOMER_NAME_consult.tmp_recommend_result_order_s1 B on A.pc_id = B.pc_id and A.date = B.date 
group by 1,2,3,4;


create index date on tmp_recommend_result_order (date);
create index pc_id on tmp_recommend_result_order (pc_id);
create index order_id on tmp_recommend_result_order (order_id);
create index device on tmp_recommend_result_order (device);


##drop-tmp_recommend_result_view
#drop table if exists tmp_recommend_result_view;
##end

##create-tmp_recommend_result_view
#CREATE TABLE tmp_recommend_result_view as
#select
#	date
#	,pc_id 
#	,device
#	,count(*) as view
#	,sum(case when rc_code is not null then 1 else 0 end) as reco_view
#	,sum(case when rc_code is null then 1 else 0 end) as no_reco_view
#from tb_log_view_daily_report
#group by date,pc_id, device;
##end

##drop-tmp_recommend_result_order
#drop table if exists tmp_recommend_result_order;
##end
##create-tmp_recommend_result_order
#CREATE TABLE tmp_recommend_result_order as
#select
#	A.date
#	,A.pc_id 
#	,A.order_id
#	,A.device
	
#	,sum(A.price*A.quantity) revenue
#	,sum(case when B.reco_view > 0 then A.price*A.quantity else 0 end) reco_revenue
#	,sum(case when B.reco_view = 0 or B.reco_view is null then A.price*A.quantity else 0 end) no_reco_revenue
	
#	,sum(distinct A.order_price) actual_payment
#	,sum(distinct case when B.reco_view > 0 then A.order_price else 0 end) reco_actual_payment
#	,sum(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_price else 0 end) no_reco_actual_payment
	
#	,sum(A.quantity) quantity
#	,sum(case when B.reco_view > 0 then A.quantity else 0 end) reco_quantity
#	,sum(case when B.reco_view = 0 or B.reco_view is null then A.quantity else 0 end) no_reco_quantity
	
#	,count(distinct A.item_id) itemvar
#	,count(distinct case when B.reco_view > 0 then A.item_id end) reco_itemvar
#	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.item_id end) no_reco_itemvar
	
#	,count(distinct A.order_id) frequency
#	,count(distinct case when B.reco_view > 0 then A.order_id end) reco_frequency
#	,count(distinct case when B.reco_view = 0 or B.reco_view is null then A.order_id end) no_reco_frequency
	
#from tb_log_order_daily_report A
#left join tmp_recommend_result_view B on A.pc_id = B.pc_id 
#group by A.date,A.pc_id,A.order_id,A.device;
##end


drop table if exists @CUSTOMER_NAME_consult.tb_recommend_loyal_group;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_loyal_group AS SELECT * FROM
    @CUSTOMER_NAME_consult.tmp_recommend_result_view
WHERE
	view > 1;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_reco_group;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_reco_group AS 
SELECT * FROM
    @CUSTOMER_NAME_consult.tmp_recommend_result_view
where reco_view > 0;

create index date on @CUSTOMER_NAME_consult.tb_recommend_loyal_group (date);
create index pc_id on @CUSTOMER_NAME_consult.tb_recommend_loyal_group (pc_id);

create index date on @CUSTOMER_NAME_consult.tb_recommend_reco_group (date);
create index pc_id on @CUSTOMER_NAME_consult.tb_recommend_reco_group (pc_id);



drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_uv_daily_device;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_uv_daily_device AS
select
	date
	,device
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv
    
from @CUSTOMER_NAME_consult.tmp_recommend_result_view
group by 1,2
order by 1,2;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_uv_daily_total;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_uv_daily_total AS
select
	date
	,'AL' device
	,count(distinct pc_id) uv
    ,count(distinct case when reco_view > 0 then pc_id end) reco_uv
    ,count(distinct case when reco_view = 0 then pc_id end) no_reco_uv
    ,count(distinct case when view > 1 then pc_id end) loyal_uv
from @CUSTOMER_NAME_consult.tmp_recommend_result_view
group by 1
order by 1;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_uv_daily_s1;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_uv_daily_s1 AS
select * from @CUSTOMER_NAME_consult.tb_recommend_result_uv_daily_device
union all
select * from @CUSTOMER_NAME_consult.tb_recommend_result_uv_daily_total;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_uv_daily;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_uv_daily AS
select
	*
	,(case when uv <> 0 then round(reco_uv / uv , 4) * 100 else null end) reco_ratio
from @CUSTOMER_NAME_consult.tb_recommend_result_uv_daily_s1;


drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_pv_daily_device;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_pv_daily_device AS
select
	date
	,device
	,sum(view) pv
    ,sum(reco_view) reco_pv
	,sum(no_reco_view) no_reco_pv
    
from @CUSTOMER_NAME_consult.tmp_recommend_result_view
group by 1,2
order by 1,2;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_pv_daily_total;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_pv_daily_total AS
select
	date
	,'AL' device
	,sum(view) pv
    ,sum(reco_view) reco_pv
	,sum(no_reco_view) no_reco_pv
    
from @CUSTOMER_NAME_consult.tmp_recommend_result_view
group by 1
order by 1;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_pv_daily_s1;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_pv_daily_s1 AS
select * from @CUSTOMER_NAME_consult.tb_recommend_result_pv_daily_device
union all
select * from @CUSTOMER_NAME_consult.tb_recommend_result_pv_daily_total;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_pv_daily;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_pv_daily AS
select
	*
	,(case when pv <> 0 then round( reco_pv/ pv, 4) * 100 else null end) reco_ratio
from @CUSTOMER_NAME_consult.tb_recommend_result_pv_daily_s1;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_frequency_daily_device;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_frequency_daily_device AS
select
	date
	,device
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency 
	,sum(no_reco_frequency) no_reco_frequency 
	    
    
from @CUSTOMER_NAME_consult.tmp_recommend_result_order
group by 1,2
order by 1,2;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_frequency_daily_total;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_frequency_daily_total AS
select
	date
	,'AL' device
	,sum(frequency) frequency
	,sum(reco_frequency) reco_frequency 
	,sum(no_reco_frequency) no_reco_frequency 
	
    
from @CUSTOMER_NAME_consult.tmp_recommend_result_order
group by 1
order by 1;


drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_frequency_daily_s1;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_frequency_daily_s1 AS
select * from @CUSTOMER_NAME_consult.tb_recommend_result_frequency_daily_device
union all
select * from @CUSTOMER_NAME_consult.tb_recommend_result_frequency_daily_total;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_frequency_daily;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_frequency_daily AS
select
	*
	,(case when frequency <> 0 then round( reco_frequency / frequency , 4) * 100 else null end) reco_ratio
from @CUSTOMER_NAME_consult.tb_recommend_result_frequency_daily_s1;







drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_revenue_daily_device;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_revenue_daily_device AS
select
	date
	,device
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
	,sum(no_reco_revenue) no_reco_revenue 
	,sum(case when pc_id is not null then revenue end) loyal_revenue    
    
from @CUSTOMER_NAME_consult.tmp_recommend_result_order
group by 1,2
order by 1,2;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_revenue_daily_total;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_revenue_daily_total AS
select
	date
	,'AL' device
	,sum(revenue) revenue
	,sum(reco_revenue) reco_revenue    
	,sum(no_reco_revenue) no_reco_revenue 
	,sum(case when pc_id is not null then revenue end) loyal_revenue    
    
from @CUSTOMER_NAME_consult.tmp_recommend_result_order
group by 1
order by 1;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_revenue_daily_s1;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_revenue_daily_s1 AS
select * from @CUSTOMER_NAME_consult.tb_recommend_result_revenue_daily_device
union all
select * from @CUSTOMER_NAME_consult.tb_recommend_result_revenue_daily_total;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_revenue_daily;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_revenue_daily AS
select
	*
	,(case when revenue <> 0 then round(reco_revenue / revenue  , 4) * 100 else null end) reco_ratio
from @CUSTOMER_NAME_consult.tb_recommend_result_revenue_daily_s1;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_prevenue_daily_s1_device;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_prevenue_daily_s1_device AS
select
	A.date
	,A.device
	,round(sum(A.revenue)/sum(A.frequency),2) prevenue
    ,round(sum(case when C.pc_id is not null then A.revenue end)/sum(case when C.pc_id is not null then A.frequency end),2) reco_prevenue
    ,round(sum(case when C.pc_id is null then A.revenue end)/sum(case when C.pc_id is null then A.frequency end),2) no_reco_prevenue
    ,round(sum(case when B.pc_id is not null then A.revenue end)/sum(case when B.pc_id is not null  then A.frequency end),2) loyal_prevenue
    
from @CUSTOMER_NAME_consult.tmp_recommend_result_order A
left join @CUSTOMER_NAME_consult.tb_recommend_loyal_group B on A.date = B.date and A.pc_id = B.pc_id
left join @CUSTOMER_NAME_consult.tb_recommend_reco_group C on A.date = C.date and A.pc_id = C.pc_id
group by 1,2
order by 1,2;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_prevenue_daily_s1_total;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_prevenue_daily_s1_total AS
select
	A.date
	,'AL' device
	
	,round(sum(A.revenue)/sum(A.frequency),2) prevenue
    ,round(sum(case when C.pc_id is not null then A.revenue end)/sum(case when C.pc_id is not null then A.frequency end),2) reco_prevenue
    ,round(sum(case when C.pc_id is null then A.revenue end)/sum(case when C.pc_id is null then A.frequency end),2) no_reco_prevenue
    ,round(sum(case when B.pc_id is not null then A.revenue end)/sum(case when B.pc_id is not null  then A.frequency end),2) loyal_prevenue
    
from @CUSTOMER_NAME_consult.tmp_recommend_result_order A
left join @CUSTOMER_NAME_consult.tb_recommend_loyal_group B on A.date = B.date and A.pc_id = B.pc_id
left join @CUSTOMER_NAME_consult.tb_recommend_reco_group C on A.date = C.date and A.pc_id = C.pc_id
group by 1
order by 1;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_prevenue_daily_s1;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_prevenue_daily_s1 AS
select * from @CUSTOMER_NAME_consult.tb_recommend_result_prevenue_daily_s1_device
union all
select * from @CUSTOMER_NAME_consult.tb_recommend_result_prevenue_daily_s1_total;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_prevenue_daily;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_prevenue_daily AS SELECT *,
    (case when prevenue <> 0  then round(reco_prevenue / (prevenue), 4) * 100 else null end) as reco_up_ratio
FROM @CUSTOMER_NAME_consult.tb_recommend_result_prevenue_daily_s1;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_quantity_daily_s1_device;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_quantity_daily_s1_device AS
select
	A.date, A.device
	,round(avg(A.quantity),3) quantity
    ,round(avg(case when C.pc_id is not null then A.quantity end),3) reco_quantity
    ,round(avg(case when C.pc_id is null then A.quantity end),3) no_reco_quantity
    ,round(avg(case when B.pc_id is not null then A.quantity end),3) loyal_quantity
from @CUSTOMER_NAME_consult.tmp_recommend_result_order A
left join @CUSTOMER_NAME_consult.tb_recommend_loyal_group B on A.date = B.date and A.pc_id = B.pc_id
left join @CUSTOMER_NAME_consult.tb_recommend_reco_group C on A.date = C.date and A.pc_id = C.pc_id
group by 1,2
order by 1,2;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_quantity_daily_s1_total;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_quantity_daily_s1_total AS
select
	A.date
	,'AL' device
	,round(avg(A.quantity),3) quantity
    ,round(avg(case when C.pc_id is not null then A.quantity end),3) reco_quantity
    ,round(avg(case when C.pc_id is null then A.quantity end),3) no_reco_quantity
    ,round(avg(case when B.pc_id is not null then A.quantity end),3) loyal_quantity
from @CUSTOMER_NAME_consult.tmp_recommend_result_order A
left join @CUSTOMER_NAME_consult.tb_recommend_loyal_group B on A.date = B.date and A.pc_id = B.pc_id
left join @CUSTOMER_NAME_consult.tb_recommend_reco_group C on A.date = C.date and A.pc_id = C.pc_id
group by 1,2
order by 1,2;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_quantity_daily_s1;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_quantity_daily_s1 AS
select * from @CUSTOMER_NAME_consult.tb_recommend_result_quantity_daily_s1_device
union all
select * from @CUSTOMER_NAME_consult.tb_recommend_result_quantity_daily_s1_total;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_quantity_daily;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_quantity_daily AS SELECT *,
    (case when quantity <> 0  then round(reco_quantity / (quantity), 4) * 100 else null end) as reco_up_ratio
FROM @CUSTOMER_NAME_consult.tb_recommend_result_quantity_daily_s1;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_click_daily_s1_device;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_click_daily_s1_device AS 
SELECT 
	date, device
	,round(AVG(view),3) click
    ,round(AVG(case when reco_view>0 then view end),3) reco_click
    ,round(AVG(case when reco_view=0 then view end),3) no_reco_click
    ,round(AVG(case when view > 1 then view end),3) loyal_click
FROM
    @CUSTOMER_NAME_consult.tmp_recommend_result_view
group by 1,2
order by 1,2;


drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_click_daily_s1_total;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_click_daily_s1_total AS 
SELECT 
	date,'AL' device
	,round(AVG(view),3) click
    ,round(AVG(case when reco_view>0 then view end),3) reco_click
    ,round(AVG(case when reco_view=0 then view end),3) no_reco_click
    ,round(AVG(case when view > 1 then view end),3) loyal_click
FROM
    @CUSTOMER_NAME_consult.tmp_recommend_result_view
group by 1
order by 1;


drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_click_daily_s1;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_click_daily_s1 AS 
select * from @CUSTOMER_NAME_consult.tb_recommend_result_click_daily_s1_device
union all
select * from @CUSTOMER_NAME_consult.tb_recommend_result_click_daily_s1_total;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_click_daily;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_click_daily AS SELECT *,
    (case when click <> 0 then round(reco_click / (click), 4)*100 else null end) as reco_up_ratio
FROM @CUSTOMER_NAME_consult.tb_recommend_result_click_daily_s1;   

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_cvr_daily_s1_device;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_cvr_daily_s1_device AS 
select 
	A.date, A.device
	
	,round(sum(B.frequency)/count(distinct A.pc_id),4)*100 cvr
	,round(sum(case when A.reco_view >0 then B.frequency  end)/count(distinct case when A.reco_view >0 then A.pc_id end),4)*100 reco_cvr
	,round(sum(case when A.reco_view =0 then B.frequency  end)/count(distinct case when A.reco_view =0 then A.pc_id end),4)*100 no_reco_cvr
	,round(sum(case when A.view >1 then B.frequency end)/count(distinct case when A.view >1 then A.pc_id end),4)*100 loyal_cr
	
from @CUSTOMER_NAME_consult.tmp_recommend_result_view A
left join @CUSTOMER_NAME_consult.tmp_recommend_result_order B on A.date = B.date  and A.pc_id = B.pc_id
group by 1,2
order by 1,2;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_cvr_daily_s1_total;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_cvr_daily_s1_total AS 
select 
	A.date, 'AL' device
	
	,round(sum(B.frequency)/count(distinct A.pc_id),4)*100 cvr
	,round(sum(case when A.reco_view >0 then B.frequency  end)/count(distinct case when A.reco_view >0 then A.pc_id end),4)*100 reco_cvr
	,round(sum(case when A.reco_view =0 then B.frequency  end)/count(distinct case when A.reco_view =0 then A.pc_id end),4)*100 no_reco_cvr
	,round(sum(case when A.view >1 then B.frequency end)/count(distinct case when A.view >1 then A.pc_id end),4)*100 loyal_cr
	
from @CUSTOMER_NAME_consult.tmp_recommend_result_view A
left join @CUSTOMER_NAME_consult.tmp_recommend_result_order B on A.date = B.date  and A.pc_id = B.pc_id
group by 1
order by 1;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_cvr_daily_s1;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_cvr_daily_s1 AS 
select * from @CUSTOMER_NAME_consult.tb_recommend_result_cvr_daily_s1_device
union all
select * from @CUSTOMER_NAME_consult.tb_recommend_result_cvr_daily_s1_total;

drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_cvr_daily;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_cvr_daily AS SELECT *,
    (case when cvr <> 0 then round(reco_cvr / (cvr), 4)*100 else null end) as reco_up_ratio
FROM @CUSTOMER_NAME_consult.tb_recommend_result_cvr_daily_s1;   






drop table if exists @CUSTOMER_NAME_consult.tb_recommend_result_daily;
CREATE TABLE @CUSTOMER_NAME_consult.tb_recommend_result_daily AS 
select
a.date,
a.device,


a.uv unique_visit_total,
a.reco_uv unique_visit_reco,
a.reco_ratio unique_visit_rate,

c.pv page_view_total,
c.reco_pv page_view_reco,
c.reco_ratio page_view_rate,

d.frequency purchase_count_total,
d.reco_frequency purchase_count_reco,
d.reco_ratio purchase_count_rate,

e.revenue revenue_total,
e.reco_revenue revenue_reco,
e.reco_ratio revenue_rate,

f.prevenue customer_transaction_avg_total,
f.reco_prevenue customer_transaction_avg_reco,
f.reco_up_ratio customer_transaction_avg_result,

g.quantity purchase_item_count_avg_total,
g.reco_quantity purchase_item_count_avg_reco,
g.reco_up_ratio purchase_item_count_avg_result,

h.click visit_item_click_avg_total,
h.reco_click visit_item_click_avg_reco,
h.reco_up_ratio visit_item_click_avg_result,

i.cvr purchase_conversion_rate_total,
i.reco_cvr purchase_conversion_rate_reco,
i.reco_up_ratio purchase_conversion_rate_result


from 
@CUSTOMER_NAME_consult.tb_recommend_result_uv_daily a 
left join @CUSTOMER_NAME_consult.tb_recommend_result_pv_daily c on a.date = c.date and a.device = c.device
left join @CUSTOMER_NAME_consult.tb_recommend_result_frequency_daily d on a.date = d.date and a.device = d.device
left join @CUSTOMER_NAME_consult.tb_recommend_result_revenue_daily e on a.date = e.date and a.device = e.device
left join @CUSTOMER_NAME_consult.tb_recommend_result_prevenue_daily f on a.date = f.date and a.device = f.device
left join @CUSTOMER_NAME_consult.tb_recommend_result_quantity_daily g on a.date = g.date and a.device = g.device
left join @CUSTOMER_NAME_consult.tb_recommend_result_click_daily h on a.date = h.date and a.device = h.device
left join @CUSTOMER_NAME_consult.tb_recommend_result_cvr_daily i on a.date = i.date and a.device = i.device;

select * from @CUSTOMER_NAME_consult.tb_recommend_result_daily order by date,device;