select
	log_id,
	server_time,
	pcid,
	device,
	reco,
	order_id,
	item_num,
	revenue
from
	thebodyshop_rblog.tb_log_order
where date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = '2016-03';