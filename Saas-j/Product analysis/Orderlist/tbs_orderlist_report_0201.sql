set @year = '2015';
set @month = '10';
set @day = '10';

drop table if exists tbs_consult.tb_log_order_list_report;
CREATE TABLE tbs_consult.tb_log_order_list_report AS SELECT 
	convert_tz(A.server_time, '+00:00', '+09:00') server_time,
	A.user_id,
	A.reco,
    A.order_id,
	A.item_num,
	A.revenue,
	B.price,
	B.quantity,
	B.item_name
FROM
    tbs_rblog.tb_log_order A LEFT OUTER JOIN
	tbs_rblog.tb_log_order_item B on A.order_id = B.order_id
WHERE
    #server_time >= @time_cut_off_3m;
    date_format(convert_tz(A.server_time, '+00:00', '+09:00'), '%Y-%m') = concat(@year, '-', @month) AND
	reco is not null;


drop table if exists tbs_consult.tb_log_order_list_1510;
CREATE TABLE tbs_consult.tb_log_order_list_1510 AS SELECT 
	convert_tz(server_time, '+00:00', '+09:00') server_time,
	user_id,
	reco,
    order_id,
	revenue,
	item_num
FROM
    tbs_rblog.tb_log_order 
WHERE
    #server_time >= @time_cut_off_3m;
    date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = concat(@year, '-', @month) AND
	reco is not null;