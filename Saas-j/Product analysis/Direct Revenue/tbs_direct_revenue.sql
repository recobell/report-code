#############################################
####           여행박사 원간 리포트           ####
#############################################

#--Normal Revenue

drop table if exists tbs_consult.tb_normal_revenue;
create table tbs_consult.tb_normal_revenue as
select
	reco_revenue,
	total_revenue,
	(reco_revenue/total_revenue) as ratio
from
(select 
sum(case when reco is not null then revenue end) reco_revenue,
sum(revenue) total_revenue
from 
tbs_rblog.tb_log_order
where date_format(convert_tz(server_time, '+00:00', '+09:00'), '%y-%m-%d') between '16-02-01' and '16-02-29') a;



#--Refine Table

drop table if exists tbs_consult.tb_log_cv_rec_s1;
create table tbs_consult.tb_log_cv_rec_s1 as
select
	pcid
	,item_id
	,user_id
	,device
	,server_time
	,reco
from tbs_rblog.tb_log_view
where date_format(convert_tz(server_time, '+00:00', '+09:00'), '%y-%m-%d') between '16-02-01' and '16-02-29';
#where date_format(convert_tz(server_time, '+00:00','+09:00'), '%y-%m-%d') >= '15-12-21' and date_format(convert_tz(server_time, '+00:00','+09:00'), '%y-%m-%d') < '15-12-31';

drop table if exists tbs_consult.tb_log_cv_rec_s2;
create table tbs_consult.tb_log_cv_rec_s2 as
select
	a.server_time
	,a.pcid
	,a.device
	,a.user_id
	,a.order_id
	,a.item_num
	,a.revenue order_price
	,a.reco
	,b.item_id
	,b.price/b.quantity price
	,b.quantity
	,b.item_name
	,b.item_category
from tbs_rblog.tb_log_order a
inner join tbs_rblog.tb_log_order_item b on a.order_id = b.order_id
where date_format(convert_tz(a.server_time, '+00:00', '+09:00'), '%y-%m-%d') between '16-02-01' and '16-02-29';
#where date_format(convert_tz(a.server_time, '+00:00','+09:00'), '%y-%m-%d') >= '15-12-21' and date_format(convert_tz(a.server_time, '+00:00','+09:00'), '%y-%m-%d') < '15-12-31';

create index pcid on tbs_consult.tb_log_cv_rec_s1 (pcid);
create index reco on tbs_consult.tb_log_cv_rec_s1 (reco);
create index item_id on tbs_consult.tb_log_cv_rec_s1 (item_id);
create index pcid on tbs_consult.tb_log_cv_rec_s2 (pcid);
create index item_id on tbs_consult.tb_log_cv_rec_s2 (item_id);

###############
###  1 day  ###
###############

#--Timeframe Setting 

drop table if exists tbs_consult.tb_log_cv_rec_s3;
create table tbs_consult.tb_log_cv_rec_s3 as
select 
	a.pcid
	,a.reco
	,a.item_id
	,a.server_time view_time
	,b.server_time order_time
	,b.order_id
	,b.order_price
	,b.price
	,b.quantity
from tbs_consult.tb_log_cv_rec_s1 a
left join tbs_consult.tb_log_cv_rec_s2 b on a.pcid = b.pcid and a.item_id = b.item_id
where timestampdiff (minute, a.server_time, b.server_time )<1440 and timestampdiff (minute, a.server_time, b.server_time ) > 0
order by a.pcid, a.item_id, a.server_time;


drop table if exists tbs_consult.tb_log_cv_rec_s4;
create table tbs_consult.tb_log_cv_rec_s4 as
select 
	pcid
	,order_id
	,max(case when reco is not null then 1 else 0 end) reco
	,max(order_price) order_price
from tbs_consult.tb_log_cv_rec_s3
group by 1,2;

drop table if exists tbs_consult.tb_log_cv_rec_s4_0;
create table tbs_consult.tb_log_cv_rec_s4_0 as
select 
	a.pcid
	,a.order_id
	,b.reco
	,count(distinct a.item_id) item_num
from tbs_consult.tb_log_cv_rec_s2 a
inner join tbs_consult.tb_log_cv_rec_s4 b on a.order_id = b.order_id and a.pcid = b.pcid
group by 1,2,3;

drop table if exists tbs_consult.tb_log_cv_rec_s4_1;
create table tbs_consult.tb_log_cv_rec_s4_1 as
select 
	pcid
	,item_id
	,order_id
	,max(case when reco is not null then 1 else 0 end) reco
	,max(order_price) order_price
	,max(price) price
	,max(quantity) quantity
	
from tbs_consult.tb_log_cv_rec_s3
group by 1,2,3;

create index pcid on tbs_consult.tb_log_cv_rec_s4_1 (pcid);
create index item_id on tbs_consult.tb_log_cv_rec_s4_1 (item_id);

drop table if exists tbs_consult.tb_log_cv_rec_s4_1_2;
create table tbs_consult.tb_log_cv_rec_s4_1_2 as
select 
	pcid
	,item_id
	,max(case when reco is not null then 1 else 0 end) reco
	,avg(price) price
	,avg(quantity) quantity
from tbs_consult.tb_log_cv_rec_s3
group by 1,2;

create index pcid on tbs_consult.tb_log_cv_rec_s4_1_2 (pcid);
create index item_id on tbs_consult.tb_log_cv_rec_s4_1_2 (item_id);


#--추천 직접 매출 -> 객단가/구매건수
drop table if exists tbs_consult.tb_log_cv_rec_s4_2;
create table tbs_consult.tb_log_cv_rec_s4_2 as
select reco, avg(order_price) ct, count(*) freq from tbs_consult.tb_log_cv_rec_s4 group by reco;

#--추천 구매전환율 ->
drop table if exists tbs_consult.tb_log_cv_rec_s5;
create table tbs_consult.tb_log_cv_rec_s5 as
select 
	pcid
	,item_id
	,case when reco is not null then 1 else 0 end as reco
from tbs_consult.tb_log_cv_rec_s1 
group by 1,2,3;

create index pcid on tbs_consult.tb_log_cv_rec_s5 (pcid);
create index item_id on tbs_consult.tb_log_cv_rec_s5 (item_id);


drop table if exists tbs_consult.tb_log_cv_rec_s6;
create table tbs_consult.tb_log_cv_rec_s6 as
select 
	a.reco
	,a.item_id
	,sum(case when b.pcid is not null then 1 else 0 end)/sum(case when a.pcid is not null then 1 else 0 end) cv
	,sum(case when b.pcid is not null then 1 else 0 end) order_item
	,sum(case when a.pcid is not null then 1 else 0 end) view_item
	,avg(case when b.price is not null then b.price else null end) avg_price
from tbs_consult.tb_log_cv_rec_s5 a
left join tbs_consult.tb_log_cv_rec_s4_1_2 b on a.reco = b.reco and a.item_id= b.item_id and a.pcid = b.pcid
group by 1,2;


drop table if exists tbs_consult.tb_log_cv_rec_1440;
create table tbs_consult.tb_log_cv_rec_1440 as
select reco, sum(order_item)/sum(view_item) cv, sum(avg_price * order_item)/sum(order_item) avg_price from tbs_consult.tb_log_cv_rec_s6 where order_item >0 group by reco;


#==============================
#	     OUTPUT
#==============================

#--Normal Revenue
select * from tbs_consult.tb_normal_revenue;

#--Direct Revenue based on Revenue
select
	reco,
	sum(price) as reco_revenue
from tbs_consult.tb_log_cv_rec_s4_1_2
group by reco;

#--Direct Revenue in total
select
	sum(price) as total_revenue
from tbs_consult.tb_log_cv_rec_s4_1_2;


#--Direct Revenue in Ratio based on Reco
select
	a.reco,
	(a.reco_revenue/b.total_revenue) as ratio
from
	(select reco, sum(price) as reco_revenue from tbs_consult.tb_log_cv_rec_s4_1_2 group by reco) a,
	(select sum(price) as total_revenue from tbs_consult.tb_log_cv_rec_s4_1_2) b;

#--CVR based on Reco
select * from tbs_consult.tb_log_cv_rec_1440;

#--Average Price
select 
	reco,  
	avg(price), 
	avg(quantity),
	count(*) 
from tbs_consult.tb_log_cv_rec_s4_1_2 group by reco;

	

###############
###60 minutes##
###############

#--Timeframe Setting 

drop table if exists tbs_consult.tb_log_cv_rec_s3_60minutes;
create table tbs_consult.tb_log_cv_rec_s3_60minutes as
select 
	a.pcid
	,a.reco
	,a.item_id
	,a.server_time view_time
	,b.server_time order_time
	,b.order_id
	,b.order_price
	,b.price
	,b.quantity
from tbs_consult.tb_log_cv_rec_s1 a
left join tbs_consult.tb_log_cv_rec_s2 b on a.pcid = b.pcid and a.item_id = b.item_id
where timestampdiff (minute, a.server_time, b.server_time )<60 and timestampdiff (minute, a.server_time, b.server_time ) > 0
order by a.pcid, a.item_id, a.server_time;


drop table if exists tbs_consult.tb_log_cv_rec_s4_60minutes;
create table tbs_consult.tb_log_cv_rec_s4_60minutes as
select 
	pcid
	,order_id
	,max(case when reco is not null then 1 else 0 end) reco
	,max(order_price) order_price
from tbs_consult.tb_log_cv_rec_s3_60minutes
group by 1,2;

drop table if exists tbs_consult.tb_log_cv_rec_s4_0_60minutes;
create table tbs_consult.tb_log_cv_rec_s4_0_60minutes as
select 
	a.pcid
	,a.order_id
	,b.reco
	,count(distinct a.item_id) item_num
from tbs_consult.tb_log_cv_rec_s2 a
inner join tbs_consult.tb_log_cv_rec_s4_60minutes b on a.order_id = b.order_id and a.pcid = b.pcid
group by 1,2,3;

drop table if exists tbs_consult.tb_log_cv_rec_s4_1_60minutes;
create table tbs_consult.tb_log_cv_rec_s4_1_60minutes as
select 
	pcid
	,item_id
	,order_id
	,max(case when reco is not null then 1 else 0 end) reco
	,max(order_price) order_price
	,max(price) price
	,max(quantity) quantity
	
from tbs_consult.tb_log_cv_rec_s3_60minutes
group by 1,2,3;

create index pcid on tbs_consult.tb_log_cv_rec_s4_1_60minutes (pcid);
create index item_id on tbs_consult.tb_log_cv_rec_s4_1_60minutes (item_id);

drop table if exists tbs_consult.tb_log_cv_rec_s4_1_2_60minutes;
create table tbs_consult.tb_log_cv_rec_s4_1_2_60minutes as
select 
	pcid
	,item_id
	,max(case when reco is not null then 1 else 0 end) reco
	,avg(price) price
	,avg(quantity) quantity
from tbs_consult.tb_log_cv_rec_s3_60minutes
group by 1,2;

create index pcid on tbs_consult.tb_log_cv_rec_s4_1_2_60minutes (pcid);
create index item_id on tbs_consult.tb_log_cv_rec_s4_1_2_60minutes (item_id);


#--추천 직접 매출 -> 객단가/구매건수
drop table if exists tbs_consult.tb_log_cv_rec_s4_2_60minutes;
create table tbs_consult.tb_log_cv_rec_s4_2 as
select reco, avg(order_price) ct, count(*) freq from tbs_consult.tb_log_cv_rec_s4_60minutes group by reco;

#--추천 구매전환율 ->
drop table if exists tbs_consult.tb_log_cv_rec_s5_60minutes;
create table tbs_consult.tb_log_cv_rec_s5_60minutes as
select 
	
	pcid
	,item_id
	,case when reco is not null then 1 else 0 end as reco
from tbs_consult.tb_log_cv_rec_s1
group by 1,2,3;

create index pcid on tbs_consult.tb_log_cv_rec_s5_60minutes (pcid);
create index item_id on tbs_consult.tb_log_cv_rec_s5_60minutes (item_id);


drop table if exists tbs_consult.tb_log_cv_rec_s6_60minutes;
create table tbs_consult.tb_log_cv_rec_s6_60minutes as
select 
	a.reco
	,a.item_id
	,sum(case when b.pcid is not null then 1 else 0 end)/sum(case when a.pcid is not null then 1 else 0 end) cv
	,sum(case when b.pcid is not null then 1 else 0 end) order_item
	,sum(case when a.pcid is not null then 1 else 0 end) view_item
	,avg(case when b.price is not null then b.price else null end) avg_price
from tbs_consult.tb_log_cv_rec_s5_60minutes a
left join tbs_consult.tb_log_cv_rec_s4_1_2_60minutes b on a.reco = b.reco and a.item_id= b.item_id and a.pcid = b.pcid
group by 1,2;


drop table if exists tbs_consult.tb_log_cv_rec_60;
create table tbs_consult.tb_log_cv_rec_60 as
select reco, sum(order_item)/sum(view_item) cv, sum(avg_price * order_item)/sum(order_item) avg_price  from tbs_consult.tb_log_cv_rec_s6_60minutes where order_item >0 group by reco;


#==============================
#	     OUTPUT
#==============================

#--Direct Revenue based on Revenue
select
	reco,
	sum(price) as reco_revenue
from tbs_consult.tb_log_cv_rec_s4_1_2_60minutes
group by reco;

#--Direct Revenue in total
select
	sum(price) as total_revenue
from tbs_consult.tb_log_cv_rec_s4_1_2_60minutes;


#--Direct Revenue in Ratio based on Reco
select
	a.reco,
	(a.reco_revenue/b.total_revenue) as ratio
from
	(select reco, sum(price) as reco_revenue from tbs_consult.tb_log_cv_rec_s4_1_2_60minutes group by reco) a,
	(select sum(price) as total_revenue from tbs_consult.tb_log_cv_rec_s4_1_2_60minutes) b;

#--CVR based on Reco
select * from tbs_consult.tb_log_cv_rec_60;

#--Average Price
select 
	reco,  
	avg(price), 
	avg(quantity),
	count(*) 
from tbs_consult.tb_log_cv_rec_s4_1_2_60minutes group by reco;


###############################################################

/*
select * from tbs_consult.tb_log_cv_rec_s4_1 where reco =0;
select * from tbs_consult.tb_log_cv_rec_s6 where order_item <> 0;
*/


/*
drop table if exists tbs_consult.tb_log_cv_rec_2880;
create table tbs_consult.tb_log_cv_rec_2880 as
select reco, sum(order_item)/sum(view_item) cv, sum(avg_price * order_item)/sum(order_item) avg_price from tbs_consult.tb_log_cv_rec_s6 where order_item >0 group by reco;
*/

	

