select 
sum(case when reco is not null then revenue end) reco_revenue,
sum(revenue) total_revenue
from 
10x10_rblog.tb_log_order
where date_format(convert_tz(server_time, '+00:00', '+09:00'), '%y-%m') = '15-10';

--@onlyfor Wconcept----
select

B.item_id,
case when A.device =1 then 'PC' else 'MO' end as device,
B.item_name,
sum(B.quantity) quantity,
sum(B.price * quantity) revenue

from mcr_rblog.tb_log_order A
inner join mcr_rblog.tb_log_order_item B on A.order_id=B.order_id
where date_format(convert_tz(A.server_time, '+00:00', '+09:00'), '%y-%m') = '15-10'
and A.reco is not null
group by B.item_id
order by 2,5 desc;

--@end