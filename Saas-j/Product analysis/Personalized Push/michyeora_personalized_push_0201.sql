#CPS Based Marketing
#Michyeora Recent Click Reminder

 #First Time Run 
#create database mcr_marketing;

use mcr_marketing_test;

drop table if exists tb_campaign_interface;
create table tb_campaign_interface (
	user_id varchar(50),
	camp_type varchar(10),
	dest_type varchar(10),
	dest_addr varchar(100),
	tag1 varchar(200),
	tag2 varchar(200),
	tag3 varchar(200),
	tag4 varchar(200),
	tag5 varchar(200),
	tag6 varchar(200),
	tag7 varchar(200),
	tag8 varchar(200),
	tag9 varchar(200),
	tag10 varchar(200),
	create_time timestamp,
	sent_time timestamp
); 

create index user_id on tb_campaign_interface (user_id);
create index camp_type on tb_campaign_interface (camp_type);
create index dest_type on tb_campaign_interface (dest_type);
create index dest_addr on tb_campaign_interface (dest_addr);
create index tag2 on tb_campaign_interface (tag2);

drop table if exists tb_campaign_interface_hist;
create table tb_campaign_interface_hist (
	user_id varchar(50),
	camp_type varchar(10),
	dest_type varchar(10),
	dest_addr varchar(100),
	tag1 varchar(200),
	tag2 varchar(200),
	tag3 varchar(200),
	tag4 varchar(200),
	tag5 varchar(200),
	tag6 varchar(200),
	tag7 varchar(200),
	tag8 varchar(200),
	tag9 varchar(200),
	tag10 varchar(200),
	create_time timestamp,
	sent_time timestamp
); 

create index user_id on tb_campaign_interface_hist (user_id);
create index camp_type on tb_campaign_interface_hist (camp_type);
create index dest_type on tb_campaign_interface_hist (dest_type);
create index dest_addr on tb_campaign_interface_hist (dest_addr);
create index tag2 on tb_campaign_interface_hist (tag2);

drop table if exists tb_login_info;
create table tb_login_info (
	pcid varchar(100),
	adid varchar(100),
	user_id varchar(100),
	server_time timestamp
);

drop table if exists tb_campaign_open_log;
create table tb_campaign_open_log (
	camp_key varchar(100), 
	camp_type varchar(10), 
	user_id varchar(100),
	adid varchar(100), 
	status_type int,  #0 sent, 1 open
	insert_time timestamp, 
	insert_date date
);

create index camp_key on tb_campaign_open_log (camp_key); 
create index camp_type on tb_campaign_open_log (camp_type); 
create index user_id on tb_campaign_open_log (user_id); 
create index adid on tb_campaign_open_log (adid); 
create index status_type on tb_campaign_open_log (status_type); 
create index insert_time on tb_campaign_open_log (insert_time); 
create index insert_date on tb_campaign_open_log (insert_date); 

drop table if exists tb_content_center;
create table tb_content_center (
	camp_type varchar(10),
	camp_title varchar(100),
	camp_key varchar(100),
	create_date date,
	device varchar(10),
	content_type varchar(20),
	position int(10),
	pid varchar(20) not null,
	pnm varchar(255) default null,
	original_price int default null,
	sale_price int default null,
	pimg varchar(255) default null,
	catlvl1 varchar(20) default null,
	catlvl2 varchar(20) default null,
	catlvl3 varchar(20) default null,
	pdesc varchar(255) default null,
	ptag varchar(1024) default null
);

create index camp_type on tb_content_center(camp_type);
create index camp_key on tb_content_center(camp_key);
create index camp_title on tb_content_center(camp_title);
create index position on tb_content_center(position);
create index pid on tb_content_center(pid);
create index content_type on tb_content_center(content_type);




drop table if exists tb_campaign_title;
create table tb_campaign_title (
	camp_type varchar(10),
	camp_day int,
	camp_title varchar(500),
	create_time timestamp,
	update_time timestamp,
	tag_adcode varchar(20)
);

create index camp_type on tb_campaign_title (camp_type); 
create index camp_day on tb_campaign_title (camp_day); 

insert into tb_campaign_title values 
('P01', 	1,	'[광고][$_RND_$] 명이 집중하고 있는 상품!, 이 상품은 당신의 것입니다!'	, now(), now(), 	'pm_recent_click_01'	) ,
('P01', 	2,	'[광고]같은 광고 아닌 광고 같은 나~'	, now(), now(), 	'pm_recent_click_01'	) ,
('P01', 	3,	'[광고]함께라면 당신도 스타!'	, now(), now(), 	'pm_recent_click_01'	) ,
('P01', 	4,	'[광고][$_STORE_$] 에서 강추하는 스타일!'	, now(), now(), 	'pm_recent_click_01'	) ,
('P01', 	5,	'[광고]당신에게 꼭 맞는 스타일!'	, now(), now(), 	'pm_recent_click_01'	) ,
('P01', 	6,	'[광고]불금!! 썸남한테 먹히는 [$_STORE_$] 수타일~' 	, now(), now(), 	'pm_recent_click_01'	) ,
('P01', 	7,	'[광고]신나는 주말은 [$_STORE_$]과 함께!'	, now(), now(), 	'pm_recent_click_01'	) ,
					
('P02', 	1,	'[광고][$_STORE_$]에 오셔서 이번주 시작하면서 새로운 스타일을 준비하세요^^'	, now(), now(), 	'pm_churn_01'	) ,
('P02', 	2,	'[광고]우쭈쭈~ 월요병 걸리지 마세요~[$_STORE_$]가 있잖아요!'	, now(), now(), 	'pm_churn_01'	) ,
('P02', 	3,	'[광고][$_STORE_$]에서 방금 입고된 스타일!'	, now(), now(), 	'pm_churn_01'	) ,
('P02', 	4,	'[광고]너무 이뻐서 미안하다..[$_STORE_$]'	, now(), now(), 	'pm_churn_01'	) ,
('P02', 	5,	'[광고]당황하지 않고 [$_STORE_$]에 들러서 빡~ 끝!'	, now(), now(), 	'pm_churn_01'	) ,
('P02', 	6,	'[광고]불금!! 썸남한테 먹히는 [$_STORE_$] 수타일~'	, now(), now(), 	'pm_churn_01'	) ,
('P02', 	7,	'[광고]내꺼 같은 내꺼 아닌 내꺼 같은 [$_STORE_$]'	, now(), now(), 	'pm_churn_01'	) ,
					
('P03', 	1,	'[광고]같이 코디할 그것은!'	, now(), now(), 	'pm_asc_pur_01'	) ,
('P03', 	2,	'[광고]신에게는 12벌의 추천상품이 있습니다~~ '	, now(), now(), 	'pm_asc_pur_01'	) ,
('P03', 	3,	'[광고][$_STORE_$] 로맨틱 성공적'	, now(), now(), 	'pm_asc_pur_01'	) ,
('P03', 	4,	'[광고]같이 안 사면 앙대영~'	, now(), now(), 	'pm_asc_pur_01'	) ,
('P03', 	5,	'[광고]특급 추천이야'	, now(), now(), 	'pm_asc_pur_01'	) ,
('P03', 	6,	'[광고]느낌 아니까~당신에게는 이것'	, now(), now(), 	'pm_asc_pur_01'	) ,
('P03', 	7,	'[광고][$_STORE_$] 스타일은 완생이야~'	, now(), now(), 	'pm_asc_pur_01'	) ,
					
('P04', 	1,	'[광고] 왜 오늘의 행복을 내일로 미루고 있나요'	, now(), now(), 	'pm_three_click_01'	) ,
('P04', 	2,	'[광고]오늘의 선택은!?'	, now(), now(), 	'pm_three_click_01'	) ,
('P04', 	3,	'[광고] 그냥 너는 웃는 얼굴이 이뻐'	, now(), now(), 	'pm_three_click_01'	) ,
('P04', 	4,	'[광고]지만 광고같지 않은 특급상품!'	, now(), now(), 	'pm_three_click_01'	) ,
('P04', 	5,	'[광고]그래 이거야'	, now(), now(), 	'pm_three_click_01'	) ,
('P04', 	6,	'[광고]머리에 멤돌던 그것!!'	, now(), now(), 	'pm_three_click_01'	) ,
('P04', 	7,	'[광고]같은 광고!'	, now(), now(), 	'pm_three_click_01'	) ;


######################################################################################
#Collect One Day User Behavior Logs
#Click and Purchase  
######################################################################################

use mcr_marketing_test;

set @url := 'http://m.michyeora.com/product/detail.html';
set @mall_name := '미쳐라';
set @url_deatil := '?product_no=';

drop table if exists tb_recent_click_step0; 
create table tb_recent_click_step0 as
	select
		*
	from
		mcr_rblog.tb_log_view
	where
		adid is not null
		and server_time >= date_add(now(),interval -24 hour);

create index item_id on tb_recent_click_step0 (item_id);
create index adid on tb_recent_click_step0 (adid);
create index server_time on tb_recent_click_step0 (server_time);

drop table if exists tb_recent_purchase_step0; 
create table tb_recent_purchase_step0 as
	select
		a.server_time, a.pcid, a.adid, a.device, a.user_id,
		a.order_id, b.item_id, b.price, b.quantity, b.item_category
	from
		mcr_rblog.tb_log_order a
		inner join  mcr_rblog.tb_log_order_item b
			on a.order_id = b.order_id
	where
		a.server_time >= date_add(now(),interval -24 hour);
	#where adid is not null 

create index item_id on tb_recent_purchase_step0 (item_id);
create index adid on tb_recent_purchase_step0 (adid);
create index server_time on tb_recent_purchase_step0 (server_time);

######################################################################################
#Create Login Information 																								 #
######################################################################################

#Insert New Login Information 
set @prev_pid := '';
set @current_pid_cnt := 0;
set @num_prd := 0;

drop table if exists tb_login_info_step0; 
create table tb_login_info_step0 as 
	select
		*
	from (
		select
			a.*,
			@num_prd:=(if(a.user_id=@prev_pid, @current_pid_cnt:=@current_pid_cnt+1, @current_pid_cnt:=1)) as number,
			@prev_pid:=a.user_id as tmp_pid 
		from (
			select
				pcid, adid, user_id, server_time
			from
				tb_recent_click_step0
			where
				user_id is not null 
			order by pcid, server_time desc
		) a
	) a
	where number = 1;

drop table if exists tb_login_info_merge;
create table tb_login_info_merge as
	select
		pcid, adid, user_id, server_time 
	from
		tb_login_info_step0;


set @prev_pid := '';
set @current_pid_cnt := 0;
set @num_prd := 0;

drop table if exists tb_login_info_step1; 
create table tb_login_info_step1 as 
	select
		*
	from (
		select
			a.*,
			@num_prd:=(if(a.user_id=@prev_pid, @current_pid_cnt:=@current_pid_cnt+1, @current_pid_cnt:=1)) as number,
			@prev_pid:=a.user_id as tmp_pid
		from (	
			select
				pcid, adid, user_id, server_time 
			from
				tb_recent_purchase_step0
			where
				user_id is not null 
			order by pcid, server_time desc
		) a 
	) a
	where number = 1; 

insert into tb_login_info_merge 
	select
		pcid, adid, user_id, server_time
	from
		tb_login_info_step1;


#Insert Existing Login Information 
insert into tb_login_info_merge 
	select
		pcid, adid, user_id, server_time
	from
		tb_login_info;


#Finalize Login Information  
drop table if exists tb_login_info;
create table tb_login_info (
	pcid varchar(100),
	adid varchar(100),
	user_id varchar(100),
	server_time timestamp
);


set @prev_pid := '';
set @current_pid_cnt := 0;
set @num_prd := 0;

insert into tb_login_info
	select pcid, adid, user_id, server_time
	from (
		select
			a.*,
			@num_prd:=(if(a.user_id=@prev_pid, @current_pid_cnt:=@current_pid_cnt+1, @current_pid_cnt:=1)) as number,
			@prev_pid:=a.user_id as tmp_pid
		from (
			select
				*
			from
				tb_login_info_merge 
			order by user_id, server_time desc
		) a 
	) a
	where number = 1;

create index pcid on tb_login_info(pcid); 
create index adid on tb_login_info(adid); 
create index user_id on tb_login_info(user_id); 



######################################################################################
#Define Target Object 
# - Retrieve Last Click
# - campaign personal key = user_id + @campaign_code + target_item 
######################################################################################


######################################################################################
#Define Target Object 
# - Retrieve Next View
# - campaign personal key = user_id + @campaign_code + target_item 
######################################################################################


drop table if exists mcr_marketing_test.tb_asc_recmd_click_info;
create table mcr_marketing_test.tb_asc_recmd_click_info as
 select
 a.login_id,
 a.pid,
 conv(substr(sha(a.uid),1,6),16,10)%10 login_id_index,
 conv(substr(sha(a.pid),1,6),16,10)%100 pid_index,
 round((1-timestampdiff(day,a.click_date, now())/31),4) recency_score, 
 a.click_timestamp
 from
 mcr_rengine.tb_clk_hist_in a 
 inner join  mcr_rengine.tb_prd_info_in b on a.pid = b.pid
 where
 a.login_id is not null;

create index login_id on mcr_marketing_test.tb_asc_recmd_click_info(login_id);
create index pid on mcr_marketing_test.tb_asc_recmd_click_info(pid);
create index pid_login_id on mcr_marketing_test.tb_asc_recmd_click_info(pid, login_id);
create index login_id_index on mcr_marketing_test.tb_asc_recmd_click_info(login_id_index);
create index pid_index on mcr_marketing_test.tb_asc_recmd_click_info(pid_index);
create index login_id_type_pid_recency on mcr_marketing_test.tb_asc_recmd_click_info(login_id, login_id_index, pid, recency_score);




drop table if exists mcr_marketing_test.tb_asc_recmd_order_info;
create table mcr_marketing_test.tb_asc_recmd_order_info as
 select
 a.login_id,
 b.pid,
 conv(substr(sha(a.uid),1,6),16,10)%10 login_id_index,
 conv(substr(sha(a.pid),1,6),16,10)%100 pid_index,
 round((1-timestampdiff(day,a.order_date, now())/31),4) recency_score, 
 a.order_timestamp
 from
 mcr_rengine.tb_pur_hist_in a
 inner join mcr_rengine.tb_asc_recmd_step0 b on a.pid = b.pid;

create index pid on mcr_marketing_test.tb_asc_recmd_order_info(pid);
create index login_id on mcr_marketing_test.tb_asc_recmd_order_info(login_id);
create index pid_index on mcr_marketing_test.tb_asc_recmd_order_info(pid_index);
create index login_id_index on mcr_marketing_test.tb_asc_recmd_order_info(login_id_index);


set @prev_pid := '';
set @current_pid_cnt := 0;
set @num_prd := 0;

drop table if exists mcr_marketing_test.tb_asc_recmd_click_step1; 
create table mcr_marketing_test.tb_asc_recmd_click_step1 as 
select login_id, pid, click_timestamp, number next, number-1 pre
from (
 select a.*,
 @num_prd:=(if(a.login_id=@prev_pid, @current_pid_cnt:=@current_pid_cnt+1, @current_pid_cnt:=1)) as number,
 @prev_pid:=a.login_id as tmp_pid
 from ( 
 select * 
 from mcr_marketing_test.tb_asc_recmd_click_info 
 order by login_id, click_timestamp
 ) a 
) a;

create index pre on mcr_marketing_test.tb_asc_recmd_click_step1(pre); 
create index next on mcr_marketing_test.tb_asc_recmd_click_step1(next); 
create index login_id on mcr_marketing_test.tb_asc_recmd_click_step1(login_id); 


set @prev_pid := '';
set @current_pid_cnt := 0;
set @num_prd := 0;

drop table if exists mcr_marketing_test.tb_asc_recmd_order_step1; 
create table mcr_marketing_test.tb_asc_recmd_order_step1 as 
select login_id, pid, order_timestamp, number next, number-1 pre
from (
 select a.*,
 @num_prd:=(if(a.login_id=@prev_pid, @current_pid_cnt:=@current_pid_cnt+1, @current_pid_cnt:=1)) as number,
 @prev_pid:=a.login_id as tmp_pid
 from ( 
 select * 
 from mcr_marketing_test.tb_asc_recmd_order_info 
 order by login_id, order_timestamp
 ) a 
) a;

create index pre on mcr_marketing_test.tb_asc_recmd_order_step1(pre); 
create index next on mcr_marketing_test.tb_asc_recmd_order_step1(next); 
create index login_id on mcr_marketing_test.tb_asc_recmd_order_step1(login_id); 


drop table if exists mcr_marketing_test.tb_asc_recmd_click_step2; 
create table mcr_marketing_test.tb_asc_recmd_click_step2 as 
select A.pid target_pid, B.pid cross_sell_pid, count(*) score, now() create_dts
from mcr_marketing_test.tb_asc_recmd_click_step1 A 
inner join mcr_marketing_test.tb_asc_recmd_click_step1 B on A.login_id = B.login_id and A.next = B.pre 
group by A.pid, B.pid;


drop table if exists mcr_marketing_test.tb_asc_recmd_order_step2; 
create table mcr_marketing_test.tb_asc_recmd_order_step2 as 
select A.pid target_pid, B.pid cross_sell_pid, count(*) score, now() create_dts
from mcr_marketing_test.tb_asc_recmd_order_step1 A 
inner join mcr_marketing_test.tb_asc_recmd_order_step1 B on A.login_id = B.login_id and A.next = B.pre 
group by A.pid, B.pid;



drop table if exists mcr_marketing_test.tb_asc_recmd_click_hist;
create table mcr_marketing_test.tb_asc_recmd_click_hist as
select * from mcr_marketing_test.tb_asc_recmd_click_step2;

drop table if exists mcr_marketing_test.tb_asc_recmd_click_hist_tmp; 
rename table mcr_marketing_test.tb_asc_recmd_click_hist to mcr_marketing_test.tb_asc_recmd_click_hist_tmp; 

drop table if exists mcr_marketing_test.tb_asc_recmd_click_hist;
create table mcr_marketing_test.tb_asc_recmd_click_hist  as 
select * from mcr_marketing_test.tb_asc_recmd_click_hist_tmp 
where create_dts >= date_add(now(), interval -30 day);


drop table if exists mcr_marketing_test.tb_asc_recmd_click_result;
create table mcr_marketing_test.tb_asc_recmd_click_result  as 
select target_pid, cross_sell_pid, sum(score * (datediff(now(), create_dts) + 1)) score 
from mcr_marketing_test.tb_asc_recmd_click_hist 
group by target_pid, cross_sell_pid; 



drop table if exists mcr_marketing_test.tb_asc_recmd_order_hist;
create table mcr_marketing_test.tb_asc_recmd_order_hist as
select * from mcr_marketing_test.tb_asc_recmd_order_step2;

drop table if exists mcr_marketing_test.tb_asc_recmd_order_hist_tmp; 
rename table mcr_marketing_test.tb_asc_recmd_order_hist to mcr_marketing_test.tb_asc_recmd_order_hist_tmp; 

drop table if exists mcr_marketing_test.tb_asc_recmd_order_hist;
create table mcr_marketing_test.tb_asc_recmd_order_hist  as 
select * from mcr_marketing_test.tb_asc_recmd_order_hist_tmp 
where create_dts >= date_add(now(), interval -30 day);


drop table if exists mcr_marketing_test.tb_asc_recmd_order_result;
create table mcr_marketing_test.tb_asc_recmd_order_result  as 
select target_pid, cross_sell_pid, sum(score * (datediff(now(), create_dts) + 1)) score 
from mcr_marketing_test.tb_asc_recmd_order_hist 
group by target_pid, cross_sell_pid; 


#Merge Click and Order Based Association Rules without Category Cap
drop table if exists mcr_marketing_test.tb_asc_recmd_result_merge;
CREATE TABLE mcr_marketing_test.tb_asc_recmd_result_merge AS SELECT a.target_pid, a.cross_sell_pid, a.score FROM
    (SELECT 
        target_pid,
            cross_sell_pid,
            ROUND(SUM(score) * SUM(priority), 4) score
    FROM
        (SELECT 
        a.*, 1 priority
    FROM
        mcr_marketing_test.tb_asc_recmd_order_result a UNION SELECT 
        a.*, 2 priority
    FROM
        mcr_marketing_test.tb_asc_recmd_click_result a) a
    GROUP BY target_pid , cross_sell_pid) a
ORDER BY a.target_pid , a.score DESC;

create index target_pid on mcr_marketing_test.tb_asc_recmd_result_merge(target_pid);
create index cross_sell_pid on mcr_marketing_test.tb_asc_recmd_result_merge(cross_sell_pid);

drop table if exists mcr_marketing_test.tb_asc_recmd_next_view_set;
CREATE TABLE mcr_marketing_test.tb_asc_recmd_next_view_set AS
select target_pid, cross_sell_pid, score from mcr_marketing_test.tb_asc_recmd_result_merge
group by target_pid
order by target_pid, score desc;



###################################################################################
#Campaign 1
#Recent Click Product and Next view product
###################################################################################

set @camp_title := 'last_click'; 
set @camp_type := 'P01';
set @camp_key_value := '_click_';





set @prev_pid := '';
set @current_pid_cnt := 0;
set @num_prd := 0;

drop table if exists tb_recent_click_step1; 
create table tb_recent_click_step1 as 
	select
		*
	from (
		select
			a.*,
			@num_prd:=(if(a.adid=@prev_pid, @current_pid_cnt:=@current_pid_cnt+1, @current_pid_cnt:=1)) as number, 
			@prev_pid:=a.adid as tmp_pid 
		from (	
			select
				a.pcid, a.adid, a.server_time, a.device,
				b.pid, b.pnm, b.original_price, b.sale_price, b.pimg,
				b.site_catlvl1 catlvl1,
				b.site_catlvl2 catlvl2,
				b.site_catlvl3 catlvl3,
				b.pdesc, b.ptag
            
			from
				tb_recent_click_step0 a 
				inner join mcr_service.tb_prd_info b
					on a.item_id = b.pid
			order by a.adid, a.server_time desc
		) a
	) a
	where number = 1;


drop table if exists tb_recent_click_filter; 
CREATE TABLE tb_recent_click_filter AS SELECT a.pcid FROM
    tb_recent_click_step1 a
        INNER JOIN
    mcr_rblog.tb_log_order b ON a.pcid = b.pcid
        INNER JOIN
    mcr_rblog.tb_log_order_item c ON b.order_id = c.order_id
WHERE
    c.server_time >= DATE_ADD(NOW(), INTERVAL - 30 DAY)
        AND a.pid = c.item_id;
    
 
drop table if exists tb_recent_click_next_view; 
create table tb_recent_click_next_view as
select 
	a.pcid, 
    a.adid, 
    a.server_time, 
    a.device, 
    (case when c.cross_sell_pid is not null then c.cross_sell_pid
		  else a.pid end) pid, 
    
    
    (case when c.cross_sell_pid is not null then d.pnm 
          else a.pnm end) pnm, 
    (case when c.cross_sell_pid is not null then d.original_price
          else a.original_price end) original_price, 
    (case when c.cross_sell_pid is not null then d.sale_price else a.sale_price end) sale_price, 
    (case when c.cross_sell_pid is not null then d.pimg else a.pimg end) pimg,
    (case when c.cross_sell_pid is not null then d.site_catlvl1 else a.catlvl1 end) catlvl1, 
    (case when c.cross_sell_pid is not null then d.site_catlvl2 else a.catlvl2 end) catlvl2, 
    (case when c.cross_sell_pid is not null then d.site_catlvl3 else a.catlvl3 end) catlvl3,
    #(case when b.pcid is not null then 'y' else 'n' end) purchased,
    #(case when c.cross_sell_pid is not null then 'y' else 'n' end) nextview
    (case when c.cross_sell_pid is not null then d.pdesc else a.pdesc end) pdesc, 
    (case when c.cross_sell_pid is not null then d.ptag else a.ptag end) ptag
    
    
    from 
	tb_recent_click_step1 a
    #left join tb_recent_click_filter b
    #on a.pcid = b.pcid
    left join tb_asc_recmd_next_view_set c
    on a.pid = c.target_pid
    inner join mcr_service.tb_prd_info d
	on c.cross_sell_pid = d.pid;


drop table if exists tb_target_item_1; 
create table tb_target_item_1 as
	select
		a.*,
		concat(user_id, @camp_key_value , pid) camp_key,
		@camp_type camp_type,
		@camp_title camp_title
	from (
		select
			a.pcid,
			a.adid,
			(case when b.user_id is null then a.pcid else user_id end) user_id,
			a.server_time,
			a.device,
			a.pid, a.pnm, a.original_price, a.sale_price, a.pimg,
			a.catlvl1, a.catlvl2, a.catlvl3, a.pdesc, a.ptag,
			current_date create_date
		from
			tb_recent_click_next_view a
			left outer join tb_login_info b
				on a.pcid = b.pcid
	) a;

create index pid on tb_target_item_1 (pid); 
create index create_date on tb_target_item_1 (create_date);

/*
drop table if exists tb_target_item_1; 
create table tb_target_item_1 as
	select
		a.*,
		concat(user_id, @camp_key_value , pid) camp_key,
		@camp_type camp_type,
		@camp_title camp_title
	from (
		select
			a.pcid,
			a.adid,
			(case when b.user_id is null then a.pcid else user_id end) user_id,
			a.server_time,
			a.device,
			a.pid, a.pnm, a.original_price, a.sale_price, a.pimg,
			a.catlvl1, a.catlvl2, a.catlvl3, a.pdesc, a.ptag,
			current_date create_date
		from
			tb_recent_click_step1 a
			left outer join tb_login_info b
				on a.pcid = b.pcid
	) a;

create index pid on tb_target_item_1 (pid); 
create index create_date on tb_target_item_1 (create_date);
*/




###################################################################################
#Campaign 2
#Protect Churning
###################################################################################

set @camp_title := 'protect_churn'; 
set @camp_type := 'P02';
set @camp_key_value := '_protect_churn_';



drop table if exists tb_churn_step0; 
create table tb_churn_step0  as
select
	
    a.pcid
    ,a.adid
    ,a.pcid user_id
    ,a.server_time
    ,a.device
from
(select
	server_time
    ,pcid
    ,adid
    ,device
    ,pcid user_id
		,min(sday) first_purchase
        ,max(next_sday)  last_purchase
        ,timestampdiff(day, max(next_sday), current_date()) recency
        ,count(*) frequency
        ,avg(purchase_interval) avg_purchase_interval
        ,min(purchase_interval) min_purchase_interval
        ,max(purchase_interval) max_purchase_interval
        ,(case when timestampdiff(day, max(next_sday), current_date) >= max(purchase_interval)*1.5 then 6
								 when timestampdiff(day, max(next_sday), current_date) > max(purchase_interval) then 5
                                 when timestampdiff(day, max(next_sday), current_date) = max(purchase_interval) then 4
								 when timestampdiff(day, max(next_sday), current_date) >= avg(purchase_interval) then 3
								 when timestampdiff(day, max(next_sday), current_date) < avg(purchase_interval) then 2
								 when timestampdiff(day, max(next_sday), current_date) < 7 then 1
						else 0 end) churn_level
from
(select a.server_time
		,a.pcid
		,a.adid
        ,a.device
        ,a.user_id
			,a.sday
            ,min(b.next_sday) next_sday
            ,timestampdiff(day, a.sday, b.next_sday) purchase_interval
from
	(SELECT 
        server_time, pcid, adid, device, user_id, server_time sday
    FROM
        mcr_rblog.tb_log_order
    WHERE
        adid IS NOT NULL
            AND server_time >= DATE_ADD(NOW(), INTERVAL - 90 DAY)
    ORDER BY adid , server_time desc) a
    inner join 
    (SELECT 
        server_time, pcid, adid, device, user_id, server_time next_sday
    FROM
        mcr_rblog.tb_log_order
    WHERE
        adid IS NOT NULL
            AND server_time >= DATE_ADD(NOW(), INTERVAL - 90 DAY)
    ORDER BY adid , server_time desc) b
    on a.adid = b.adid
    where a.sday < b.next_sday
	group by a.adid, a.sday
) a 
group by adid
having(count(*))>=2) a
where churn_level=4;

drop table if exists tb_churn_step1; 
CREATE TABLE tb_churn_step1 AS SELECT pcid,
    adid,
    server_time,
    device,
    pid,
    pnm,
    original_price,
    sale_price,
    pimg,
    site_catlvl1 catlvl1,
    site_catlvl2 catlvl2,
    site_catlvl3 catlvl3,
    pdesc,
    ptag FROM
    tb_churn_step0,
    (SELECT 
        *
    FROM
        mcr_service.tb_cat_best
    ORDER BY score DESC
    LIMIT 1) b;

drop table if exists tb_target_item_2; 
create table tb_target_item_2 as
	select
		a.*,
		concat(user_id, @camp_key_value , pid) camp_key,
		@camp_type camp_type,
		@camp_title camp_title
	from (
		select
			a.pcid,
			a.adid,
			(case when b.user_id is null then a.pcid else user_id end) user_id,
			a.server_time,
			a.device,
			a.pid, a.pnm, a.original_price, a.sale_price, a.pimg,
			a.catlvl1, a.catlvl2, a.catlvl3, a.pdesc, a.ptag,
			current_date create_date
		from
			tb_churn_step1 a
			left outer join tb_login_info b
				on a.pcid = b.pcid
	) a;

create index pid on tb_target_item_2 (pid); 
create index create_date on tb_target_item_2 (create_date);


/*
drop table if exists tb_user_cat_info; 
create table tb_user_cat_info  as
select * from mcr_rblog.tb_log_view a inner join mcr_rengine.tmp_prd_info_in b on a.item_id = b.pid WHERE
    a.adid IS NOT NULL
        AND a.server_time >= DATE_ADD(NOW(), INTERVAL - 24 hour)
;
create index pcid on tb_user_cat_info (pcid); 
create index catlvl1 on tb_user_cat_info (catlvl1); 


SELECT 
    a.pcid
    , b.catlvl1
    #, count(*)
FROM
    mcr_rblog.tb_log_view a
        inner JOIN
    mcr_rengine.tmp_prd_info_in b ON a.item_id = b.pid
WHERE
    a.adid IS NOT NULL
        AND a.server_time >= DATE_ADD(NOW(), INTERVAL - 7 DAY)
#GROUP BY pcid , b.catlvl1
ORDER BY pcid , b.catlvl1;
*/

#drop table if exists tb_churn_step1; 
#create table tb_churn_step1  as






######################################################################################
#Campaign 3
#recommeand complementary goods
###################################################################################

set @camp_title := 'complement_purchase'; 
set @camp_type := 'P03';
set @camp_key_value := '_complement_purchase_';

drop table if exists tb_complement_purchase_step0; 
create table tb_complement_purchase_step0  as
	select
		a.server_time, a.pcid, a.adid, a.device, a.user_id,
		a.order_id, b.item_id, b.price, b.quantity, b.item_category
	from
		mcr_rblog.tb_log_order a
		inner join  mcr_rblog.tb_log_order_item b
			on a.order_id = b.order_id
	where
		a.server_time >= date_add(now(),interval -10 day ) and adid is not null ;

create index item_id on tb_complement_purchase_step0 (item_id);
create index adid on tb_complement_purchase_step0 (adid);
create index server_time on tb_complement_purchase_step0 (server_time);



set @prev_pid := '';
set @current_pid_cnt := 0;
set @num_prd := 0;

drop table if exists tb_complement_purchase_step1; 
create table tb_complement_purchase_step1 as 
	select
		*
	from (
		select
			a.*,
			@num_prd:=(if(a.adid=@prev_pid, @current_pid_cnt:=@current_pid_cnt+1, @current_pid_cnt:=1)) as number, 
			@prev_pid:=a.adid as tmp_pid 
		from (	
			select
				a.pcid, a.adid, a.server_time, a.device,
				b.pid, (case when locate('$$',b.pnm) <> 0 then substring(b.pnm, 1, locate('$$',b.pnm)-1) else b.pnm end) pnm, b.original_price, b.sale_price, b.pimg,
				b.site_catlvl1 catlvl1,
				b.site_catlvl2 catlvl2,
				b.site_catlvl3 catlvl3,
				b.pdesc, b.ptag
			from
				tb_complement_purchase_step0 a 
				inner join mcr_service.tb_prd_info b
					on a.item_id = b.pid
			where adid is not null
			order by a.adid, a.server_time desc, b.original_price desc
		) a
	) a
	where number = 1;

drop table if exists tb_target_item_3; 
create table tb_target_item_3 as
	select
		a.*,
		concat(user_id, @camp_key_value , pid) camp_key,
		@camp_type camp_type,
		@camp_title camp_title
	from (
		select
			a.pcid,
			a.adid,
			a.pcid user_id,
			a.server_time,
			a.device,
			a.pid, a.pnm, a.original_price, a.sale_price, a.pimg,
			a.catlvl1, a.catlvl2, a.catlvl3, a.pdesc, a.ptag,
			current_date create_date
		from
			tb_complement_purchase_step1 a
			#left outer join tb_login_info b
			#	on a.pcid = b.pcid
	) a;

create index pid on tb_target_item_3 (pid); 
create index create_date on tb_target_item_3 (create_date);



###################################################################################
#Campaign 4
#3 click 
###################################################################################

set @camp_title := 'three_click'; 
set @camp_type := 'P04';
set @camp_key_value := '_three_click_';


set @prev_pid := '';
set @current_pid_cnt := 0;
set @num_prd := 0;



drop table if exists tb_three_click_step0; 
create table tb_three_click_step0 as

SELECT 
    a.pcid pcid, a.adid ,a.server_time, a.device, a.item_id pid
FROM
    (SELECT 
        a.*,
            @num_prd:=(IF(a.pcid = @prev_pid, @current_pid_cnt:=@current_pid_cnt + 1, @current_pid_cnt:=1)) AS number,
            @prev_pid:=a.pcid AS tmp_pid
    FROM
        (SELECT 
        pcid, adid, server_time, device, item_id, COUNT(*) click_cnt
    FROM
        mcr_rblog.tb_log_view
	where adid is not null
		and server_time >= date_add(now(),interval -7 day)
    GROUP BY pcid , item_id
    ORDER BY pcid , COUNT(*) DESC) a) a
WHERE
    number = 1 and  click_cnt>=3;


drop table if exists tb_three_click_filter; 
create table tb_three_click_filter as 
select  
	a.pcid
	from tb_three_click_step0 a
    inner join mcr_rblog.tb_log_order b
    on a.pcid = b.pcid
    inner join mcr_rblog.tb_log_order_item c 
    on b.order_id = c.order_id
    where c.server_time >= date_add(now(),interval -30 day)
    and a.pid = c.item_id;
    

drop table if exists tb_three_click_step1; 
CREATE TABLE tb_three_click_step1 AS SELECT a.pcid, a.adid, a.server_time, a.device, a.pid FROM
    tb_three_click_step0 a
        LEFT JOIN
    tb_three_click_filter b ON a.pcid = b.pcid
WHERE
    b.pcid IS NULL;



drop table if exists tb_target_item_4; 
create table tb_target_item_4 as
	select
		a.*,
		concat(user_id, @camp_key_value , pid) camp_key,
		@camp_type camp_type,
		@camp_title camp_title
	from (
		select
			a.pcid,
			a.adid,
			a.pcid user_id,
			a.server_time,
			a.device,
			b.pid, b.pnm, b.original_price, b.sale_price, b.pimg,
			b.site_catlvl1, b.site_catlvl2, b.site_catlvl3, b.pdesc, b.ptag,
			current_date create_date
		from
			tb_three_click_step1 a
		left join mcr_service.tb_prd_info b
			on a.pid = b.pid
		where b.pid is not null
	) a;


######################################################################################
#target_item aggregator
#collect all campaign
######################################################################################

set @prev_adid := '';
set @current_adid_cnt := 0;
set @num_prd := 0;


drop table if exists tb_target_item; 
CREATE TABLE tb_target_item AS SELECT a.* FROM
    (SELECT 
        a.*,
            @num_prd:=(IF(a.adid = @prev_adid, @current_adid_cnt:=@current_adid_cnt + 1, @current_adid_cnt:=1)) AS number,
            @prev_adid:=a.adid AS tmp_adid
    FROM
        (
	
    (SELECT * FROM tb_target_item_1) 
    UNION 
    (SELECT * FROM tb_target_item_4) 
    UNION 
    (SELECT * FROM tb_target_item_3) 
    UNION
    (SELECT * FROM tb_target_item_2) 
    
    ORDER BY adid) a
        ) a
        
WHERE
    number = 1;

create index pid on tb_target_item (pid); 
create index create_date on tb_target_item (create_date);





######################################################################################
#Declare Push/Email Content Candidate List
#Filter out Candidate List that are okay to send
#tb_target_item -> tb_target_item_filtered
######################################################################################


#filter out conditions
drop table if exists tb_target_item_filter_step0; 
create table tb_target_item_filter_step0 as
	select
		a.*
	from
		tb_target_item a
		left outer join tb_campaign_open_log b
			on a.camp_key = b.camp_key
			and status_type = 1
	where
		b.camp_key is null; 

drop table if exists tb_target_item_filter_step1;
create table tb_target_item_filter_step1 as
	select
		a.*
	from
		tb_target_item_filter_step0 a
		left outer join tb_campaign_interface b
			on a.user_id = b.user_id
	where
		b.user_id is null;

drop table if exists tb_target_item_filtered; 
create table tb_target_item_filtered as
	select
		a.*
	from
		tb_target_item_filter_step1 a
		left outer join tb_campaign_open_log b
			on a.user_id = b.user_id
			and a.create_date = b.insert_date
	where
		b.user_id is null;

create index pid on tb_target_item_filtered (pid);
create index create_date on tb_target_item_filtered (create_date);

######################################################################################
#Create Marketing Contents Based on Association Rule
######################################################################################

drop table if exists tb_content_center_tmp;
create table tb_content_center_tmp as
	select
		a.camp_type, a.camp_title, a.camp_key, a.create_date, a.device,
		'target_pid' content_type, 1 position,
		a.pid, a.pnm, a.original_price, a.sale_price, a.pimg,
		a.catlvl1, a.catlvl2, a.catlvl3, a.pdesc, a.ptag
	from
		tb_target_item_filtered a;

insert into tb_content_center_tmp
	select
		a.camp_type, a.camp_title, a.camp_key, a.create_date, a.device,
		'recmd_pid' concent_type, b.rank + 1 position,
		b.rpid pid, b.pnm, b.original_price, b.sale_price, b.pimg,
		b.site_catlvl1 catlvl1, b.site_catlvl2 catlvl2, b.site_catlvl3 catlvl3,
		b.pdesc, b.ptag
	from
		tb_target_item_filtered a
		inner join mcr_service.tb_asc_wtn b
			on a.pid = b.tpid
	where rank <= 10;

drop table if exists tb_content_center_check;
create table tb_content_center_check (
	camp_key varchar(100)
);

insert into tb_content_center_check
	select
		camp_key
	from
		tb_content_center_tmp
	group by camp_key
		having count(*) <= 5;

create index camp_key on tb_content_center_check(camp_key); 

insert into tb_content_center_tmp
	select
		b.camp_type, b.camp_title, b.camp_key, b.create_date, b.device,
		'recmd_pid' concent_type, c.score + 10 position,
		c.pid, c.pnm, c.original_price, c.sale_price, c.pimg,
		c.site_catlvl1 catlvl1, c.site_catlvl2 catlvl2, c.site_catlvl3 catlvl3,
		c.pdesc, c.ptag
	from
		tb_content_center_check a
		inner join tb_target_item_filtered b
			on a.camp_key = b.camp_key
		inner join mcr_service.tb_asc_backup c
			on b.catlvl1 = c.site_catlvl1
	where
		score <= 10;

drop table if exists tb_content_center_final_check;
create table tb_content_center_final_check as
	select
		max(camp_type) camp_type, 
		max(camp_title) camp_title,
		camp_key
	from
		tb_content_center_tmp
	group by camp_key
		having count(*) >= 5;

insert into tb_content_center
	select
		b.*
	from
		tb_content_center_final_check a
		inner join tb_content_center_tmp b
			on a.camp_key = b.camp_key;


######################################################################################
#Finalized Pushable Contents 
######################################################################################
/* tag list
[$_RND_$] random from 1000 ~ 10000
[$_PNM_$] product name 
[$_STORE_$] score name 
*/
drop table if exists tb_campaign_interface_new;
create table tb_campaign_interface_new (
	user_id varchar(50),
	camp_type varchar(10),
	dest_type varchar(10),
	dest_addr varchar(100),
	tag1 varchar(200),
	tag2 varchar(200),
	tag3 varchar(200),
	tag4 varchar(200),
	tag5 varchar(200),
	tag6 varchar(200),
	tag7 varchar(200),
	tag8 varchar(200),
	tag9 varchar(200),
	tag10 varchar(200),
	create_time timestamp,
	sent_time timestamp
); 

create index user_id on tb_campaign_interface_new (user_id); 
create index camp_type on tb_campaign_interface_new (camp_type); 
create index dest_type on tb_campaign_interface_new (dest_type); 
create index dest_addr on tb_campaign_interface_new (dest_addr); 
create index tag2 on tb_campaign_interface_new (tag2); 

/*
insert into tb_campaign_interface_new
	select
		b.user_id user_id, 
		b.camp_type camp_type, 
		device dest_type,
		b.adid dest_addr,
		replace(replace(replace(c.camp_title, '[$_RND_$]', FLOOR( 2000 + RAND( ) * 5000 )) ,'[$_PNM_$]', b.pnm) , '[$_STORE_$]', '미쳐라') tag1, #title
		b.camp_key tag2, #camp_key
		b.camp_title tag3, #camp_title - last_click
		@url tag4, #url
		#concat('?camp_key=', b.camp_key, '&rbadcd=', c.tag_adcode, '&camp_type=', c.camp_type, '&send_day=', b.create_date) tag5, #parameters
		concat('?product_no=', b.pid, '&rbadcd=', c.tag_adcode) tag5, #parameters
		null tag6,
		null tag7,
		null tag8,
		null tag9,
		null tag10,
		now() create_time,
		now() sent_time
	from
		tb_content_center_final_check a
		inner join tb_target_item_filtered b
			on a.camp_key = b.camp_key
		inner join tb_campaign_title c
			on a.camp_type = c.camp_type
			and c.camp_day = dayofweek(current_date);
*/


#P01
/*
insert into tb_campaign_interface_new
	select
		b.user_id user_id, 
		b.camp_type camp_type, 
		device dest_type,
		b.adid dest_addr,
		replace(replace(replace(c.camp_title, '[$_RND_$]', FLOOR( 2000 + RAND( ) * 5000 )) ,'[$_PNM_$]', b.pnm) , '[$_STORE_$]', @mall_name) tag1, #title
		b.camp_key tag2, #camp_key
		b.camp_title tag3, #camp_title - last_click
		@url tag4, #url
		concat(@url_deatil, b.pid, '&rbadcd=', c.tag_adcode) tag5, #parameters #cafe24
		null tag6,
		null tag7,
		null tag8,
		null tag9,
		null tag10,
		now() create_time,
		now() sent_time
	from
		#tb_content_center_final_check a
		#inner join tb_target_item_filtered b
        tb_target_item_filtered b
			#on a.camp_key = b.camp_key
		inner join tb_campaign_title c
			on b.camp_type = c.camp_type
			and c.camp_day = dayofweek(current_date);
 
 */
/*
 #P02
insert into tb_campaign_interface_new
	select
		b.user_id user_id, 
		b.camp_type camp_type, 
		device dest_type,
		b.adid dest_addr,
		replace(replace(replace(c.camp_title, '[$_RND_$]', FLOOR( 2000 + RAND( ) * 5000 )) ,'[$_PNM_$]', b.pnm) , '[$_STORE_$]', @mall_name) tag1, #title
		b.camp_key tag2, #camp_key
		b.camp_title tag3, #camp_title - last_click
		@url tag4, #url
		#concat('?camp_key=', b.camp_key, '&rbadcd=', c.tag_adcode, '&camp_type=', c.camp_type, '&send_day=', b.create_date) tag5, #parameters
		concat(@url_deatil, b.pid, '&rbadcd=', c.tag_adcode) tag5, #parameters #cafe24
        #concat('?branduid=', b.pid, '&rbadcd=', c.tag_adcode) tag5, #parameters #makeshop
		null tag6,
		null tag7,
		null tag8,
		null tag9,
		null tag10,
		now() create_time,
		now() sent_time
	from
		tb_content_center_final_check a
		inner join tb_target_item_filtered b
			on a.camp_key = b.camp_key
		inner join tb_campaign_title c
			on a.camp_type = c.camp_type
			and c.camp_day = dayofweek(current_date)
	where a.camp_type='P02';
 
 
#P3
insert into tb_campaign_interface_new
	select
		#b.user_id user_id, 
        #d.content_type,
        b.user_id user_id, 
		b.camp_type camp_type, 
		b.device dest_type,
		b.adid dest_addr,
		replace(replace(replace(c.camp_title, '[$_RND_$]', FLOOR( 2000 + RAND( ) * 5000 )) ,'[$_PNM_$]', d.pnm) , '[$_STORE_$]', @mall_name) tag1, #title
		b.camp_key tag2, #camp_key
		b.camp_title tag3, #camp_title - last_click
		@url tag4, #url
		#concat('?camp_key=', b.camp_key, '&rbadcd=', c.tag_adcode, '&camp_type=', c.camp_type, '&send_day=', b.create_date) tag5, #parameters
		concat(@url_deatil, d.pid, '&rbadcd=', c.tag_adcode) tag5, #parameters #cafe24
        #concat('?branduid=', d.pid, '&rbadcd=', c.tag_adcode) tag5, #parameters #makeshop
		null tag6,
		null tag7,
		null tag8,
		null tag9,
		null tag10,
		now() create_time,
		now() sent_time
        #d.position
	from
		tb_content_center_final_check a
		inner join tb_target_item_filtered b
			on a.camp_key = b.camp_key
        inner join tb_campaign_title c
			on a.camp_type = c.camp_type
		inner join tb_content_center d
			on a.camp_key = d.camp_key
	where a.camp_type='P03'and d.position=timestampdiff(day,date_format(b.server_time,'%y-%m-%d'),current_date)+1 and c.camp_day = dayofweek(current_date);
*/
#P01
insert into tb_campaign_interface_new
	select
		b.user_id user_id, 
		b.camp_type camp_type, 
		device dest_type,
		b.adid dest_addr,
		replace(replace(replace(c.camp_title, '[$_RND_$]', FLOOR( 2000 + RAND( ) * 5000 )) ,'[$_PNM_$]', b.pnm) , '[$_STORE_$]', @mall_name) tag1, #title
		b.camp_key tag2, #camp_key
		b.camp_title tag3, #camp_title - last_click
		@url tag4, #url
        concat(@url_deatil, b.pid, '&rbadcd=', c.tag_adcode) tag5, #parameters 
		null tag6,
		null tag7,
		null tag8,
		null tag9,
		null tag10,
		now() create_time,
		now() sent_time
	from
		tb_content_center_final_check a
		inner join tb_target_item_filtered b
			on a.camp_key = b.camp_key
		inner join tb_campaign_title c
			on a.camp_type = c.camp_type
			and c.camp_day = dayofweek(current_date)
	where a.camp_type='P01';
 
 #P02
insert into tb_campaign_interface_new
	select
		b.user_id user_id, 
		b.camp_type camp_type, 
		device dest_type,
		b.adid dest_addr,
		replace(replace(replace(c.camp_title, '[$_RND_$]', FLOOR( 2000 + RAND( ) * 5000 )) ,'[$_PNM_$]', b.pnm) , '[$_STORE_$]', @mall_name) tag1, #title
		b.camp_key tag2, #camp_key
		b.camp_title tag3, #camp_title - protect churn
		@url tag4, #url
        concat(@url_deatil, b.pid, '&rbadcd=', c.tag_adcode) tag5, #parameters 
		null tag6,
		null tag7,
		null tag8,
		null tag9,
		null tag10,
		now() create_time,
		now() sent_time
	from
		tb_content_center_final_check a
		inner join tb_target_item_filtered b
			on a.camp_key = b.camp_key
		inner join tb_campaign_title c
			on a.camp_type = c.camp_type
			and c.camp_day = dayofweek(current_date)
	where a.camp_type='P02';
 
 
#P3
insert into tb_campaign_interface_new
	select
		b.user_id user_id, 
		b.camp_type camp_type, 
		b.device dest_type,
		b.adid dest_addr,
		replace(replace(replace(c.camp_title, '[$_RND_$]', FLOOR( 2000 + RAND( ) * 5000 )) ,'[$_PNM_$]', d.pnm) , '[$_STORE_$]', @mall_name) tag1, #title
		b.camp_key tag2, #camp_key
		b.camp_title tag3, #camp_title - purchase complement
		@url tag4, #url
        concat(@url_deatil, d.pid, '&rbadcd=', c.tag_adcode) tag5, #parameters #makeshop
		null tag6,
		null tag7,
		null tag8,
		null tag9,
		null tag10,
		now() create_time,
		now() sent_time
	from
		tb_content_center_final_check a
		inner join tb_target_item_filtered b
			on a.camp_key = b.camp_key
        inner join tb_campaign_title c
			on a.camp_type = c.camp_type
		inner join tb_content_center d
			on a.camp_key = d.camp_key
	where a.camp_type='P03'and d.position=timestampdiff(day,date_format(b.server_time,'%y-%m-%d'),current_date)+1 and c.camp_day = dayofweek(current_date) and d.create_date = b.create_date;

#P04
insert into tb_campaign_interface_new
	select
		b.user_id user_id, 
		b.camp_type camp_type, 
		device dest_type,
		b.adid dest_addr,
		replace(replace(replace(c.camp_title, '[$_RND_$]', FLOOR( 2000 + RAND( ) * 5000 )) ,'[$_PNM_$]', b.pnm) , '[$_STORE_$]', @mall_name) tag1, #title
		b.camp_key tag2, #camp_key
		b.camp_title tag3, #camp_title - protect churn
		@url tag4, #url
        concat(@url_deatil, b.pid, '&rbadcd=', c.tag_adcode) tag5, #parameters 
		null tag6,
		null tag7,
		null tag8,
		null tag9,
		null tag10,
		now() create_time,
		now() sent_time
	from
		tb_content_center_final_check a
		inner join tb_target_item_filtered b
			on a.camp_key = b.camp_key
		inner join tb_campaign_title c
			on a.camp_type = c.camp_type
			and c.camp_day = dayofweek(current_date)
	where a.camp_type='P04';

insert into tb_campaign_interface
	select
		*
	from
		tb_campaign_interface_new;

insert into tb_campaign_interface_hist
	select
		*
	from
		tb_campaign_interface_new;

insert into tb_campaign_open_log
	select
		tag2 camp_key,
		camp_type,
		user_id,
		dest_addr,
		0 status_type,
		create_time insert_time,
		date(create_time) insert_date
	from
		tb_campaign_interface_new;
