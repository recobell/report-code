SELECT 
    month
    ,day
    ,count(pcid) view
    ,count(case when reco is not null then pcid end) reco_view
    ,count(case when reco is null then pcid end) no_reco_view
FROM
thebodyshop_consult.tb_log_view_report 
group by day;

drop table if exists thebodyshop_consult.tb_uv;
CREATE TABLE thebodyshop_consult.tb_uv AS
select
    month,
    day,
    count(pcid) uv,
	count(case when reco_view>0 then pcid end) reco_uv,	
	sum(view) pv,
	sum(case when reco_view>0 then view end) reco_pv
from thebodyshop_consult.tb_recommend_territory_result_view
group by day;

drop table if exists thebodyshop_consult.tb_freq;
CREATE TABLE thebodyshop_consult.tb_freq AS
select 
	month,
	day,
	count(revenue) freq,
	count(case when reco is not null then revenue end) reco_freq,
	TRUNCATE(sum(revenue),2) revenue,
	TRUNCATE(sum(case when reco is not null then revenue end),2) reco_revenue,
	sum(item_num) quantity,
	sum(case when reco is not null then item_num end) reco_quantity	
FROM
thebodyshop_consult.tb_log_order_report
group by day;

select
	month,
	day,
	round(SUM(revenue) / sum(freq),0) prevenue,
	round(SUM(reco_revenue) / sum(reco_freq),0) reco_prevenue

FROM
    thebodyshop_consult.tb_recommend_territory_result_order
group by day;


SELECT 
	month,
	day,
    avg(view) click
    ,avg(case when reco_view>0 then view end) reco_click
    
FROM
    thebodyshop_consult.tb_recommend_territory_result_view
group by day;


select 
	month,
	day,
    round(B.freq/A.uv, 4) cr,
    round(B.reco_freq/A.reco_uv, 4) reco_cr
FROM
	from thebodyshop_consult.tb_uv A,
	thebodyshop_consult.tb_freq B
group by day;