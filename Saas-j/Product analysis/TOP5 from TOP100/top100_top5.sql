
drop table if exists nubizio_consult_jensen.tb_recommend_result_cross_selling_item_id;
create table nubizio_consult_jensen.tb_recommend_result_cross_selling_item_id as
select a.item_id, 
	b.item_id cross_sell_item_id, 
	count(*) quantity 
from nubizio_rblog.tb_log_order_item a
inner join nubizio_rblog.tb_log_order_item b on a.order_id = b.order_id
where a.item_id <> b.item_id 
and date_format(convert_tz(a.server_time, '+00:00', '+09:00'), '%y-%m') = '15-12' 
and b.item_id <> "" and a.item_id <> ""
group by 1,2
order by 3 desc;

drop table if exists nubizio_consult_jensen.tb_recommend_result_top100_s1;
create table nubizio_consult_jensen.tb_recommend_result_top100_s1 as
select
	item_id,
	max(item_name) item_name,
	sum(quantity) quantity,
	round(sum(price),2) revenue
from nubizio_rblog.tb_log_order_item
where date_format(convert_tz(server_time, '+00:00', '+09:00'), '%y-%m') = '15-12'
and item_id not like ''
group by 1
order by 4 desc;


drop table if exists nubizio_consult_jensen.tb_recommend_result_top100_s2;
CREATE TABLE nubizio_consult_jensen.tb_recommend_result_top100_s2
select 
	@curRank:= CASE WHEN @incRank <> item_id THEN @curRank := @curRank+1 ELSE @curRank END AS rn,
	@incRank:= item_id AS rankSet,
	item_name,
	quantity,
	revenue
FROM
  (SELECT @curRank:= 0) s,
  (SELECT @incRank:= 0) c,
  (SELECT distinct item_id, item_name, quantity, revenue
   FROM nubizio_consult_jensen.tb_recommend_result_top100_s1 order by revenue desc) t limit 100;

drop table if exists nubizio_consult_jensen.tb_recommend_result_top100;
CREATE TABLE nubizio_consult_jensen.tb_recommend_result_top100 as
select rn as rank, rankSet as item_id, quantity, revenue
from nubizio_consult_jensen.tb_recommend_result_top100_s2;


drop table if exists nubizio_consult_jensen.tb_recommend_result_top100_join_s0;
CREATE TABLE nubizio_consult_jensen.tb_recommend_result_top100_join_s0
select 	
	A.rank,
	A.item_id, 
	B.cross_sell_item_id, 
	B.quantity,
	(A.revenue*B.quantity) revenue
from
nubizio_consult_jensen.tb_recommend_result_top100 A 
inner join
nubizio_consult_jensen.tb_recommend_result_cross_selling_item_id B on A.item_id = B.item_id
order by rank ASC, B.quantity DESC, A.item_id;


drop table if exists nubizio_consult_jensen.tb_recommend_result_top100_join_s1;
CREATE TABLE nubizio_consult_jensen.tb_recommend_result_top100_join_s1
SELECT rank, item_id, cross_sell_item_id,quantity, revenue,
   @curRank:= CASE WHEN @incRank <> item_id THEN 1 ELSE @curRank + 1 END AS rn,
   @incRank:= item_id AS rankSet
FROM
  (SELECT @curRank:= 1) s,
  (SELECT @incRank:= 1) c,
  (SELECT *
   FROM nubizio_consult_jensen.tb_recommend_result_top100_join_s0
   ORDER BY rank) t;


drop table if exists nubizio_consult_jensen.tb_recommend_result_top100_join;
CREATE TABLE nubizio_consult_jensen.tb_recommend_result_top100_join
select rank,
	rn,
	item_id,
	cross_sell_item_id,
	quantity,
	revenue
from 
	nubizio_consult_jensen.tb_recommend_result_top100_join_s1
where rn < 6;

select *
from
(select rank, item_id, count(*) as cnt from nubizio_consult_jensen.tb_recommend_result_top100_join 
group by rank) r
where cnt < 5;
	