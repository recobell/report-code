#CRM KPI 
#mcr CRM KPI based on SaaS data

#First Time Run 

#create database mcr_crm_kpi;

/*Global Variable Declaration*/
SET @TIME_CUT_OFF_1YR := DATE_ADD(NOW(), INTERVAL - 365 DAY);
SET @TIME_CUT_OFF_1W := DATE_ADD(NOW(), INTERVAL - 7 DAY);
SET @TIME_CUT_OFF_1M := DATE_ADD(NOW(), INTERVAL - 30 DAY);
SET @TIME_CUT_OFF_3M := DATE_ADD(NOW(), INTERVAL - 91 DAY);




######################################################################################
#CRM DATA based on Click
###################################################################################

#변수 정의
set @p_fav_cat_rank := 0; 
set @p_fav_cat_num_orders := 0; 
set @p_fav_cat_prev_member := 0;
set @p_fav_cat_current_member_cnt := 0;
set @p_fav_cat_current_mai_deal_cnt := 0;



set @c_fav_cat_rank := 0; 
set @c_fav_cat_num_orders := 0; 
set @c_fav_cat_prev_member := 0;
set @c_fav_cat_current_member_cnt := 0;
set @c_fav_cat_current_mai_deal_cnt := 0;

SET @time_prev_member := 0;
SET @time_current_member_cnt := 0;
SET @time_rank := 0;
SET @time_current_main_deal_cnt := 0;
SET @time_num_orders := 0;


set @recent_click_prev_pid := '';
set @recent_click_click_current_pid_cnt := 0;
set @recent_click_num_prd := 0;


set @recent_purchase_prev_pid := '';
set @recent_purchase_current_pid_cnt := 0;
set @recent_purchase_num_prd := 0;


#RFM 지수

drop table if exists mcr_crm_kpi.tb_member;
create table mcr_crm_kpi.tb_member as
	SELECT 
        a.pcid pcid
        ,b.adid adid
        ,(CASE
                WHEN LOCATE('iPhone', b.user_agent) <> 0 THEN 'iPhone'
                ELSE 'Android'
            END) AS platform
    FROM
        mcr_rblog.tb_log_view a
	LEFT JOIN 
		mcr_rblog.tb_log_visitor b 
	on a.pcid = b.pcid
    
    WHERE
		a.server_time > @TIME_CUT_OFF_1M
    GROUP BY a.pcid; 
    
create index pcid on mcr_crm_kpi.tb_member (pcid);
create index adid on mcr_crm_kpi.tb_member (adid);

    

drop table if exists mcr_crm_kpi.tb_order;
create table mcr_crm_kpi.tb_order as
	SELECT 
        a.pcid,
        FLOOR(SUM(a.revenue)) total_revenue,
        FLOOR(AVG(a.revenue)) avg_revenue,
        COUNT(*) frequency, 
        floor(sum((CASE WHEN reco is not null then revenue else 0 end))) reco_revenue, 
        floor(sum((case when adcode is not null then revenue else 0 end))) adcode_revenue
    
    FROM
        mcr_rblog.tb_log_order a
    WHERE
        a.server_time > @TIME_CUT_OFF_3M
    GROUP BY a.pcid
    ORDER BY a.pcid	;

create index pcid on mcr_crm_kpi.tb_order (pcid);


drop table if exists mcr_crm_kpi.tb_p_fav_cat;
create table mcr_crm_kpi.tb_p_fav_cat as
SELECT 
        a.pcid,
            MAX(CASE
                WHEN rank = 1 THEN category
                ELSE NULL
            END) main_favorite_p_category,
            ROUND(MAX(CASE
                WHEN rank = 1 THEN gross_revenue
                ELSE NULL
            END) / MAX(num_orders), 4) main_favorite_p_category_ratio,
            MAX(CASE
                WHEN rank = 2 THEN category
                ELSE NULL
            END) second_favorite_p_category,
            ROUND(MAX(CASE
                WHEN rank = 2 THEN gross_revenue
                ELSE NULL
            END) / MAX(num_orders), 4) second_favorite_p_category_ratio,
            MAX(rank) p_category_diversity
    FROM
        (SELECT 
        a.*,
            @p_fav_cat_rank:=(IF(pcid = @p_fav_cat_prev_member, @p_fav_cat_current_member_cnt:=@p_fav_cat_current_member_cnt + 1, @p_fav_cat_current_member_cnt:=1)) AS rank,
            @p_fav_cat_num_orders:=(IF(pcid = @p_fav_cat_prev_member, @p_fav_cat_current_main_deal_cnt:=@p_fav_cat_current_main_deal_cnt + gross_revenue, @p_fav_cat_current_main_deal_cnt:=gross_revenue)) AS num_orders,
            @p_fav_cat_prev_member:=pcid AS tmp_member
    FROM
        (SELECT 
        a.pcid,
            b.item_category category,
            SUM(b.price * b.quantity) gross_revenue
    FROM
        mcr_rblog.tb_log_order a
    LEFT JOIN mcr_rblog.tb_log_order_item b ON a.order_id = b.order_id
    WHERE
        a.server_time > @TIME_CUT_OFF_1M
#            AND a.pcid IN (142413952793320197 , 142414196782944145, 142414348589034930, 142416627912882125)
    GROUP BY 1 , 2
    ORDER BY 1 , 3 DESC) a) a
    GROUP BY 1;

create index pcid on mcr_crm_kpi.tb_p_fav_cat (pcid);


drop table if exists mcr_crm_kpi.tb_c_fav_cat;
create table mcr_crm_kpi.tb_c_fav_cat as
SELECT 
        a.pcid,
            MAX(CASE
                WHEN rank = 1 THEN category
                ELSE NULL
            END) main_favorite_c_category,
            ROUND(MAX(CASE
                WHEN rank = 1 THEN view_count
                ELSE NULL
            END) / MAX(num_orders), 4) main_favorite_c_category_ratio,
            MAX(CASE
                WHEN rank = 2 THEN category
                ELSE NULL
            END) second_favorite_c_category,
            ROUND(MAX(CASE
                WHEN rank = 2 THEN view_count
                ELSE NULL
            END) / MAX(num_orders), 4) second_favorite_c_category_ratio,
            MAX(rank) c_category_diversity
    FROM
        (SELECT 
        a.*,
            @c_fav_cat_rank:=(IF(pcid = @c_fav_cat_prev_member, @c_fav_cat_current_member_cnt:=@c_fav_cat_current_member_cnt + 1, @c_fav_cat_current_member_cnt:=1)) AS rank,
            @c_fav_cat_num_orders:=(IF(pcid = @c_fav_cat_prev_member, @c_fav_cat_current_main_deal_cnt:=@c_fav_cat_current_main_deal_cnt + view_count, @c_fav_cat_current_main_deal_cnt:=view_count)) AS num_orders,
            @c_fav_cat_prev_member:=pcid AS tmp_member
    FROM
        (SELECT 
        a.pcid, b.catlvl1 category, COUNT(*) view_count
    FROM
        mcr_rblog.tb_log_view a
    INNER JOIN mcr_rengine.tmp_prd_info_in b ON a.item_id = b.pid
    WHERE
    a.server_time > @TIME_CUT_OFF_1W
#            AND         a.pcid IN (142413952793320197 , 142414196782944145, 142414348589034930, 142416627912882125)

    GROUP BY 1 , 2
    ORDER BY 1 , 3 DESC) a) a
    GROUP BY 1;

create index pcid on mcr_crm_kpi.tb_c_fav_cat (pcid);


drop table if exists mcr_crm_kpi.tb_churn;
create table mcr_crm_kpi.tb_churn as
SELECT 
        server_time,
            pcid,
            MIN(sday) first_purchase,
            MAX(next_sday) last_purchase,
            TIMESTAMPDIFF(DAY, MAX(next_sday), CURRENT_DATE()) recency,
            COUNT(*) frequency,
            AVG(purchase_interval) avg_purchase_interval,
            MIN(purchase_interval) min_purchase_interval,
            MAX(purchase_interval) max_purchase_interval,
            (CASE
                WHEN TIMESTAMPDIFF(DAY, MAX(next_sday), CURRENT_DATE) >= MAX(purchase_interval) * 1.5 THEN 5
                WHEN TIMESTAMPDIFF(DAY, MAX(next_sday), CURRENT_DATE) >= MAX(purchase_interval) THEN 4
                WHEN TIMESTAMPDIFF(DAY, MAX(next_sday), CURRENT_DATE) >= AVG(purchase_interval) THEN 3
                WHEN TIMESTAMPDIFF(DAY, MAX(next_sday), CURRENT_DATE) < AVG(purchase_interval) THEN 2
                WHEN TIMESTAMPDIFF(DAY, MAX(next_sday), CURRENT_DATE) < 7 THEN 1
                ELSE 0
            END) churn_level
    FROM
        (SELECT 
        a.server_time,
            a.pcid,
            a.sday,
            MIN(b.next_sday) next_sday,
            TIMESTAMPDIFF(DAY, a.sday, b.next_sday) purchase_interval
    FROM
        (SELECT 
        server_time, pcid, server_time sday
    FROM
        mcr_rblog.tb_log_order
    WHERE
		server_time >= @TIME_CUT_OFF_3M
    ORDER BY pcid, server_time DESC) a
    INNER JOIN (SELECT 
        server_time, pcid, server_time next_sday
    FROM
        mcr_rblog.tb_log_order
    WHERE
        server_time >= @TIME_CUT_OFF_3M
    ORDER BY pcid,server_time DESC) b ON a.pcid = b.pcid
    WHERE
        a.sday < b.next_sday
    GROUP BY a.pcid , a.sday) a
    GROUP BY pcid
    HAVING (COUNT(*) >= 2);
    
    
create index pcid on mcr_crm_kpi.tb_churn (pcid);    
    
drop table if exists mcr_crm_kpi.tb_time_slot;
create table mcr_crm_kpi.tb_time_slot as
SELECT
	A.pcid
	,count(distinct TIME_SLOT) time_slot_diversity
    ,max(case when rank = 1 then time_slot else null end) best_time_slot
    ,round(max(case when rank = 1 then BUY_MAIN_DEAL else null end )  / MAX(NUM_ORDERS), 4) time_slot_ratio

FROM
(SELECT
	A.*,
	@time_rank:=(if(pcid=@time_prev_member, @time_current_member_cnt:=@time_current_member_cnt+1, @time_current_member_cnt:=1)) as rank,
	@time_num_orders:=(if(pcid=@time_prev_member, @time_current_main_deal_cnt:=@time_current_main_deal_cnt+BUY_MAIN_DEAL, @time_current_main_deal_cnt:=BUY_MAIN_DEAL)) as num_orders,
	@time_prev_member:=pcid as tmp_member
FROM
(SELECT 
    a.pcid pcid,
    (CASE
        WHEN a.p_hour IN (02 , 03, 04, 05, 06, 07) THEN 'T_02'
        WHEN a.p_hour IN (08 , 09) THEN 'T_08'
        WHEN a.p_hour IN (10 , 11, 12) THEN 'T_10'
        WHEN a.p_hour IN (13 , 14, 15) THEN 'T_13'
        WHEN a.p_hour IN (16 , 17) THEN 'T_16'
        WHEN a.p_hour IN (18 , 19, 20, 21, 22) THEN 'T_18'
        WHEN a.p_hour IN (23 , 00, 01) THEN 'T_23'
    END) TIME_SLOT
    ,count(*) BUY_MAIN_DEAL
FROM
    (SELECT 
        pcid,
            DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'), '%H') p_hour
    FROM
        mcr_rblog.tb_log_order
    WHERE
        server_time >= @TIME_CUT_OFF_3M
    ORDER BY pcid) a
group by 1,2
order by 1,3 desc) A) A 
GROUP BY 1;
    
create index pcid on mcr_crm_kpi.tb_time_slot (pcid);    
    
drop table if exists mcr_crm_kpi.tb_last_click;
create table mcr_crm_kpi.tb_last_click as  
select
	a.pcid, 
	max((case when number = 1 then a.item_id else null end)) recent_click_product, 
    count(distinct number) number_click_product
    
from
(select
	a.*,
	@recent_click_num_prd:=(if(a.pcid=@recent_click_prev_pid, @recent_click_current_pid_cnt:=@recent_click_current_pid_cnt+1, @recent_click_current_pid_cnt:=1)) as number, 
	@recent_click_prev_pid:=a.pcid as tmp_pid 
from
(select
pcid,  
item_id
FROM mcr_rblog.tb_log_view
#group by pcid
where server_time >= date_add(now(), interval -1 day)
order by pcid,server_time desc) a)a
group by 1;

create index pcid on mcr_crm_kpi.tb_last_click (pcid);    


drop table if exists mcr_crm_kpi.tb_last_purchase;
create table mcr_crm_kpi.tb_last_purchase as 
select
	a.pcid, 
	max((case when number = 1 then a.item_id else null end)) recent_purchase_product, 
    count(distinct number) number_purchase_product
    
from
(select
	a.*,
	@recent_purchase_num_prd:=(if(a.pcid=@recent_purchase_prev_pid, @recent_purchase_current_pid_cnt:=@recent_purchase_current_pid_cnt+1, @recent_purchase_current_pid_cnt:=1)) as number, 
	@recent_purchase_prev_pid:=a.pcid as tmp_pid 
from
(select
a.server_time,
a.pcid pcid,  
item_id
from mcr_rblog.tb_log_order a
inner join  mcr_rblog.tb_log_order_item b
on a.order_id = b.order_id
#FROM mcr_rblog.tb_log_order_item
#group by pcid
where a.server_time >= @TIME_CUT_OFF_1M
order by a.pcid,a.server_time desc) a)a
group by 1;


create index pcid on mcr_crm_kpi.tb_last_purchase (pcid);    


drop table if exists mcr_crm_kpi.tb_crm_kpi;
create table mcr_crm_kpi.tb_crm_kpi as 
SELECT 
    S_MEMBER.pcid pcid,
    S_MEMBER.adid adid,
    S_MEMBER.platform,
    
    
    (case when S_ORDER.total_revenue is null then 0 else S_ORDER.total_revenue END) total_revenue,
    (case when S_ORDER.avg_revenue is null then 0 else S_ORDER.avg_revenue END) avg_revenue,
    (case when S_ORDER.frequency is null then 0 else S_ORDER.frequency END) frequency,
    (case when S_ORDER.reco_revenue is null then 0 else S_ORDER.reco_revenue END) reco_revenue,
    (case when S_ORDER.adcode_revenue is null then 0 else S_ORDER.adcode_revenue END) adcode_revenue,
    #S_ORDER.total_revenue,
    #S_ORDER.avg_revenue,
    #S_ORDER.frequency,`
    #S_ORDER.reco_revenue,
    #S_ORDER.adcode_revenue,
    
    S_P_FAV_CAT.main_favorite_p_category,
    S_P_FAV_CAT.main_favorite_p_category_ratio,
    S_P_FAV_CAT.second_favorite_p_category,
    S_P_FAV_CAT.second_favorite_p_category_ratio,
    S_P_FAV_CAT.p_category_diversity,
    
    
    S_C_FAV_CAT.main_favorite_c_category,
    S_C_FAV_CAT.main_favorite_c_category_ratio,
    S_C_FAV_CAT.second_favorite_c_category,
    S_C_FAV_CAT.second_favorite_c_category_ratio,
    S_C_FAV_CAT.c_category_diversity,
    
    
	S_CHURN.first_purchase,
    S_CHURN.last_purchase,
    S_CHURN.recency,
	S_CHURN.avg_purchase_interval,
	S_CHURN.min_purchase_interval,
	S_CHURN.max_purchase_interval,
	S_CHURN.churn_level,
    
    
    S_TIME_SLOT.time_slot_diversity,
    S_TIME_SLOT.best_time_slot,
    S_TIME_SLOT.time_slot_ratio,
    
    S_LAST_CLICK.recent_click_product, 
    S_LAST_CLICK.number_click_product,
    
    S_LAST_PURCHASE.recent_purchase_product, 
    S_LAST_PURCHASE.number_purchase_product
    
FROM mcr_crm_kpi.tb_member S_MEMBER
LEFT JOIN mcr_crm_kpi.tb_order S_ORDER ON S_MEMBER.pcid = S_ORDER.pcid
LEFT JOIN mcr_crm_kpi.tb_p_fav_cat S_P_FAV_CAT ON S_ORDER.pcid = S_P_FAV_CAT.pcid
LEFT JOIN mcr_crm_kpi.tb_c_fav_cat S_C_FAV_CAT ON S_ORDER.pcid = S_C_FAV_CAT.pcid
LEFT JOIN mcr_crm_kpi.tb_churn S_CHURN ON S_MEMBER.pcid = S_CHURN.pcid
left join mcr_crm_kpi.tb_time_slot S_TIME_SLOT on S_MEMBER.pcid = S_TIME_SLOT.pcid
LEFT JOIN mcr_crm_kpi.tb_last_click S_LAST_CLICK on S_MEMBER.pcid = S_LAST_CLICK.pcid
LEFT JOIN mcr_crm_kpi.tb_last_purchase S_LAST_PURCHASE on S_MEMBER.pcid = S_LAST_PURCHASE.pcid;


