

set @year = '2016';
set @month = '01';
set @day = '01';

#recommend accumulated table
/*
drop table if exists tbs_consult.tb_recommend_territory_result_uv_total;
create table tbs_consult.tb_recommend_territory_result_uv_total
(
    year int, 
    month int, 
    pc_uv int, 
    mo_uv int, 
    pc_reco_uv int, 
    mo_reco_uv int, 
    pc_no_reco_uv int, 
    mo_no_reco_uv int
); 

drop table if exists tbs_consult.tb_recommend_territory_result_freq_total;
create table tbs_consult.tb_recommend_territory_result_freq_total
(
    year int, 
    month int, 
    pc_freq double, 
    mo_freq double, 
    pc_reco_freq double, 
    mo_reco_freq double, 
    pc_no_reco_freq double, 
    mo_no_reco_freq double
); 
drop table if exists tbs_consult.tb_recommend_territory_result_revenue_total;
create table tbs_consult.tb_recommend_territory_result_revenue_total
(
    year int, 
    month int, 
    pc_revenue int, 
    mo_revenue int, 
    pc_reco_revenue int, 
    mo_reco_revenue int, 
    pc_no_reco_revenue int, 
    mo_no_reco_revenue int
); 
drop table if exists tbs_consult.tb_recommend_territory_result_pv_total;
create table tbs_consult.tb_recommend_territory_result_pv_total
(
    year int, 
    month int, 
    pc_pv int, 
    mo_pv int, 
    pc_reco_pv int, 
    mo_reco_pv int, 
    pc_no_reco_pv int, 
    mo_no_reco_pv int
); 
drop table if exists tbs_consult.tb_recommend_territory_result_click_total;
create table tbs_consult.tb_recommend_territory_result_click_total
(
    year int, 
    month int, 
    pc_click double, 
    mo_click double, 
    pc_reco_click double, 
    mo_reco_click double, 
    pc_no_reco_five_click double, 
    mo_no_reco_five_click double,
    pc_no_reco_four_click double, 
    mo_no_reco_four_click double,
    pc_no_reco_three_click double, 
    mo_no_reco_three_click double,
    pc_no_reco_two_click double, 
    mo_no_reco_two_click double,
    pc_no_reco_one_click double, 
    mo_no_reco_one_click double,
    pc_click_up_ratio double, 
    mo_click_up_ratio double
); 
drop table if exists tbs_consult.tb_recommend_territory_result_prevenue_total;
create table tbs_consult.tb_recommend_territory_result_prevenue_total
(
    year int, 
    month int, 
    pc_prevenue int, 
    mo_prevenue int, 
    pc_reco_prevenue int, 
    mo_reco_prevenue int, 
    pc_no_reco_prevenue int, 
    mo_no_reco_prevenue int, 
    pc_prevenue_up_ratio double, 
    mo_prevenue_up_ratio double

); 
drop table if exists tbs_consult.tb_recommend_territory_result_cr_total;
create table tbs_consult.tb_recommend_territory_result_cr_total
(
    year int, 
    month int, 
    pc_cr double, 
    mo_cr double, 
    pc_reco_cr double, 
    mo_reco_cr double, 
    pc_no_reco_cr double, 
    mo_no_reco_cr double, 
    pc_cr_up_ratio double, 
    mo_cr_up_ratio double

); 
drop table if exists tbs_consult.tb_recommend_territory_result_cat_var_total;
create table tbs_consult.tb_recommend_territory_result_cat_var_total
(
    year int, 
    month int, 
    pc_cat_var double, 
    mo_cat_var double, 
    pc_reco_cat_var double, 
    mo_reco_cat_var double, 
    pc_no_reco_cat_var double, 
    mo_no_reco_cat_var double, 
    pc_cat_var_up_ratio double, 
    mo_cat_var_up_ratio double

); 
drop table if exists tbs_consult.tb_recommend_territory_result_quan_total;
create table tbs_consult.tb_recommend_territory_result_quan_total
(
    year int, 
    month int, 
    pc_quan double, 
    mo_quan double, 
    pc_reco_quan double, 
    mo_reco_quan double, 
    pc_no_reco_quan double, 
    mo_no_reco_quan double, 
    pc_quan_up_ratio double, 
    mo_quan_up_ratio double

); 
*/

#report용 log table로 다시 만들기
drop table if exists tbs_consult.tb_log_view_report;
CREATE TABLE tbs_consult.tb_log_view_report AS SELECT server_time,
    pcid,
    device,
    reco,
    #adcode,
    item_id,
    #push_id,
    DAYOFWEEK(server_time) AS dayofweek,
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%H') AS hour,
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%Y') AS year,
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%m') AS month,
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%d') AS day FROM
    tbs_rblog.tb_log_view
WHERE
    date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = concat(@year, '-', @month);


drop table if exists tbs_consult.tb_log_order_report;
CREATE TABLE tbs_consult.tb_log_order_report AS SELECT server_time,
    pcid,
    device,
    reco,
    order_id,
    #adcode,
    item_num,
    revenue,
    DAYOFWEEK(server_time) AS dayofweek,
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%H') AS hour,
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%Y') AS year,
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%m') AS month,
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%d') AS day FROM
    tbs_rblog.tb_log_order
WHERE
    #server_time >= @time_cut_off_3m;
    date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = concat(@year, '-', @month);


drop table if exists tbs_consult.tb_log_order_item_report;
CREATE TABLE tbs_consult.tb_log_order_item_report AS SELECT server_time,
    order_id,
    item_id, 
    price,
    quantity,
    item_name, 
    item_category,
    DAYOFWEEK(server_time) AS dayofweek,
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%H') AS hour,
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%Y') AS year,
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%m') AS month,
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%d') AS day FROM
    tbs_rblog.tb_log_order_item
WHERE
    #server_time >= @time_cut_off_3m;
    date_format(convert_tz(server_time, '+00:00', '+09:00'), '%Y-%m') = concat(@year, '-', @month);


#두 테이블에 index를 먹이기
create index pcid on tbs_consult.tb_log_view_report (pcid);
create index server_time on tbs_consult.tb_log_view_report (server_time);
create index device on tbs_consult.tb_log_view_report (device);
create index day on tbs_consult.tb_log_view_report (day);
create index dayofweek on tbs_consult.tb_log_view_report (dayofweek);
create index hour on tbs_consult.tb_log_view_report (hour);

create index pcid on tbs_consult.tb_log_order_report (pcid);
create index server_time on tbs_consult.tb_log_order_report (server_time);
create index device on tbs_consult.tb_log_order_report (device);
create index day on tbs_consult.tb_log_order_report (day);
create index dayofweek on tbs_consult.tb_log_order_report (dayofweek);
create index hour on tbs_consult.tb_log_order_report (hour);

create index order_id on tbs_consult.tb_log_order_item_report (order_id);
create index server_time on tbs_consult.tb_log_order_item_report (server_time);
create index price on tbs_consult.tb_log_order_item_report (price);
create index day on tbs_consult.tb_log_order_item_report (day);
create index dayofweek on tbs_consult.tb_log_order_item_report (dayofweek);
create index hour on tbs_consult.tb_log_order_item_report (hour);

/*
#상품 DB 만들기
drop table if exists tbs_consult.tb_prd_info_report;
CREATE TABLE tbs_consult.tb_prd_info_report AS 
SELECT
    pid
    ,max(pnm) pnm
    ,max(original_price) original_price
    ,max(sale_price) sale_price
    ,max(pimg) pimg
    ,max(catlvl1) catlvl1
    ,max(catlvl2) catlvl2
    ,max(catlvl3) catlvl3
from tbs_rengine.tmp_prd_info_in group by pid;   

create index pid on tbs_consult.tb_prd_info_report (pid);
*/

#전체 모수 구하기
set @user_num_view = 0; 
set @user_num_order = 0;
select @user_num_view := count(distinct pcid) from tbs_consult.tb_log_view_report where year = @year AND month = @month; 
select @user_num_order := count(distinct pcid) from tbs_consult.tb_log_order_report where year = @year AND month = @month;

#################################################################################################################################################
# 운영지표 (operation)
#################################################################################################################################################

# 요일 관련 kpi 정보 만들기
drop table if exists tbs_consult.t_operation_day;
CREATE TABLE tbs_consult.t_operation_day AS SELECT 
    @year year, 
    @month month,
    (case when A.dayofweek = 1 then 'SUN'
    when A.dayofweek = 2 then 'moN' 
    when A.dayofweek = 3 then 'TUE' 
    when A.dayofweek = 4 then 'WED' 
    when A.dayofweek = 5 then 'THU' 
    when A.dayofweek = 6 then 'FRI' 
    when A.dayofweek = 7 then 'SAT' 
    end) as dayofweek
    ,
    A.UV,
    A.View,
    B.buy_uv,
    B.buy_v,
    B.buy_num,
    B.avg_item_num,
    B.avg_revenue FROM
    (SELECT 
        dayofweek dayofweek, COUNT(DISTINCT pcid) UV, COUNT(pcid) View
    FROM
        tbs_consult.tb_log_view_report
    WHERE
        year=@year and month = @month
    GROUP BY 1) A
        LEFT OUTER JOIN
    (SELECT 
        dayofweek dayofweek,
            COUNT(DISTINCT pcid) buy_uv,
            COUNT(pcid) buy_v,
            COUNT(DISTINCT order_id) buy_num,
            AVG(item_num) avg_item_num,
            AVG(revenue) avg_revenue
    FROM
        tbs_consult.tb_log_order_report
    WHERE
        year=@year and month = @month
    GROUP BY 1) B ON A.dayofweek = B.dayofweek;


#시간대 관련 kpi 정보 만들기
drop table if exists tbs_consult.t_operation_hour;
CREATE TABLE tbs_consult.t_operation_hour AS SELECT 
    @year year, 
    @month month,
    A.hour,
    A.UV,
    A.View,
    B.buy_uv,
    B.buy_v,
    B.buy_num,
    B.avg_item_num,
    B.avg_revenue FROM
    (SELECT 
        hour hour, COUNT(DISTINCT pcid) UV, COUNT(pcid) View
    FROM
        tbs_consult.tb_log_view_report
    WHERE
        year=@year and month = @month
    GROUP BY 1) A
        LEFT OUTER JOIN
    (SELECT 
        hour hour,
            COUNT(DISTINCT pcid) buy_uv,
            COUNT(pcid) buy_v,
            COUNT(DISTINCT order_id) buy_num,
            AVG(item_num) avg_item_num,
            AVG(revenue) avg_revenue
    FROM
        tbs_consult.tb_log_order_report
    WHERE
        year=@year and month = @month
    GROUP BY 1) B ON A.hour = B.hour;

#플랫폼 관련 kpi 정보 만들기
drop table if exists tbs_consult.t_operation_platform;
CREATE TABLE tbs_consult.t_operation_platform AS SELECT 
    @year year, 
    @month month,
    (case when A.device=1 then 'pc' else 'mo' end) platform,
    A.UV,
    A.View,
    B.buy_uv,
    B.buy_v,
    B.buy_num,
    B.avg_item_num,
    B.avg_revenue FROM
    (SELECT 
        device device, COUNT(DISTINCT pcid) UV, COUNT(pcid) View
    FROM
        tbs_consult.tb_log_view_report
    WHERE
        year=@year and month = @month
    GROUP BY 1) A
        LEFT OUTER JOIN
    (SELECT 
        device device,
            COUNT(DISTINCT pcid) buy_uv,
            COUNT(pcid) buy_v,
            COUNT(DISTINCT order_id) buy_num,
            AVG(item_num) avg_item_num,
            AVG(revenue) avg_revenue
    FROM
        tbs_consult.tb_log_order_report
    WHERE
        year=@year and month = @month
    GROUP BY 1) B ON A.device = B.device;
    

# 성장KPI 관련
drop table if exists tbs_consult.t_operation_daily_s1;
CREATE TABLE tbs_consult.t_operation_daily_s1 as
SELECT 
        A.day day
        , COUNT(DISTINCT A.pcid) UV
        , COUNT(A.pcid) PV
    FROM
        tbs_consult.tb_log_view_report A
    WHERE
        year = @year AND month = @month
    GROUP BY A.day
    ORDER BY A.day;
    

drop table if exists tbs_consult.t_operation_daily_s2;
CREATE TABLE tbs_consult.t_operation_daily_s2 as   
SELECT 
        day day,
            AVG(item_num) avg_item_num,
            SUM(item_num) sum_item_num,
            AVG(revenue) avg_revenue,
            SUM(revenue) sum_revenue
    FROM
        tbs_consult.tb_log_order_report
    WHERE
        year = @year AND month = @month
    GROUP BY day
    ORDER BY day;


drop table if exists tbs_consult.t_operation_daily;
CREATE TABLE tbs_consult.t_operation_daily AS SELECT 
    @year year, 
    @month month,
    A.day,
    A.UV,
    A.PV,
    F.avg_item_num,
    F.sum_item_num,
    F.avg_revenue,
    F.sum_revenue FROM
    tbs_consult.t_operation_daily_s1 A
        LEFT OUTER JOIN 
    tbs_consult.t_operation_daily_s2 F ON A.day = F.day;


#################################################################################################################################################
# 고객관리 (customer)
#################################################################################################################################################

#################################################################################################################################################
# 상품관리 (product)
#################################################################################################################################################
# 매출액 기준 Best상품
/*
drop table if exists tbs_consult.t_product_best_gross;
create table tbs_consult.t_product_best_gross as
SELECT 
    @year year, 
    @month month,
    B.item_id,
    max(C.pnm) pnm,
    max(C.original_price) original_price,
    sum(B.quantity) quantity,
    sum(C.original_price * B.quantity) revenue
FROM
    tbs_consult.tb_log_order_report A
        LEFT JOIN
    tbs_rblog.tb_log_order_item B ON A.order_id = B.order_id
        LEFT JOIN
    tbs_consult.tb_prd_info_report C ON B.item_id = C.pid
    #tbs_service.tb_prd_info C ON B.item_id = C.pid
WHERE
    A.year = @year AND A.month = @month and C.pid is not null
GROUP BY item_id
order by 5 desc;




drop table if exists tbs_consult.t_product_best_gross_asc;
create table tbs_consult.t_product_best_gross_asc as
SELECT 
    @year year, 
    @month month,
    A.item_id as best_pid,
    D.pnm as best_pnm,
    D.original_price as best_price,
    B.rpid as wtn_pid,
    E.pnm as wtn_pnm,
    E.original_price as wtn_price,
    C.rpid wto_pid,
    F.pnm wto_pnm,
    F.original_price wto_price
FROM
    tbs_consult.t_product_best_gross A
        INNER JOIN
    tbs_service.tb_asc_wtn B ON A.item_id = B.tpid
        INNER JOIN
    tbs_service.tb_asc_without C ON A.item_id = C.tpid
        INNER JOIN
    tbs_consult.tb_prd_info_report D ON A.item_id = D.pid
        INNER JOIN
    tbs_consult.tb_prd_info_report E ON B.rpid = E.pid
        INNER JOIN
    tbs_consult.tb_prd_info_report F ON C.rpid = F.pid
WHERE
    B.rank = 1 AND C.rank = 1
limit 10; 

# 구매횟수 기준 Best상품
drop table if exists tbs_consult.t_product_best_frequency;
create table tbs_consult.t_product_best_frequency as
SELECT 
    @year year, 
    @month month,
    B.item_id,
    max(C.pnm) pnm,
    max(C.original_price) original_price,
    sum(B.quantity) quantity,
    sum(C.original_price * B.quantity) revenue
FROM
    tbs_consult.tb_log_order_report A
        LEFT JOIN
    tbs_rblog.tb_log_order_item B ON A.order_id = B.order_id
        LEFT JOIN
    tbs_consult.tb_prd_info_report C ON B.item_id = C.pid
        
WHERE
    A.year = @year AND A.month = @month and C.pid is not null
GROUP BY item_id
order by 4 desc;

drop table if exists tbs_consult.t_product_best_frequency_asc;
create table tbs_consult.t_product_best_frequency_asc as
SELECT 
    @year year, 
    @month month,
    A.item_id as best_pid,
    D.pnm best_pnm,
    D.original_price best_price,
    B.rpid wtn_pid,
    E.pnm wtn_pnm,
    E.original_price wtn_price,
    C.rpid wto_pid,
    F.pnm wto_pnm,
    F.original_price wto_price
FROM
    tbs_consult.t_product_best_frequency A
        INNER JOIN
    tbs_service.tb_asc_wtn B ON A.item_id = B.tpid
        INNER JOIN
    tbs_service.tb_asc_without C ON A.item_id = C.tpid
        INNER JOIN
    tbs_consult.tb_prd_info_report D ON A.item_id = D.pid
        INNER JOIN
    tbs_consult.tb_prd_info_report E ON B.rpid = E.pid
        INNER JOIN
    tbs_consult.tb_prd_info_report F ON C.rpid = F.pid
WHERE
    B.rank = 1 AND C.rank = 1;

# 클릭횟수 기준 Best상품
drop table if exists tbs_consult.t_product_best_click;
create table tbs_consult.t_product_best_click as
SELECT 
    @year year, 
    @month month,
    item_id, 
    C.pnm, 
    C.original_price, 
    COUNT(item_id) count_view
FROM
    tbs_consult.tb_log_view_report A
    left join tbs_consult.tb_prd_info_report C ON A.item_id = C.pid
WHERE
    year = @year AND month = @month and C.pid is not null
GROUP BY item_id
ORDER BY 2 desc;

drop table if exists tbs_consult.t_product_best_click_asc;
create table tbs_consult.t_product_best_click_asc as
SELECT 
    @year year, 
    @month month,
    A.item_id best_pid,
    D.pnm best_pnm,
    D.original_price best_price,
    B.rpid wtn_pid,
    E.pnm wtn_pnm,
    E.original_price wtn_price,
    C.rpid wto_pid,
    F.pnm wto_pnm,
    F.original_price wto_price
FROM
    tbs_consult.t_product_best_click A
        INNER JOIN
    tbs_service.tb_asc_wtn B ON A.item_id = B.tpid
        INNER JOIN
    tbs_service.tb_asc_without C ON A.item_id = C.tpid
        INNER JOIN
    tbs_consult.tb_prd_info_report D ON A.item_id = D.pid
        INNER JOIN
    tbs_consult.tb_prd_info_report E ON B.rpid = E.pid
        INNER JOIN
    tbs_consult.tb_prd_info_report F ON C.rpid = F.pid
WHERE
    B.rank = 1 AND C.rank = 1;




# 구매전환율 Best상품

drop table if exists tbs_consult.t_product_best_cr_s1;
CREATE TABLE tbs_consult.t_product_best_cr_s1 AS SELECT item_id, COUNT(item_id) count_order FROM
    tbs_consult.tb_log_order_report A
        INNER JOIN
    tbs_rblog.tb_log_order_item B ON A.order_id = B.order_id
GROUP BY B.item_id;
    
drop table if exists tbs_consult.t_product_best_cr_s2;
CREATE TABLE tbs_consult.t_product_best_cr_s2 AS SELECT item_id, COUNT(item_id) count_view FROM
    tbs_consult.tb_log_view_report A
WHERE
    year = @year AND month = @month
GROUP BY item_id;

drop table if exists tbs_consult.t_product_best_cr;
CREATE TABLE tbs_consult.t_product_best_cr AS
select
    @year year, 
    @month month,
    A.item_id
    ,C.pnm
    ,C.original_price
    ,A.count_order/B.count_view CR
from tbs_consult.t_product_best_cr_s1 A
inner join tbs_consult.t_product_best_cr_s2 B on A.item_id = B.item_id
left outer join tbs_consult.tb_prd_info_report C on A.item_id = C.pid
where C.pid is not null
order by 4 desc;

drop table if exists tbs_consult.t_product_best_cr_asc;
create table tbs_consult.t_product_best_cr_asc as
SELECT 
    @year year, 
    @month month,
    A.item_id best_pid,
    D.pnm best_pnm,
    D.original_price best_price,
    B.rpid wtn_pid,
    E.pnm wtn_pnm,
    E.original_price wtn_price,
    C.rpid wto_pid,
    F.pnm wto_pnm,
    F.original_price wto_price,
    A.CR
FROM
    tbs_consult.t_product_best_cr A
        INNER JOIN
    tbs_service.tb_asc_wtn B ON A.item_id = B.tpid
        INNER JOIN
    tbs_service.tb_asc_without C ON A.item_id = C.tpid
        INNER JOIN
    tbs_consult.tb_prd_info_report D ON A.item_id = D.pid
        INNER JOIN
    tbs_consult.tb_prd_info_report E ON B.rpid = E.pid
        INNER JOIN
    tbs_consult.tb_prd_info_report F ON C.rpid = F.pid
WHERE
    B.rank = 1 AND C.rank = 1;
*/
#################################################################################################################################################
# 고객 관리 (customer)
#############################################################################################################################################

drop table if exists tbs_consult.t_customer_visit;
CREATE TABLE tbs_consult.t_customer_visit AS
select
    count(pcid) UV, count(distinct pcid) distinct_UV, count(pcid)/count(distinct pcid) visit_per_pcid
from tbs_consult.tb_log_view_report;




#################################################################################################################################################
# 카테고리 관리 (category)
################################################################################################################################################


drop table if exists tbs_consult.t_category_best_gross;
CREATE TABLE tbs_consult.t_category_best_gross AS
SELECT 
    B.catgroup, SUM(A.price * A.quantity) revenue
FROM
    tbs_rblog.tb_log_order_item A
    inner join tbs_service.tb_prd_info B on A.item_id = B.pid
WHERE
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%Y-%m') = concat(@year, '-', @month)
GROUP BY B.catgroup
ORDER BY 2 DESC;

drop table if exists tbs_consult.t_category_best_frequency;
CREATE TABLE tbs_consult.t_category_best_frequency AS
SELECT 
    B.catgroup, SUM(A.quantity) frequency
FROM
    tbs_rblog.tb_log_order_item A
    inner join tbs_service.tb_prd_info B on A.item_id = B.pid
WHERE
    DATE_FORMAT(CONVERT_TZ(server_time, '+00:00', '+09:00'),
            '%Y-%m') = concat(@year, '-', @month)
GROUP BY B.catgroup
ORDER BY 2 DESC;



drop table if exists tbs_consult.t_category_best_click;
create table tbs_consult.t_category_best_click as
SELECT 
    B.catgroup, COUNT(item_id) count_view
FROM
    tbs_consult.tb_log_view_report A
    inner join tbs_service.tb_prd_info B on A.item_id = B.pid
    left join tbs_consult.tb_prd_info_report C ON A.item_id = C.pid
WHERE
    year = @year AND month = @month and C.pid is not null
GROUP BY B.catgroup
ORDER BY 2 desc;



drop table if exists tbs_consult.t_category_best_cr_s1;
CREATE TABLE tbs_consult.t_category_best_cr_s1 AS SELECT C.catgroup catgroup, COUNT(B.item_id) count_order
from tbs_consult.tb_log_order_report A
        INNER JOIN
    tbs_rblog.tb_log_order_item B ON A.order_id = B.order_id
        inner join 
    tbs_service.tb_prd_info C on B.item_id = C.pid
where A.year = @year AND A.month = @month and C.pid is not null
GROUP BY C.catgroup;
    
drop table if exists tbs_consult.t_category_best_cr_s2;
CREATE TABLE tbs_consult.t_category_best_cr_s2 AS SELECT C.catgroup catgroup, COUNT(A.item_id) count_view FROM
    tbs_consult.tb_log_view_report A
    inner join tbs_service.tb_prd_info C on A.item_id = C.pid
WHERE
    year = @year AND month = @month
GROUP BY C.catgroup;

drop table if exists tbs_consult.t_category_best_cr;
CREATE TABLE tbs_consult.t_category_best_cr AS
select
    A.catgroup
    ,A.count_order/B.count_view CR
from tbs_consult.t_category_best_cr_s1 A
inner join tbs_consult.t_category_best_cr_s2 B on A.catgroup = B.catgroup
order by 2 desc;


#################################################################################################################################################
# 유지율 관리 (retention)
#############################################################################################################################################
/*
#####################
#####Active user#####
#####################
# 각 pcid의 첫 방문 기록
drop table if exists tbs_consult.tb_log_visitor_total;
CREATE TABLE tbs_consult.tb_log_visitor_total AS SELECT pcid, MIN(server_time) first_visit FROM
    tbs_rblog.tb_log_view
GROUP BY pcid;

drop table if exists tbs_consult.tb_log_visitor_1month;
CREATE TABLE tbs_consult.tb_log_visitor_1month AS
select * from tbs_consult.tb_log_visitor_total where date_format(convert_tz(first_visit, '+00:00', '+09:00'), '%Y-%m') = concat(@year , '-' , @month);

create index pcid on tbs_consult.tb_log_visitor_1month (pcid);

drop table if exists tbs_consult.tb_retention_new_user;
CREATE TABLE tbs_consult.tb_retention_new_user AS
SELECT 
    DATE_FORMAT(CONVERT_TZ(first_visit, '+00:00', '+09:00'),
        '%Y-%m-%d') date
    , count(pcid) new_user
FROM
    tbs_consult.tb_log_visitor_1month
GROUP BY 1;


drop table if exists tbs_consult.tb_retention_total_user;
CREATE TABLE tbs_consult.tb_retention_total_user AS
SELECT 
    concat(year,'-',month,'-',day) date
    , count(distinct pcid) total_user
FROM
    tbs_consult.tb_log_view_report
GROUP BY 1;


drop table if exists tbs_consult.tb_retention_activeuser;
CREATE TABLE tbs_consult.tb_retention_activeuser AS
select 
    A.date
    ,A.new_user new_user
    ,B.total_user - A.new_user retention_user
    ,(B.total_user - A.new_user)/B.total_user as retention_rate
from tbs_consult.tb_retention_new_user A
inner join 
tbs_consult.tb_retention_total_user B
on A.date = B.date;

#####################
#####Stickiness######
#####################
set @MAU := 0;
select @MAU := count(*)
from (select pcid from tbs_consult.tb_log_view_report group by pcid) A ;

drop table if exists tbs_consult.tb_retention_stickiness;
CREATE TABLE tbs_consult.tb_retention_stickiness AS SELECT date, total_user / @MAU AS stickiness FROM
    tbs_consult.tb_retention_total_user;

##########################
#####Day N retention######
##########################
drop table if exists tbs_consult.tb_retention_cohort;
CREATE TABLE tbs_consult.tb_retention_cohort AS
select date_format(convert_tz(A.first_visit,'+00:00','+09:00'), '%Y-%m-%d') first_date
, count(distinct A.pcid) init_visit
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=0 then B.pcid end) day0
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=1 then B.pcid end) day1
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=2 then B.pcid end) day2
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=3 then B.pcid end) day3
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=4 then B.pcid end) day4
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=5 then B.pcid end) day5
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=6 then B.pcid end) day6
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=7 then B.pcid end) day7
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=8 then B.pcid end) day8
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=9 then B.pcid end) day9
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=10 then B.pcid end) day10
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=11 then B.pcid end) day11
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=12 then B.pcid end) day12
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=13 then B.pcid end) day13
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=14 then B.pcid end) day14
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=15 then B.pcid end) day15
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=16 then B.pcid end) day16
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=17 then B.pcid end) day17
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=18 then B.pcid end) day18
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=19 then B.pcid end) day19
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=20 then B.pcid end) day20
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=21 then B.pcid end) day21
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=22 then B.pcid end) day22
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=23 then B.pcid end) day23
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=24 then B.pcid end) day24
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=25 then B.pcid end) day25
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=26 then B.pcid end) day26
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=27 then B.pcid end) day27
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=28 then B.pcid end) day28
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=29 then B.pcid end) day29
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=30 then B.pcid end) day30
, count(distinct case when datediff(convert_tz(B.server_time,'+00:00','+09:00'),convert_tz(A.first_visit,'+00:00','+09:00'))=31 then B.pcid end) day31
from tbs_consult.tb_log_visitor_1month A
inner join tbs_consult.tb_log_view_report B on A.pcid = B.pcid
GROUP BY date_format(convert_tz(A.first_visit,'+00:00','+09:00'), '%m-%d');
*/


#################################################################################################################################################
# 추천 성과 관련 (recommend)
#################################################################################################################################################

#1. 추천영역 클릭한 사람의 객단가, 구매전환율
#2. 추천영역 1,2,3번 이상 클릭한 사람의 객단가, 구매전환율

#클릭 관련 데이터를 일별,pcid 별로 수집
drop table if exists tbs_consult.tb_recommend_territory_result_view;
CREATE TABLE tbs_consult.tb_recommend_territory_result_view AS 
SELECT 
    month
    ,day
    ,pcid
    ,device
    ,count(pcid) view
    ,count(case when reco is not null then pcid end) reco_view
    ,count(case when reco is null then pcid end) no_reco_view
FROM
tbs_consult.tb_log_view_report 
group by month, day, pcid, device;


create index pcid on tbs_consult.tb_recommend_territory_result_view(pcid);

#하루에 추천을 한번이상 클릭한 사람의 상품 클릭수와 한번도 클릭하지 않은 사람의 상품 클릭 수 비교
drop table if exists tbs_consult.tb_recommend_territory_result_click_s1;
CREATE TABLE tbs_consult.tb_recommend_territory_result_click_s1 AS 
SELECT 
    

    avg(case when device = 1 then view end) pc_click
    ,avg(case when device = 2 then view end) mo_click

    ,avg(case when device = 1 and reco_view>0 then view end) pc_reco_click
    ,avg(case when device = 2 and reco_view>0 then view end) mo_reco_click

    ,avg(case when device = 1 and reco_view=0 and no_reco_view > 4 then view end) pc_no_reco_five_click
    ,avg(case when device = 2 and reco_view=0 and no_reco_view > 4 then view end) mo_no_reco_five_click
    
    ,avg(case when device = 1 and reco_view=0 and no_reco_view > 3 then view end) pc_no_reco_four_click
    ,avg(case when device = 2 and reco_view=0 and no_reco_view > 3 then view end) mo_no_reco_four_click

    ,avg(case when device = 1 and reco_view=0 and no_reco_view > 2 then view end) pc_no_reco_three_click
    ,avg(case when device = 2 and reco_view=0 and no_reco_view > 2 then view end) mo_no_reco_three_click

    ,avg(case when device = 1 and reco_view=0 and no_reco_view > 1 then view end) pc_no_reco_two_click
    ,avg(case when device = 2 and reco_view=0 and no_reco_view > 1 then view end) mo_no_reco_two_click

    ,avg(case when device = 1 and reco_view=0 and no_reco_view > 0 then view end) pc_no_reco_one_click
    ,avg(case when device = 2 and reco_view=0 and no_reco_view > 0 then view end) mo_no_reco_one_click

FROM
    tbs_consult.tb_recommend_territory_result_view;


#클릭 관련 데이터 


drop table if exists tbs_consult.tb_recommend_territory_result_click;
CREATE TABLE tbs_consult.tb_recommend_territory_result_click AS SELECT *,
    round(pc_reco_click / pc_no_reco_one_click, 4) as pc_click_up_ratio,
    round(mo_reco_click / mo_no_reco_one_click, 4) as mo_click_up_ratio FROM
    tbs_consult.tb_recommend_territory_result_click_s1;   
    
drop table if exists tbs_consult.tb_recommend_territory_result_uv;
CREATE TABLE tbs_consult.tb_recommend_territory_result_uv AS
select
    count(case when device = 1 then pcid end) pc_uv
    ,count(case when device = 2 then pcid end) mo_uv
    ,count(case when device = 1 and reco_view > 0 then pcid end) pc_reco_uv
    ,count(case when device = 2 and reco_view > 0 then pcid end) mo_reco_uv
    ,count(case when device = 1 and reco_view = 0 and no_reco_view > 0 then pcid end) pc_no_reco_uv
    ,count(case when device = 2 and reco_view = 0 and no_reco_view > 0 then pcid end) mo_no_reco_uv
from tbs_consult.tb_recommend_territory_result_view
group by month;

drop table if exists tbs_consult.tb_recommend_territory_result_pv;
CREATE TABLE tbs_consult.tb_recommend_territory_result_pv AS
select
    sum(case when device = 1 then view end) pc_pv
    ,sum(case when device = 2 then view end) mo_pv
    ,sum(case when device = 1 and reco_view > 0 then view end) pc_reco_pv
    ,sum(case when device = 2 and reco_view > 0 then view end) mo_reco_pv
    ,sum(case when device = 1 and reco_view = 0 and no_reco_view > 0 then view end) pc_no_reco_pv
    ,sum(case when device = 2 and reco_view = 0 and no_reco_view > 0 then view end) mo_no_reco_pv
from tbs_consult.tb_recommend_territory_result_view
group by month;

drop table if exists tbs_consult.tb_recommend_territory_result_order;
CREATE TABLE tbs_consult.tb_recommend_territory_result_order AS
SELECT 
    month
    ,day
    ,pcid
    ,device
    ,count(revenue) frequency
    ,sum(revenue) revenue
    ,sum(case when reco is null then revenue end) no_reco_revenue
    ,sum(case when reco is not null then revenue end) reco_revenue
    
    ,count(revenue) freq
    ,count(case when reco is null then revenue end) no_reco_freq
    ,count(case when reco is not null then revenue end) reco_freq

    ,sum(item_num) quan
    ,sum(case when reco is null then item_num end) no_reco_quan
    ,sum(case when reco is not null then item_num end) reco_quan
    
FROM
tbs_consult.tb_log_order_report
group by month, day, pcid, device;

create index pcid on tbs_consult.tb_recommend_territory_result_order(pcid);

drop table if exists tbs_consult.tb_recommend_territory_result_revenue;
CREATE TABLE tbs_consult.tb_recommend_territory_result_revenue AS
select
    sum(case when device = 1 then revenue end) pc_revenue
    ,sum(case when device = 2 then revenue end) mo_revenue
    ,sum(case when device = 1 then reco_revenue end) pc_reco_revenue
    ,sum(case when device = 2 then reco_revenue end) mo_reco_revenue
    ,sum(case when device = 1 then no_reco_revenue end) pc_no_reco_revenue
    ,sum(case when device = 2 then no_reco_revenue end) mo_no_reco_revenue
    
from tbs_consult.tb_recommend_territory_result_order
group by month;



drop table if exists tbs_consult.tb_recommend_territory_result_freq;
CREATE TABLE tbs_consult.tb_recommend_territory_result_freq AS
select
    sum(case when device = 1 then freq end) pc_freq
    ,sum(case when device = 2 then freq end) mo_freq
    ,sum(case when device = 1 then reco_freq end) pc_reco_freq
    ,sum(case when device = 2 then reco_freq end) mo_reco_freq    
    ,sum(case when device = 1 then no_reco_freq end) pc_no_reco_freq
    ,sum(case when device = 2 then no_reco_freq end) mo_no_reco_freq
    
from tbs_consult.tb_recommend_territory_result_order
group by month;


drop table if exists tbs_consult.tb_recommend_territory_result_quan_s1;
CREATE TABLE tbs_consult.tb_recommend_territory_result_quan_s1 AS
select
    avg(case when device = 1 then quan end) pc_quan
    ,avg(case when device = 2 then quan end) mo_quan
    ,avg(case when device = 1 then reco_quan end) pc_reco_quan
    ,avg(case when device = 2 then reco_quan end) mo_reco_quan    
    ,avg(case when device = 1 then no_reco_quan end) pc_no_reco_quan
    ,avg(case when device = 2 then no_reco_quan end) mo_no_reco_quan
    
from tbs_consult.tb_recommend_territory_result_order
group by month;

drop table if exists tbs_consult.tb_recommend_territory_result_quan;
CREATE TABLE tbs_consult.tb_recommend_territory_result_quan AS
select 
    *, round(pc_reco_quan/pc_no_reco_quan,4) pc_quan_up_ratio
    , round(mo_reco_quan/mo_no_reco_quan,4) mo_quan_up_ratio
from tbs_consult.tb_recommend_territory_result_quan_s1;


drop table if exists tbs_consult.tb_recommend_territory_result_prevenue_s1;
CREATE TABLE tbs_consult.tb_recommend_territory_result_prevenue_s1 AS SELECT 
    round(SUM(CASE WHEN device = 1 THEN revenue END) / sum(CASE WHEN device = 1 THEN freq END),0) pc_prevenue
    ,round(SUM(CASE WHEN device = 2 THEN revenue END) / sum(CASE WHEN device = 2 THEN freq END),0) mo_prevenue
    ,round(SUM(CASE WHEN device = 1 THEN reco_revenue END) / sum(CASE WHEN device = 1  THEN reco_freq END),0) pc_reco_prevenue
    ,round(SUM(CASE WHEN device = 2 THEN reco_revenue END) / sum(CASE WHEN device = 2  THEN reco_freq END),0) mo_reco_prevenue
    ,round(SUM(CASE WHEN device = 1 THEN no_reco_revenue END) / sum(CASE WHEN device = 1  THEN no_reco_freq END),0) pc_no_reco_prevenue
    ,round(SUM(CASE WHEN device = 2 THEN no_reco_revenue END) / sum(CASE WHEN device = 2  THEN no_reco_freq END),0) mo_no_reco_prevenue
    FROM
    tbs_consult.tb_recommend_territory_result_order;


drop table if exists tbs_consult.tb_recommend_territory_result_prevenue;
CREATE TABLE tbs_consult.tb_recommend_territory_result_prevenue AS SELECT 
    A.*
    ,round(pc_reco_prevenue / pc_no_reco_prevenue,4) pc_prevenue_up_ratio
    ,round(mo_reco_prevenue / mo_no_reco_prevenue,4) mo_prevenue_up_ratio FROM
    tbs_consult.tb_recommend_territory_result_prevenue_s1 A;

#추천을 클릭한 사람과 클릭하지 않은 사람 사이의 구매전환율 비교(모바일/pc 분리)
drop table if exists tbs_consult.tb_recommend_territory_result_cr_s1;
CREATE TABLE tbs_consult.tb_recommend_territory_result_cr_s1 AS
select 
    round(B.pc_freq/A.pc_uv,4) pc_cr
    ,round(B.mo_freq/A.mo_uv,4) mo_cr
    ,round(B.pc_reco_freq/A.pc_reco_uv,4) pc_reco_cr
    ,round(B.mo_reco_freq/A.mo_reco_uv,4) mo_reco_cr
    ,round(B.pc_no_reco_freq/A.pc_no_reco_uv,4) pc_no_reco_cr
    ,round(B.mo_no_reco_freq/A.mo_no_reco_uv,4) mo_no_reco_cr

from tbs_consult.tb_recommend_territory_result_uv A
, tbs_consult.tb_recommend_territory_result_freq B;

drop table if exists tbs_consult.tb_recommend_territory_result_cr;
CREATE TABLE tbs_consult.tb_recommend_territory_result_cr AS
select 
    *, round(pc_reco_cr/pc_no_reco_cr,4) pc_cr_up_ratio
    , round(mo_reco_cr/mo_no_reco_cr,4) mo_cr_up_ratio
from tbs_consult.tb_recommend_territory_result_cr_s1;

/*
#카테고리 다양성
drop table if exists tbs_consult.tb_recommend_territory_result_cat_var_s1;
CREATE TABLE tbs_consult.tb_recommend_territory_result_cat_var_s1 AS
select 
    avg(case when device = 1 then A.category_var end) pc_cat_var
    ,avg(case when device = 2 then A.category_var end) mo_cat_var
    ,avg(case when A.reco is not null and device = 1 then A.category_var end) pc_reco_cat_var
    ,avg(case when A.reco is not null and device = 2 then A.category_var end) mo_reco_cat_var
    ,avg(case when A.reco is null and device = 1 then A.category_var end) pc_no_reco_cat_var
    ,avg(case when A.reco is null and device = 2 then A.category_var end) mo_no_reco_cat_var
    
from
(select 
    A.device,
    A.order_id, 
    A.reco,
    count(distinct C.catgroup) category_var
from tbs_consult.tb_log_order_report A
inner join tbs_consult.tb_log_order_item_report B on A.order_id = B.order_id
inner join tbs_service.tb_prd_info C on B.item_id = C.pid
group by A.order_id) A;
select * from tbs_service.tb_prd_info
drop table if exists tbs_consult.tb_recommend_territory_result_cat_var;
CREATE TABLE tbs_consult.tb_recommend_territory_result_cat_var AS
select 
    *, round(pc_reco_cat_var/pc_no_reco_cat_var,4) pc_cat_var_up_ratio
    , round(mo_reco_cat_var/mo_no_reco_cat_var,4) mo_cat_var_up_ratio
from tbs_consult.tb_recommend_territory_result_cat_var_s1;

drop table if exists tbs_consult.tb_recommend_territory_result_drevenue;
CREATE TABLE tbs_consult.tb_recommend_territory_result_drevenue AS
select
    (A.pc_reco_prevenue - B.pc_loyal_prevenue) * C.pc_reco_freq + (A.mo_reco_prevenue-B.mo_loyal_prevenue)*C.mo_reco_freq as drevenue
from tbs_consult.tb_recommend_territory_result_prevenue A,
tbs_consult.tb_recommend_territory_result_loyal_prevenue B,
tbs_consult.tb_recommend_territory_result_freq C;
*/
 

#loyal#
drop table if exists tbs_consult.tb_recommend_territory_result_S1;
CREATE TABLE tbs_consult.tb_recommend_territory_result_S1 AS 
SELECT 
    month
    ,day
    ,pcid
    ,device
    ,count(case when reco like 'cart%' then pcid end) cart_count
    ,count(case when reco like 'mcart%' then pcid end) mcart_count
    ,count(case when reco like 'detail%' then pcid end) detail_count
    ,count(case when reco like 'mdetail%' then pcid end) mdetail_count
    ,count(case when reco is null then pcid end) no_reco_count
FROM
tbs_consult.tb_log_view_report 
group by month, day, pcid, device;

drop table if exists tbs_consult.tb_recommend_territory_result_S3;
CREATE TABLE tbs_consult.tb_recommend_territory_result_S3 AS
SELECT 
    month
    ,day
    ,pcid
    ,device
    ,count(revenue) frequency
    ,sum(revenue) revenue
    ,sum(case when reco is null then revenue end) no_reco_revenue
    ,sum(case when reco is not null then revenue end) reco_revenue
    ,count(case when reco like 'cart%' then pcid end) cart_count
    ,count(case when reco like 'mcart%' then pcid end) mcart_count
    ,count(case when reco like 'detail%' then pcid end) detail_count
    ,count(case when reco like 'mdetail%' then pcid end) mdetail_count
FROM
tbs_consult.tb_log_order_report
group by month, day, pcid, device;

create index pcid on tbs_consult.tb_recommend_territory_result_S3(pcid);

drop table if exists tbs_consult.tb_recommend_territory_result_loyal_group;
CREATE TABLE tbs_consult.tb_recommend_territory_result_loyal_group AS SELECT * FROM
    tbs_consult.tb_recommend_territory_result_S1
WHERE
    cart_count+mcart_count+detail_count+mdetail_count=0
    AND
    no_reco_count > 1;

drop table if exists tbs_consult.tb_recommend_territory_result_loyal_prevenue;
CREATE TABLE tbs_consult.tb_recommend_territory_result_loyal_prevenue AS
SELECT 
    round(avg(case when A.device = 1 then B.revenue end)) PC_loyal_prevenue
    ,round(avg(case when A.device = 2 then B.revenue end)) MO_loyal_prevenue
FROM
    (SELECT * from tbs_consult.tb_recommend_territory_result_loyal_group) A
        inner JOIN
    (select * from tbs_consult.tb_recommend_territory_result_S3) B ON A.day = B.day AND A.pcid = B.pcid;
    

drop table if exists tbs_consult.tb_recommend_territory_result_loyal_group2;
CREATE TABLE tbs_consult.tb_recommend_territory_result_loyal_group2 AS SELECT * FROM
    tbs_consult.tb_recommend_territory_result_S1
WHERE
    cart_count+mcart_count+detail_count+mdetail_count+no_reco_count > 1;

drop table if exists tbs_consult.tb_recommend_territory_result_loyal_prevenue2;
CREATE TABLE tbs_consult.tb_recommend_territory_result_loyal_prevenue2 AS
SELECT 
    round(avg(case when A.device = 1 then B.revenue end)) PC_loyal_prevenue
    ,round(avg(case when A.device = 2 then B.revenue end)) MO_loyal_prevenue
FROM
    (SELECT * from tbs_consult.tb_recommend_territory_result_loyal_group2) A
        inner JOIN
    (select * from tbs_consult.tb_recommend_territory_result_S3) B ON A.day = B.day AND A.pcid = B.pcid;    
   


#################################################################################################################################################
# Report Table output
################################################################################################################################################

select * from tbs_consult.t_operation_day;
select * from tbs_consult.t_operation_hour;
select * from tbs_consult.t_operation_daily;
select * from tbs_consult.t_operation_platform;
select * from tbs_consult.t_product_best_gross;
select * from tbs_consult.t_product_best_gross_asc;
select * from tbs_consult.t_product_best_frequency;
select * from tbs_consult.t_product_best_frequency_asc;
select * from tbs_consult.t_product_best_click;
select * from tbs_consult.t_product_best_click_asc;
select * from tbs_consult.t_product_best_cr;
select * from tbs_consult.t_product_best_cr_asc;
select * from tbs_consult.t_category_best_gross;
select * from tbs_consult.t_category_best_frequency;
select * from tbs_consult.t_category_best_click;
select * from tbs_consult.t_category_best_cr;

#추천 클릭한 사람과 클릭하지 않은 사람의 클릭수, 객단가, 구매전환율 계산
#quantity
select * from tbs_consult.tb_recommend_territory_result_uv;
select * from tbs_consult.tb_recommend_territory_result_freq;
select * from tbs_consult.tb_recommend_territory_result_revenue;
select * from tbs_consult.tb_recommend_territory_result_pv;
#quality
select * from tbs_consult.tb_recommend_territory_result_click;
select * from tbs_consult.tb_recommend_territory_result_prevenue;
select * from tbs_consult.tb_recommend_territory_result_cr;
#select * from tbs_consult.tb_recommend_territory_result_cat_var;
select * from tbs_consult.tb_recommend_territory_result_quan;
#final index
select * from tbs_consult.tb_recommend_territory_result_drevenue;
select * from tbs_consult.tb_recommend_territory_result_loyal_prevenue;


#total에 삽입
insert into tbs_consult.tb_recommend_territory_result_uv_total
select
    @year year
    ,@month month
    ,pc_uv
    ,mo_uv
    ,pc_reco_uv
    ,mo_reco_uv
    ,pc_no_reco_uv
    ,mo_no_reco_uv
from tbs_consult.tb_recommend_territory_result_uv;

insert into tbs_consult.tb_recommend_territory_result_pv_total 
select 
    @year year
    ,@month month
    ,pc_pv
    ,mo_pv
    ,pc_reco_pv
    ,mo_reco_pv
    ,pc_no_reco_pv
    ,mo_no_reco_pv
    
from tbs_consult.tb_recommend_territory_result_pv;

insert into tbs_consult.tb_recommend_territory_result_revenue_total 
select 
    @year year
    ,@month month
    ,pc_revenue
    ,mo_revenue
    ,pc_reco_revenue
    ,mo_reco_revenue
    ,pc_no_reco_revenue
    ,mo_no_reco_revenue
from tbs_consult.tb_recommend_territory_result_revenue;


insert into tbs_consult.tb_recommend_territory_result_freq_total 
select 
    @year year
    ,@month month
    ,pc_freq
    ,mo_freq
    ,pc_reco_freq
    ,mo_reco_freq
    ,pc_no_reco_freq
    ,mo_no_reco_freq
from tbs_consult.tb_recommend_territory_result_freq;


insert into tbs_consult.tb_recommend_territory_result_click_total 
select 
    @year year
    ,@month month
    ,pc_click
    ,mo_click
    ,pc_reco_click
    ,mo_reco_click
    ,pc_no_reco_five_click
    ,mo_no_reco_five_click
    ,pc_no_reco_four_click
    ,mo_no_reco_four_click
    ,pc_no_reco_three_click
    ,mo_no_reco_three_click
    ,pc_no_reco_two_click
    ,mo_no_reco_two_click
    ,pc_no_reco_one_click
    ,mo_no_reco_one_click
    ,pc_click_up_ratio
    ,mo_click_up_ratio
from tbs_consult.tb_recommend_territory_result_click;

insert into tbs_consult.tb_recommend_territory_result_prevenue_total 
select 
    @year year
    ,@month month
    ,pc_prevenue
    ,mo_prevenue
    ,pc_reco_prevenue
    ,mo_reco_prevenue
    ,pc_no_reco_prevenue
    ,mo_no_reco_prevenue
    ,pc_prevenue_up_ratio
    ,mo_prevenue_up_ratio

from tbs_consult.tb_recommend_territory_result_prevenue;

insert into tbs_consult.tb_recommend_territory_result_cr_total 
select 
    @year year
    ,@month month
    ,pc_cr
    ,mo_cr
    ,pc_reco_cr
    ,mo_reco_cr
    ,pc_no_reco_cr
    ,mo_no_reco_cr
    ,pc_cr_up_ratio
    ,mo_cr_up_ratio

from tbs_consult.tb_recommend_territory_result_cr;
/*
insert into tbs_consult.tb_recommend_territory_result_cat_var_total 
select 
    @year year
    ,@month month
    ,pc_cat_var
    ,mo_cat_var
    ,pc_reco_cat_var
    ,mo_reco_cat_var
    ,pc_no_reco_cat_var
    ,mo_no_reco_cat_var
    ,pc_cat_var_up_ratio
    ,mo_cat_var_up_ratio

from tbs_consult.tb_recommend_territory_result_cat_var;
*/
insert into tbs_consult.tb_recommend_territory_result_quan_total 
select 
    @year year
    ,@month month
    ,pc_quan
    ,mo_quan
    ,pc_reco_quan
    ,mo_reco_quan
    ,pc_no_reco_quan
    ,mo_no_reco_quan
    ,pc_quan_up_ratio
    ,mo_quan_up_ratio
from tbs_consult.tb_recommend_territory_result_quan;

#quantity
select * from tbs_consult.tb_recommend_territory_result_uv_total;
select * from tbs_consult.tb_recommend_territory_result_freq_total;
select * from tbs_consult.tb_recommend_territory_result_revenue_total;
select * from tbs_consult.tb_recommend_territory_result_pv_total;
#quality
select * from tbs_consult.tb_recommend_territory_result_click_total;
select * from tbs_consult.tb_recommend_territory_result_prevenue_total;
select * from tbs_consult.tb_recommend_territory_result_cr_total;
#select * from tbs_consult.tb_recommend_territory_result_cat_var_total;
select * from tbs_consult.tb_recommend_territory_result_quan_total;




drop table if exists tbs_consult.tb_recommend_product_result_s1;
CREATE TABLE tbs_consult.tb_recommend_product_result_s1 AS 
SELECT 
    convert_tz(server_time, '+00:00', '+09:00') server_time,
    pcid,
    device,
    (case when reco is not null then 1 else 0 end) reco,
    item_id 
FROM
    tbs_consult.tb_log_view_report;

create index pcid on tbs_consult.tb_recommend_product_result_s1 (pcid);
create index server_time on tbs_consult.tb_recommend_product_result_s1 (server_time);

drop table if exists tbs_consult.tb_recommend_product_result_s2;
CREATE TABLE tbs_consult.tb_recommend_product_result_s2 AS
SELECT 
    convert_tz(A.server_time, '+00:00', '+09:00') server_time,
    A.pcid,
    A.device,
    A.reco,
    A.order_id,
    B.item_id,
    B.price,
    B.item_name,
    B.quantity
FROM
    tbs_consult.tb_log_order_report A
inner JOIN
    tbs_consult.tb_log_order_item_report B ON A.order_id = B.order_id;

create index pcid on tbs_consult.tb_recommend_product_result_s2 (pcid);
create index server_time on tbs_consult.tb_recommend_product_result_s2 (server_time);


drop table if exists tbs_consult.tb_recommend_product_result_s3;
CREATE TABLE tbs_consult.tb_recommend_product_result_s3 AS SELECT 
    B.order_id, 
    A.item_id,
    max(A.server_time) view_time,
    max(B.server_time) order_time,
    unix_timestamp(max(B.server_time)) - unix_timestamp(max(A.server_time)) time_diff,
    max(A.device) device,
    max(A.reco) reco_view,
    max(B.reco) reco_order,
    max(B.quantity) quantity,
    max(B.price) price,
    max(B.item_name) item_name
    FROM
    tbs_consult.tb_recommend_product_result_s1 A
    INNER JOIN
    tbs_consult.tb_recommend_product_result_s2 B 
    ON A.pcid = B.pcid AND A.item_id = B.item_id
    where A.reco = 1 
    group by B.order_id, A.item_id;

#추천클릭한 상품을 구매한 금액의 합
select round(sum(price * quantity),0) gross_revenue from tbs_consult.tb_recommend_product_result_s3 where time_diff<30*60 and time_diff>0;
select order_id, item_id, count(*) from tbs_consult.tb_recommend_product_result_s3 group by order_id,item_id;
Select sum(revenue) from tbs_consult.tb_log_order_report where reco is not null;

